import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import pybullet as p
import cv2

def run(args):

  PATH = home + '/results/hex_model/latest/' + args.exp + '/'
  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/hex_model/latest/' + args.exp + '/'
    # WEIGHTS_PATH = home + '/hpc-home/results/hex_model/latest/weights/' + args.exp + '/'
  else:
    # WEIGHTS_PATH = home + '/results/hex_model/latest/weights/' + args.exp + '/'
    WEIGHTS_PATH = home + '/results/hex_model/latest/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 
  
  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 8*256

  lstm_size = 128

  if not args.baseline:
    if args.eth_rnn:
      am_lstm_size = 32
      from models import eth_rnn
      eth_model = eth_rnn.RNN(name='actuator_model', input_size=3, output_size=1, sess=sess)
    else:
      from models import eth_model
      eth_model = eth_model.NN(name='eth_model', input_size=15, output_size=1, ac_size=3, sess=sess)
    if args.fm:
      from models import simple_rnn
      forward_model = simple_rnn.RNN(name='forward_model', input_size=9, output_size=6, lstm_size=128, dense_size=128, sess=sess, norm=True, delta=False)
    elif args.residual:
      from models import residual
      rrnn = residual.ResidualNet(name='residual', input_size=9, output_size=6, lstm_size=128, sess=sess)  
    elif args.forward_residual:
      from models import forward_residual
      fr_rnn = forward_residual.FRNet(name='fr', input_size=15, output_size=6, lstm_size=128, sess=sess)  

  if args.test_pol:
    record_step = False
    args.rand_dynamics = False
  else:
    record_step = True

  if args.stand:
    from assets.just_test_stand_rl import Env
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test_pol=args.test_pol, record_step=record_step, rand_dynamics=args.rand_dynamics, history=args.history, obstacle_type=args.obstacle_type, display_hm=args.display_hm) 
  else:
    from assets.just_test_leg_rl import Env
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test_pol=args.test_pol, record_step=record_step, rand_dynamics=args.rand_dynamics, history=args.history) 

  if args.lstm_pol:
    from models.ppo_lstm import Model
  else:
    from models.ppo import Model
  print()
  print(env.ob_size)
  print()

  if args.stand:
    args.vis = True

  pol = Model("pi", env, ob_size=env.ob_size, ac_size=env.ac_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  
  initialize()
  sync_from_root(sess, pol.vars, comm=comm) #pylint: disable=E1101
  pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if not args.baseline:
    if args.eth_rnn:
      try:
        eth_model.load(home + '/data/iros_data/real/13_2/weights_eth_rnn_/best/')
      except Exception as e:
        print(e)
        print("trying to load weights, no weights")
    else:
      eth_model.load(home + '/data/iros_data/real/13_2/weights_eth_/best/')
    if args.fm:
      forward_model.load(home + '/data/iros_data/real/13_2/weights_/best/')
    elif args.residual:      
      if args.rand_residual:
        # rrnn.load(home + '/data/iros_data/real/13_2/residual/rand/weights_residual_/best/')
        rrnn.load(home + '/data/iros_data/real/13_2/residual/rand/weights_residual_/best/')
      else:
        rrnn.load(home + '/data/iros_data/real/13_2/residual/weights_residual_/best/')
    elif args.forward_residual:      
      if args.rand_residual:
        fr_rnn.load(home + '/data/iros_data/real/13_2/fr/rand/weights_/best/')
      else:
        fr_rnn.load(home + '/data/iros_data/real/13_2/fr/weights_/best/')

  if args.test_pol: 
    pol.load(WEIGHTS_PATH)

  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  tf.get_default_graph().finalize()

  prev_done = True
  ob = env.reset()
  prev_ob = ob
  if args.history:
    act = np.array([0,0,0])
    state_buffer = deque([0]*env.ob_size, maxlen=env.ob_size)
  
  if not args.baseline: 
    if args.eth_rnn:
      eth_model_state = [np.zeros([3,am_lstm_size]), np.zeros([3,am_lstm_size])] 
    else:
      eth_input_buf = {i:deque([0.0]*15,maxlen=15) for i in env.motor_names}
    if args.fm:
      fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
    elif args.residual:
      rrnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
    elif args.forward_residual:
      fr_rnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]

  im = np.zeros(env.im_size)
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  
  env.reward_breakdown = {'pos':deque(maxlen=100), 'tor':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100)}
  
  data_length = 128
  plot_length = 256
  fm_pred = deque(maxlen=plot_length)
  fm_label = deque(maxlen=plot_length)
  fm_target = deque(maxlen=plot_length)
  pol_pred = deque(maxlen=plot_length)
  pol_label = deque(maxlen=plot_length)
  pol_target = deque(maxlen=plot_length)
  if args.lstm_pol:
    pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  
  id_pointer = 0
  ep_steps = 0
  if args.test_pol:
    stochastic = False
    metric = deque(maxlen=env.sample_size)
    metrics = []
    torque_metric = deque(maxlen=env.sample_size)
    torque_metrics = []
  else:
    stochastic = True
  training_steps = 0
  action_buffer = deque(maxlen=3)
  prev_cmd_torque = [0,0,0]
  while True:
    if args.history:
      state_buffer.extend(np.concatenate((ob, act)))
      obs = np.array(state_buffer)
    else:
      obs = ob
      
    if pol.timesteps_so_far > pol.max_timesteps:
      break 
    if args.lstm_pol:
      act, vpred, pol_states, nlogp = pol.step(ob, im, stochastic=stochastic, states=pol_states)
    else:
      if args.history:
        act, vpred, _, nlogp = pol.step(obs, im, stochastic=stochastic)
      else:
        act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
      
    cmd_torque = act*env.max_torque
    
    if not args.baseline:
      # Through actuator model
      t1 = time.time()

      if args.eth_rnn:
        torque, eth_model_state = eth_model.step(env.joints + env.joint_vel + list(cmd_torque), eth_model_state)  
      else:
        batch_input = [] 
        for m,j,v,dt in zip(env.motor_names, env.joints, env.joint_vel, cmd_torque):
          eth_input_buf[m].extend([j,v,dt])
          batch_input.append(np.array(eth_input_buf[m]))
        torque = eth_model.step(batch_input)  
      # print(time.time() - t1  )
        # print(torque)
      # Through dynamics model
      if args.fm:
        fm_output, fm_state = forward_model.step(np.array(env.joints + env.joint_vel + list(torque)), fm_state)       
        next_ob, rew, done, _ = env.step(torque, fm_output, cmd_torque=cmd_torque)    
        fm_pred.append(fm_output)  
      elif args.residual:        
        next_ob, rew, done, _ = env.step(torque, cmd_torque=cmd_torque)    
        rrnn_output, rrnn_state = rrnn.step(np.array(env.prev_joints + env.prev_joint_vel + list(torque)), rrnn_state)       
        next_ob[:6] = np.array(env.joints + env.joint_vel) + rrnn_output
        # Set position is a bad idea!! Hopefully ok for collecting residual data.
        # env.set_position([0,0,2],[0,0,0,1],joints=next_ob[:3], joint_vel=next_ob[3:6])
        for j, jv, m in zip(next_ob[:3], next_ob[3:6], env.motors):
          # p.setJointMotorControl2(env.Id, m,controlMode=p.POSITION_CONTROL, targetPosition=j, targetVelocity=jv, maxVelocity=8)
          p.setJointMotorControl2(env.Id, m,controlMode=p.POSITION_CONTROL, targetPosition=j, targetVelocity=jv)
        # Need to reset the joint to use torque control at the next timestep
        p.setJointMotorControlArray(env.Id, env.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(env.motor_names))
        fm_pred.append(next_ob[:6])        
      elif args.forward_residual:        
        _, rew, done, _ = env.step(torque, cmd_torque=cmd_torque)    
        fr_rnn_output, fr_rnn_state = fr_rnn.step(np.array(env.prev_joints + env.prev_joint_vel + list(torque) + env.joints + env.joint_vel), fr_rnn_state)
        next_ob = list(fr_rnn_output) + env.exp_joints
        # print(next_ob)
        fm_pred.append(next_ob[:6])  
        
        if False:
          # Reward must come after model, therefore reward must be in the transform somewhere:
          pos = np.exp(-2*np.sum(abs(np.array(env.exp_joints) - np.array(next_ob[:3]))))
          tor = np.exp(-5*np.sum(abs(np.array(env.exp_torques) - np.array(cmd_torque))))
          vel = np.exp(-0.1*np.sum(abs(np.array(env.exp_joint_vel) - np.array(next_ob[3:6]))))
          neg = max(-0.01*np.sum(abs(np.array(cmd_torque) - np.array(prev_cmd_torque))) - 0.005*np.sum(abs(np.array(cmd_torque))), -1)
          # neg = max(-0.02*np.sum(abs(np.array(cmd_torque) - np.array(prev_cmd_torque))) - 0.005*np.sum(abs(np.array(cmd_torque))), -1)
          tip = 0.0
          env.reward_breakdown['pos'].append(pos)
          env.reward_breakdown['tor'].append(tor)
          env.reward_breakdown['vel'].append(vel)
          env.reward_breakdown['neg'].append(neg)
          env.reward_breakdown['tip'].append(tip)
          rew = 0.6*pos + 0.1*tor + 0.1*vel + 0.2*neg
        else:
          pos = np.exp(-2*np.sum(abs(np.array(env.exp_joints) - np.array(next_ob[:3]))))
          vel = -env.Kc*0.01*np.mean(np.array(next_ob[3:6])**2)
          tor = -env.Kc*0.005*np.mean(np.array(cmd_torque)**2)
          neg = -env.Kc*0.1*np.mean((np.array(cmd_torque) - np.array(prev_cmd_torque))**2) 
          tip = 0
          env.reward_breakdown['pos'].append(pos)
          env.reward_breakdown['tor'].append(tor)
          env.reward_breakdown['vel'].append(vel)
          env.reward_breakdown['neg'].append(neg)
          env.reward_breakdown['tip'].append(tip)
          rew = pos + vel + neg + tor
        prev_cmd_torque = cmd_torque
      else:
        next_ob, rew, done, _ = env.step(torque, cmd_torque=cmd_torque)
        fm_pred.append(next_ob[:6])  
     

    else:
      torque = cmd_torque
      next_ob, rew, done, _ = env.step(torque)
      fm_pred.append(next_ob[:6])  

    next_im = env.get_hm()

    # cv2.imshow('frame', next_im)
    # cv2.waitKey(0)

    fm_label.append(env.joints+env.joint_vel)
    fm_target.append(list(env.exp_joints)+list(env.exp_joint_vel))

    pol_label.append(act*env.max_torque)
    pol_pred.append(torque)
    pol_target.append(env.exp_torques)

    # print(state_buffer)
    if not args.test_pol:
      if args.history:
        pol.add_to_buffer([obs, im, act, rew, prev_done, vpred, nlogp])
      else:
        pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])

    prev_done = done
    ob = next_ob
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
     
    if args.test_pol:
      metric.append(np.mean((np.array(env.joints) - np.array(env.exp_joints))**2))
      torque_metric.append(np.mean((np.array(cmd_torque) - np.array(env.exp_torques))**2))
      if len(metric) == env.sample_size:
        metrics.append(np.mean(metric))
        metric = deque(maxlen=env.sample_size)
        torque_metrics.append(np.mean(torque_metric))
        torque_metric = deque(maxlen=env.sample_size)

    if not args.test_pol and ep_steps % horizon == 0:
      if args.lstm_pol:
        _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True, states=pol_states)
      else:
        if args.history:    
          # state_buffer.extend(ob)
          state_buffer.extend(np.concatenate((ob, act)))
          obs = np.array(state_buffer)
          # _, vpred, _, _ = pol.step(np.array(state_buffer), next_im, stochastic=True)
          _, vpred, _, _ = pol.step(obs, next_im, stochastic=True)
        else:
          _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
      pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done) 
      pol.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
      training_steps += 1

    if done:    
      if (rank == 0 and training_steps % 10 == 0) or args.test_pol:
        if args.test_pol:
          tag = 'test_'
          print("----------------------------------")
          print("closeness to expert: ", args.exp)
          print("pos", np.mean(metric))
          print("torque", np.mean(torque_metric))
          print("----------------------------------")
        else:
          tag = ''
        subplot([[np.array(fm_label)[:,0], np.array(fm_pred)[:,0], np.array(fm_target)[:,0]],[np.array(fm_label)[:,1], np.array(fm_pred)[:,1], np.array(fm_target)[:,1]],[np.array(fm_label)[:,2], np.array(fm_pred)[:,2], np.array(fm_target)[:,2]],[np.array(fm_label)[:,3], np.array(fm_pred)[:,3], np.array(fm_target)[:,3]],[np.array(fm_label)[:,4], np.array(fm_pred)[:,4], np.array(fm_target)[:,4]],[np.array(fm_label)[:,5], np.array(fm_pred)[:,5], np.array(fm_target)[:,5]]], legend=[["actual","model", "target"]]*6, PATH=PATH + 'fm_' + tag)
        subplot([[np.array(pol_label)[:,0], np.array(pol_pred)[:,0], np.array(pol_target)[:,0]],[np.array(pol_label)[:,1], np.array(pol_pred)[:,1], np.array(pol_target)[:,1]],[np.array(pol_label)[:,2], np.array(pol_pred)[:,2], np.array(pol_target)[:,2]]], legend=[["policy torque","applied","target"]]*6, PATH=PATH + 'torques_' + tag)
        if args.test_pol:
          exit()
        
      fm_pred = deque(maxlen=plot_length)
      fm_label = deque(maxlen=plot_length)
      fm_target = deque(maxlen=plot_length)
      pol_pred = deque(maxlen=plot_length)
      pol_label = deque(maxlen=plot_length)
      pol_target = deque(maxlen=plot_length)
      action_buffer = deque(maxlen=3)
      ob = env.reset()
      if args.history:
        act = np.array([0,0,0])
        state_buffer = deque([0]*env.ob_size, maxlen=env.ob_size)

      prev_ob = ob
      
      if not args.baseline: 
        if args.eth_rnn:
          eth_model_state = [np.zeros([3,am_lstm_size]), np.zeros([3,am_lstm_size])] 
        else:
          eth_input_buf = {i:deque([0.0]*15,maxlen=15) for i in env.motor_names}
        if args.fm:
          fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
        elif args.residual:
          rrnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
        elif args.forward_residual:
          fr_rnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]

      im = np.zeros(env.im_size)
      
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0        

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--ae', default=False, action='store_true')
  parser.add_argument('--baseline', default=False, action='store_true')
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--test_pol', default=False, action='store_true') 
  parser.add_argument('--vis', default=False, action='store_true')  
  parser.add_argument('--const_std', default=False, action='store_true')  
  parser.add_argument('--lstm_pol', default=True, action='store_false')  
  parser.add_argument('--set_pos_test', default=False, action='store_true')  
  parser.add_argument('--obs_torque', default=False, action='store_true')  
  parser.add_argument('--stand', default=False, action='store_true')  
  parser.add_argument('--residual', default=False, action='store_true')  
  parser.add_argument('--forward_residual', default=False, action='store_true')  
  parser.add_argument('--fm', default=False, action='store_true')  
  parser.add_argument('--eth_rnn', default=False, action='store_true')  
  parser.add_argument('--history', default=False, action='store_true')  
  parser.add_argument('--new_urdf', default=False, action='store_true')  
  parser.add_argument('--mpi_avail', default=True, action='store_false')  
  parser.add_argument('--add_dist', default=True, action='store_false')  
  parser.add_argument('--neg_rew', default=True, action='store_false')  
  parser.add_argument('--rand_dynamics', default=True, action='store_false')  
  parser.add_argument('--rand_residual', default=False, action='store_true')  
  parser.add_argument('--display_hm', default=False, action='store_true')  
  parser.add_argument('--obstacle_type', default=None)  
  parser.add_argument('--max_ts', default=50e6, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)