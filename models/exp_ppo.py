import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import numpy as np
import argparse
from algs.utils import *
from glob import glob   
import re 
import tensorboardX
import time
from pathlib import Path
home = str(Path.home())

def normc_initializer(std=1.0, axis=0):
  def _initializer(shape, dtype=None, partition_info=None):  # pylint: disable=W0613
    out = np.random.randn(*shape).astype(dtype.as_numpy_dtype)
    out *= std / np.sqrt(np.square(out).sum(axis=axis, keepdims=True))
    return tf.constant(out)
  return _initializer

def gaussian_likelihood(x, mu, log_std):
  EPS = 1e-8
  pre_sum = -0.5 * (((x-mu)/(tf.exp(log_std)+EPS))**2 + 2*log_std + np.log(2*np.pi))
  return tf.reduce_sum(pre_sum, axis=1)

class ReplayBuffer():
  """
  A simple FIFO experience replay buffer.
  """
  def __init__(self, obs_dim, act_dim, size):
    self.obs1_buf = np.zeros([size, obs_dim], dtype=np.float32)
    self.obs2_buf = np.zeros([size, obs_dim], dtype=np.float32)
    self.acts_buf = np.zeros([size, act_dim], dtype=np.float32)
    self.rews_buf = np.zeros(size, dtype=np.float32)
    self.done_buf = np.zeros(size, dtype=np.float32)
    self.ptr, self.size, self.max_size = 0, 0, size

  def store(self, obs, rew, next_obs, done):
    self.obs1_buf[self.ptr] = obs
    self.rews_buf[self.ptr] = rew
    self.done_buf[self.ptr] = done
    self.obs2_buf[self.ptr] = next_obs
    self.ptr = (self.ptr+1) % self.max_size
    self.size = min(self.size+1, self.max_size)

  def sample_batch(self, batch_size=32):
    idxs = np.random.randint(0, self.size, size=batch_size)
    return dict(obs1=self.obs1_buf[idxs],
                rews=self.rews_buf[idxs],
                done=self.done_buf[idxs],
                obs2=self.obs2_buf[idxs]), idxs
  
  def all_data(self):
    idxs = [i for i in range(self.size)]
    return dict(obs1=self.obs1_buf[idxs],
                rews=self.rews_buf[idxs],
                done=self.done_buf[idxs],
                obs2=self.obs2_buf[idxs])

class Model():
  GAMMA = 0.99
  def __init__(self, env, PATH, horizon, batch_size, writer):
    self.PATH = PATH
    self.horizon = horizon
    self.batch_size = batch_size
    self.writer = writer
    self.ob_size = env.ob_size
    self.replay_buffer = ReplayBuffer(env.ob_size, env.ac_size, 1000)
    # Place holders for input and label
    self.ob = tf.placeholder(dtype=tf.float32, shape=(None,env.ob_size))
    self.rew = tf.placeholder(dtype=tf.float32, shape=(None,1))
    self.done = tf.placeholder(dtype=tf.float32, shape=(None,1))
    self.R = tf.placeholder(dtype=tf.float32, shape=(None,1))
    self.A = tf.placeholder(dtype=tf.float32, shape=(None,1))
    self.OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
    temp = 1.0
    clip_val = 20
    # TODO Normalise input
    # Network
    with tf.variable_scope('vf'):
      x = tf.nn.softsign(tf.layers.dense(self.ob, 128, name="fc1"))
      x = tf.nn.softsign(tf.layers.dense(x, 64, name="fc2"))
      self.vpred = tf.layers.dense(x, 1, name="final")
    with tf.variable_scope('pol'):
      x = tf.nn.softsign(tf.layers.dense(self.ob, 128, name="fc1"))
      x = tf.nn.softsign(tf.layers.dense(x, 64, name="fc2"))
      self.mean = tf.layers.dense(x, env.ac_size, name="mean", kernel_initializer=normc_initializer(0.01))
      # self.logstd = tf.layers.dense(x, env.ac_size, name="logstd", kernel_initializer=tf.zeros_initializer())
      self.logstd = tf.get_variable(name="logstd", shape=[1, env.ac_size], initializer=tf.zeros_initializer())
      # AWR doesn't use trainable logstd?
      # logstd = tf.get_variable(dtype=tf.float32, name="logstd", initializer=0.4,trainable=False)
      # self.logstd = tf.broadcast_to(logstd, tf.shape(self.mean))
      self.std = tf.exp(self.logstd)
      self.pol_sample = self.mean + tf.random_normal(tf.shape(self.mean)) * self.std
      self.logp_pi = gaussian_likelihood(self.pol_sample, self.mean, self.logstd)
    # MSE loss
    # adv = self.rew + self.GAMMA*(1-self.done)*self.vpred
    # self.R_op = 
    self.R_op = self.rew + self.GAMMA*(1-self.done)*self.vpred
    self.vf_loss = 0.5 * tf.reduce_mean((self.R - self.vpred)**2)
    # TODO: Normalize advantage. Use TD error 0.95
    # self.pol_loss = -tf.reduce_mean(self.logp_pi * tf.math.minimum(tf.exp(self.A/temp), clip_val))
    self.neg_logp = -self.logp_pi
    ratio = tf.exp(self.OLDNEGLOGPAC - self.neg_logp)
    pg_losses = -self.A * ratio
    pg_losses2 = -self.A * tf.clip_by_value(ratio, 1.0 - 0.2, 1.0 + 0.2)
    self.pol_loss = tf.reduce_mean(tf.maximum(pg_losses, pg_losses2))
    # loss = pg_loss - entropy * ent_coef + vf_loss * vf_coef
  
    # Optimizer
    self.vf_optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
    self.pol_optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
    self.vf_vars = [x for x in tf.global_variables() if 'vf' in x.name]
    self.vf_train_op = self.vf_optimizer.minimize(self.vf_loss, var_list=self.vf_vars)
    self.pol_vars = [x for x in tf.global_variables() if 'pol' in x.name]
    self.pol_train_op = self.pol_optimizer.minimize(self.pol_loss, var_list=self.pol_vars)

    # Initialize stuff
    self.policy_saver = tf.train.Saver(var_list=self.vf_vars + self.pol_vars)
    self.sess = tf.Session()
    self.sess.run(tf.global_variables_initializer())

  def get_returns(self, rew, done, vpred, last_value, last_done):
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(rew)
    lastgaelam = 0
    for t in reversed(range(len(rew))):
      if t == len(rew) - 1:
        nextnonterminal = 1.0 - last_done
        nextvalues = last_value
      else:
        nextnonterminal = 1.0 - done[t+1]
        nextvalues = vpred[t+1]
      delta = rew[t] + gamma * nextvalues * nextnonterminal - vpred[t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    vpred_label = advs + vpred
    return vpred_label

  def step(self, x, stochastic=True):
    if stochastic:
      # print(x.shape)
      return self.sess.run(self.pol_sample, feed_dict = {self.ob: x.reshape(-1, self.ob_size)})[0]
    else:
      return self.sess.run(self.mean, feed_dict = {self.ob: x.reshape(-1, self.ob_size)})[0]

  def train(self, next_ob, last_done, rewbuffer):
    data = self.replay_buffer.all_data()
    vpred = self.get_vpred(data['obs1'])
    nlogp = self.get_nlogp(data['obs1'])
    next_vpred = self.get_vpred(next_ob)
    returns = self.get_returns(data['rews'].reshape(-1,1), data['done'].reshape(-1,1), vpred, next_vpred, last_done)
    advs = returns - vpred
    advs = (advs - advs.mean()) / (advs.std() + 1e-8)
    # Train vf:
    vf_loss = []
    pol_loss = []
    total_idx = [i for i in range(1000)]
    batch_size = 32
    for epoch in range(10):
      np.random.shuffle(total_idx)
      for b in range(0,1000, batch_size):
        idx = total_idx[b:b+batch_size]
        # sample, idx = rb.sample_batch(256)
        # R = self.get_R(sample['obs2'], sample['rews'], sample['done'])
        vf_loss.append(self.train_vf(data['obs1'][idx], returns[idx]))
      # Train pol:
      
      # for _ in range(1000):
        # sample, idx = rb.sample_batch(256)
        # R = self.get_R(sample['obs2'], sample['rews'], sample['done'])
        pol_loss.append(self.train_pol(data['obs1'][idx], advs[idx], nlogp[idx]))
    self.loss = [np.mean(vf_loss, axis=0), np.mean(pol_loss, axis=0)]
    print(self.loss, np.mean(rewbuffer))
    # self.evaluate(rews, lens)

  def get_nlogp(self, x):
    feed_dict = {self.ob: x.reshape(-1,self.ob_size)}
    return self.sess.run(self.neg_logp, feed_dict)

  def get_vpred(self, x):
    feed_dict = {self.ob: x.reshape(-1,self.ob_size)}
    return self.sess.run(self.vpred, feed_dict)
  
  def get_R(self, x, r, d):
    feed_dict = {self.ob: x.reshape(-1,self.ob_size), self.rew: r.reshape(-1,1), self.done: d.reshape(-1,1)}
    return self.sess.run(self.R_op, feed_dict)

  def train_vf(self, x, R):
    feed_dict = {self.ob: x.reshape(-1,self.ob_size), self.R: R.reshape(-1,1)}
    return self.sess.run([self.vf_loss, self.vf_train_op], feed_dict)[0]
  
  def train_pol(self, x, adv, nlogp):
    feed_dict = {self.ob: x.reshape(-1,self.ob_size), self.A: adv.reshape(-1,1), self.OLDNEGLOGPAC:nlogp}
    return self.sess.run([self.pol_loss, self.pol_train_op], feed_dict)[0]

  def save(self):
    self.policy_saver.save(self.sess, self.PATH + 'model.ckpt')

  def load(self, WEIGHT_PATH):
    self.policy_saver.restore(self.sess, WEIGHT_PATH + 'model.ckpt')
    print("Loaded weights")

if __name__ == '__main__':
  import gym
  import argparse
  import tensorboardX
  from collections import deque   
  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="0"
  
  env = gym.make('Pendulum-v0')
  env.ob_size = 3
  env.ac_size = 1
  env.im_size = [48,48,4]
  PATH = home + '/results/test/'
  writer = tensorboardX.SummaryWriter(log_dir=PATH)

  sess = tf.Session()

  pol = Model(env, PATH, horizon=1000, batch_size=100, writer=writer)
  # replay_buffer =  ReplayBuffer(env.ob_size, env.ac_size, 1000)

  # # Count variables: could be a nice feature
  # var_counts = tuple(count_vars(scope) for scope in 
  #                     ['main/pi', 'main/q1', 'main/q2', 'main/v', 'main'])
  # print(('\nNumber of parameters: \t pi: %d, \t' + \
  #         'q1: %d, \t q2: %d, \t v: %d, \t total: %d\n')%var_counts)

  sess.run(tf.global_variables_initializer())
  prev_done = True
  ob = env.reset()
  im = np.zeros(env.im_size)
  ep_ret = 0
  ep_len = 0
  rew_buffer = deque(maxlen=100)
  len_buffer = deque(maxlen=100)
  steps = 0
  env.total_steps = 0
  while True:
    # Consider putting this in main.py for sac and td3
    # if steps > pol.start_steps:
      # a = get_action(o)
    act = pol.step(ob, stochastic=True)
    # else:
      # act = env.action_space.sample()
    if args.render:
      env.render()
    next_ob, rew, done, _ = env.step(act)
    next_ob = np.squeeze(next_ob)
    next_im = np.zeros(env.im_size)
    
    replay_buffer.store(ob, rew, next_ob, prev_done)
    
    ob = next_ob
    steps += 1
    ep_ret += rew
    ep_len += 1
    env.total_steps += 1
    if done or steps % 1000 == 0:
      ob = env.reset()
      im = np.zeros(env.im_size)
      rew_buffer.append(ep_ret)     
      len_buffer.append(ep_len)     
      ep_ret = 0
      ep_len = 0
        
    if steps % 1000 == 0:
      pol.train(replay_buffer, next_ob, done, rew_buffer)
        # pol.train(rew_buffer, len_buffer)