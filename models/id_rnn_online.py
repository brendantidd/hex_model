# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *
from baselines.common.mpi_running_mean_std import RunningMeanStd
import pybullet as p
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI

comm = MPI.COMM_WORLD

class Env():
  ob_size = 7
  ac_size = 3
  im_size = [48,48,4]
  simtimeStep = 1/800
  actionRepeat = 1
  def __init__(self, render=False, PATH=None):
    self.args = args
    self.render = render
    self.PATH = PATH
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot
    self.load_model()

  def load_model(self):
    p.loadMJCF("./assets/ground.xml")
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)

    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    self.vel_max = [8,8,8]
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}

    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self, initial_pos):
    self.set_position([0,0,2],[0,0,0,1],joints=initial_pos[:3], joint_vel=initial_pos[3:6])
    self.get_observation()

  def step(self, actions):
    self.prev_joints, self.prev_joint_vel = self.joints, self.joint_vel
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=actions)
    p.stepSimulation()
    if self.render:
      time.sleep(0.001)
    self.get_observation()
    reward, done = self.get_reward()
    return np.array(self.joints + self.joint_vel)

  def get_reward(self):
    reward = 0
    done = False
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    # print(self.joints, s)

  def set_position(self, pos=[0,0,2], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])


class RNN():
  def __init__(self, name='model', input_size=1, output_size=1, lstm_size=128, dense_size=64, sess=None, adam=False, norm=True):
    self.name = name
    self.adam = adam
    self.input_size = input_size
    self.output_size = output_size
    self.lstm_size = lstm_size
    self.dense_size = dense_size
    self.sess = sess
    self.inputs = tf.placeholder(tf.float32, [None, None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, None, output_size])
    self.norm = norm
    # self.norm = tf.placeholder_with_default(True, shape=())
    self.keep_prob = tf.placeholder_with_default(1.0, shape=())
    self.batch_size = tf.placeholder(tf.int32, [None])
    self.initialise()
  
  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'id_model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'id_model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for inverse dynamics module from ", WEIGHT_PATH)
    
  def initialise(self):
    with tf.variable_scope(self.name):
      self.mean = tf.get_variable('mean', initializer=np.zeros(self.input_size, dtype=np.float32), dtype=tf.float32)
      self.std = tf.get_variable('std', initializer=np.ones(self.input_size, dtype=np.float32), dtype=tf.float32)
      self.scalar = tf.get_variable('scalar', initializer=np.ones(self.output_size, dtype=np.float32), dtype=tf.float32)
      # self.label_mean = tf.get_variable('label_mean', initializer=np.zeros(self.output_size, dtype=np.float32), dtype=tf.float32)
      # self.label_std = tf.get_variable('label_std', initializer=np.ones(self.output_size, dtype=np.float32), dtype=tf.float32)
      self.model()
    vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name or 'Adam' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)
    # print(self.vars)
  
  def model(self):
    # self.input_rms = RunningMeanStd(shape=[self.input_size,])
    # last_out = tf.cond(self.norm, lambda: self.inputs, lambda: tf.clip_by_value((self.inputs - self.input_rms.mean)/tf.stop_gradient(self.input_rms.std), -5, 5))
    # last_out = tf.stop_gradient(tf.clip_by_value((self.inputs - self.input_rms.mean)/self.input_rms.std, -5, 5))
    # last_out = tf.cond(self.norm, lambda: self.inputs, lambda: tf.clip_by_value((self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std), -5, 5))
    if self.norm:
      last_out = tf.clip_by_value((self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std), -5, 5)
    else:
      last_out = self.inputs
    # last_out = tf.clip_by_value((self.inputs - self.mean)/self.std, -5, 5)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.tanh)
    self.cell = nn.rnn_cell.LSTMCell(self.lstm_size, name="r1")
    # lstm_stacked = tf.contrib.rnn.MultiRNNCell([self.lstm_cell() for _ in range(n_layers)])
    self.batch_state_in = self.cell.zero_state(self.batch_size, tf.float32) 
    # last_out, self.states = nn.dynamic_rnn(self.cell,self.inputs,dtype=tf.float32, initial_state=self.batch_state_in)  
    last_out, self.states = nn.dynamic_rnn(self.cell,last_out,dtype=tf.float32, initial_state=self.batch_state_in)  
    last_out = tf.nn.dropout(last_out, self.keep_prob)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.tanh)
    if self.norm:
      self.outputs = tf.layers.dense(last_out, self.output_size, activation=tf.nn.tanh)
      self.scaled_outputs = self.outputs * tf.stop_gradient(self.scalar)
      self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels/tf.stop_gradient(self.scalar)))
      # self.scaled_outputs = self.outputs * tf.stop_gradient(self.label_std) + tf.stop_gradient(self.label_mean)
      # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, tf.clip_by_value((self.labels - tf.stop_gradient(self.label_mean))/self.label_std,-5,5)))

    else:
      self.outputs = tf.layers.dense(last_out, self.output_size)
      self.scaled_outputs = self.outputs
      self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))
    if self.adam:
      # Use MPI adam optimiser 
      params = tf.trainable_variables(self.name + "/")
      self.trainer = MpiAdamOptimizer(comm, learning_rate=0.001, epsilon=1e-5)
      # self.trainer = MpiAdamOptimizer(comm, learning_rate=0.0001, epsilon=1e-5)
      grads_and_var = self.trainer.compute_gradients(self.loss, params)
      grads, var = zip(*grads_and_var)
      grads_and_var = list(zip(grads, var))
      self.optimise = self.trainer.apply_gradients(grads_and_var)
    else:
      # Use RMS prop (no multi processing)
      optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
      self.optimise = optimizer.minimize(self.loss)

  def step(self, X, state=None, seq_length=1):
    if state is None:
      state = [np.zeros([1,self.lstm_size]),np.zeros([1,self.lstm_size])]
    output, state = self.sess.run([self.scaled_outputs, self.states],feed_dict={self.inputs:X.reshape(1,seq_length,self.input_size), self.batch_state_in:state,self.batch_size:[1]})
    # output, state = self.sess.run([self.outputs, self.states],feed_dict={self.inputs:X.reshape(1,seq_length,self.input_size), self.batch_state_in:state,self.batch_size:[1], self.norm:norm})
    return output[0][0], state

  def train(self, X, y, states=None, batch_size=1, seq_length=1):
    # self.input_rms.update(X.reshape(-1,self.input_size))      
    if states is None:
      states = [np.zeros([batch_size,self.lstm_size]), np.zeros([batch_size,self.lstm_size])]  
    X = X.reshape(batch_size,seq_length,self.input_size)
    y = y.reshape(batch_size,seq_length,self.output_size)
    # _, loss, outputs, states = self.sess.run([self.optimise, self.loss, self.outputs, self.states],feed_dict={self.inputs:X, self.labels:y, self.batch_state_in: states,self.batch_size:[batch_size], self.keep_prob:0.8, self.norm:norm})
    # scaled_y = self.sess.run(y/self.scalar)
    _, loss, outputs, states = self.sess.run([self.optimise, self.loss, self.scaled_outputs, self.states],feed_dict={self.inputs:X, self.labels:y, self.batch_state_in: states,self.batch_size:[batch_size], self.keep_prob:0.8})
    return loss, outputs, states

  def get_loss(self, X, y, states=None, batch_size=1, seq_length=1):
    if states is None:
      states = [np.zeros([batch_size,self.lstm_size]), np.zeros([batch_size,self.lstm_size])]  
    X = X.reshape(batch_size,seq_length,self.input_size)
    y = y.reshape(batch_size,seq_length,self.output_size)
    loss = self.sess.run(self.loss,feed_dict={self.inputs:X, self.labels:y, self.batch_state_in: states,self.batch_size:[batch_size], self.keep_prob:0.8})
    return loss

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--cl', default=False, action='store_true')
  parser.add_argument('--ft', default=False, action='store_true')
  parser.add_argument('--scale', default=True, action='store_false')
  parser.add_argument('--norm', default=True, action='store_false')
  parser.add_argument('--adam', default=False, action='store_true')
  args = parser.parse_args()
  
  if args.adam:
    args.exp = "_" + args.exp + "adam"
  else:
    args.exp = "_" + args.exp
  data_path = home + '/data/iros_data/real/3_2/all/'
  WEIGHTS_PATH = data_path + 'id_weights' + args.exp + '/'
  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.2)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

  # batch_size = 256
  batch_size = 4
  seq_length = 256

  input_size = 12
  output_size = 3

  lstm_size = 128
  dense_size = 128
  epochs = 20000
  best_val_loss = 10000
  slide_num = seq_length

  rnn = RNN(name='id_model', input_size=input_size, output_size=output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess, adam=args.adam, norm=args.norm)
  
  # if args.fm:
  input_size = 9
  output_size = 6
  from models import simple_rnn
  lstm_size = 128
  dense_size = 128
  forward_model = simple_rnn.RNN(name='forward_model', input_size=input_size, output_size=output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess)

  sess.run(tf.global_variables_initializer())

  forward_model.load(home + '/data/iros_data/real/3_2/all/weights_/best/')
  samples = np.load(data_path + 'inputs.npy')
  dones = np.load(data_path + 'dones.npy')

  env = Env(render=args.render)

  print("loaded data from ", data_path)
  print(samples.shape, dones.shape)
  done_idx = list(np.where(dones==1.0)[0]) + [dones.shape[0]-1] 
  print(done_idx)

  input_mean, input_std = np.mean(samples[:,:6], axis=0, dtype=np.float32), np.std(samples[:,:6], axis=0, dtype=np.float32)
  sess.run([rnn.mean.assign(np.concatenate((input_mean,input_mean), axis=0)), rnn.std.assign(np.concatenate((input_std, input_std), axis=0))])
  scale = np.array([10,10,3])
  sess.run([rnn.scalar.assign(scale)])
  print(rnn.mean.eval(), rnn.std.eval(), rnn.scalar.eval())

  for e in range(10):
    t2 = time.time()

    inputs = []
    labels = []
    inp = []
    lab = []
    fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
    id_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
    seq_count = 0
    sampled_done_idx = np.random.randint(0,15)
    print("running data from", done_idx[sampled_done_idx])
    env.reset(samples[done_idx[sampled_done_idx],:])

    for i in range(done_idx[sampled_done_idx],dones.shape[0]):
      fm_output, fm_state = forward_model.step(np.array(env.joints + env.joint_vel + list(samples[i,6:])), fm_state)
      id_output, id_state = rnn.step(np.array(env.joints + env.joint_vel + list(fm_output)), id_state)
      
      env.step(id_output)
      inp.append(env.prev_joints + env.prev_joint_vel + env.joints + env.joint_vel)
      lab.append(list(samples[i,6:]))
      seq_count += 1
      # print(seq_count, seq_length)
      if i % 10000 == 0:
        print("sample ", done_idx[sampled_done_idx] + i)
      if seq_count == seq_length:
        inputs.append(inp)
        labels.append(lab)
        if np.array(inputs).shape[0] * seq_length > 50000:
        # if np.array(inputs).shape[0] * seq_length > 10000:
          break
        inp = []
        lab = []
        seq_count = 0
      if dones[i]:
        # break
        fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
        id_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
        env.reset(samples[i,:])
        inp = []
        lab = []
        seq_count = 0
    inputs = np.array(inputs)
    labels = np.array(labels)
    train_inputs, test_inputs = inputs[:int(inputs.shape[0]*0.9),:,:],inputs[int(inputs.shape[0]*0.9):,:,:] 
    train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:,:],labels[int(labels.shape[0]*0.9):,:,:] 
    print(train_inputs.shape, train_labels.shape, test_inputs.shape, test_labels.shape)

    
    # TODO: Stateful training: reset the state at the beginning of every epoch (batches must be contiguous), can't currently do with closed loop data generation.
    # print(train_inputs.shape[0]//batch_size)
    print("data collected in ", (time.time()-t2)/60, "now training")
    
    epoch_loss = []    
    t3 = time.time()
    for e in range(500):
      t1 = time.time()

      idx = [i for i in range(train_inputs.shape[0])]
      np.random.shuffle(idx)
      batch_size = 32
      for batch in range(train_inputs.shape[0]//batch_size):
        # print(batch)
        b_idx = batch*batch_size
        batch_x, batch_y = train_inputs[idx[b_idx:b_idx+batch_size],:,:], train_labels[idx[b_idx:b_idx+batch_size],:,:]
        loss, outputs, states = rnn.train(batch_x, batch_y, batch_size=batch_size, seq_length=seq_length)
        epoch_loss.append(loss)

      # val_loss = []
      val_batch_size = test_inputs.shape[0]
    
      states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
      val_loss, ol_predictions = sess.run([rnn.loss, rnn.scaled_outputs],feed_dict={rnn.inputs:test_inputs, rnn.labels:test_labels, rnn.batch_state_in: states,
      rnn.batch_size:[val_batch_size]})

      # print("Epoch,",e,"Loss", np.mean(epoch_loss), "Val loss", np.mean(val_loss), "time: ", time.time() - t1)
      print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
      writer.add_scalar("error", np.mean(epoch_loss), e)
      writer.add_scalar("val error", np.mean(val_loss), e)
      if np.mean(val_loss) < best_val_loss and e > 200:
        val_idx = np.random.randint(0,test_labels.shape[0])
        rnn.save(WEIGHTS_PATH + 'best/')
        best_val_loss = np.mean(val_loss)
        y1,y2,y3 = test_labels[val_idx,:,0],test_labels[val_idx,:,1],test_labels[val_idx,:,2]
        
        q1,q2,q3 = ol_predictions[val_idx,:,0],ol_predictions[val_idx,:,1],ol_predictions[val_idx,:,2]
        if args.cl:
          k1,k2,k3 = cl_predictions[val_idx,:,0],cl_predictions[val_idx,:,1],cl_predictions[val_idx,:,2]
          subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3]], legend=[['labels','pred','pred']]*3,PATH=WEIGHTS_PATH + 'best_')
        else:
          subplot([[y1,q1],[y2,q2],[y3,q3]], legend=[['labels','pred']]*3,PATH=WEIGHTS_PATH + 'best_')
      if e % 5 == 0:
        rnn.save(WEIGHTS_PATH)
      if e % 20 == 0:
        val_idx = np.random.randint(0,test_labels.shape[0])
        # cl_predictions = np.array(cl_predictions)
        y1,y2,y3 = test_labels[val_idx,:,0],test_labels[val_idx,:,1],test_labels[val_idx,:,2]
        q1,q2,q3 = ol_predictions[val_idx,:,0],ol_predictions[val_idx,:,1],ol_predictions[val_idx,:,2]
        if args.cl:
          k1,k2,k3 = cl_predictions[val_idx,:,0],cl_predictions[val_idx,:,1],cl_predictions[val_idx,:,2]
          subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3]], legend=[['labels','pred','pred']]*3,PATH=WEIGHTS_PATH)
        else:
          subplot([[y1,q1],[y2,q2],[y3,q3]], legend=[['labels','pred']]*3,PATH=WEIGHTS_PATH)
    print("total train time ", (time.time() - t3)/60)
