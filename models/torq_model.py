# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *
import pybullet as p

class Env():
  ob_size = 7
  ac_size = 3
  im_size = [48,48,4]
  simtimeStep = 1/800
  actionRepeat = 1
  def __init__(self, render=False, PATH=None):
    self.args = args
    self.render = render
    self.PATH = PATH
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot
    self.load_model()

  def load_model(self):
    p.loadMJCF("./assets/ground.xml")
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)

    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    self.vel_max = [8,8,8]
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}

    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def step(self, actions):
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=actions)
    p.stepSimulation()
    if self.render:
      time.sleep(0.001)
    self.get_observation()
    reward, done = self.get_reward()
    return np.array(self.joints + self.joint_vel)

  def get_reward(self):
    reward = 0
    done = False
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    # print(self.joints, s)

  def set_position(self, pos=[0,0,2], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])


class RNN():
  def __init__(self, name='model', input_size=1, output_size=1, lstm_size=128, dense_size=64, sess=None, norm=True):
    self.name = name
    self.norm = norm
    self.input_size = input_size
    self.output_size = output_size
    self.lstm_size = lstm_size
    self.dense_size = dense_size
    self.sess = sess
    self.inputs = tf.placeholder(tf.float32, [None, None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, None, output_size])
    self.train = tf.placeholder_with_default(True, shape=())
    self.keep_prob = tf.placeholder_with_default(1.0, shape=())
    self.batch_size = tf.placeholder(tf.int32, [None])
    self.initialise(mean=np.zeros(self.input_size, dtype=np.float32), std=np.ones(self.input_size, dtype=np.float32), scalar=np.ones(self.output_size, dtype=np.float32))
  
  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'model.ckpt')
  
  def load(self, WEIGHT_PATH):
    model_name = 'model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for inverse dynamics module")
    
  def initialise(self, mean, std, scalar):
    with tf.variable_scope(self.name):
      self.mean = tf.get_variable('mean', initializer=mean, dtype=tf.float32)
      self.std = tf.get_variable('std', initializer=std, dtype=tf.float32)
      self.scalar = tf.get_variable('scalar', initializer=scalar, dtype=tf.float32)
      self.model()
    vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)
    # print(self.vars)
  
  def model(self):
    last_out = tf.cond(self.train, lambda: self.inputs, lambda: tf.clip_by_value((self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std), -5, 5))
    # last_out = tf.clip_by_value((self.inputs - self.mean)/self.std, -5, 5)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.tanh)
    self.cell = nn.rnn_cell.LSTMCell(self.lstm_size, name="r1")
    # lstm_stacked = tf.contrib.rnn.MultiRNNCell([self.lstm_cell() for _ in range(n_layers)])
    self.batch_state_in = self.cell.zero_state(self.batch_size, tf.float32) 
    # last_out, self.states = nn.dynamic_rnn(self.cell,self.inputs,dtype=tf.float32, initial_state=self.batch_state_in)  
    last_out, self.states = nn.dynamic_rnn(self.cell,last_out,dtype=tf.float32, initial_state=self.batch_state_in)  
    last_out = tf.nn.dropout(last_out, self.keep_prob)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.tanh)
    self.outputs = tf.layers.dense(last_out, self.output_size, activation=tf.nn.tanh)
    self.scaled_outputs = self.outputs * tf.stop_gradient(self.scalar)
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))
    optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
    self.optimise =  optimizer.minimize(self.loss)

  def step(self, X, state=None, seq_length=1):
    if state is None:
      state = [np.zeros([1,self.lstm_size]),np.zeros([1,self.lstm_size])]
    output, state = self.sess.run([self.scaled_outputs, self.states],feed_dict={self.inputs:X.reshape(1,seq_length,self.input_size), self.batch_state_in:state,self.batch_size:[1], self.train:False})
    return output[0][0], state

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--cl', default=False, action='store_true')
  parser.add_argument('--norm', default=True, action='store_false')
  args = parser.parse_args()
  
  args.exp = "_" + args.exp
  # data_path = home + '/data/iros_data/pb_id2/'
  # data_path = home + '/data/iros_data/pb_id3/'
  # data_path = home + '/data/iros_data/pb_id_100_rand/'
  data_path = home + '/data/iros_data/pb_id_100_rand_norm/'
  # data_path = home + '/results/hex_model/latest/pb_id3/'
  WEIGHTS_PATH = data_path + 'weights' + args.exp + '/'
  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.4)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

  # handle sequence for batch training
  batch_size = 64
  seq_length = 512
  input_size = 12
  lstm_size = 128
  dense_size = 128
  output_size = 3
  epochs = 2000
  best_val_loss = 10000
  slide_num = seq_length

  rnn = RNN(name='id_model', input_size=input_size, output_size=output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess, norm=args.norm)
  
  sess.run(tf.global_variables_initializer())

  env = Env(render=args.render)

  if args.test:
    labels = np.array(pd.read_excel(data_path + 'test_labels.xlsx'))
    inputs = np.array(pd.read_excel(data_path + 'test_inputs.xlsx'))
  else:
    # labels = np.concatenate((np.array(pd.read_excel(data_path + 'train_labels.xlsx')),np.array(pd.read_excel(data_path + 'test_labels.xlsx')))) 
    # inputs = np.concatenate((np.array(pd.read_excel(data_path + 'train_inputs.xlsx')),np.array(pd.read_excel(data_path + 'test_inputs.xlsx')))) 
    # inputs = np.array(pd.read_excel(data_path + 'train_inputs.xlsx'))
    # labels = np.array(pd.read_excel(data_path + 'train_labels.xlsx'))
    inputs = np.array(pd.read_excel(data_path + 'inputs.xlsx'))
    labels = np.array(pd.read_excel(data_path + 'labels.xlsx'))
  print(labels.shape, inputs.shape)
  if args.norm:
    input_mean, input_std = np.mean(inputs, axis=0, dtype=np.float32), np.std(inputs, axis=0, dtype=np.float32)
    # print(input_mean, input_std)
    inputs = np.clip((inputs - input_mean)/input_std, -5, 5)
    # Scale labels (we have a tanh at the last output layer)
    max_scale, min_scale = np.max(labels, axis=0), np.min(labels,axis=0)
    scale = []
    for ma, mi in zip(max_scale, min_scale):
      if abs(mi) > ma:
        scale.append(abs(mi))
      else:
        scale.append(ma)
    # Add 10% to handle unseen labels
    scale = np.array(scale, dtype=np.float32) * 1.1
    labels = labels/scale
    sess.run([rnn.mean.assign(input_mean), rnn.std.assign(input_std), rnn.scalar.assign(scale)])
    print("Mean: ", input_mean)
    print("Stds: ", input_std)
    print("Scale: ", scale)
    print(rnn.mean.eval(), rnn.std.eval(), rnn.scalar.eval())

  # test_mean, test_std, test_scalar = sess.run([rnn.mean, rnn.std, rnn.scalar])
  # print("Means n stuff")
  # print(test_mean, test_std, test_scalar)

  dim = (inputs.shape[0]//seq_length)*seq_length
  new_inputs = np.zeros([dim//seq_length, seq_length, inputs.shape[1]])
  new_labels = np.zeros([dim//seq_length, seq_length, labels.shape[1]])
  print(inputs.shape, labels.shape, dim, new_inputs.shape, new_labels.shape)

  # TODO Consider sliding window, note: can't use stateful training if not sliding over whole seq_length! But can shuffle sequences, so might be better? 
  for i in range(0,dim//seq_length):
    new_inputs[i,:,:] = inputs[i*seq_length:i*seq_length+seq_length,:]
    new_labels[i,:,:] = labels[i*seq_length:i*seq_length+seq_length,:]

  
  train_inputs, test_inputs = new_inputs[:int(new_inputs.shape[0]*0.9),:,:],new_inputs[int(new_inputs.shape[0]*0.9):,:,:] 
  train_labels, test_labels = new_labels[:int(new_labels.shape[0]*0.9),:,:],new_labels[int(new_labels.shape[0]*0.9):,:,:] 

  print(train_inputs.shape, test_inputs.shape)
  print(train_labels.shape, test_labels.shape)
  for e in range(epochs):
    t1 = time.time()
    epoch_loss = []
    # TODO: Stateful training: reset the state at the beginning of every epoch (batches must be contiguous), can't currently do with closed loop data generation.
    for batch in range(train_inputs.shape[0]//batch_size):
      idx = batch*batch_size
      batch_x, batch_y = train_inputs[idx:idx+batch_size,:,:], train_labels[idx:idx+batch_size,:,:]
      # if args.cl and batch%2 == 0:
      if True:
      # if batch%3 == 0 and batch != 0:
      # if False:
        input_x = np.zeros_like(batch_x)
        # print(batch_x.shape)
        for b in range(batch):
          prev_cl_input, cl_input = batch_x[b,0,:6], batch_x[b,0,6:]
          temp = destandardise(prev_cl_input, input_mean[:6], input_std[:6])
          # temp = prev_cl_input
          env.set_position(joints=temp[:3], joint_vel=temp[3:6])
          states = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
          for j in range(seq_length):
            net_in = np.concatenate((prev_cl_input.reshape(1,6), cl_input.reshape(1,6)),axis=1)
            input_x[b,j,:] = net_in
            # print(net_in)
            # print(input_x)
            torque, states = sess.run([rnn.outputs, rnn.states],feed_dict={rnn.inputs:net_in.reshape(1,1,input_size), rnn.batch_state_in: states,rnn.batch_size:[1]})
            prev_cl_input = standardise(env.step(torque[0][0] * scale), input_mean[6:], input_std[6:])
            if j < seq_length - 1:
              cl_input = batch_x[b,j+1,6:]
            # cl_input = env.step(torque[0][0] * scale)
      else:
        input_x = batch_x
      # print(input_x.shape, batch_y.shape)
      states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      
      _, loss, train_states = sess.run([rnn.optimise, rnn.loss, rnn.states],feed_dict={rnn.inputs:input_x, rnn.labels:batch_y, rnn.batch_state_in: states,rnn.batch_size:[batch_size], rnn.keep_prob:0.9})
      epoch_loss.append(loss)
    # Validation step: both closed loop and open loop predictions

    # test_mean, test_std, test_scalar = sess.run([rnn.mean, rnn.std, rnn.scalar])
    # print("Means n stuff")
    # print(test_mean, test_std, test_scalar)

    val_loss = []
    val_batch_size = test_inputs.shape[0]
    if args.cl:
      # cl_input = test_inputs[:,0,:6]
      cl_inputs = np.zeros_like(test_inputs[0,:,:])
      cl_predictions = np.zeros_like(test_labels[0,:,:])
      # for b in range(val_batch_size):
      # for b in range(1):
      b = 0
      prev_cl_input, cl_input = test_inputs[b,0,:6], test_inputs[b,0,6:]
      temp = destandardise(prev_cl_input, input_mean[:6], input_std[:6])
      # temp = prev_cl_input
      env.set_position(joints=temp[:3], joint_vel=temp[3:6])
      val_states = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
      for j in range(seq_length):
        net_in = np.concatenate((prev_cl_input.reshape(1,6), cl_input.reshape(1,6)),axis=1)
        # cl_inputs[b,j,:] = net_in
        cl_inputs[j,:] = net_in
        torque, val_states = sess.run([rnn.outputs, rnn.states],feed_dict={rnn.inputs:net_in.reshape(1,1,input_size),rnn.batch_state_in: val_states,rnn.batch_size:[1]})
        prev_cl_input = standardise(env.step(torque[0][0] * scale), input_mean[6:], input_std[6:])
        if j < seq_length - 1:
          cl_input = batch_x[b,j+1,6:]
        # cl_input = env.step(torque[0][0] * scale)
        # cl_predictions[b,j,:] = torque.reshape(1, output_size)
        cl_predictions[j,:] = torque.reshape(1, output_size)
      cl_predictions = cl_predictions.reshape(1,seq_length,output_size)
      # Get open loop predictions
    val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
    _, ol_predictions = sess.run([rnn.loss, rnn.outputs],feed_dict={rnn.inputs:test_inputs, rnn.labels:test_labels, rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
    # Get closed loop loss
    if args.cl:
      val_states = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]    
      # print(cl_inputs.reshape(1,seq_length,input_size).shape, test_labels[0,:,output_size].shape)
      # print(cl_inputs.reshape(1,seq_length,input_size).shape, test_labels[0,:,output_size].reshape(1,seq_length,output_size).shape)
      loss = sess.run(rnn.loss,feed_dict={rnn.inputs:cl_inputs.reshape(1,seq_length,input_size), rnn.labels:test_labels[0,:,:].reshape(1,seq_length,output_size), rnn.batch_state_in: val_states,rnn.batch_size:[1]})
    val_loss.append(loss)
    # print("Epoch,",e,"Loss", np.mean(epoch_loss), "Val loss", np.mean(val_loss), "time: ", time.time() - t1)
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    if np.mean(val_loss) < best_val_loss:
      rnn.save(WEIGHTS_PATH)
      best_val_loss = np.mean(val_loss)
      y1,y2,y3 = test_labels[0,:,0],test_labels[0,:,1],test_labels[0,:,2]
      
      q1,q2,q3 = ol_predictions[0,:,0],ol_predictions[0,:,1],ol_predictions[0,:,2]
      if args.cl:
        k1,k2,k3 = cl_predictions[0,:,0],cl_predictions[0,:,1],cl_predictions[0,:,2]
        subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3]], legend=[['output','closed loop pred','open loop pred']]*3,PATH=WEIGHTS_PATH + 'best_')
      else:
        subplot([[y1,q1],[y2,q2],[y3,q3]], legend=[['output','open loop pred']]*3,PATH=WEIGHTS_PATH + 'best_')
    if e % 20 == 0:
      # cl_predictions = np.array(cl_predictions)
      y1,y2,y3 = test_labels[0,:,0],test_labels[0,:,1],test_labels[0,:,2]
      q1,q2,q3 = ol_predictions[0,:,0],ol_predictions[0,:,1],ol_predictions[0,:,2]
      if args.cl:
        k1,k2,k3 = cl_predictions[0,:,0],cl_predictions[0,:,1],cl_predictions[0,:,2]
        subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3]], legend=[['output','close loop pred','open loop pred']]*3,PATH=WEIGHTS_PATH)
      else:
        subplot([[y1,q1],[y2,q2],[y3,q3]], legend=[['output','open loop pred']]*3,PATH=WEIGHTS_PATH)
