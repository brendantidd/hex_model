# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import pybullet as p
import numpy as np
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD

class Env():
  rank = comm.Get_rank()
  ob_size = 9
  ac_size = 3
  im_size = [48,48,4]
  simtimeStep = 1/100
  # simtimeStep = 1/60
  actionRepeat = 1
  rew_buffer = deque(maxlen=5)
  episodes = -1
  Kp = 20
  initial_Kp = 20
  total_reward = 0
  def __init__(self, render=False, PATH=None, args=None, horizon=500, display_expert=True, record_step=True, cur=False, test=False, use_expert=False, act_model=None, sim_model=None):
    self.vis_exp = True
    self.args = args

    self.render = render
    self.PATH = PATH
    self.display_expert = display_expert
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.use_expert = use_expert
    # self.all_legs = True
    self.all_legs = False

    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot

    self.load_model()
    self.sim_data = []
    self.exp_sim_data = []
    self.inputs = []
    self.labels = []
    self.rew_buffer = deque(maxlen=5)

  def load_model(self):
    p.loadMJCF("./assets/ground.xml")
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.Id = objs
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.exp1_Id = objs
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.exp2_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)

    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
      print("id",j)
    numJoints = p.getNumJoints(self.exp2_Id)
    self.jdict2 = {}
    self.ordered_joints2 = []
    self.ordered_joint_indices2 = []
    for j in range( p.getNumJoints(self.exp2_Id) ):
      info = p.getJointInfo(self.exp2_Id, j)
      link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices2.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints2.append( (j, lower, upper) )
      self.jdict2[jname] = j
      print("exp2 id",j)

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]
    self.motors2 = [self.jdict2[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    # self.motor_power =  [5, 10, 5]
    # self.vel_max = [8,11,8]
    self.vel_max = [8,8,8]
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}
    
    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp2_Id, self.motors2, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self, start_pos=None):
    self.sample_pointer = 0
    # temp = self.samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torques = temp[:3], temp[3:6], [temp[6:]]
    if start_pos is not None:
      self.exp_joints, self.exp_joint_vel = start_pos[:3], start_pos[3:6]
    else:
      self.exp_joints, self.exp_joint_vel, self.exp_torques = [0,0,1.5], [0,0,0], [0,0,0]
    self.coxa, self.femur, self.tibia = self.exp_joints
    self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel)
    self.set_position([0,2,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, robot_id=self.exp2_Id)
    self.input_buffers = {n:{m:deque([0.0]*3, maxlen=3) for m in ['coxa','femur','tibia']} for n in ['torq', 'vel']}

    # Number of timesteps per step
    self.episodes += 1

    self.total_reward = 0
    self.steps = 0
    self.get_observation()
    self.prev_state = self.joints + self.joint_vel
    if self.record_step:
      self.save_sim_data()
      # if self.episodes > 1:
      #   exit()
    self.desired_torque = [0,0,0]
    self.prev_actions = [0.0,0.0,0.0]
    return np.array(self.joints + self.joint_vel)

  def step(self, actions, exp_actions=None):
    if exp_actions is not None:
      self.exp_joints, self.exp_joint_vel, self.exp_torque = exp_actions[:3], exp_actions[3:6], exp_actions[6:]
    # exp_torques = (self.Kp/self.initial_Kp)*(80*(np.array(self.exp_joints)-np.array(self.joints)) - 0.1*np.array(self.joint_vel))
    # torques = exp_torques
    # torques = [np.clip(e + a,-tm,tm) for e,a,tm in zip(exp_torques, torques, self.tor_max)]
    # torques = self.actions = np.array(actions) * np.array([12,10,5])
    torques = self.actions = np.array(actions) 
    self.set_position([0,1,2], [0,0,0,1], self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp1_Id)
    # p.setJointMotorControlArray(self.exp2_Id, self.motors2,controlMode=p.TORQUE_CONTROL, forces=self.exp_torque)
    # print(self.actions)
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=torques)
    p.stepSimulation()
    if self.render:
      time.sleep(0.02)
    self.get_observation()
    if self.record_step: 
      self.record_sim_data()
    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.total_reward += reward
    self.steps += 1
    return np.array(self.joints + self.joint_vel), reward, done, None

  def get_reward(self):
    reward = 0
    done = False
    if self.steps > 2000:
      done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    exp2_jointStates = p.getJointStates(self.exp2_Id,self.ordered_joint_indices)
    self.exp2_joints = [exp2_jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp2_joint_vel = [exp2_jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    # print(self.joints, s)

  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))       
        np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))       
        self.sim_data = []
        self.exp_sim_data = []
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)
    
    if len(self.sim_data) > 100000: return
    data = [[0,1,2], [0,0,0,1]]
    data.append(self.exp_joints)
    self.exp_sim_data.append(data)

if __name__=="__main__":
  # from models.ppo import Model
  from models.ppo_error import Model

  import argparse
  import random
  import tensorboardX
  from baselines.common.mpi_util import sync_from_root

  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--ae', default=False, action='store_true')
  parser.add_argument('--do_plot', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--seed', default=42, type=int)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"

  PATH = home + '/results/hex_model/latest/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)

  writer = tensorboardX.SummaryWriter(log_dir=PATH)

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  env = Env(render=args.render, PATH=PATH, args=args)

  pol = Model("rl_model", env, ob_size=env.ob_size, ac_size=env.ac_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e7), vis=False)

  initialize()
  sync_from_root(sess, pol.vars, comm=comm) #pylint: disable=E1101
  
  # data_path = '/home/brendan/results/hex_model/latest/gz_sample/'
  data_path = '/home/brendan/results/hex_model/latest/pb_tm/'
  # data_path = home + '/data/iros_data/gz_fm_100_rand/'
  samples = np.array(pd.read_excel(data_path + 'inputs.xlsx'))[1:,:]
  time.sleep(5)
  print("Loaded reference data", samples.shape)
  sample_pointer = 0
  
  if args.test_pol:
    # pol.load(home + '/results/hex_model/latest/gz_test/')
    # pol.load(home + '/results/hex_model/latest/boobies3/')
    # pol.load(home + '/results/hex_model/latest/rl_model9/')
    pol.load(PATH)
    # pol.load(home + '/results/hex_model/latest/boobies/')
    print("Loaded weights for model")

  prev_done = True
  sample_pointer = np.random.randint(0,97000)
  # sample_pointer = 0
  ob = env.reset(samples[sample_pointer])
  assert all(ob == samples[sample_pointer][:6])
  im = np.zeros(env.im_size)
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  steps = 0
  env.total_steps = 0
  prev_torques = np.zeros(env.ac_size)
  env.reward_breakdown = {'pos':deque(maxlen=100), 'vel':deque(maxlen=100), 'neg':deque(maxlen=100)}
  plot_len = 200
  y1 = [[],[],[],[],[],[]]
  y2 = [[],[],[],[],[],[]]
  x1 = [[],[],[]]
  x2 = [[],[],[]]
  x3 = [[],[],[]]
  env.Kp = env.initial_Kp = 50
  while True:
    sample = samples[sample_pointer]
    ob = np.array(list(ob) + list(sample[6:]))
    # print(ob.shape)
    act, vpred, _, nlogp = pol.step(ob, im, stochastic=True)
    # act = sample[6:9]
    # if env.total_reward > 1000 and env.Kp != 0:
    #   env.Kp -= 1
    #   print("lowering Kp")
    # error = np.array(act) + (env.Kp/env.initial_Kp)*2*(np.array(samples[sample_pointer+1])[:3] - np.array(env.joints)) + 0.05*(np.array(samples[sample_pointer+1])[3:6] - np.array(env.joint_vel))
    # torques = error
    torques = act
    next_ob, _, done, _ = env.step(torques, sample)
    
    # print(sum((next_ob[:3] - samples[sample_pointer+1][:3])**2))
    # if not args.test_pol and (sum((next_ob[:3] - samples[sample_pointer+1][:3])**2) > 3 or abs(next_ob[1] - samples[sample_pointer+1][1]) > 1.0):
    # if not args.test_pol and (sum((next_ob[:3] - samples[sample_pointer+1][:3])**2) > 2.0 or abs(next_ob[1] - samples[sample_pointer+1][1]) > 0.75):
    if not args.test_pol and (np.any(abs(next_ob[:3] - samples[sample_pointer+1][:3]) > 0.90)):
      done = True
    pos = np.exp(-2*sum((next_ob[:3] - np.array(samples[sample_pointer+1])[:3])**2))
    vel = np.exp(-0.1*sum((next_ob[3:6] - np.array(samples[sample_pointer+1])[3:6])**2))
    # neg = -0.01*sum(abs(np.array(prev_torques)-torques)) - 0.1*sum(abs(torques))
    neg = 0
    prev_torques = torques
    # pos_errors = abs(next_ob[:3] - samples[sample_pointer+1][:3])
    # pos = np.exp(-2*sum(abs(next_ob[:3] - samples[sample_pointer+1][:3])))
    # vel = np.exp(-0.1*sum(abs(next_ob[3:6] - samples[sample_pointer+1][3:6])))
    env.reward_breakdown['pos'].append(pos)
    env.reward_breakdown['vel'].append(vel)
    env.reward_breakdown['neg'].append(neg)
    rew = pos + vel + neg
  
    if args.do_plot:
      for a in range(3):
        # y3[a].append(fm_output[a])
        x1[a].append(act[a])
        x2[a].append(sample[6:9][a])
        y1[a].append(env.joints[a])
        y1[a+env.ac_size].append(env.joint_vel[a])
        y2[a].append(samples[sample_pointer+1][a])
        y2[a+env.ac_size].append(samples[sample_pointer+1][a+env.ac_size])
        
      if len(x1[0]) > plot_len:
        print("MSE joints", np.mean((np.array(y1)[:,:3]- np.array(y2)[:,:3])**2))
        print("MSE joint vel", np.mean((np.array(y1)[:,3:]- np.array(y2)[:,3:])**2))
        subplot([[x1[0],x2[0]],[x1[1],x2[1]],[x1[2],x2[2]]], legend=[["pred", "real"]]*3)
        subplot([[y1[0],y2[0]],[y1[1],y2[1]],[y1[2],y2[2]],[y1[3],y2[3]],[y1[4],y2[4]],[y1[5],y2[5]]], legend=[["pred", "real"]]*6)


    next_im = np.zeros(env.im_size)
    pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
    prev_done = done
    ob = next_ob
    steps += 1
    ep_ret += rew
    ep_len += 1
    env.total_steps += 1
    sample_pointer += 1

    if not args.test_pol and steps % horizon == 0:
      temp_ob = np.array(list(next_ob) + list(samples[sample_pointer][6:]))
      _, vpred, _, _ = pol.step(temp_ob, next_im, stochastic=True)
      pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
      pol.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
    if done:
      # print("reseting", done, steps, horizon)
      # sample_pointer = 0
      sample_pointer = np.random.randint(0,97000)
      ob = env.reset(samples[sample_pointer])
      assert all(ob == samples[sample_pointer][:6])
      # exit()
      im = np.zeros(env.im_size)
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0