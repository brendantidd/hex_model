# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import pybullet as p
import numpy as np
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD

class Env():
  rank = comm.Get_rank()
  ob_size = 7
  ac_size = 3
  im_size = [48,48,4]
  simtimeStep = 1/200
  # simtimeStep = 1/60
  actionRepeat = 1
  rew_buffer = deque(maxlen=5)
  episodes = -1
  Kp = 20
  initial_Kp = 20
  total_reward = 0
  def __init__(self, render=False, PATH=None, args=None, horizon=500, display_expert=True, record_step=True, cur=False, test=False, use_expert=False, act_model=None, sim_model=None):
    self.vis_exp = True
    self.act_model = act_model
    self.args = args
    # if self.act_model is not None:
    #   self.rnn_length = self.act_model.rnn_length
    self.sim_model = sim_model
    self.model_breakdown = True
    # self.model_breakdown = False
    self.render = render
    self.PATH = PATH
    self.display_expert = display_expert
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.use_expert = use_expert
    # self.all_legs = True
    self.all_legs = False

    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot

    self.load_model()
    self.sim_data = []
    self.inputs = []
    self.labels = []
    self.rew_buffer = deque(maxlen=5)

  def load_model(self):
    p.loadMJCF("./assets/ground.xml")
    objs = p.loadURDF("./assets/new_urdf.urdf")
    self.Id = objs
    objs = p.loadURDF("./assets/new_urdf.urdf")
    self.exp1_Id = objs
    objs = p.loadURDF("./assets/new_urdf.urdf")
    self.exp2_Id = objs
    objs = p.loadURDF("./assets/new_urdf.urdf")
    self.exp3_Id = objs
    objs = p.loadURDF("./assets/new_urdf.urdf")
    self.exp4_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)

    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    # self.jdict2 = {}
    # self.ordered_joints2 = []
    # self.ordered_joint_indices2 = []
    # numJoints = p.getNumJoints(self.exp2_Id)
    # for j in range( p.getNumJoints(self.exp2_Id) ):
    #   info = p.getJointInfo(self.exp2_Id, j)
    #   link_name = info[12].decode("ascii")
    #   if link_name == 'BR_foot_link': self.foot_link = j
    #   self.ordered_joint_indices2.append(j)
    #   if info[2] != p.JOINT_REVOLUTE: continue
    #   jname = info[1].decode("ascii")
    #   lower, upper = (info[8], info[9])
    #   self.ordered_joints2.append( (j, lower, upper) )
    #   self.jdict2[jname] = j
    # self.jdict4 = {}
    # self.ordered_joints4 = []
    # self.ordered_joint_indices4 = []
    # for j in range( p.getNumJoints(self.exp4_Id) ):
    #   info = p.getJointInfo(self.exp4_Id, j)
    #   link_name = info[12].decode("ascii")
    #   if link_name == 'BR_foot_link': self.foot_link = j
    #   self.ordered_joint_indices4.append(j)
    #   if info[2] != p.JOINT_REVOLUTE: continue
    #   jname = info[1].decode("ascii")
    #   lower, upper = (info[8], info[9])
    #   self.ordered_joints4.append( (j, lower, upper) )
    #   self.jdict4[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]
    # self.motors2 = [self.jdict2[n] for n in self.motor_names]
    # self.motors4 = [self.jdict4[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    # self.motor_power =  [5, 10, 5]
    # self.vel_max = [8,11,8]
    self.vel_max = [8,8,8]
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}
    
    if not self.args.pos_id:
      p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
      p.setJointMotorControlArray(self.exp4_Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp1_Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self, start_pos=None):
    self.sample_pointer = 0
    # temp = self.samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torques = temp[:3], temp[3:6], [temp[6:]]
    if start_pos is not None:
      self.exp_joints, self.exp_joint_vel = start_pos[:3], start_pos[3:6]
    else:
      self.exp_joints, self.exp_joint_vel, self.exp_torques = [0,0,1.5], [0,0,0], [0,0,0]
    self.coxa, self.femur, self.tibia = self.exp_joints
    self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel)
    self.set_position([0,1,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, robot_id=self.exp1_Id)
    self.set_position([0,2,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, robot_id=self.exp2_Id)
    self.set_position([0,3,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, robot_id=self.exp3_Id)
    self.set_position([0,4,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, robot_id=self.exp4_Id)
    self.input_buffers = {n:{m:deque([0.0]*3, maxlen=3) for m in ['coxa','femur','tibia']} for n in ['torq', 'vel']}
    
    self.pos_error_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.vel_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.label_torques = {n:0.0 for n in ['coxa','femur','tibia']}
    self.desired_torque_buf = deque([0.0]*3*5,maxlen=5*3)
    self.prev_state_buf = deque([0.0]*6*5,maxlen=5*6)

    self.speed = -1.0
    self.step_length = 0.5
    # Number of timesteps per step
    self.episodes += 1
    self.rew_buffer.append(self.total_reward)
    # if self.Kp > 0 and len(self.rew_buffer) > 3 and np.all(np.array(self.total_reward) > 6400):
    # if self.Kp > 0 and len(self.rew_buffer) > 3 and np.all(np.array(self.total_reward) > 5800):
    if self.Kp > 0 and len(self.rew_buffer) > 4:
      self.Kp -= 1
      self.rew_buffer = deque(maxlen=5)
    self.total_reward = 0
    self.steps = 0
    self.get_observation()

    self.prev_state = self.joints + self.joint_vel

      # if self.episodes > 1:
      #   exit()
    self.desired_torque = [0,0,0]
    self.prev_actions = [0.0,0.0,0.0]
    # return np.array(self.desired_torque + self.exp_joints + self.exp_joint_vel+list(self.history))
    # return np.array(self.desired_torque + self.joints + self.joint_vel)
    return np.array(self.joints + self.joint_vel + [self.speed])

  def step(self, actions, exp_actions, id_torques, fm_actions):
    self.exp_joints, self.exp_joint_vel, self.exp_torque = exp_actions[:3], exp_actions[3:6], exp_torques
    # exp_torques = (self.Kp/self.initial_Kp)*(80*(np.array(self.exp_joints)-np.array(self.joints)) - 0.1*np.array(self.joint_vel))
    # torques = exp_torques
    # torques = [np.clip(e + a,-tm,tm) for e,a,tm in zip(exp_torques, torques, self.tor_max)]
    self.input_torque = self.actions = actions
    self.prev_state = self.joints + self.joint_vel
    # if not self.args.pos_id:
      # p.setJointMotorControlArray(self.exp2_Id, self.motors2,controlMode=p.TORQUE_CONTROL, forces=id_torques)
    # else:
    #   # print(actions.shape, id_torques.shape)
    #   p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=self.input_torque[:3])
      # p.setJointMotorControlArray(self.exp2_Id, self.motors2,controlMode=p.POSITION_CONTROL, targetPositions=id_torques[:3])
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque)
    p.setJointMotorControlArray(self.exp1_Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=id_torques[:3])
    self.set_position([0,2,2], [0,0,0,1], fm_actions[:3], joint_vel=fm_actions[3:6], robot_id=self.exp2_Id)
    self.set_position([0,3,2], [0,0,0,1], self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp3_Id)
    p.setJointMotorControlArray(self.exp4_Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.exp_torque)

    # print(self.actions)
    p.stepSimulation()
    if self.render:
      time.sleep(0.005)
    self.get_observation()
    if self.record_step: 
      self.record_sim_data()
    if self.steps == self.sample_size - 1 and self.record_step:
    # if self.steps == 84000 and self.record_step:
      self.save_sim_data()
    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.total_reward += reward
    self.steps += 1

    # self.sample_pointer += 1
    # temp = self.samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torques = temp[:3], temp[3:6], temp[6:]

    # print(self.steps)
    return np.array(self.joints + self.joint_vel + [self.speed]), reward, done, None

  def get_reward(self):
    reward = 0
    done = False
    # if self.steps == 89990:
    # if self.steps == 10000:
    # if self.steps == 500:
      # done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    exp1_jointStates = p.getJointStates(self.exp2_Id,self.ordered_joint_indices)
    self.exp1_joints = [exp1_jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp1_joint_vel = [exp1_jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    exp2_jointStates = p.getJointStates(self.exp2_Id,self.ordered_joint_indices)
    self.exp2_joints = [exp2_jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp2_joint_vel = [exp2_jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    exp3_jointStates = p.getJointStates(self.exp3_Id,self.ordered_joint_indices)
    self.exp3_joints = [exp3_jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp3_joint_vel = [exp3_jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    exp4_jointStates = p.getJointStates(self.exp3_Id,self.ordered_joint_indices)
    self.exp4_joints = [exp4_jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp4_joint_vel = [exp4_jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]

  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))
        np.save(path + 'inputs.npy', np.array(self.inputs))
        np.save(path + 'labels.npy', np.array(self.labels))
        
        # print("episodes", self.episodes)
        # if self.episodes > 0:
        exit()
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    self.inputs.append(self.prev_state + self.joints + self.joint_vel)
    self.labels.append(self.input_torque)
    
    if len(self.sim_data) > 1000000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)

if __name__=="__main__":
  # Import model
  import argparse
  import tensorboardX
  from pathlib import Path
  home = str(Path.home())

  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--test_torq', default=False, action='store_true')
  parser.add_argument('--do_plot', default=False, action='store_true')
  parser.add_argument('--norm', default=False, action='store_true')
  parser.add_argument('--fm', default=True, action='store_false')
  parser.add_argument('--delta', default=False, action='store_true')
  parser.add_argument('--norm_fm', default=True, action='store_false')
  parser.add_argument('--nn_fm', default=False, action='store_true')
  parser.add_argument('--nn_id', default=False, action='store_true')
  parser.add_argument('--pos_id', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)

  # if args.fm:
  input_size = 9
  output_size = 6
  if args.nn_fm:
    from models import simple_nn
    forward_model = simple_nn.NN(name='forward_model', input_size=input_size, output_size=output_size, sess=sess)
  else:
    from models import simple_rnn
    lstm_size = 128
    dense_size = 128
    forward_model = simple_rnn.RNN(name='forward_model', input_size=input_size, output_size=output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess, delta=args.delta, norm=args.norm_fm)
  
  
  seq_length = 128
  input_size = 12
  output_size = 3
  lstm_size = 128
  dense_size = 128
  name = 'id_model'
  data = tf.placeholder(tf.float32, [None, seq_length, input_size])
  target = tf.placeholder(tf.float32, [None, seq_length, output_size])
  if not args.pos_id:
    if args.nn_id:
      from models import id_nn
      id_model = id_nn.NN(name='id_model', input_size=input_size, output_size=output_size, dense_size=dense_size, sess=sess)
    else:
      from models import id_rnn
      id_model = id_rnn.RNN(name='id_model', input_size=input_size, output_size=output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess)
  
  sess.run(tf.initialize_all_variables())
  # ===================================================================================================
  # Load weights
  # ===================================================================================================
  if args.fm:
    # forward_model.load(home + '/data/iros_data/real_29_1/double/weights_/best/')
    # forward_model.load(home + '/data/iros_data/real/mine/weights_/best/')
    # forward_model.load(home + '/data/iros_data/real/all/weights_/best/')
    # forward_model.load(home + '/data/iros_data/real/3_2/weights_/best/')
    # forward_model.load(home + '/data/iros_data/real/3_2/all/weights_/best/')
    forward_model.load(home + '/data/iros_data/real/8_2/weights_/best/')
    # forward_model.load(home + '/data/iros_data/real/toms/300/weights_/best/')
    # forward_model.load(home + '/data/iros_data/real/300/weights_/best/')
    print(forward_model.mean.eval(), forward_model.std.eval(), forward_model.scalar.eval())
  if not args.pos_id:
    id_model.load(home + '/data/iros_data/pb/weights_/best/')
    # id_model.load(home + '/data/iros_data/pb_500/rnn/weights_norm_output/')
    # id_model.load(home + '/data/iros_data/pb_400/toms_ft/weights_/best/')
    # id_model.load(home + '/data/iros_data/pb_400/weights_/best/')
    # id_model.load(home + '/data/iros_data/pb_400/my_ft/weights_/')
    print(id_model.mean.eval(), id_model.std.eval(), id_model.label_mean.eval(), id_model.label_std.eval())
    # ===================================================================================================

  # Load real data to use as reference trajectory
  samples = np.load(home + '/data/06_02_2020/baseline/inputs.npy')[:,:]
  torque_samples = np.load(home + '/data/06_02_2020/baseline/observed_torques.npy')[:,:]
  # samples = np.load(home + '/data/iros_data/real/toms/300/inputs.npy').reshape(-1,9)
  # samples = np.load('samples/samples.npy')

  torque_samples = samples[:,6:]

  sample_size = samples.shape[0]
  # sample_size = 200
  print("Loaded reference data", samples.shape)
  initial_pointer = 0
  sample_pointer = initial_pointer
  save_path = '/home/brendan/data/joint_test'

  inputs = []
  labels = []
  input_buf = deque([[0.0]*input_size]*seq_length,maxlen=seq_length)

  PATH = home + '/results/hex_model/latest/' + args.exp + '/'
  # print("saving to ", PATH)
  writer = tensorboardX.SummaryWriter(log_dir=PATH)

  env = Env(render=args.render, PATH=PATH, args=args, record_step=False)

  # Run, options: ID model, torque model
  env.reset(samples[sample_pointer])
  env.sample_size = sample_size

  fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
  fm_state1 = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
  id_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]   
  id_state1 = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]   
  y1 = [[],[],[],[],[],[]]
  y2 = [[],[],[],[],[],[]]
  y3 = [[],[],[],[],[],[]]
  y4 = [[],[],[],[],[],[]]
  x1 = [[],[],[]]
  x2 = [[],[],[]]
  x3 = [[],[],[]]
  while True:
    sample = samples[sample_pointer][:6]
    exp_torques = torque_samples[sample_pointer]
    # exp_torques = samples[sample_pointer][6:]
    if args.fm:
      if args.nn_fm:
        fm_output1 = forward_model.step(np.array(env.exp3_joints + env.exp3_joint_vel + list(exp_torques)))
        fm_output = forward_model.step(np.array(env.joints + env.joint_vel + list(exp_torques)))
      else:
        fm_output1, fm_state1 = forward_model.step(np.array(env.exp2_joints + env.exp2_joint_vel + list(exp_torques)), fm_state1)
        fm_output, fm_state = forward_model.step(np.array(env.joints + env.joint_vel + list(exp_torques)), fm_state)

      if not args.pos_id:
        if args.nn_id:
          id_torques = id_model.step(np.array(list(samples[sample_pointer][:6]) + list(samples[sample_pointer+1][:6])))
          actions = id_model.step(np.array(env.joints + env.joint_vel + list(fm_output)))
        else:
          id_torques, id_state1 = id_model.step(np.array(list(samples[sample_pointer][:6]) + list(samples[sample_pointer+1][:6])), id_state1)
          actions, id_state = id_model.step(np.array(env.joints + env.joint_vel + list(fm_output)), id_state)
          # print(id_torques)
        if args.do_plot:
          for a in range(3):
            # y3[a].append(fm_output[a])
            x1[a].append(actions[a])
            x2[a].append(exp_torques[a])
            x3[a].append(id_torques[a])
          if sample_pointer > 200+initial_pointer:
            subplot([[x1[0],x2[0],x3[0]],[x1[1],x2[1],x3[0]],[x1[2],x2[2],x3[0]]], legend=[["fm into id", "expert torque", "id torque"]]*3)
            subplot([[y1[0],y2[0],y3[0],y4[0]],[y1[1],y2[1],y3[1],y4[1]],[y1[2],y2[2],y3[2],y4[2]],[y1[3],y2[3],y3[3],y4[3]],[y1[4],y2[4],y3[4],y4[4]],[y1[5],y2[5],y3[5],y4[5]]], legend=[["fm", "observed next state", "fm into id", "expert next state"]]*6)
          
    elif not args.test_torq:
      input_buf.append(sample)
      actions = model.step([input_buf])[0,-1,:]
    else:
      if args.norm:
        model_inputs = standardise(np.array(env.joints + env.joint_vel + list(exp_torques)), input_mean, input_std)
      else:
        model_inputs = np.array(env.joints + env.joint_vel + list(exp_torques))
      input_buf.append(model_inputs)
      actions = model.step([input_buf])[0,-1,:]
      
    if args.fm:
      if args.pos_id:
        _,_,done,_ = env.step(fm_output, samples[sample_pointer], list(samples[sample_pointer+1][:6]), fm_output1)
      else:
        # print(id_torques)
        _,_,done,_ = env.step(actions, samples[sample_pointer], id_torques, fm_output1)
    else:
      _,_,done,_ = env.step(actions, samples[sample_pointer], exp_torques)
    
    if args.do_plot:
      observed_joints = env.joints + env.joint_vel
      des_joints = samples[sample_pointer+1]
      for a in range(3):
        y1[a].append(fm_output1[a])
        y2[a].append(observed_joints[a])
        y3[a].append(fm_output[a])
        y4[a].append(des_joints[a])
        y1[a+3].append(fm_output1[a+3])
        y2[a+3].append(observed_joints[a+3])
        y3[a+3].append(fm_output[a+3])
        y4[a+3].append(des_joints[a+3])

    if done:
      sample_pointer = initial_pointer
      env.reset(samples[sample_pointer])
      fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
      fm_state1 = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
      id_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]   
      id_state1 = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]   
    inputs.append(list(sample[:6]) + list(exp_torques))
    if not args.pos_id:
      labels.append(actions)
    # sample_pointer += 4
    sample_pointer += 1
    if sample_pointer > sample_size - 2:
      sample_pointer = 0
    if sample_pointer % 1000 == 0:
      print(env.steps, sample_pointer, samples.shape[0])