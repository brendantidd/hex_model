import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import pybullet as p
import numpy as np
import time
import tensorflow as tf
from tensorflow import nn
from scripts.change_urdf import ChangeURDF

class Env():
  timeStep = 1/200
  ac_size = 3
  def __init__(self, args=None, rand_dynamics=True):
    self.rand_dynamics = rand_dynamics
    self.args = args
    if self.args.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT)
    self.change_urdf = ChangeURDF('assets/test_leg.urdf', debug=False, normal=False)
    self.episodes = 0
    if not rand_dynamics:
      self.load()
  
  def load(self):
    p.resetSimulation()
    p.loadMJCF("assets/ground.xml")
    
    if self.rand_dynamics:
      urdf_path = "assets/temp_urdf_" + self.args.exp + "_" + str(self.episodes) + ".urdf"
      self.change_urdf.write_new_dynamics(urdf_path)
      self.Id = p.loadURDF(urdf_path)
      print("loading urdf from ", urdf_path, [p.getDynamicsInfo(self.Id, i)[0] for i in [1,2,3]])
      os.remove(urdf_path)
    else:
      objs = p.loadURDF("assets/test_leg.urdf")
      self.Id = objs

    # objs = p.loadURDF("/home/brendan/results/hex_model/latest/new_urdf_std/new_urdf.urdf")
    objs = p.loadURDF("assets/test_leg.urdf")
    self.exp_Id = objs
    # if self.args.cmd:
    #   objs = p.loadURDF("assets/new_urdf.urdf")
    #   self.Id = objs
    #   objs = p.loadURDF("assets/new_urdf.urdf")
    #   self.exp_Id = objs
    # else:
    #   objs = p.loadURDF("assets/obs_torque_new_urdf.urdf")
    #   self.Id = objs
    #   objs = p.loadURDF("assets/obs_torque_new_urdf.urdf")
    #   self.exp_Id = objs
    
    self.episodes += 1
    p.setTimeStep(self.timeStep)
    p.setGravity(0,0,-9.8)
    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    self.joint_links = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      # exp_info = p.getJointInfo(self.exp_Id, j)
      link_name = info[12].decode("ascii")
      # exp_link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      self.joint_links.append(j)
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    self.tor_max = np.array([10,10,3])

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]

    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

  def reset(self, initial_position=None, initial_vel=None):
    if self.rand_dynamics:
      self.load()
    self.steps = 0
    self.set_position([0,0,2], [0,0,0,1], joints=initial_position, joint_vel=initial_vel)
    self.set_position([0,1,2], [0,0,0,1], joints=initial_position, joint_vel=initial_vel, robot_id=self.exp_Id)
    self.get_observation()
    self.initial_position = self.joints
    return self.joints, self.joint_vel

  def step(self, actions, desired_joints):
    self.set_position([0,1,2], [0,0,0,1], desired_joints, robot_id=self.exp_Id)
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=actions)
    p.stepSimulation()
    self.get_observation()
    self.steps += 1
    if self.args.render:
      time.sleep(0.005)
    reward, done = 0, False
    # if not self.args.test_pol and self.steps > 255:
    #   done = True
    return self.joints, self.joint_vel, reward, done

  def get_observation(self):
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]))

  def set_position(self, pos=[0,0,2], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def play(self, input_torque, input_pos, initial_position):
    for _ in range(10):
      self.reset(initial_position)
      for torque, pos in zip(input_torque, input_pos):
        self.step(torque, pos)

class ResidualNet():
  def __init__(self, name='residual', input_size=1, output_size=1, lstm_size=128, dense_size=128, sess=None):
    self.name = name
    self.input_size = input_size
    self.output_size = output_size
    self.lstm_size = lstm_size
    self.dense_size = dense_size
    self.sess = sess
    self.inputs = tf.placeholder(tf.float32, [None, None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, None, output_size])
    self.keep_prob = tf.placeholder_with_default(1.0, shape=())
    self.train = tf.placeholder_with_default(True, shape=())
    self.batch_size = tf.placeholder(tf.int32, [None])
    self.initialise()
    
  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'residual.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'residual.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for residual net")
    
  def model(self):
    # activation = tf.nn.tanh
    activation = tf.nn.softsign
    # activation = tf.nn.relu
    last_out = (self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    self.cell = nn.rnn_cell.LSTMCell(self.lstm_size, name="r1")
    self.batch_state_in = self.cell.zero_state(self.batch_size, tf.float32) 
    last_out, self.states = nn.dynamic_rnn(self.cell,last_out,dtype=tf.float32, initial_state=self.batch_state_in)  
    last_out = tf.nn.dropout(last_out, self.keep_prob)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    self.outputs = tf.layers.dense(last_out, self.output_size)
    self.scaled_outputs = (self.outputs*tf.stop_gradient(self.output_std[:self.output_size])) + tf.stop_gradient(self.output_mean[:self.output_size])
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, tf.stop_gradient((self.labels - self.output_mean[:self.output_size])/self.output_std[:self.output_size])))
    optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
    self.optimise =  optimizer.minimize(self.loss)

  def initialise(self):
    with tf.variable_scope(self.name):
      self.mean = tf.get_variable('mean', initializer=np.zeros(self.input_size, dtype=np.float32), dtype=tf.float32)
      self.std = tf.get_variable('std', initializer=np.ones(self.input_size, dtype=np.float32), dtype=tf.float32)
      self.output_mean = tf.get_variable('output_mean', initializer=np.zeros(self.output_size, dtype=np.float32), dtype=tf.float32)
      self.output_std = tf.get_variable('output_std', initializer=np.ones(self.output_size, dtype=np.float32), dtype=tf.float32)
      self.model()
    vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)
    # print(self.vars)

  def step(self, X, state=None):
    if state is None:
      state = [np.zeros([1,self.lstm_size]),np.zeros([1,self.lstm_size])]
    output, state = self.sess.run([self.scaled_outputs, self.states],feed_dict={self.inputs:X.reshape(1,1,self.input_size), self.batch_state_in:state,self.batch_size:[1]})
    return output[0][0], state

if __name__=="__main__":
  import os,sys,inspect
  currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
  parentdir = os.path.dirname(currentdir)
  sys.path.insert(0,parentdir) 
  from pathlib import Path
  from scripts.utils import *
  home = str(Path.home())
  import argparse

  import tensorboardX
  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--get_data', default=False, action='store_true')
  parser.add_argument('--save', default=False, action='store_true')
  parser.add_argument('--cmd', default=False, action='store_true')
  parser.add_argument('--rand', default=False, action='store_true')
  parser.add_argument('--exp', default='')
  args = parser.parse_args()

  if args.get_data:
    env = Env(args, rand_dynamics=args.rand)
    env.reset()
    data_path = home + '/data/iros_data/real/10_2/'
    if args.rand:
      save_path = home + '/data/iros_data/real/10_2/residual/rand/'
    else:
      save_path = home + '/data/iros_data/real/10_2/residual/'
    inputs = np.load(data_path + 'inputs.npy')
    # if args.cmd:
    #   torques = inputs[:,6:9]
    # else:
    torques = np.load(data_path + 'observed_torques.npy')
    dones = np.load(data_path + 'dones.npy')
    labels = np.load(data_path + 'labels.npy')

    residual_inputs = []
    residual_labels = []
    residual_dones = []
    residual_real_labels = []
    if args.rand:
      epochs = 5
    else:
      epochs = 1
    for _ in range(epochs):
      for i in range(inputs.shape[0]):
        
        target_joints, target_joint_vel, target_torque = inputs[i,:3], inputs[i,3:6], torques[i,:]
        if dones[i]:
          # print("done")
          env.reset(target_joints, target_joint_vel)
        else:
          for j, jv, m in zip(target_joints, target_joint_vel, env.motors):
            p.setJointMotorControl2(env.Id, m,controlMode=p.POSITION_CONTROL, targetPosition=j, targetVelocity=jv)
          # Need to reset the joint to use torque control at the next timestep
          p.setJointMotorControlArray(env.Id, env.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(env.motor_names))
        env.step(target_torque, target_joints)
        next_state = env.joints + env.joint_vel
        residual_inputs.append(list(inputs[i,:3]) + list(inputs[i,3:6]) + list(torques[i,:]))
        residual_dones.append(dones[i])
        residual_labels.append(next_state)
        residual_real_labels.append(labels[i])

        if i % 1000 == 0:
          print(i)
    # dones = np.load(data_path + 'dones.npy')
    # residual_labels = np.array(residual_labels)
    # print(labels.shape, residual_labels.shape)
    residual_labels = np.array(residual_labels)
    residual_real_labels = np.array(residual_real_labels)
    
    residual_labels = residual_real_labels - residual_labels
    print(residual_labels.shape)
    # np.save(save_path + 'inputs.npy',  np.array(inputs))
    # np.save(save_path + 'observed_torques.npy',  np.array(torques))
    # np.save(save_path + 'labels.npy',  np.array(labels))
    # np.save(save_path + 'dones.npy',  np.array(dones))
    # if args.cmd:
    #   np.save(save_path + 'residual_cmd_labels.npy',  np.array(residual_labels))
    # else:
    if args.save:
      np.save(save_path + 'residual_labels.npy',  np.array(residual_labels))
      np.save(save_path + 'residual_inputs.npy',  np.array(residual_inputs))
      np.save(save_path + 'residual_real_labels.npy',  np.array(residual_real_labels))
      np.save(save_path + 'residual_dones.npy',  np.array(residual_dones))

    i, j = 0, 5000
    source = residual_real_labels[i:j,:] - residual_labels[i:j,:]
    target = residual_real_labels[i:j,:]
    # TODO this is a good way to visualise the sim to real gap, and modifying the urdf is helping.
    subplot([[target[:,0],source[:,0]],[target[:,1],source[:,1]],[target[:,2],source[:,2]],[target[:,3],source[:,3]],[target[:,4],source[:,4]],[target[:,5],source[:,5]]], legend=[['coxa_pos', 'sim'],['femur_pos', 'sim'],['tibia_pos', 'sim'],['coxa_vel', 'sim'],['femur_vel', 'sim'],['tibia_vel', 'sim']], PATH=save_path + 'residual_')

  if not args.get_data:
    args.exp = "_" + args.exp

    if args.rand:
      data_path = home + '/data/iros_data/real/10_2/residual/rand/'
      batch_size = 512
    else:
      data_path = home + '/data/iros_data/real/10_2/residual/'
      batch_size = 256

    seq_length = 256
    epochs = 20000
    best_val_loss = 10000
    lstm_size = 128
    
    if args.cmd:
      args.exp += 'cmd_'
    WEIGHTS_PATH = data_path + 'weights' + args.exp + 'residual_/'
    print("SAVING TO", WEIGHTS_PATH)

    writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.2)
    sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

    rnn = ResidualNet(input_size=9, output_size=6, sess=sess, lstm_size=lstm_size)
    sess.run(tf.global_variables_initializer())

 
    inputs = np.load(data_path + 'residual_inputs.npy')
    residual_labels = np.load(data_path + 'residual_labels.npy')  

    # if args.cmd:
    #   residual_labels = np.load(data_path + 'residual_cmd_labels.npy')
    # else:
    #   residual_labels = np.load(data_path + 'residual_labels.npy')  
    #   torques = np.load(data_path + 'observed_torques.npy')
    #   inputs[:,6:9] = torques

    labels = np.load(data_path + 'residual_real_labels.npy')
    dones = np.load(data_path + 'residual_dones.npy')
  

    input_mean, input_std = np.mean(inputs, axis=0, dtype=np.float32), np.std(inputs, axis=0, dtype=np.float32)
    output_mean, output_std = np.mean(residual_labels, axis=0, dtype=np.float32), np.std(residual_labels, axis=0, dtype=np.float32)
    sess.run([rnn.mean.assign(input_mean), rnn.std.assign(input_std), rnn.output_mean.assign(output_mean), rnn.output_std.assign(output_std)])
    
    # dim = (inputs.shape[0]//seq_length)*seq_length
    # new_inputs = np.zeros([dim//seq_length, seq_length, inputs.shape[1]])
    # new_labels = np.zeros([dim//seq_length, seq_length, labels.shape[1]])
    # if args.stand:
    #   new_dones = np.zeros([dim//seq_length, seq_length, 1])
    # print(inputs.shape, labels.shape, dim, new_inputs.shape, new_labels.shape)
   
    idxs = list(np.where(dones==1.0)[0]) + [dones.shape[0]-1] 
    new_inputs = []
    new_labels = []  
    new_residual_labels = []  
    lost_data = 0
    for idx in range(len(idxs)-1): 
      dim = ((idxs[idx+1]-idxs[idx])//seq_length)*seq_length
      lost_data += (idxs[idx+1]-idxs[idx] - dim)
      for i in range(0,dim//seq_length):
        new_inputs.append(inputs[idxs[idx]+i*seq_length:idxs[idx]+i*seq_length+seq_length,:])
        new_labels.append(labels[idxs[idx]+i*seq_length:idxs[idx]+i*seq_length+seq_length,:])
        new_residual_labels.append(residual_labels[idxs[idx]+i*seq_length:idxs[idx]+i*seq_length+seq_length,:])
    inputs = np.array(new_inputs)
    labels = np.array(new_labels)
    residual_labels = np.array(new_residual_labels)

    print(inputs.shape, labels.shape, lost_data)

    idx = [i for i in range(inputs.shape[0])]
    np.random.shuffle(idx)

    train_inputs, test_inputs = inputs[idx[:int(len(idx)*0.9)],:,:],inputs[idx[int(len(idx)*0.9):],:,:] 
    train_labels, test_labels = labels[idx[:int(len(idx)*0.9)],:,:],labels[idx[int(len(idx)*0.9):],:,:] 
    train_residual_labels, test_residual_labels = residual_labels[idx[:int(len(idx)*0.9)],:,:],residual_labels[idx[int(len(idx)*0.9):],:,:] 
    
    batch_size = min(batch_size, train_inputs.shape[0])

    for e in range(epochs):

      idx = [i for i in range(train_inputs.shape[0])]
      np.random.shuffle(idx)

      t1 = time.time()
      epoch_loss = []
      for batch in range(train_inputs.shape[0]//batch_size):
        b_idx = batch*batch_size
        batch_x, batch_y = train_inputs[idx[b_idx:b_idx+batch_size],:,:], train_residual_labels[idx[b_idx:b_idx+batch_size],:,:]
        
        states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      
        states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      
        _, loss, train_states = sess.run([rnn.optimise, rnn.loss, rnn.states],feed_dict={rnn.inputs:batch_x, rnn.labels:batch_y, rnn.batch_state_in: states,rnn.batch_size:[batch_size], rnn.keep_prob:0.75})
        epoch_loss.append(loss)

      val_batch_size = test_inputs.shape[0]
      # Get open loop predictions
      val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
      predictions, val_loss = sess.run([rnn.scaled_outputs,rnn.loss],feed_dict={rnn.inputs:test_inputs, rnn.labels:test_residual_labels, rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
      preds = test_labels + predictions
      
      # -----------------------------------------------------------
      # Display epoch losses. Save best weights, periodically plot things
      # -----------------------------------------------------------
      print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
      writer.add_scalar("error", np.mean(epoch_loss), e)
      writer.add_scalar("val error", np.mean(val_loss), e)
      if np.mean(val_loss) < best_val_loss and e > 200:
        rnn.save(WEIGHTS_PATH + '/best/')
        val_idx = np.random.randint(0,test_labels.shape[0]-1)
        best_val_loss = np.mean(val_loss)
        y1,y2,y3,y4,y5,y6 = test_labels[val_idx,:,0],test_labels[val_idx,:,1],test_labels[val_idx,:,2],test_labels[val_idx,:,3],test_labels[val_idx,:,4],test_labels[val_idx,:,5]
        k1,k2,k3,k4,k5,k6  = preds[val_idx,:,0],preds[val_idx,:,1],preds[val_idx,:,2],preds[val_idx,:,3],preds[val_idx,:,4],preds[val_idx,:,5]
        subplot([[y1,k1],[y2,k2],[y3,k3],[y4,k4],[y5,k5],[y6,k6]], legend=[['target','predictions']]*6,PATH=WEIGHTS_PATH + 'best_')

      if e % 5 == 0:
        rnn.save(WEIGHTS_PATH)      
      if e % 20 == 0:
        val_idx = np.random.randint(0,test_labels.shape[0]-1)
        y1,y2,y3,y4,y5,y6 = test_labels[val_idx,:,0],test_labels[val_idx,:,1],test_labels[val_idx,:,2],test_labels[val_idx,:,3],test_labels[val_idx,:,4],test_labels[val_idx,:,5]
        k1,k2,k3,k4,k5,k6 = preds[val_idx,:,0],preds[val_idx,:,1],preds[val_idx,:,2],preds[val_idx,:,3],preds[val_idx,:,4],preds[val_idx,:,5]
        subplot([[y1,k1],[y2,k2],[y3,k3],[y4,k4],[y5,k5],[y6,k6]], legend=[['target','predictions']]*6,PATH=WEIGHTS_PATH)
        
