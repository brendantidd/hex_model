import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from utils import *


def ForwardModel(input_size, output_size):

  model = keras.Sequential([
      keras.layers.Flatten(input_shape=([input_size])),
      keras.layers.Dense(32, activation='softsign'),
      keras.layers.Dense(32, activation='softsign'),
      keras.layers.Dense(32, activation='softsign'),
      keras.layers.Dense(output_size)
  ])
  adam = keras.optimizers.Adam(learning_rate=0.0001, beta_1=0.9, beta_2=0.999, amsgrad=False)
  model.compile(optimizer=adam,
                loss='mse',
                metrics=['mae', 'mse'])

  model.summary()
  return model

def ForwardRNNModel(input_size, output_size, seq_length=1, batch_size=1, stateful=True):
  model = keras.Sequential([
      # keras.layers.Dense(input_shape=([32,input_size])),
      # keras.layers.LSTM(100, input_shape=([32, input_size]), return_sequences=True),
      # keras.layers.Dropout(0.2),
      # keras.layers.LSTM(100, input_shape=([32, input_size]),return_sequences=True),
      # keras.layers.CuDNNLSTM(100, input_shape=([32, input_size]),return_sequences=True),
      keras.layers.CuDNNLSTM(100, input_shape=([seq_length, input_size]),batch_size=batch_size, stateful=stateful),
      # keras.layers.Dropout(0.2),
      # keras.layers.CuDNNLSTM(100),
      # keras.layers.LSTM(100),
      # keras.layers.Dropout(0.2),
      keras.layers.Dense(seq_length*output_size),
      keras.layers.Reshape((seq_length, output_size))
  ])
  rms_prop = keras.optimizers.RMSprop(learning_rate=0.001)
  model.compile(optimizer=rms_prop,
                loss='mse',
                metrics=['mae', 'mse'])

  model.summary()
  return model

class plotCallback(tf.keras.callbacks.Callback): 
  def __init__(self, PATH, inputs, labels, writer, args, means_stds):
    # super(plotCallback.__init__())
    self.PATH = PATH
    self.inputs = inputs
    self.labels = labels
    self.writer = writer
    self.args = args
    # self.input_mean = means_stds[0]
    # self.input_std = means_stds[1]
    # self.label_mean = means_stds[2]
    # self.label_std = means_stds[3]

  def on_epoch_end(self, epoch, logs=None):
  # def on_epoch_begin(self, epoch, logs=None):
    if epoch % 5 == 0:
      num = 500

      predictions = self.model.predict(self.inputs)
      labels = self.labels
      x1 = predictions[:num,0]
      y1 = labels[:num,0]
      fig = plt.figure(figsize=(20, 15))
      xs = [_ for _ in range(num)]
      # plt.plot(xs, x1,xs, y1, xs, self.inputs[:num,6], alpha=1.0)   
      plt.plot(xs, x1,xs, y1, alpha=1.0)   
      plt.savefig(self.PATH + 'coxa.png', bbox_inches='tight')
      # plt.plot([_ for _ in range(num)], x1,[_ for _ in range(num)], self.inputs[:num,6], alpha=1.0)   
      # plt.savefig(self.PATH + 'real.png', bbox_inches='tight')
      # labels = self.labels
      # x1 = predictions[10000:10000+num,0]
      # y1 = labels[10000:10000+num,0]
      # fig = plt.figure(figsize=(30, 15))
      # plt.plot([_ for _ in range(num)], x1,[_ for _ in range(num)], y1, alpha=1.0)   
      # plt.savefig(self.PATH + 'femur.png', bbox_inches='tight')
      # labels = self.labels
      # x1 = predictions[20000:20000+num,0]
      # y1 = labels[20000:20000+num,0]
      # fig = plt.figure(figsize=(30, 15))
      # plt.plot([_ for _ in range(num)], x1,[_ for _ in range(num)], y1, alpha=1.0)   
      # plt.savefig(self.PATH + 'tibia.png', bbox_inches='tight')



    writer.add_scalar("error", logs['mean_absolute_error'], epoch)
    writer.add_scalar("val error", logs['val_mean_absolute_error'], epoch)

    # print('The average loss for epoch {} is {:7.2f} and mean absolute error is {:7.2f}.'.format(epoch, logs['loss'], logs['mae']))

if __name__=="__main__":
  import matplotlib.pyplot as plt
  from pathlib import Path
  import tensorboardX
  import argparse
  from tensorflow.keras.callbacks import ModelCheckpoint

  parser = argparse.ArgumentParser()
  parser.add_argument('--my', default=False, action='store_true')
  parser.add_argument('--real', default=False, action='store_true')
  parser.add_argument('--rnn', default=False, action='store_true')
  args = parser.parse_args()


  home = str(Path.home())
  if args.my:
    name = "my_model"
    if args.real:
      if args.rnn:
        output_size = 3
        input_size = 9
        data_path = home + '/data/my_rnn_real/'
      else:
        data_path = home + '/data/my_real/'
    else:
      data_path = home + '/data/my_gz/'
      output_size = 3
      input_size = 45
  else:
    if args.real:
      data_path = home + '/data/eth_real/'
    else:
      data_path = home + '/data/eth_gz/'
    name = "eth_model"  
    output_size = 1
    input_size = 10
    
  WEIGHTS_PATH = data_path + 'weights/'
  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  train_inputs = np.array(pd.read_excel(data_path + 'train_inputs.xlsx'))
  train_labels = np.array(pd.read_excel(data_path + 'train_labels.xlsx'))
  test_inputs = np.array(pd.read_excel(data_path + 'test_inputs.xlsx'))
  test_labels = np.array(pd.read_excel(data_path + 'test_labels.xlsx'))
  
  # Standardize ---------------------------------------------
  inputs_mean, inputs_std = np.mean(train_inputs, axis=0), np.std(train_inputs, axis=0) 
  labels_mean, labels_std = np.mean(train_labels, axis=0), np.std(train_labels, axis=0)
  # train_inputs = np.clip((train_inputs - inputs_mean)/inputs_std, -5,5)
  # train_labels = np.clip((train_labels - labels_mean)/labels_std, -5,5)
  # test_inputs = np.clip((test_inputs - inputs_mean)/inputs_std, -5,5)
  # test_labels = np.clip((test_labels - labels_mean)/labels_std, -5,5)
  print(inputs_mean)
  print(inputs_std)
  print(labels_mean)
  print(labels_std)
  # -------------------------------------------------------------
  print("Data loaded: ", train_inputs.shape, train_labels.shape)

  if args.rnn:
    seq_length = 128
    model = ForwardRNNModel(input_size, output_size, seq_length, batch_size=-1)
  else:
    model = ForwardModel(input_size, output_size)
  model._name = name

  # model.load_weights(data_path + 'weights/model_weights.hdf5')
  # print("Loaded weights for model")

  model_checkpoint = ModelCheckpoint(data_path + 'weights/model_weights.hdf5',monitor='val_loss',mode='min',save_best_only=True,verbose=0)
  plot_callback = plotCallback(WEIGHTS_PATH, test_inputs, test_labels, writer, args=args, means_stds=[inputs_mean, inputs_std, labels_mean, labels_std])
  epochs = 1000
  if args.rnn:
    dim = seq_length*(((train_inputs.shape[0]-1)*input_size)//int(seq_length*input_size))
    test_dim = seq_length*(((test_inputs.shape[0]-1)*output_size)//int(seq_length*output_size))
    print(dim, test_dim )
    train_inputs = train_inputs[:dim,:]
    train_labels = train_labels[:dim,:]
    test_inputs = test_inputs[:test_dim,:]
    test_labels = test_labels[:test_dim,:]

    train_inputs = np.reshape(train_inputs, (-1,seq_length,input_size))
    test_inputs = np.reshape(test_inputs, (-1,seq_length,input_size))
    train_labels = np.reshape(train_labels, (-1,seq_length,output_size))
    test_labels = np.reshape(test_labels, (-1,seq_length,output_size))
    batch_size = dim//seq_length
    print("batch size", batch_size)
    for i in range(epochs):
      print('Epoch', i + 1, '/', epochs)
      history = model.fit([train_inputs], [train_labels], epochs=1, batch_size=-1, validation_split = 0.1, verbose=2, shuffle=False, callbacks=[plot_callback, model_checkpoint])
      model_stateful.reset_states()
  else:
    history = model.fit(train_inputs, train_labels, epochs=epochs, validation_split = 0.1, verbose=2, shuffle=True, callbacks=[plot_callback, model_checkpoint])


