# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *

class NN():
  def __init__(self, name='eth_model', input_size=1, output_size=1, ac_size=3, dense_size=32, sess=None):
    self.name = name
    self.input_size = input_size
    self.output_size = output_size
    self.ac_size = ac_size
    self.dense_size = dense_size
    self.sess = tf.get_default_session() if sess is None else sess
    self.inputs = tf.placeholder(tf.float32, [None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, output_size])
    with tf.variable_scope(self.name):
      self.model()
    var = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in var if 'RMSProp' not in v.name or 'Adam' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)

  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'eth_model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'eth_model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for eth module")
    
  def model(self):
    activation = tf.nn.softsign
    # activation = tf.nn.relu
    last_out = self.inputs
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    self.outputs = tf.layers.dense(last_out, self.output_size)
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.003)
    optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
    self.optimise = optimizer.minimize(self.loss)

  def step(self, X):
    output = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(self.ac_size,self.input_size)}).reshape(1,self.ac_size)
    return output[0]

  def train(self, X, y):
    _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.inputs:np.array(X).reshape(-1,self.input_size), self.labels:np.array(y).reshape(-1,self.output_size)})
    return loss

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--ft', default=False, action='store_true')
  args = parser.parse_args()
  
  args.exp = "_" + args.exp
  # data_path = home + '/data/iros_data/real/300/'
  # data_path = home + '/data/iros_data/real_29_1/'
  data_path = home + '/data/iros_data/real/12_2/'
  WEIGHTS_PATH = data_path + 'weights_eth' + args.exp + '/'
  print("SAVING TO", WEIGHTS_PATH)

  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.3)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

  # batch_size = 64
  batch_size = 512
  ac_size = 3
  input_size = 15
  output_size = 1
  epochs = 10000
  best_val_loss = 10000

  rnn = NN(input_size=input_size, output_size=output_size, ac_size=ac_size, sess=sess)
  sess.run(tf.global_variables_initializer())
  
  if args.ft:
    rnn.load(WEIGHTS_PATH)

  # labels = np.array(pd.read_excel(data_path + 'labels.xlsx'))
  # inputs = np.array(pd.read_excel(data_path + 'inputs.xlsx'))
  inputs = np.load(data_path + 'eth_inputs.npy')
  labels = np.load(data_path + 'eth_labels.npy').reshape(-1,1)
  
  train_inputs, test_inputs = inputs[:int(inputs.shape[0]*0.9),:], inputs[int(inputs.shape[0]*0.9):,:]
  train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:], labels[int(labels.shape[0]*0.9):,:]

  print(train_inputs.shape, test_inputs.shape)
  print(train_labels.shape, test_labels.shape)

  batch_size = min(batch_size, train_inputs.shape[0])

  for e in range(epochs):
    
    idx = [i for i in range(train_inputs.shape[0])]
    np.random.shuffle(idx)
    
    t1 = time.time()
    epoch_loss = []
    for batch in range(train_inputs.shape[0]//batch_size):
      b_idx = batch*batch_size
      batch_x, batch_y = train_inputs[idx[b_idx:b_idx+batch_size],:], train_labels[idx[b_idx:b_idx+batch_size],:]
      loss = rnn.train(batch_x, batch_y)
      epoch_loss.append(loss)
   
    val_loss, predictions = sess.run([rnn.loss, rnn.outputs],feed_dict={rnn.inputs:test_inputs, rnn.labels:test_labels})
    # -----------------------------------------------------------
    # Display epoch losses. Save best weights, periodically plot things
    # -----------------------------------------------------------
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    plot_len = 256
    if np.mean(val_loss) < best_val_loss and e > 200:
      rnn.save(WEIGHTS_PATH + '/best/')
      best_val_loss = np.mean(val_loss)
      y1 = test_labels[:plot_len,0]
      k1 = predictions[:plot_len,0]
      subplot([[y1,k1],[y1,k1]], legend=[['target','predictions']]*2,PATH=WEIGHTS_PATH + 'best_')
    if e % 5 == 0:
      rnn.save(WEIGHTS_PATH)      
    if e % 20 == 0:
      plot_idx = [i for i in range(500) if i%ac_size==0]
      y1 = test_labels[:plot_len,0]
      k1 = predictions[:plot_len,0]
      subplot([[y1,k1],[y1,k1]], legend=[['target','predictions']]*2,PATH=WEIGHTS_PATH)

      # y1,y2,y3 = test_inputs[:plot_len,0],test_inputs[:plot_len,1],test_inputs[:plot_len,2]
      # k1,k2,k3 = test_inputs[:plot_len,3],test_inputs[:plot_len,4],test_inputs[:plot_len,5]
      # subplot([[y1,k1],[y2,k2],[y3,k3]], legend=[['target','predictions']]*3,PATH=WEIGHTS_PATH)
      # y1 = test_labels[plot_idx,0]
      # q1 = test_inputs[plot_idx,0]
      # y1 = labels[plot_idx,0]
      # q1 = inputs[plot_idx,0]
      # k1 = predictions[plot_idx,0]
      # subplot([[y1,q1,k1],[y1,q1,k1]], legend=[['target','desired','predictions']]*2,PATH=WEIGHTS_PATH)