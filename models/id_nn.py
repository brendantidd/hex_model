# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *
from baselines.common.mpi_running_mean_std import RunningMeanStd
import pybullet as p
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI

comm = MPI.COMM_WORLD

class NN():
  def __init__(self, name='model', input_size=1, output_size=1, dense_size=128, sess=None, adam=False, norm=True):
    self.name = name
    self.adam = adam
    self.input_size = input_size
    self.output_size = output_size
    self.dense_size = dense_size
    self.sess = sess
    self.inputs = tf.placeholder(tf.float32, [None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, output_size])
    self.norm = norm
    # self.norm = tf.placeholder_with_default(True, shape=())
    # self.keep_prob = tf.placeholder_with_default(1.0, shape=())
    # self.batch_size = tf.placeholder(tf.int32, [None])
    self.initialise(mean=np.zeros(self.input_size, dtype=np.float32), std=np.ones(self.input_size, dtype=np.float32), scalar=np.ones(self.output_size, dtype=np.float32))
  
  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'id_model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'id_model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for inverse dynamics module")
    
  def initialise(self, mean, std, scalar):
    with tf.variable_scope(self.name):
      self.mean = tf.get_variable('mean', initializer=mean, dtype=tf.float32)
      self.std = tf.get_variable('std', initializer=std, dtype=tf.float32)
      self.scalar = tf.get_variable('scalar', initializer=scalar, dtype=tf.float32)
      self.model()
    vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name or 'Adam' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)
    # print(self.vars)
  
  def model(self):
    if self.norm:
      last_out = tf.clip_by_value((self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std), -5, 5)
    else:
      last_out = self.inputs
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.softsign)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.softsign)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.softsign)
    if self.norm:
      self.outputs = tf.layers.dense(last_out, self.output_size, activation=tf.nn.softsign)
      self.scaled_outputs = self.outputs * tf.stop_gradient(self.scalar)
      self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels/tf.stop_gradient(self.scalar)))
    else:
      self.outputs = tf.layers.dense(last_out, self.output_size)
      self.scaled_outputs = self.outputs
      self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))
    if self.adam:
      # Use MPI adam optimiser 
      params = tf.trainable_variables(self.name + "/")
      self.trainer = MpiAdamOptimizer(comm, learning_rate=0.001, epsilon=1e-5)
      # self.trainer = MpiAdamOptimizer(comm, learning_rate=0.0001, epsilon=1e-5)
      grads_and_var = self.trainer.compute_gradients(self.loss, params)
      grads, var = zip(*grads_and_var)
      grads_and_var = list(zip(grads, var))
      self.optimise = self.trainer.apply_gradients(grads_and_var)
    else:
      # Use RMS prop (no multi processing)
      # optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
      optimizer = tf.train.AdamOptimizer(learning_rate=0.003)
      self.optimise = optimizer.minimize(self.loss)

  def step(self, X):
    output = self.sess.run(self.scaled_outputs, feed_dict={self.inputs:X.reshape(1,self.input_size)})
    return output[0]

  def train(self, X, y, batch_size=1):
    X = X.reshape(batch_size, self.input_size)
    y = y.reshape(batch_size, self.output_size)
    _, loss, outputs = self.sess.run([self.optimise, self.loss, self.scaled_outputs],feed_dict={self.inputs:X, self.labels:y})
    return loss, outputs

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--ft', default=False, action='store_true')
  parser.add_argument('--scale', default=True, action='store_false')
  parser.add_argument('--norm', default=True, action='store_false')
  args = parser.parse_args()
    
  args.exp = "_" + args.exp
  
  # data_path = home + '/data/iros_data/pb_300/'
  data_path = home + '/data/iros_data/pb_500/'
  
  WEIGHTS_PATH = data_path + 'weights' + args.exp + '/'
  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.2)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

  
  batch_size = 512
  
  input_size = 12
  output_size = 3

  dense_size = 128
  epochs = 20000
  best_val_loss = 10000

  nn = NN(name='id_model', input_size=input_size, output_size=output_size, dense_size=dense_size, sess=sess, norm=args.norm)
  
  sess.run(tf.global_variables_initializer())


  inputs = np.load(data_path + 'inputs.npy')
  labels = np.load(data_path + 'labels.npy')
  
  print(inputs.shape, labels.shape)
  if len(inputs.shape) > 2:
    print("reshaping from 3D (sequences, batch, input_size)")
    new_inputs = []
    new_labels = []
    # for inp, lab in zip(inputs, labels):
    for i in range(inputs.shape[0]):
      new_inputs.extend(inputs[i,:,:])
      new_labels.extend(labels[i,:,:])
    inputs = np.array(new_inputs)
    labels = np.array(new_labels)
    np.save(data_path + 'inputs.npy', inputs)
    np.save(data_path + 'labels.npy', labels)
    print(inputs.shape, labels.shape)

  if not args.ft:
    input_mean, input_std = np.mean(inputs, axis=0, dtype=np.float32), np.std(inputs, axis=0, dtype=np.float32)
    sess.run([nn.mean.assign(input_mean), nn.std.assign(input_std)])
    scale = np.max(np.abs(labels), axis=0)*1.1
    sess.run([nn.scalar.assign(scale)])
    print(nn.mean.eval(), nn.std.eval(), nn.scalar.eval())
  else:
    print("need load path")
    rnn.load()
    
  train_inputs, test_inputs = inputs[:int(inputs.shape[0]*0.9),:], inputs[int(inputs.shape[0]*0.9):,:]
  train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:], labels[int(labels.shape[0]*0.9):,:]

  print(train_inputs.shape, test_inputs.shape)
  print(train_labels.shape, test_labels.shape)

  batch_size = min(batch_size, train_inputs.shape[0])
  
  for e in range(epochs):
    
    idx = [i for i in range(train_inputs.shape[0])]
    np.random.shuffle(idx)

    t1 = time.time()
    epoch_loss = []
    
    for batch in range(train_inputs.shape[0]//batch_size):
    
      b_idx = batch*batch_size
      batch_x, batch_y = train_inputs[idx[b_idx:b_idx+batch_size],:], train_labels[idx[b_idx:b_idx+batch_size],:]
      loss, outputs = nn.train(batch_x, batch_y, batch_size=batch_size)
      epoch_loss.append(loss)

    val_loss = []
    val_batch_size = test_inputs.shape[0]
    loss, preds = sess.run([nn.loss, nn.scaled_outputs],feed_dict={nn.inputs:test_inputs, nn.labels:test_labels})
    val_loss.append(loss)
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    plot_len = 256
    if np.mean(val_loss) < best_val_loss and e > 200:
      val_idx = np.random.randint(0,test_labels.shape[0]-plot_len)  
      nn.save(WEIGHTS_PATH + 'best/')
      best_val_loss = np.mean(val_loss)
      y1,y2,y3 = test_labels[val_idx:val_idx+plot_len,0],test_labels[val_idx:val_idx+plot_len,1],test_labels[val_idx:val_idx+plot_len,2]
      
      q1,q2,q3 = preds[val_idx:val_idx+plot_len,0],preds[val_idx:val_idx+plot_len,1],preds[val_idx:val_idx+plot_len,2]
      subplot([[y1,q1],[y2,q2],[y3,q3]], legend=[['labels','pred']]*3,PATH=WEIGHTS_PATH + 'best_')
    if e % 5 == 0:
      nn.save(WEIGHTS_PATH)
    if e % 20 == 0:
      val_idx = np.random.randint(0,test_labels.shape[0]-plot_len)  
      y1,y2,y3 = test_labels[val_idx:val_idx + plot_len,0],test_labels[val_idx:val_idx + plot_len,1],test_labels[val_idx:val_idx + plot_len,2]
      q1,q2,q3 = preds[val_idx:val_idx + plot_len,0],preds[val_idx:val_idx + plot_len,1],preds[val_idx:val_idx + plot_len,2]
      subplot([[y1,q1],[y2,q2],[y3,q3]], legend=[['labels','pred']]*3,PATH=WEIGHTS_PATH)
