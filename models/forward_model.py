import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
          (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
          (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
          (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
          (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
for i in range(len(tableau20)):
  r, g, b = tableau20[i]
  tableau20[i] = (r / 255., g / 255., b / 255.)

def subplot(y_data, PATH=None, legend=None, title=None, x_initial=0, timestep=1/800):
  num_axes = len(y_data)
#   fig, axes = plt.subplots(num_axes, figsize=(20*num_axes, 30))
  fig, axes = plt.subplots(num_axes, figsize=(20, 30))
  cols = [0,4,6,0,4,6]
  for j,c in zip(range(num_axes),cols):
    num_plots = len(y_data[j])
    for i in range(num_plots):
      axes[j].plot([(_ + x_initial)*timestep for _ in range(len(y_data[j][i]))], y_data[j][i], c=tableau20[c+i], alpha=1.0) 
      if legend is not None:
        axes[j].legend(legend[j])  
      if title is not None:
        axes[j].set_title(title[j], loc='left')
        if 'vel' in title[j]:
            axes[j].set_ylabel('rad/s')  
        elif 'pos' in title[j]:
            axes[j].set_ylabel('rad')  
        else:
            axes[j].set_ylabel('Nm')  
        axes[j].set_xlabel('seconds')  
  if PATH is not None:
    plt.savefig(PATH + 'torques.png', bbox_inches='tight',dpi=fig.dpi, pad_inches=0.5)
  else:
    plt.show()

def ForwardModel(input_size, output_size):
  model = keras.Sequential([
      keras.layers.Flatten(input_shape=([input_size])),
      keras.layers.Dense(128, activation='tanh'),
      keras.layers.Dense(128, activation='tanh'),
      # keras.layers.Dense(256, activation='tanh'),
      # keras.layers.Dense(256, activation='tanh'),
      # keras.layers.Dropout(0.2),
      # keras.layers.Dense(512, activation='tanh'),
      # keras.layers.Dropout(0.2),
      keras.layers.Dense(output_size)
  ])
  adam = keras.optimizers.Adam(learning_rate=0.0001, beta_1=0.9, beta_2=0.999, amsgrad=False)
  model.compile(optimizer=adam,
                loss='mse',
                metrics=['mae', 'mse'])

  model.summary()
  return model

def ForwardRNNModel(seq_length=32):
  model = keras.Sequential([
      # keras.layers.Dense(input_shape=([32,input_size])),
      # keras.layers.LSTM(100, input_shape=([32, input_size]), return_sequences=True),
      # keras.layers.Dropout(0.2),
      # keras.layers.LSTM(100, input_shape=([32, input_size]),return_sequences=True),
      # keras.layers.CuDNNLSTM(100, input_shape=([32, input_size]),return_sequences=True),
      keras.layers.CuDNNLSTM(100, input_shape=([seq_length, input_size])),
      # keras.layers.Dropout(0.2),
      # keras.layers.CuDNNLSTM(100),
      # keras.layers.LSTM(100),
      # keras.layers.Dropout(0.2),
      keras.layers.Dense(seq_length*output_size),
      keras.layers.Reshape((seq_length, output_size))
  ])
  rms_prop = keras.optimizers.RMSprop(learning_rate=0.001)
  model.compile(optimizer=rms_prop,
                loss='mse',
                metrics=['mae', 'mse'])

  model.summary()
  return model

class plotCallback(tf.keras.callbacks.Callback): 
  def __init__(self, PATH, inputs, labels, writer, args, means_stds):
    # super(plotCallback.__init__())
    self.PATH = PATH
    self.inputs = inputs
    self.labels = labels
    self.writer = writer
    self.args = args
    # self.input_mean = means_stds[0]
    # self.input_std = means_stds[1]
    # self.label_mean = means_stds[2]
    # self.label_std = means_stds[3]
    self.inputs_max = means_stds[0]
    self.inputs_min = means_stds[1]
    self.labels_max = means_stds[2]
    self.labels_min = means_stds[3]

  def on_epoch_end(self, epoch, logs=None):
  # def on_epoch_begin(self, epoch, logs=None):
    if epoch % 5 == 0:
      num = 100
      if self.args.rnn:
        predictions = self.model.predict(self.inputs[:num,:,:])[:,-1,:]
        inputs = self.inputs[:num,-1,:6]
        labels = self.labels[:num,-1,:6]
      else:
        predictions = self.model.predict(self.inputs[:num,:])
        # inputs = (self.inputs[:num,:6]*self.input_std[:6])+self.input_mean[:6]
        inputs = denormalise(self.inputs, self.inputs_max, self.inputs_min)[:num,:6]
        labels = self.labels[:num, :6]
        # inputs = self.inputs
      if self.args.id:
        print(predictions.shape, labels.shape)
        x1,x2,x3 = predictions[:,0],predictions[:,1],predictions[:,2]
        y1,y2,y3 = labels[:num,0],labels[:num,1],labels[:num,2]
        subplot([[x1,y1],[x2,y2],[x3,y3]], PATH=self.PATH)
      else:
        x1,x2,x3,x4,x5,x6 = predictions[:,0],predictions[:,1],predictions[:,2],predictions[:,3],predictions[:,4],predictions[:,5]
        y1,y2,y3,y4,y5,y6 = labels[:num,0],labels[:num,1],labels[:num,2],labels[:num,3],labels[:num,4],labels[:num,5]
        # z1,z2,z3,z4,z5,z6 = inputs[:num,0],inputs[:num,1],inputs[:num,2],inputs[:num,3],inputs[:num,4],inputs[:num,5]
        # subplot([[x1,y1,z1],[x2,y2,z2]], PATH=self.PATH)
        subplot([[x1,y1],[x2,y2],[x3,y3],[x4,y4],[x5,y5],[x6,y6]], PATH=self.PATH)
        # subplot([[z1],[z2],[z3],[z4],[z5],[z6]], PATH=self.PATH + "pos_")
        # subplot([[x1,y1,z1],[x2,y2,z2],[x3,y3,z3],[x4,y4,z4],[x5,y5,z5],[x6,y6,z6]])
        
        # new_predicts = predictions + self.inputs[:num,:6]
        # new_predicts = (predictions*self.label_std)+self.label_mean + inputs
        print(predictions.shape, inputs.shape)
        new_predicts = denormalise(predictions, self.labels_max, self.labels_min) + inputs
        # # print(new_predicts[:5,:])
        # # print(self.labels[:5,:])
        # labels = self.labels[:num,:] + self.inputs[:num,:6]
        # labels = (self.labels[:num,:6]*self.label_std[:6])+self.label_mean[:6] + inputs
        labels = denormalise(labels, self.labels_max, self.labels_min)[:num,:6] + inputs
        x1,x2,x3,x4,x5,x6 = new_predicts[:num,0],new_predicts[:num,1],new_predicts[:num,2],new_predicts[:num,3],new_predicts[:num,4],new_predicts[:num,5]
        y1,y2,y3,y4,y5,y6 = labels[:num,0],labels[:num,1],labels[:num,2],labels[:num,3],labels[:num,4],labels[:num,5]
        z1,z2,z3,z4,z5,z6 = inputs[:num,0],inputs[:num,1],inputs[:num,2],inputs[:num,3],inputs[:num,4],inputs[:num,5]
        subplot([[x1,y1,z1],[x2,y2,z2],[x3,y3,z3],[x4,y4,z4],[x5,y5,z5],[x6,y6,z6]], PATH=self.PATH + 'recreate_')


        # z1,z2,z3 = inputs[:num,6],inputs[:num,7],inputs[:num,8]
        # subplot([[z1],[z2],[z3]],PATH=self.PATH + '_torq')
        # subplot([[z1],[z2],[z3]])
    # if epoch > 1:
    #   print(epoch, logs)
    writer.add_scalar("error", logs['mean_absolute_error'], epoch)
    writer.add_scalar("val error", logs['val_mean_absolute_error'], epoch)

    # print('The average loss for epoch {} is {:7.2f} and mean absolute error is {:7.2f}.'.format(epoch, logs['loss'], logs['mae']))

if __name__=="__main__":
  import matplotlib.pyplot as plt
  from pathlib import Path
  import tensorboardX
  import argparse
  from tensorflow.keras.callbacks import ModelCheckpoint

  parser = argparse.ArgumentParser()
  # parser.add_argument('--pb', default=True, action='store_false')
  parser.add_argument('--pb', default=False, action='store_true')
  # parser.add_argument('--norm', default=True, action='store_false')
  parser.add_argument('--norm', default=False, action='store_true')
  parser.add_argument('--exp', default="")
  parser.add_argument('--rnn', default=False, action='store_true')
  parser.add_argument('--id', default=False, action='store_true')
  parser.add_argument('--closed_loop', default=False, action='store_true')
  # parser.add_argument('--norm', default=False, action='store_true')
  args = parser.parse_args()

  # output_size = 6
  # input_size = 9
  output_size = 3
  input_size = 12

  home = str(Path.home())
  # data_path = home + '/data/test_pb_fm/'
  # data_path = home + '/data/small/'
  # data_path = home + '/data/pb_id/'
  data_path = home + '/data/pb_id/'
  # data_path = home + '/data/sixty/'
  data_type = ""
  args.exp = "_" + args.exp
  if args.norm:
    WEIGHTS_PATH = data_path + data_type + 'weights_norm' + args.exp + '/'
  else:
    WEIGHTS_PATH = data_path + data_type + 'weights' + args.exp + '/'
  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  train_inputs = np.array(pd.read_excel(data_path + 'train_' + data_type + 'inputs.xlsx'))
  train_labels = np.array(pd.read_excel(data_path + 'train_' + data_type + 'labels.xlsx'))
  if not args.id:
    train_labels = train_labels - train_inputs[:,:6]

  test_inputs = np.array(pd.read_excel(data_path + 'test_' + data_type + 'inputs.xlsx'))
  test_labels = np.array(pd.read_excel(data_path + 'test_' + data_type + 'labels.xlsx'))
  if not args.id:
    test_labels = test_labels - test_inputs[:,:6]

  # Standardize ---------------------------------------------
  # inputs_mean, inputs_std = np.mean(train_inputs, axis=0), np.std(train_inputs, axis=0) 
  # labels_mean, labels_std = np.mean(train_labels, axis=0), np.std(train_labels, axis=0)
   # train_inputs =  np.clip((train_inputs - inputs_mean)/inputs_std, -5,5)
  # train_labels = np.clip((train_labels - labels_mean)/labels_std, -5,5)
  # test_inputs =  np.clip((test_inputs - inputs_mean)/inputs_std, -5,5)+ args.exp + ' 
  # print(inputs_mean)
  # print(inputs_std)
  # print(labels_mean)
  # print(labels_std)
  # -------------------------------------------------------------

  # Normalize ---------------------------------------------
  inputs_max, inputs_min = np.max(train_inputs, axis=0), np.min(train_inputs, axis=0)
  labels_max, labels_min = np.max(train_labels, axis=0), np.min(train_labels, axis=0)
  print(inputs_max, inputs_min)
  print(labels_max, labels_min)
  train_inputs = normalise(train_inputs, inputs_max, inputs_min)
  train_labels = normalise(train_labels, labels_max, labels_min)
  test_inputs = normalise(np.clip(test_inputs,inputs_min, inputs_max), inputs_max, inputs_min)
  test_labels = normalise(np.clip(test_labels,labels_min, labels_max), labels_max, labels_min)
  # ---------------------------------------------------------

  print("Data loaded: ", train_inputs.shape, train_labels.shape)

  if args.rnn:
    seq_length = 128
    dim = seq_length*(((train_inputs.shape[0]-1)*input_size)//int(seq_length*input_size))
    test_dim = seq_length*(((test_inputs.shape[0]-1)*output_size)//int(seq_length*output_size))
    print(dim, test_dim  )
    train_inputs = train_inputs[:dim,:]
    train_labels = train_labels[:dim,:]
    test_inputs = test_inputs[:test_dim,:]
    test_labels = test_labels[:test_dim,:]

    train_inputs = np.reshape(train_inputs, (-1,seq_length,input_size))
    test_inputs = np.reshape(test_inputs, (-1,seq_length,input_size))
    train_labels = np.reshape(train_labels, (-1,seq_length,output_size))
    test_labels = np.reshape(test_labels, (-1,seq_length,output_size))
    model = ForwardRNNModel(input_size, output_size, seq_length=seq_length)
  else:
    model = ForwardModel(input_size, output_size)
  print(train_inputs.shape, test_inputs.shape, train_labels.shape, test_labels.shape)

  # model.load_weights(data_path + 'weights_/model_weights.hdf5')
  # print("Loaded weights for model")

  model_checkpoint = ModelCheckpoint(data_path + 'weights' + args.exp + '/model_weights.hdf5',monitor='val_loss',mode='min',save_best_only=True,verbose=0)
  # plot_callback = plotCallback(WEIGHTS_PATH, test_inputs, test_labels, writer, args=args, means_stds=[inputs_mean, inputs_std, labels_mean, labels_std])
  plot_callback = plotCallback(WEIGHTS_PATH, test_inputs, test_labels, writer, args=args, means_stds=[inputs_max, inputs_min, labels_max, labels_min])

  if not args.closed_loop:
    if args.rnn:
      history = model.fit(train_inputs, train_labels, epochs=1000, validation_split = 0.1, verbose=2,shuffle=False,callbacks=[plot_callback, model_checkpoint])
    else:
      history = model.fit(train_inputs, train_labels, epochs=1000, validation_split = 0.1, verbose=2, shuffle=True,callbacks=[plot_callback, model_checkpoint])

    # Closed loop test
  else:
    # for j in range(num_epochs):
    
    def data_gen():
      for j in range(train_inputs.shape[0]):
        num = 32
        state = np.array([train_inputs[0,:]])
        states = []
        for i in range(1,num):
          st = model.predict(state)
          print(st.shape, np.array([train_inputs[i,-1,6:]]).shape)
          state = np.concatenate((st[0,-1,], np.array(train_inputs)[i,-1,6:]), axis=1)
          states.append(state)
        inputs = states
        print(train_labels.shape)
        labels = train_labels[j,:,:]
        print(inputs, labels)
        yield inputs, labels
    def val_gen():
      while True:
        num = 32
        batch_count = 0
        state = np.array([train_inputs[0,:]])
        states = []
        for i in range(1,num):
          st = model.predict(state)
          state = np.concatenate((st, np.array([train_inputs[i,6:]])), axis=1)
          states.append(state)
        yield inputs, labels
        batch_count += 1
        if batch_count > 100:
          break
    gen = data_gen()
    next(gen)
    # val_gen()
    # history = model.fit_generator(data_gen, validation_data=val_gen, epochs=1, validation_steps = 10, verbose=2,shuffle=False,callbacks=[plot_callback, model_checkpoint])
    
      
    # predictions = model.predict(test_inputs[:num,:])
    print(np.array(states)[:,0,:].shape)
    predictions = np.array(states)[:,0,:]
    x1,x2,x3,x4,x5,x6 = predictions[:,0],predictions[:,1],predictions[:,2],predictions[:,3],predictions[:,4],predictions[:,5]
    y1,y2,y3,y4,y5,y6 = train_labels[:num,0],train_labels[:num,1],train_labels[:num,2],train_labels[:num,3],train_labels[:num,4],train_labels[:num,5]
    z1,z2,z3,z4,z5,z6 = train_inputs[:num,0],train_inputs[:num,1],train_inputs[:num,2],train_inputs[:num,3],train_inputs[:num,4],train_inputs[:num,5]
    # subplot([[x1,y1,z1],[x2,y2,z2]], PATH=self.PATH)
    subplot([[x1,y1],[x2,y2],[x3,y3],[x4,y4],[x5,y5],[x6,y6]])
    # subplot([[z1],[z2],[z3],[z4],[z5],[z6]])

    # new_predicts = predictions + test_inputs[:num,:6]
    # labels = test_labels[:num,:] + test_inputs[:num,:6]
    # x1,x2,x3,x4,x5,x6 = new_predicts[:num,0],new_predicts[:num,1],new_predicts[:num,2],new_predicts[:num,3],new_predicts[:num,4],new_predicts[:num,5]
    # y1,y2,y3,y4,y5,y6 = labels[:num,0],labels[:num,1],labels[:num,2],labels[:num,3],labels[:num,4],labels[:num,5]
    # subplot([[x1,y1],[x2,y2],[x3,y3],[x4,y4],[x5,y5],[x6,y6]])


    # z1,z2,z3 = test_inputs[:num,6],test_inputs[:num,7],test_inputs[:num,8]
    # subplot([[z1],[z2],[z3]])

    # if __name__=="__main__":
    #     pass