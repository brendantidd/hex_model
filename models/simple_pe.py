# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *

class NN():
  def __init__(self, name='forward_model', input_size=1, output_size=1, dense_size=256, sess=None):
    self.name = name
    self.input_size = input_size
    self.output_size = output_size
    self.dense_size = dense_size
    self.sess = tf.get_default_session() if sess is None else sess
    self.inputs = tf.placeholder(tf.float32, [None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, output_size])
    with tf.variable_scope(self.name):
      self.model()
    self.vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    # self.vars = [v for v in var if 'RMSProp' not in v.name or 'Adam' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)

  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'fm_model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'fm_model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for forward module")
    
  def model(self):
    activation = tf.nn.softsign
    last_out = self.inputs
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    self.outputs = tf.layers.dense(last_out, self.output_size)
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))
    optimizer = tf.train.AdamOptimizer(learning_rate=0.003)
    self.optimise =  optimizer.minimize(self.loss)

  def step(self, X):
    output = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(1,self.input_size)})
    return output[0]

  def train(self, X, y):
    _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.inputs:np.array(X).reshape(-1,self.input_size), self.labels:np.array(y).reshape(-1,self.output_size)})
    return loss

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--test', default=False, action='store_true')
  args = parser.parse_args()
  
  args.exp = "_nn_" + args.exp
  # data_path = home + '/data/iros_data/forward/'
  data_path = home + '/data/iros_data/gz/'
  WEIGHTS_PATH = data_path + 'weights' + args.exp + '/'
  print("SAVING TO", WEIGHTS_PATH)

  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.3)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

  batch_size = 64
  input_size = 9
  output_size = 6
  epochs = 10000
  best_val_loss = 10000

  rnn = NN(input_size=input_size, output_size=output_size, sess=sess)
  sess.run(tf.global_variables_initializer())
    
  labels = np.array(pd.read_excel(data_path + 'labels.xlsx'))
  inputs = np.array(pd.read_excel(data_path + 'inputs.xlsx'))
  
  idx = [i for i in range(inputs.shape[0])]
  np.random.shuffle(idx)
  train_inputs, test_inputs = inputs[idx[:int(inputs.shape[0]*0.9)],:], inputs[idx[int(inputs.shape[0]*0.9):],:]
  train_labels, test_labels = labels[idx[:int(labels.shape[0]*0.9)],:], labels[idx[int(labels.shape[0]*0.9):],:]

  print(train_inputs.shape, test_inputs.shape)
  print(train_labels.shape, test_labels.shape)

  batch_size = min(batch_size, train_inputs.shape[0])

  for e in range(epochs):
    t1 = time.time()
    epoch_loss = []
    for batch in range(train_inputs.shape[0]//batch_size):
      idx = batch*batch_size
      batch_x, batch_y = train_inputs[idx:idx+batch_size,:], train_labels[idx:idx+batch_size,:]
      loss = rnn.train(batch_x, batch_y)
      epoch_loss.append(loss)
   
    val_loss, predictions = sess.run([rnn.loss, rnn.outputs],feed_dict={rnn.inputs:test_inputs, rnn.labels:test_labels})
    # -----------------------------------------------------------
    # Display epoch losses. Save best weights, periodically plot things
    # -----------------------------------------------------------
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    plot_len = 500
    if np.mean(val_loss) < best_val_loss and e > 200:
      rnn.save(WEIGHTS_PATH + '/best/')
      best_val_loss = np.mean(val_loss)
      y1,y2,y3,y4,y5,y6 = test_labels[:plot_len,0],test_labels[:plot_len,1],test_labels[:plot_len,2],test_labels[:plot_len,3],test_labels[:plot_len,4],test_labels[:plot_len,5]
      k1,k2,k3,k4,k5,k6 = predictions[:plot_len,0],predictions[:plot_len,1],predictions[:plot_len,2],predictions[:plot_len,3],predictions[:plot_len,4],predictions[:plot_len,5]
      subplot([[y1,k1],[y2,k2],[y3,k3],[y4,k4],[y5,k5],[y6,k6]], legend=[['target','predictions']]*6,PATH=WEIGHTS_PATH + 'best_')
    if e % 5 == 0:
      rnn.save(WEIGHTS_PATH)      
    if e % 20 == 0:
      y1,y2,y3,y4,y5,y6 = test_labels[:plot_len,0],test_labels[:plot_len,1],test_labels[:plot_len,2],test_labels[:plot_len,3],test_labels[:plot_len,4],test_labels[:plot_len,5]
      k1,k2,k3,k4,k5,k6 = predictions[:plot_len,0],predictions[:plot_len,1],predictions[:plot_len,2],predictions[:plot_len,3],predictions[:plot_len,4],predictions[:plot_len,5]
      subplot([[y1,k1],[y2,k2],[y3,k3],[y4,k4],[y5,k5],[y6,k6]], legend=[['target','predictions']]*6,PATH=WEIGHTS_PATH)
