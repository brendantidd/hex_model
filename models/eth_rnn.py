# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *

class RNN():
  def __init__(self, name='actuator_model', input_size=1, output_size=1, lstm_size=32, dense_size=32, sess=None):
    self.name = name
    self.input_size = input_size
    self.output_size = output_size
    self.lstm_size = lstm_size
    self.dense_size = dense_size
    self.sess = sess
    self.inputs = tf.placeholder(tf.float32, [None, None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, None, output_size])
    self.keep_prob = tf.placeholder_with_default(1.0, shape=())
    self.train = tf.placeholder_with_default(True, shape=())
    self.batch_size = tf.placeholder(tf.int32, [None])
    self.initialise()
    
  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'actuator_model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'actuator_model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for actuator_model net")
    
  def model(self):
    # activation = tf.nn.tanh
    activation = tf.nn.softsign
    # activation = tf.nn.relu
    last_out = (self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    self.cell = nn.rnn_cell.LSTMCell(self.lstm_size, name="r1")
    self.batch_state_in = self.cell.zero_state(self.batch_size, tf.float32) 
    last_out, self.states = nn.dynamic_rnn(self.cell,last_out,dtype=tf.float32, initial_state=self.batch_state_in)  
    last_out = tf.nn.dropout(last_out, self.keep_prob)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    self.outputs = tf.layers.dense(last_out, self.output_size)
    self.scaled_outputs = (self.outputs*tf.stop_gradient(self.output_std[:self.output_size])) + tf.stop_gradient(self.output_mean[:self.output_size])
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, tf.stop_gradient((self.labels - self.output_mean[:self.output_size])/self.output_std[:self.output_size])))
    optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
    self.optimise =  optimizer.minimize(self.loss)

  def initialise(self):
    with tf.variable_scope(self.name):
      self.mean = tf.get_variable('mean', initializer=np.zeros(self.input_size, dtype=np.float32), dtype=tf.float32)
      self.std = tf.get_variable('std', initializer=np.ones(self.input_size, dtype=np.float32), dtype=tf.float32)
      self.output_mean = tf.get_variable('output_mean', initializer=np.zeros(self.output_size, dtype=np.float32), dtype=tf.float32)
      self.output_std = tf.get_variable('output_std', initializer=np.ones(self.output_size, dtype=np.float32), dtype=tf.float32)
      self.model()
    vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)
    # print(self.vars)

  def step(self, X, state=None):
    # State comes in as [coxa_pos, femur_pos, tibia_pos, coxa_vel, femur_vel, tibia_vel, coxa_cmd_torque, femur_cmd_torque, tibia_cmd_torque]
    # Need it to be: [[coxa_pos, coxa_vel, coxa_cmd_torq],[femur_pos, femur_vel, femur_cmd_torq],[tibia_pos, tibia_vel, tibia_cmd_torq]]
    # Step through the lstm with batch of 3 (number of joints), seq length of 1, input_size of 3 (pos,  vel, torque)
    # print(np.array(state))
    # print(np.array(state).shape)
    inputs = []
    for j in range(3):
      idx = [j,j+3,j+6]
      inputs.append(np.array(X)[idx])
    if state is None:
      state = [np.zeros([1,self.lstm_size]),np.zeros([1,self.lstm_size])]
    output, state = self.sess.run([self.scaled_outputs, self.states],feed_dict={self.inputs:np.array(inputs).reshape(3,1,3), self.batch_state_in:state,self.batch_size:[3]})
    return output.T[0][0], state

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--ft', default=False, action='store_true')
  args = parser.parse_args()
  
  args.exp = "_" + args.exp

  data_path = home + '/data/iros_data/real/12_2/'
  batch_size = 512
  seq_length = 256
  epochs = 20000
  best_val_loss = 10000
  lstm_size = 32
  
  WEIGHTS_PATH = data_path + 'weights_eth_rnn' + args.exp + '/'
  print("SAVING TO", WEIGHTS_PATH)

  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.2)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

  rnn = RNN(input_size=3, output_size=1, sess=sess, lstm_size=lstm_size)
  sess.run(tf.global_variables_initializer())

  if args.ft:
    rnn.load(WEIGHTS_PATH)

  inputs = np.load(data_path + 'inputs.npy')

  torques = np.load(data_path + 'observed_torques.npy')
  labels = np.load(data_path + 'labels.npy')
  dones = np.load(data_path + 'dones.npy')

    # dim = (inputs.shape[0]//seq_length)*seq_length
  # new_inputs = np.zeros([dim//seq_length, seq_length, inputs.shape[1]])
  # new_labels = np.zeros([dim//seq_length, seq_length, labels.shape[1]])
  # if args.stand:
  #   new_dones = np.zeros([dim//seq_length, seq_length, 1])
  # print(inputs.shape, labels.shape, dim, new_inputs.shape, new_labels.shape)
  
  idxs = list(np.where(dones==1.0)[0]) + [dones.shape[0]-1] 
  new_inputs = []
  new_labels = []  
  lost_data = 0
  # This is where we separate the data: 
    # Into episodes (defined by dones)
      # Into sequence_lengths
        # Joints. Joint data arrives as [coxa_pos, femur_pos, tibia_pos, coxa_vel, femur_vel, tibia_vel, coxa_cmd_torque, femur_cmd_torque, tibia_cmd_torque]
  for idx in range(len(idxs)-1): 
    dim = ((idxs[idx+1]-idxs[idx])//seq_length)*seq_length
    lost_data += (idxs[idx+1]-idxs[idx] - dim)
    for i in range(0,dim//seq_length):
      for j in range(3):
        new_inputs.append(inputs[idxs[idx]+i*seq_length:idxs[idx]+i*seq_length+seq_length,[j,j+3,j+6]])
        new_labels.append(torques[idxs[idx]+i*seq_length:idxs[idx]+i*seq_length+seq_length,[j]])
  inputs = np.array(new_inputs)
  labels = np.array(new_labels)

  print(inputs.shape, labels.shape, lost_data)

  temp_inputs = inputs.reshape(-1,3)
  temp_labels = labels.reshape(-1,1)
  input_mean, input_std = np.mean(temp_inputs, axis=0, dtype=np.float32), np.std(temp_inputs, axis=0, dtype=np.float32)
  output_mean, output_std = np.mean(temp_labels, axis=0, dtype=np.float32), np.std(temp_labels, axis=0, dtype=np.float32)
  print(input_mean, input_std, output_mean, output_std)
  print(input_mean.shape, input_std.shape, output_mean.shape, output_std.shape)
  
  sess.run([rnn.mean.assign(input_mean), rnn.std.assign(input_std), rnn.output_mean.assign(output_mean), rnn.output_std.assign(output_std)])
  

  idx = [i for i in range(inputs.shape[0])]
  np.random.shuffle(idx)

  train_inputs, test_inputs = inputs[idx[:int(len(idx)*0.9)],:,:],inputs[idx[int(len(idx)*0.9):],:,:] 
  train_labels, test_labels = labels[idx[:int(len(idx)*0.9)],:],labels[idx[int(len(idx)*0.9):],:] 
  
  batch_size = min(batch_size, train_inputs.shape[0])

  for e in range(epochs):

    idx = [i for i in range(train_inputs.shape[0])]
    np.random.shuffle(idx)

    t1 = time.time()
    epoch_loss = []
    for batch in range(train_inputs.shape[0]//batch_size):
      b_idx = batch*batch_size
      batch_x, batch_y = train_inputs[idx[b_idx:b_idx+batch_size],:,:], train_labels[idx[b_idx:b_idx+batch_size],:,:]
      
      states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      
      states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      
      _, loss, train_states = sess.run([rnn.optimise, rnn.loss, rnn.states],feed_dict={rnn.inputs:batch_x, rnn.labels:batch_y, rnn.batch_state_in: states,rnn.batch_size:[batch_size], rnn.keep_prob:0.75})
      epoch_loss.append(loss)

    val_batch_size = test_inputs.shape[0]
    # Get open loop predictions
    val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
    preds, val_loss = sess.run([rnn.scaled_outputs,rnn.loss],feed_dict={rnn.inputs:test_inputs, rnn.labels:test_labels, rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
    
    # -----------------------------------------------------------
    # Display epoch losses. Save best weights, periodically plot things
    # -----------------------------------------------------------
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    if np.mean(val_loss) < best_val_loss and e > 200:
      rnn.save(WEIGHTS_PATH + '/best/')
      val_idx = np.random.randint(0,test_labels.shape[0]-3)
      best_val_loss = np.mean(val_loss)
      y1,y2,y3  = test_labels[val_idx,:],test_labels[val_idx+1,:],test_labels[val_idx+2,:]
      k1,k2,k3  = preds[val_idx,:],preds[val_idx+1,:],preds[val_idx+2,:]
      subplot([[y1,k1],[y2,k2],[y3,k3]], legend=[['target','predictions']]*3,PATH=WEIGHTS_PATH + 'best_')

    if e % 5 == 0:
      rnn.save(WEIGHTS_PATH)      
    if e % 20 == 0:
      val_idx = np.random.randint(0,test_labels.shape[0]-3)
      y1,y2,y3  = test_labels[val_idx,:],test_labels[val_idx+1,:],test_labels[val_idx+2,:]
      k1,k2,k3  = preds[val_idx,:],preds[val_idx+1,:],preds[val_idx+2,:]
      subplot([[y1,k1],[y2,k2],[y3,k3]], legend=[['target','predictions']]*3,PATH=WEIGHTS_PATH)
      
