# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *
import random

class RNN():
  def __init__(self, name='model', input_size=1, output_size=1, dense_size=64, sess=None, norm=True):
    self.name = name
    self.norm = norm
    self.input_size = input_size
    self.output_size = output_size
    self.dense_size = dense_size
    self.sess = sess
    self.inputs = tf.placeholder(tf.float32, [None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, output_size])
    # self.train = tf.placeholder_with_default(tf.bool, True, shape=())
    self.train = tf.placeholder_with_default(True, shape=())
    # self.model()
    # vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    # self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name]
    # self.saver = tf.train.Saver(var_list=self.vars)
    # if not train:
    self.initialise(mean=np.zeros(self.input_size, dtype=np.float32), std=np.ones(self.input_size, dtype=np.float32), scalar=np.ones(self.output_size, dtype=np.float32))

  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'model.ckpt')
  
  def load(self, WEIGHT_PATH):
    model_name = 'model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for forward module")
    
  def model(self):
    last_out = tf.cond(self.train, lambda: self.inputs, lambda: tf.clip_by_value((self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std), -5, 5))
    # last_out = tf.clip_by_value((self.inputs - self.mean)/self.std, -5, 5)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.tanh)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.tanh)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.tanh)
    self.outputs = tf.layers.dense(last_out, self.output_size, activation=tf.nn.tanh)
    self.scaled_outputs = self.outputs * tf.stop_gradient(self.scalar)
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))
    # optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
    optimizer = tf.train.AdamOptimizer(learning_rate=0.003)
    self.optimise =  optimizer.minimize(self.loss)

  def initialise(self, mean, std, scalar):
    with tf.variable_scope(self.name):
      self.mean = tf.get_variable('mean', initializer=mean, dtype=tf.float32)
      self.std = tf.get_variable('std', initializer=std, dtype=tf.float32)
      self.scalar = tf.get_variable('scalar', initializer=scalar, dtype=tf.float32)
      self.model()
    vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)
    # print(self.vars)

  def step(self, X, state=None):
    output, state = self.sess.run([self.scaled_outputs, self.states],feed_dict={self.inputs:X.reshape(1,1,self.input_size), self.train:False})
    return output[0][0], state

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--gz', default=False, action='store_true')
  parser.add_argument('--cl', default=False, action='store_true')
  parser.add_argument('--ft', default=False, action='store_true')
  parser.add_argument('--norm', default=True, action='store_false')
  args = parser.parse_args()
  
  args.exp = "_mlp_" + args.exp
  if args.gz:
    # data_path = home + '/data/iros_data/gz_fm1/'
    # data_path = home + '/data/iros_data/gz_fm_100_rand/'
    # data_path = home + '/data/iros_data/gz_fm_100_rand_norm/'
    data_path = home + '/data/iros_data/gz_fm_100_real_rand/'
  else:
    data_path = home + '/data/iros_data/my_fm/'
  WEIGHTS_PATH = data_path + 'weights' + args.exp + '/'
  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.3)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

  # handle sequence for batch training
  batch_size = 64
  input_size = 9
  output_size = 6
  epochs = 1000
  dense_size = 128
  best_val_loss = 10000

  rnn = RNN(name='forward_model', input_size=input_size, output_size=output_size, dense_size=128, sess=sess, norm=args.norm)
  sess.run(tf.global_variables_initializer())

  if args.gz:
    if args.test:
      labels = np.array(pd.read_csv(data_path + 'test_labels.csv'))
      inputs = np.array(pd.read_csv(data_path + 'test_inputs.csv'))
    else:
      if args.ft:
        rnn.load('/home/brendan/data/iros_data/gz_fm_100_rand_norm/weights_cl/')
        # rnn.load('/home/brendan/data/iros_data/pb_id_100_rand_norm/weights_ft/')
        ft_data_path = home + '/data/iros_data/gz_sample/'
        # s_torq = np.array(pd.read_excel(ft_data_path + 'inputs.xlsx'))
        # st_1 = np.array(pd.read_excel(ft_data_path + 'labels.xlsx'))
        # inputs = np.concatenate((s_torq[:,:6], st_1), axis=1)
        # labels = s_torq[:,6:]

        inputs = np.array(pd.read_excel(ft_data_path + 'inputs.xlsx'))
        labels = np.array(pd.read_excel(ft_data_path + 'labels.xlsx'))
      else:
        labels = np.array(pd.read_excel(data_path + 'labels.xlsx'))
        inputs = np.array(pd.read_excel(data_path + 'inputs.xlsx'))
  else:
    if args.test:
      labels = np.array(pd.read_excel(data_path + 'test_labels.xlsx'))
      inputs = np.array(pd.read_excel(data_path + 'test_inputs.xlsx'))
    else:
      labels = np.array(pd.read_excel(data_path + 'train_labels.xlsx'))
      inputs = np.array(pd.read_excel(data_path + 'train_inputs.xlsx'))
  print("Data dimensions", inputs.shape, labels.shape)

  # if args.norm:
  input_mean, input_std = np.mean(inputs, axis=0, dtype=np.float32), np.std(inputs, axis=0, dtype=np.float32)
  # print(input_mean, input_std)
  inputs = np.clip((inputs - input_mean)/input_std, -5, 5)
  # Scale labels (we have a tanh at the last output layer)
  max_scale, min_scale = np.max(labels, axis=0), np.min(labels,axis=0)
  scale = []
  for ma, mi in zip(max_scale, min_scale):
    if abs(mi) > ma:
      scale.append(abs(mi))
    else:
      scale.append(ma)
  # Add 10% to handle unseen labels
  scale = np.array(scale, dtype=np.float32) * 1.1
  labels = labels/scale
  # rnn.initialise(mean=np.zeros(self.input_size, dtype=np.float32), std=np.ones(self.input_size, dtype=np.float32), scalar=np.ones(self.output_size, dtype=np.float32))
  # rnn.initialise(input_mean, input_std, scale)
  sess.run([rnn.mean.assign(input_mean), rnn.std.assign(input_std), rnn.scalar.assign(scale)])
  print("Scale: ", scale)
  print("Mean: ", input_mean)
  print("Stds: ", input_std)
  print(rnn.mean.eval(), rnn.std.eval(), rnn.scalar.eval())

  idx = [i for i in range(inputs.shape[0])]
  train_idx, test_idx = idx[:int(len(idx)*0.9)], idx[int(len(idx)*0.9):]
  random.shuffle(train_idx)

  print(len(idx))
  print(idx[:20])
  train_inputs, test_inputs = inputs[train_idx, :],inputs[test_idx, :] 
  train_labels, test_labels = labels[train_idx, :],labels[test_idx, :] 
  
  


  print(train_inputs.shape, test_inputs.shape)
  print(train_labels.shape, test_labels.shape)
  batch_size = min(batch_size, train_inputs.shape[0])

  for e in range(epochs):
    t1 = time.time()
    epoch_loss = []
    # TODO: Stateful training: reset the state at the beginning of every epoch (batches must be contiguous), can't currently do with closed loop data generation.
    # ---------------------------------------------------------
    # Training step: both closed loop and open loop predictions
    # ---------------------------------------------------------
    for batch in range(train_inputs.shape[0]//batch_size):
      idx = batch*batch_size
      batch_x, batch_y = train_inputs[idx:idx+batch_size,:], train_labels[idx:idx+batch_size,:]
      input_x = batch_x
      _, loss = sess.run([rnn.optimise, rnn.loss],feed_dict={rnn.inputs:input_x, rnn.labels:batch_y})
      epoch_loss.append(loss)
    # -----------------------------------------------------------
    # Validation step: both closed loop and open loop predictions
    # -----------------------------------------------------------
    val_loss = []
    # val_batch_size = test_inputs.shape[0]
    cl_input = test_inputs[0,:6]
    seq_length = 512

    cl_inputs = np.zeros((seq_length, input_size))
    cl_predictions = np.zeros((seq_length, output_size))
    # cl_predictions = np.zeros_like(test_labels)
    for j in range(seq_length):
      cl_input = np.concatenate((cl_input.reshape(1,6), np.array(test_inputs[j,6:]).reshape(1,3)),axis=1)
      cl_inputs[j,:] = cl_input.reshape(1,input_size)
      cl_input = sess.run(rnn.outputs,feed_dict={rnn.inputs:cl_input.reshape(1,input_size)})
      cl_predictions[j,:] = cl_input.reshape(1,output_size)
    # # Get open loop predictions
    # val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
    loss, ol_predictions = sess.run([rnn.loss, rnn.outputs],feed_dict={rnn.inputs:test_inputs, rnn.labels:test_labels})
    # loss = sess.run(rnn.loss,feed_dict={rnn.inputs:cl_inputs, rnn.labels:test_labels})
    val_loss.append(loss)
    # -----------------------------------------------------------
    # Display epoch losses. Save best weights, periodically plot things
    # -----------------------------------------------------------
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    l = 512
    if np.mean(val_loss) < best_val_loss:
      rnn.save(WEIGHTS_PATH)
      # print("saved with means, stds, and scalars:")
      # print(rnn.mean.eval(), rnn.std.eval(), rnn.scalar.eval())
      best_val_loss = np.mean(val_loss)
      cl_predictions = np.array(cl_predictions)
      k1,k2,k3,k4,k5,k6 = cl_predictions[0:l,0],cl_predictions[0:l,1],cl_predictions[0:l,2],cl_predictions[0:l,3],cl_predictions[0:l,4],cl_predictions[0:l,5]

      y1,y2,y3,y4,y5,y6 = test_labels[0:l,0],test_labels[0:l,1],test_labels[0:l,2],test_labels[0:l,3],test_labels[0:l,4],test_labels[0:l,5]
      q1,q2,q3,q4,q5,q6 = ol_predictions[0:l,0],ol_predictions[0:l,1],ol_predictions[0:l,2],ol_predictions[0:l,3],ol_predictions[0:l,4],ol_predictions[0:l,5]
      # subplot([[y1,q1],[y2,q2],[y3,q3],[y4,q4],[y5,q5],[y6,q6]], legend=[['target', 'open loop pred']]*6,PATH=WEIGHTS_PATH + 'best_')
      subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3],[y4,k4,q4],[y5,k5,q5],[y6,k6,q6]], legend=[['target','closed loop pred', 'open loop pred']]*6,PATH=WEIGHTS_PATH + '_best')

    if e % 50 == 0:
      cl_predictions = np.array(cl_predictions)
      k1,k2,k3,k4,k5,k6 = cl_predictions[0:l,0],cl_predictions[0:l,1],cl_predictions[0:l,2],cl_predictions[0:l,3],cl_predictions[0:l,4],cl_predictions[0:l,5]

      y1,y2,y3,y4,y5,y6 = test_labels[0:l,0],test_labels[0:l,1],test_labels[0:l,2],test_labels[0:l,3],test_labels[0:l,4],test_labels[0:l,5]
      
      q1,q2,q3,q4,q5,q6 = ol_predictions[0:l,0],ol_predictions[0:l,1],ol_predictions[0:l,2],ol_predictions[0:l,3],ol_predictions[0:l,4],ol_predictions[0:l,5]
      # subplot([[y1,q1],[y2,q2],[y3,q3],[y4,q4],[y5,q5],[y6,q6]], legend=[['target', 'open loop pred']]*6,PATH=WEIGHTS_PATH)
      subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3],[y4,k4,q4],[y5,k5,q5],[y6,k6,q6]], legend=[['target','closed loop pred', 'open loop pred']]*6,PATH=WEIGHTS_PATH)
      # plot([test_inputs[0:l,:], test_labels[0:l,:], cl_inputs[0:l,:], cl_predictions[0:l,:], ol_predictions[0:l,:]], legend=['input', 'output', 'cl_input','closed loop pred', 'open loop pred'],PATH='/home/brendan/')
      # plot([test_inputs[0:l,:], cl_predictions[0:l,:], ol_predictions[0:l,:]], legend=['input', 'closed loop', 'open loop'],PATH='/home/brendan/')

