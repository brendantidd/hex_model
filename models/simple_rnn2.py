# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *

class RNN():
  def __init__(self, name='model', input_size=1, output_size=1, lstm_size=128, sess=None):
    self.name = name
    self.input_size = input_size
    self.output_size = output_size
    self.lstm_size = lstm_size
    self.sess = sess
    self.inputs = tf.placeholder(tf.float32, [None, None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, None, output_size])
    self.batch_size = tf.placeholder(tf.int32, [None])
    self.model()
    vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)

  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'model.ckpt')

  def model(self):
    with tf.variable_scope(self.name):
      self.cell = nn.rnn_cell.LSTMCell(self.lstm_size, name="r1")
      self.batch_state_in = self.cell.zero_state(self.batch_size, tf.float32) 
      last_out, self.states = nn.dynamic_rnn(self.cell,self.inputs,dtype=tf.float32, initial_state=self.batch_state_in)  
      self.outputs = tf.layers.dense(last_out, self.output_size, activation=tf.nn.tanh)
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))
    optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
    self.optimise =  optimizer.minimize(self.loss)

  def step(self, X, state=None):
    if state is None:
      state = [np.zeros([1,self.lstm_size]),np.zeros([1,self.lstm_size])]
    output, state = self.sess.run([rnn.outputs, rnn.states],feed_dict={rnn.inputs:X.reshape(1,1,self.input_size), rnn.batch_state_in:state,rnn.batch_size:[1]})
    return output, state

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--cl', default=False, action="store_true")
  parser.add_argument('--ft', default=False, action="store_true")
  args = parser.parse_args()
  
  args.exp = "_" + args.exp
  data_path = home + '/data/iros_data/rnn_test/'
  WEIGHTS_PATH = data_path + 'weights' + args.exp + '/'
  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  minimum = 0
  maximum = 2000
  num_data_points = 20000
  data_points = np.linspace(minimum, maximum, num_data_points)
  # dataset = (np.sin(data_points) + np.random.normal(0,0.01, size=num_data_points)).reshape(-1,1)
  dataset = np.sin(data_points).reshape(-1,1)
  inputs, labels = dataset[:-1, :], dataset[1:,:]
  print(inputs.shape, labels.shape)
  # plot([dataset[:2000,:]])

  # handle sequence for batch training
  batch_size = 32
  seq_length = 128
  input_size = 1
  output_size = 1
  epochs = 1000
  lstm_size = 64
  best_val_loss = 10000
  slide_num = seq_length
  dim = (inputs.shape[0]//seq_length)*seq_length
  new_inputs = np.zeros([dim//seq_length, seq_length, inputs.shape[1]])
  new_labels = np.zeros([dim//seq_length, seq_length, labels.shape[1]])

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.5)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))
  
  rnn = RNN(input_size=1, output_size=1, lstm_size=lstm_size, sess=sess)
  sess.run(tf.global_variables_initializer())
  # TODO Consider sliding window, note: can't use stateful training if not sliding over whole seq_length! But can shuffle sequences, so might be better? 
  for i in range(0,dim//seq_length):
    new_inputs[i,:,:] = inputs[i*seq_length:i*seq_length+seq_length,:]
    new_labels[i,:,:] = labels[i*seq_length:i*seq_length+seq_length,:]

  train_inputs, test_inputs = new_inputs[:int(new_inputs.shape[0]*0.9),:,:],new_inputs[int(new_inputs.shape[0]*0.9):,:,:] 
  train_labels, test_labels = new_labels[:int(new_labels.shape[0]*0.9),:,:],new_labels[int(new_labels.shape[0]*0.9):,:,:] 
  print(train_inputs.shape, test_inputs.shape)
  print(train_labels.shape, test_labels.shape)
  for e in range(epochs):
    t1 = time.time()
    epoch_loss = []
    # TODO: Stateful training: reset the state at the beginning of every epoch (batches must be contiguous), can't currently do with closed loop data generation.
    for batch in range(train_inputs.shape[0]//batch_size):
      idx = batch*batch_size
      batch_x, batch_y = train_inputs[idx:idx+batch_size,:,:], train_labels[idx:idx+batch_size,:,:]
      states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      
      if args.cl and batch%2 == 0 and batch != 0:
      # if False:
        cl_input = batch_x[:,0,:]
        input_x = np.zeros_like(batch_x)
        for j in range(seq_length):
          input_x[:,j,:] = cl_input.reshape(batch_size,input_size)
          cl_input, states = sess.run([rnn.outputs, rnn.states],feed_dict={rnn.inputs:cl_input.reshape(batch_size,1,input_size), rnn.batch_state_in: states,rnn.batch_size:[batch_size]})
      else:
        input_x = batch_x
      states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      
      _, loss, train_states = sess.run([rnn.optimise, rnn.loss, rnn.states],feed_dict={rnn.inputs:input_x, rnn.labels:batch_y, rnn.batch_state_in: states,rnn.batch_size:[batch_size]})
      epoch_loss.append(loss)
    # Validation step: both closed loop and open loop predictions
    val_loss = []
    val_batch_size = test_inputs.shape[0]
    cl_input = test_inputs[:,0,:]
    cl_inputs = np.zeros_like(test_inputs)
    cl_predictions = np.zeros_like(test_inputs)
    val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]
    for j in range(seq_length):
      cl_input = cl_input.reshape(val_batch_size,1,input_size)
      cl_inputs[:,j,:] = cl_input.reshape(test_inputs.shape[0],input_size)
      cl_input, val_states = sess.run([rnn.outputs, rnn.states],feed_dict={rnn.inputs:cl_input,rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
      cl_predictions[:,j,:] = cl_input.reshape(test_inputs.shape[0],input_size)
    # Get open loop predictions
    val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
    ol_predictions = sess.run(rnn.outputs,feed_dict={rnn.inputs:test_inputs, rnn.labels:test_labels, rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
    # Get closed loop loss
    val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
    loss = sess.run(rnn.loss,feed_dict={rnn.inputs:cl_inputs, rnn.labels:test_labels, rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
    val_loss.append(loss)
    # print("Epoch,",e,"Loss", np.mean(epoch_loss), "Val loss", np.mean(val_loss), "time: ", time.time() - t1)
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1), best_val_loss)
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    if np.mean(val_loss) < best_val_loss:
      rnn.save(WEIGHTS_PATH + 'best/')
      best_val_loss = np.mean(val_loss)
      cl_predictions = np.array(cl_predictions)
      y1, y2, y3 = test_labels[0,:,:],test_labels[1,:,:],test_labels[2,:,:]
      k1, k2, k3 = cl_predictions[0,:,:],cl_predictions[1,:,:],cl_predictions[2,:,:]
      q1, q2, q3 = ol_predictions[0,:,:],ol_predictions[1,:,:],ol_predictions[2,:,:]
      subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3]], legend=[['output','closed loop pred', 'open loop pred']]*3,PATH=WEIGHTS_PATH + 'best_')
    if e % 5 == 0:
      rnn.save(WEIGHTS_PATH)
    if e % 20 == 0:
      cl_predictions = np.array(cl_predictions)
      y1, y2, y3 = test_labels[0,:,:],test_labels[1,:,:],test_labels[2,:,:]
      k1, k2, k3 = cl_predictions[0,:,:],cl_predictions[1,:,:],cl_predictions[2,:,:]
      q1, q2, q3 = ol_predictions[0,:,:],ol_predictions[1,:,:],ol_predictions[2,:,:]
      subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3]], legend=[['output','closed loop pred', 'open loop pred']]*3,PATH=WEIGHTS_PATH)
      # plot([test_inputs[0,:,:], test_labels[0,:,:], cl_inputs[0,:,:], cl_predictions[0,:,:], ol_predictions[0,:,:]], legend=['input', 'output', 'cl_input','closed loop pred', 'open loop pred'],PATH='/home/brendan/')
      # plot([test_inputs[0,:,:], cl_predictions[0,:,:], ol_predictions[0,:,:]], legend=['input', 'closed loop', 'open loop'],PATH='/home/brendan/')

# TODO for real data:
# standardise input (-mean/std)
# divide output by the max of the absolute value + 10%, and tanh as final activation
