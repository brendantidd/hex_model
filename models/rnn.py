# Updated to work with TF 1.4
# Working example for my blog post at:
# http://danijar.com/variable-sequence-lengths-in-tensorflow/
import functools
import sets
import tensorflow as tf
from tensorflow import nn
import numpy as np
from scipy.io import loadmat
from sklearn.model_selection import train_test_split
import pandas as pd
import time

def print_trainable_params():
    total_parameters = 0
    for variable in tf.trainable_variables():
        # shape is an array of tf.Dimension
        shape = variable.get_shape()
        print(shape)
        print(len(shape))
        variable_parameters = 1
        for dim in shape:
            print(dim)
            variable_parameters *= dim.value
        print(variable_parameters)
        total_parameters += variable_parameters
    print(total_parameters)


def lazy_property(function):
    attribute = '_' + function.__name__

    @property
    @functools.wraps(function)
    def wrapper(self):
        if not hasattr(self, attribute):
            setattr(self, attribute, function(self))
        return getattr(self, attribute)
    return wrapper

def get_next_batch(x, y, start, end):
    x_batch = x[start:end,:,:]
    y_batch = y[start:end,:]    
    return x_batch, y_batch
  
class RNN:
    def __init__(self, name, sess, data, target=None, input_size=3, seq_length=50, num_hidden=128, num_layers=1):
        self.name =name
        self.sess = sess
        self.data = data
        self.target = target   
        self.input_size = input_size
        self.seq_length = seq_length           
        self._num_hidden = num_hidden
        self._num_layers = num_layers
        self.prediction
        self.error
        self.optimize
        vars_with_adam = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
        self.vars = [v for v in vars_with_adam if 'RMSProp' not in v.name]
        self.saver = tf.train.Saver(var_list=self.vars)

    def save(self, SAVE_PATH):
        self.saver.save(self.sess, SAVE_PATH + 'model.ckpt')

    def load(self, WEIGHT_PATH):
        model_name = 'model.ckpt'
        self.saver.restore(self.sess, WEIGHT_PATH + model_name)
        print("Loaded weights for actuator module")

    @lazy_property
    def prediction(self, state=None):
        data_3D = self.data
        with tf.variable_scope(self.name):
            print(data_3D.shape)
            # data_3D
            last_out = data_3D
            
            last_out = tf.nn.tanh(tf.layers.dense(last_out, 128, name="fc1"))
            # last_out = tf.nn.relu(tf.layers.dense(last_out, 128, name="fc2"))
            # last_out = tf.nn.dropout(last_out, 0.75)
            # output = tf.nn.relu(tf.layers.dense(last_out, 128, name="fc3"))

            # lstm_cell = rnn.DropoutWrapper(
            # rnn.LSTMBlockCell(num_hidden, forget_bias=1.0),
            # input_keep_prob=0.5,
            # output_keep_prob=0.5,
            # state_keep_prob=0.5,
            # variational_recurrent=True,
            # dtype=tf.float32
            # )

            output, _ = nn.dynamic_rnn(
                nn.rnn_cell.LSTMCell(self._num_hidden, name="r1"),
                last_out,
                dtype=tf.float32,            
            )   
            # print(output.shape)
            # output, _ = nn.dynamic_rnn(
            #     nn.rnn_cell.LSTMCell(self._num_hidden, name="r2"),
            #     output,
            #     dtype=tf.float32,            
            # )   

            output_2D = tf.reshape(output, (-1, output.get_shape()[2]))
            print(output_2D)
            
            weight, bias = self._weight_and_bias(
                self._num_hidden, int(self.target.get_shape()[2]))
            # prediction_2D = tf.tanh(tf.matmul(output_2D, weight) + bias)
            prediction_2D = tf.matmul(output_2D, weight) + bias
    #         prediction_2D = tf.matmul(output_2D, weight) + bias
            prediction = tf.reshape(prediction_2D, (-1, self.target.get_shape()[1], self.target.get_shape()[2]))
        return prediction

    def step(self, x):
        predict = self.sess.run(self.prediction, {self.data: x})
        return predict
    
    @lazy_property
    def cost(self):
        mean_square_error = tf.reduce_mean(tf.squared_difference(self.prediction, self.target))
#         cross_entropy = -tf.reduce_sum(self.target * tf.log(self.prediction))
#         tf.summary.scalar('cross_entropy', cross_entropy)
        return mean_square_error
    

    @lazy_property
    def optimize(self):
        learning_rate = 0.003
        # learning_rate = 0.01
        optimizer = tf.train.RMSPropOptimizer(learning_rate)
        return optimizer.minimize(self.cost)

    @lazy_property
    def error(self):
#         mistakes = tf.not_equal(
#             tf.argmax(self.target, 1), tf.argmax(self.prediction, 1))
#         tf.summary.scalar('error', tf.reduce_mean(tf.cast(mistakes, tf.float32)))
        L2 = tf.reduce_mean(tf.squared_difference(self.prediction, self.target))
        return L2
    

    @staticmethod
    def _weight_and_bias(in_size, out_size):
        weight = tf.truncated_normal([in_size, out_size], stddev=0.01)
        bias = tf.constant(0.1, shape=[out_size])
        return tf.Variable(weight), tf.Variable(bias)


# This is with normalized data
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    # We treat images as sequences of pixel rows.
    from pathlib import Path
    home = str(Path.home())
    import tensorboardX
    import argparse
    parser = argparse.ArgumentParser()
    # parser.add_argument('--pb', default=True, action='store_false')
    parser.add_argument('--pb', default=False, action='store_true')
    # parser.add_argument('--norm', default=True, action='store_false')
    parser.add_argument('--norm', default=False, action='store_true')
    parser.add_argument('--exp', default="")
    parser.add_argument('--test', default=False, action='store_true')
    # parser.add_argument('--norm', default=False, action='store_true')
    args = parser.parse_args()

    # epochs = 10000
    epochs = 2000
    batch_size = 64
    output_size = 3
    
    if args.pb:
        data_type = 'pb_'
        name = 'sim_model'
        input_size = 12
        output_size = 3
    else:
        name = 'model'
        data_type = ''
        # input_size = 12
        # input_size = 6
        # output_size = 3
        # input_size = 9
        # output_size = 6
        input_size = 12
        output_size = 3
        # input_size = 9
        # output_size = 3
        # input_size = 1
        # output_size = 1

    seq_length = 128
    # seq_length = 1024

    # data_path = home + '/data/my_rnn_real/'
    # data_path = home + '/data/iros_data/pb_id_100/'
    data_path = home + '/data/iros_data/pb_id_rand/'
    # data_path = home + '/data/iros_data/joint_test/'
    # data_path = home + '/data/iros_data/my_fm/'
    # data_path = home + '/data/torq_to_torq/'
    # data_path = home + '/data/temp/'
    args.exp = "_" + args.exp
    if args.norm:
        WEIGHTS_PATH = data_path + data_type + 'weights_norm' + args.exp + '/'
    else:
        WEIGHTS_PATH = data_path + data_type + 'weights' + args.exp + '/'
    writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

    inputs = np.array(pd.read_excel(data_path + 'inputs.xlsx'))
    labels = np.array(pd.read_excel(data_path + 'labels.xlsx'))

    train_inputs, test_inputs = inputs[:int(inputs.shape[0]*0.9),:],inputs[int(inputs.shape[0]*0.9):,:] 
    train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:],labels[int(labels.shape[0]*0.9):,:] 
    # if args.test:
    #     inputs = pd.read_excel(data_path + 'test_' + data_type + 'inputs.xlsx')
    # else:
    #     inputs = pd.read_excel(data_path + 'train_' + data_type + 'inputs.xlsx')

    inputs_ar = np.array(inputs)
    # print(inputs_ar.shape)
    inputs_ar1 = inputs_ar[1:,:]
    inputs_arr1 = np.reshape(inputs_ar1, (-1,input_size), order='F')
    # inputs_arr2 = inputs_arr1[0:599750,:]   
    dim = seq_length*(((inputs_ar.shape[0]-1)*input_size)//int(seq_length*input_size))
    # dim = 349750
    # test_dim = 25000
    # print("dim", dim)
    # print(inputs_arr1.shape)
    inputs_arr2 = inputs_arr1[0:dim,:]   
    print(inputs_arr2.shape, dim,inputs_ar.shape)
    inputs_arr = np.reshape(inputs_arr2, (-1,seq_length,input_size))
    
    
    # if args.test:
    #     labels = pd.read_excel(data_path + 'test_' + data_type + 'labels.xlsx')
    # else:
    #     labels = pd.read_excel(data_path + 'train_' + data_type + 'labels.xlsx')

    labels_ar = np.array(labels)
    # print(labels_ar.shape)
    labels_ar1 = labels_ar[1:,:]
    labels_arr1 = np.reshape(labels_ar1, (-1,output_size), order='F')
    labels_arr2 = labels_arr1[0:dim,:]   
    print(labels_arr2.shape)
    labels_arr = np.reshape(labels_arr2, (-1,seq_length,output_size))
    
    
    
    # inputs1_tes = pd.read_excel(data_path + 'test_' + data_type + 'inputs.xlsx')
    inputs1_tes = test_inputs
    inputs_ar1_tes = np.array(inputs1_tes)
    inputs_ar11_tes = inputs_ar1_tes[1:,:]    
    inputs_arr1_tes = np.reshape(inputs_ar11_tes, (-1,input_size), order='F')

    test_dim = seq_length*(((inputs_ar1_tes.shape[0]-1)*input_size)//int(seq_length*input_size))

    inputs_arr2_tes = inputs_arr1_tes[0:test_dim,:]
    print(inputs_arr2_tes.shape)
    inputs_arr_tes = np.reshape(inputs_arr2_tes, (-1,seq_length,input_size))

    # labels1_tes = pd.read_excel(data_path + 'test_' + data_type + 'labels.xlsx')
    labels1_tes = test_labels
    labels_ar1_tes = np.array(labels1_tes)
    labels_ar11_tes = labels_ar1_tes[1:,:]    
    labels_arr1_tes = np.reshape(labels_ar11_tes, (-1,output_size), order='F')
    labels_arr2_tes = labels_arr1_tes[0:test_dim,:]
    labels_arr_tes = np.reshape(labels_arr2_tes, (-1,seq_length,output_size))
   


    # ----------------------------------
    normed_inp = (inputs_arr - 
                  inputs_arr.mean(axis=(0,1))) / inputs_arr.std(axis=(0,1))
    normed_lab = (labels_arr - labels_arr.mean(axis=(0,1))) / labels_arr.std(axis=(0,1))
    # normed_lab = labels_arr

    # label_max = normed_lab.max(axis=(0,1))+10
    # normed_lab = normed_lab / label_max
    
    label_mean = labels_arr.mean(axis=(0,1))
    label_std = labels_arr.std(axis=(0,1))
    input_mean = inputs_arr.mean(axis=(0,1))
    input_std = inputs_arr.std(axis=(0,1))
    print("means")
    print("input mean:")
    print(input_mean)
    print("label mean:")
    print(label_mean)
    print("stds")
    print("input std:")
    print(input_std)
    print("label std:")
    print(label_std)

    if args.norm:
        x_train = normed_inp
        y_train = normed_lab
    else:
        x_train = inputs_arr
        y_train = labels_arr
    
    
    normed_inp_tes = (inputs_arr_tes - 
                  input_mean) / input_std
    # normed_lab = (labels_arr - labels_arr.mean(axis=(0,1))) / labels_arr.std(axis=(0,1))
    # normed_lab_tes = labels_arr_tes   
    # normed_lab_tes = normed_lab_tes / label_max
    normed_lab_tes = (labels_arr_tes - label_mean)/label_std
    
    if args.norm:
        x_valid = normed_inp_tes
        y_valid = normed_lab_tes
    else:
        x_valid = inputs_arr_tes
        y_valid = labels_arr_tes

    # ----------------------------------

    data = tf.placeholder(tf.float32, [None, x_train.shape[1], x_train.shape[2]])
    target = tf.placeholder(tf.float32, [None, y_train.shape[1], y_train.shape[2]])

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.5)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,             
                                            gpu_options=gpu_options), graph=None)

    # sess = tf.Session()   
    model = RNN(name, sess, data, target, seq_length=seq_length, input_size=input_size)
    sess.run(tf.initialize_all_variables())
    global_step = 0
    
    # print_trainable_params()
    # Add ops to save and restore all the variables.
    
    # Number of training iterations in each epoch
    num_tr_iter = int(len(y_train) / batch_size)
    try:
        print("loading weights")
        model.load(WEIGHTS_PATH)
    except:
        print("no weights to load")

    print(num_tr_iter)
    t1 = time.time()
    for epoch in range(epochs):
        total_err = 0.0
        for iteration in range(num_tr_iter):           
            global_step += 1
            start = iteration * batch_size
            end = (iteration + 1) * batch_size
            x_batch, y_batch = get_next_batch(x_train, y_train, start, end)
            # Close the loop
            # Get closed loop data:
            # x_batch, y_batch = get_cl_batch(x_train, y_train, start, end)
            # print(x_batch.shape, y_batch.shape)
            # x_cl_batch = []
            # for i in x_batch:
            #     for j in i:
            #         print(j.shape)
            #         cl_output = sess.run(model.prediction, {data: j})
            #         x_cl_batch.append(cl_output)
            # x_batch.append(x_cl_batch)
            # y_batch.append(y_batch)

#             x_batch = x_batch.reshape((batch_size, timesteps, num_input))
 
            sess.run(model.optimize, {data: x_batch, target: y_batch})
            error = sess.run(model.error, {data: x_batch, target: y_batch})
            total_err = total_err + error


        error_eval = sess.run(model.error, {data: x_valid, target: y_valid})
        # if(error_eval < 0.00015):
        if(epoch % 20 == 0):
            # path = '/home/ahm040/Documents/Research/New_Project/one_leg/saved_model3/model{:4d}.ckpt'.format(epoch)
            # path = home + '/data/test_leg_data/model{:4d}.ckpt'.format(epoch)
            # path = home + '/data/weights/test_leg_15_11/'
            # print(path)
            model.save(WEIGHTS_PATH)
            # if epoch == 100:
            #     exit()
        print('Epoch {:2d} error {:3.7f}%  validation error {:3.7f}% time {:3.3f}'.format(epoch + 1, (total_err/num_tr_iter), error_eval, time.time() - t1))
        t1 = time.time()
        writer.add_scalar("error", (total_err/num_tr_iter), epoch)
        writer.add_scalar("val error", error_eval, epoch)
#         print(total_err/num_tr_iter)
        if epoch % 20 == 0:
            test_prediction = sess.run(model.prediction, {data: x_valid, target: y_valid})
            print(test_prediction.shape, x_valid.shape, y_valid.shape)
            ind1 = -4;ind2 = -3;ind3 = -2;ind4 = -1
            t = np.arange(0.0,model.seq_length,1.0)
            plt.figure(figsize=(12, 5))
            # +++++++++++++ 6 plots +++++++++++++++++++++++==
            # plt.subplot(231)
            # plt.plot(t, test_prediction[ind1,:,0],label = "Pred")
            # plt.plot(t, y_valid[ind1,:,0], label = "Label")
            # plt.legend() 
            # plt.subplot(232)
            # plt.plot(t, test_prediction[ind3,:,1], t, y_valid[ind3,:,1])
            # plt.subplot(233)
            # plt.plot(t, test_prediction[ind4,:,2], t, y_valid[ind4,:,2])
            # plt.subplot(234)
            # plt.plot(t, test_prediction[ind2,:,3], t, y_valid[ind2,:,3])
            # plt.subplot(235)
            # plt.plot(t, test_prediction[ind3,:,4], t, y_valid[ind3,:,4])
            # plt.subplot(236)
            # plt.plot(t, test_prediction[ind4,:,5], t, y_valid[ind4,:,5])
            # plt.savefig(WEIGHTS_PATH + '/rnn.png', bbox_inches='tight')
            # ++++++++++++++++++++++++++++++++++++++++===

            plt.subplot(221)
            plt.plot(t, test_prediction[ind1,:,0],label = "Pred")
            plt.plot(t, y_valid[ind1,:,0], label = "Label")
            plt.legend() 
            plt.subplot(222)
            plt.plot(t, test_prediction[ind2,:,0], t, y_valid[ind2,:,0])
            plt.subplot(223)
            plt.plot(t, test_prediction[ind3,:,1], t, y_valid[ind3,:,1])
            plt.subplot(224)
            plt.plot(t, test_prediction[ind4,:,2], t, y_valid[ind4,:,2])
            plt.savefig(WEIGHTS_PATH + '/rnn.png', bbox_inches='tight')

            # plt.subplot(222)
            # plt.plot(t, test_prediction[ind2,:,0], t, y_valid[ind2,:,0])
            # plt.subplot(223)
            # plt.plot(t, test_prediction[ind3,:,1], t, y_valid[ind3,:,1])
            # plt.subplot(224)
            # plt.plot(t, test_prediction[ind4,:,2], t, y_valid[ind4,:,2])
            # plt.savefig(WEIGHTS_PATH + '/rnn.png', bbox_inches='tight')
        
            # plt.show()
