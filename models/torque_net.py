from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
from scripts.utils import *
from glob import glob   
import re 
import tensorboardX
import time
from pathlib import Path
home = str(Path.home())

class Model(Base):
  max_timesteps = int(2e7)
  def __init__(self, input_size, label_size, PATH, writer, env):
    self.PATH = PATH
    self.writer = writer
    self.replay_buffer = ReplayBuffer(input_size, label_size, 10000)
    self.batch_size = 32
    self.epochs = 10
    self.lr = 0.0001
    self.env = env
    self.input_size = input_size
    # Place holders for input and label
    self.input = tf.placeholder(dtype=tf.float32, shape=(None, self.input_size))
    self.label = tf.placeholder(dtype=tf.float32, shape=(None, label_size))
    # Network
    with tf.variable_scope('main'):
      x = tf.nn.tanh(tf.layers.dense(self.input, 64, name="fc1"))
      x = tf.nn.tanh(tf.layers.dense(x, 64, name="fc2"))
      x = tf.nn.tanh(tf.layers.dense(x, 64, name="fc3"))
      self.output = tf.layers.dense(x, label_size, name="final")
    # MSE loss
    self.loss_fn = 0.5 * tf.reduce_mean((self.label - self.output)**2)
    self.loss_names = ["mse_loss"]
    # Optimizer
    self.optimizer = tf.train.AdamOptimizer(learning_rate=self.lr)
    self.vars = [x for x in tf.global_variables() if 'main' in x.name]
    self.train_op = self.optimizer.minimize(self.loss_fn, var_list=self.vars)
    # Initialize stuff
    self.policy_saver = tf.train.Saver(var_list=self.vars)
    self.sess = tf.Session()
    self.sess.run(tf.global_variables_initializer())

  def step(self, x, im, stochastic):
    action = self.sess.run(self.output, feed_dict = {self.input: x.reshape(-1,self.input_size)})
    return action[0], 1,1,1

  def train(self, x, y):
    feed_dict = {self.input: x, self.label: y}
    return self.sess.run([self.loss_fn, self.train_op], feed_dict)[0]

  def run_train(self, rews, lens):
    self.loss = []
    for i in range(self.epochs):
      batch = self.replay_buffer.sample_batch(self.batch_size)
      self.loss.append(self.train(batch['input'],batch['label']))
    self.evaluate(rews, lens)

  def finalize_buffer(self, rew, len):
    pass

  def save(self):
    self.policy_saver.save(self.sess, self.PATH + 'model.ckpt')

  def load(self, WEIGHT_PATH):
    self.policy_saver.restore(self.sess, WEIGHT_PATH + 'model.ckpt')
    print("Loaded weights")

  def add_to_buffer(self, data):
    input_data, label = data
    self.replay_buffer.store(input_data, label)

class ReplayBuffer():
  """
  A simple FIFO experience replay buffer.
  """
  def __init__(self, input_dim, labels_dim, size):
    self.input_data = np.zeros([size, input_dim], dtype=np.float32)
    self.labels = np.zeros([size, labels_dim], dtype=np.float32)
    self.ptr, self.size, self.max_size = 0, 0, size

  def store(self, input_data, label):
    self.input_data[self.ptr] = input_data
    self.labels[self.ptr] = label
    self.ptr = (self.ptr+1) % self.max_size
    self.size = min(self.size+1, self.max_size)

  def sample_batch(self, batch_size=32):
    idxs = np.random.randint(0, self.size, size=batch_size)
    return dict(input=self.input_data[idxs],
                label=self.labels[idxs])