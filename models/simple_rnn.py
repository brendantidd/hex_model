# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *

class RNN():
  def __init__(self, name='model', input_size=1, output_size=1, lstm_size=128, dense_size=64, sess=None, norm=False, delta=False):
    self.name = name
    self.norm = norm
    self.delta = delta
    self.input_size = input_size
    self.output_size = output_size
    self.lstm_size = lstm_size
    self.dense_size = dense_size
    self.sess = sess
    self.inputs = tf.placeholder(tf.float32, [None, None, input_size])
    self.labels = tf.placeholder(tf.float32, [None, None, output_size])
    self.keep_prob = tf.placeholder_with_default(1.0, shape=())
    # self.train = tf.placeholder_with_default(tf.bool, True, shape=())
    self.train = tf.placeholder_with_default(True, shape=())
    self.batch_size = tf.placeholder(tf.int32, [None])
    # self.model()
    # vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    # self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name]
    # self.saver = tf.train.Saver(var_list=self.vars)
    # if not train:
    self.initialise(mean=np.zeros(self.input_size, dtype=np.float32), std=np.ones(self.input_size, dtype=np.float32), scalar=np.ones(self.output_size, dtype=np.float32),delta_mean=np.zeros(self.output_size, dtype=np.float32), delta_std=np.ones(self.output_size, dtype=np.float32))
    
  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'fm_model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'fm_model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for forward module")
    
  def model(self):
    activation = tf.nn.tanh
    # activation = tf.nn.relu
    # last_out = tf.cond(self.train, lambda: self.inputs, lambda: tf.clip_by_value((self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std), -5, 5))
    if self.norm:
      last_out = (self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std)
    else:
      last_out = self.inputs
    # last_out = self.inputs
    # last_out = tf.clip_by_value((self.inputs - self.mean)/self.std, -5, 5)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    self.cell = nn.rnn_cell.LSTMCell(self.lstm_size, name="r1")
    # lstm_stacked = tf.contrib.rnn.MultiRNNCell([self.lstm_cell() for _ in range(n_layers)])
    self.batch_state_in = self.cell.zero_state(self.batch_size, tf.float32) 
    # last_out, self.states = nn.dynamic_rnn(self.cell,self.inputs,dtype=tf.float32, initial_state=self.batch_state_in)  
    last_out, self.states = nn.dynamic_rnn(self.cell,last_out,dtype=tf.float32, initial_state=self.batch_state_in)  
    last_out = tf.nn.dropout(last_out, self.keep_prob)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation)
    # self.scaled_outputs = self.outputs * tf.stop_gradient(self.scalar)
    if self.delta:
      # self.outputs = tf.layers.dense(last_out, self.output_size, activation=tf.nn.tanh)
      self.outputs = tf.layers.dense(last_out, self.output_size)
      # scaled_outputs = (self.outputs*tf.stop_gradient(self.delta_std)) + tf.stop_gradient(self.delta_mean)
      self.scaled_outputs = self.outputs + self.inputs[:,:,:self.output_size]
      
      # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, tf.stop_gradient(tf.clip_by_value((self.labels - self.delta_mean)/self.delta_std,-5,5))))
      # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, tf.stop_gradient((self.labels/self.scalar))))
      self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, tf.stop_gradient(self.labels)))
    else:
      self.outputs = tf.layers.dense(last_out, self.output_size)
      if self.norm:
        self.scaled_outputs = (self.outputs*tf.stop_gradient(self.std[:self.output_size])) + tf.stop_gradient(self.mean[:self.output_size])
        self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, tf.stop_gradient((self.labels - self.mean[:self.output_size])/self.std[:self.output_size])))
      else:
        self.scaled_outputs = self.outputs
        self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))

    optimizer = tf.train.RMSPropOptimizer(learning_rate=0.003)
    self.optimise =  optimizer.minimize(self.loss)

  def initialise(self, mean, std, scalar, delta_mean, delta_std):
    with tf.variable_scope(self.name):
      self.mean = tf.get_variable('mean', initializer=mean, dtype=tf.float32)
      self.std = tf.get_variable('std', initializer=std, dtype=tf.float32)
      if self.delta:
        self.delta_mean = tf.get_variable('delta_mean', initializer=delta_mean, dtype=tf.float32)
        self.delta_std = tf.get_variable('delta_std', initializer=delta_std, dtype=tf.float32)
      self.scalar = tf.get_variable('scalar', initializer=scalar, dtype=tf.float32)
      self.model()
    vars_with_rmsprop = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_rmsprop if 'RMSProp' not in v.name]
    self.saver = tf.train.Saver(var_list=self.vars)
    # print(self.vars)

  def step(self, X, state=None):
    if state is None:
      state = [np.zeros([1,self.lstm_size]),np.zeros([1,self.lstm_size])]
    # output, state = self.sess.run([self.scaled_outputs, self.states],feed_dict={self.inputs:X.reshape(1,1,self.input_size), self.batch_state_in:state,self.batch_size:[1], self.train:False})
    output, state = self.sess.run([self.scaled_outputs, self.states],feed_dict={self.inputs:X.reshape(1,1,self.input_size), self.batch_state_in:state,self.batch_size:[1]})
    return output[0][0], state

if __name__=="__main__":
  from pathlib import Path
  home = str(Path.home())
  import tensorboardX
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="")
  parser.add_argument('--test', default=False, action='store_true')
  # parser.add_argument('--gz', default=False, action='store_true')
  parser.add_argument('--gz', default=True, action='store_false')
  parser.add_argument('--cl', default=True, action='store_false')
  parser.add_argument('--ft', default=False, action='store_true')
  parser.add_argument('--norm', default=True, action='store_false')
  parser.add_argument('--stand', default=False, action='store_true')
  parser.add_argument('--delta', default=False, action='store_true')
  args = parser.parse_args()
  
  args.exp = "_" + args.exp
  if args.delta:
    args.exp += 'delta'
  if not args.norm:
    args.exp += 'no_norm'
  if args.gz:
    # data_path = home + '/data/iros_data/gz_fm1/'
    # data_path = home + '/data/iros_data/gz_fm_100_rand/'
    # data_path = home + '/data/iros_data/gz_fm_100_rand_norm/'
    # data_path = home + '/data/iros_data/gz_fm_100_real_rand/'
    if args.stand:
      data_path = home + '/data/iros_data/gz_100_delayed_stand/'
    else:
      # data_path = home + '/data/iros_data/gz_100_delayed/'
      # data_path = home + '/data/iros_data/gz_150_delayed/'
      # data_path = home + '/data/iros_data/gz_100_real/'
      # data_path = home + '/data/iros_data/gz_sin_100/'
      # data_path = home + '/data/iros_data/gz_300/'
      # data_path = home + '/data/iros_data/gz_27_1/'
      # data_path = home + '/data/iros_data/real_29_1/'
      # data_path = home + '/data/iros_data/real_29_1/double/'
      # data_path = home + '/data/iros_data/real/mine/'
      # data_path = home + '/data/iros_data/real/all/'
      # data_path = home + '/data/iros_data/real/3_2/all/'
      # data_path = home + '/data/iros_data/real/4_2/mine/'
      data_path = home + '/data/iros_data/real/12_2/'
      # data_path = home + '/data/iros_data/my_gz/'
  else:
    data_path = home + '/data/iros_data/my_fm/'
  WEIGHTS_PATH = data_path + 'weights' + args.exp + '/'
  print("SAVING TO", WEIGHTS_PATH)

  writer = tensorboardX.SummaryWriter(log_dir=WEIGHTS_PATH)

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.2)
  sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

  # handle sequence for batch training
  # batch_size = 64
  batch_size = 256
  # batch_size = 512
  # seq_length = 512
  seq_length = 256
  # seq_length = 128
  # seq_length = 1024
  ac_size = 3
  if args.stand:
    input_size = 11
    output_size = 8
  else:  
    input_size = 9
    output_size = 6
  epochs = 20000
  lstm_size = 128
  dense_size = 128
  best_val_loss = 10000
  slide_num = seq_length

  rnn = RNN(name='forward_model', input_size=input_size, output_size=output_size, lstm_size=lstm_size, dense_size=128, sess=sess, norm=args.norm, delta=args.delta)
  sess.run(tf.global_variables_initializer())
  
  # Residual inputs is state + observed torques
  inputs = np.load(data_path + 'inputs.npy')
  torques = np.load(data_path + 'observed_torques.npy')
  inputs[:,6:] = torques
  labels = np.load(data_path + 'labels.npy')
  dones = np.load(data_path +  'dones.npy')
  
  if not args.ft:
    input_mean, input_std = np.mean(inputs, axis=0, dtype=np.float32), np.std(inputs, axis=0, dtype=np.float32)
    sess.run([rnn.mean.assign(input_mean), rnn.std.assign(input_std)])
    scale = np.max(np.abs(labels), axis=0)*1.1
    sess.run([rnn.scalar.assign(scale)])
    print(rnn.mean.eval(), rnn.std.eval(), rnn.scalar.eval())
  else:
    rnn.load(WEIGHTS_PATH)


  dim = (inputs.shape[0]//seq_length)*seq_length
  new_inputs = np.zeros([dim//seq_length, seq_length, inputs.shape[1]])
  new_labels = np.zeros([dim//seq_length, seq_length, labels.shape[1]])
  if args.stand:
    new_dones = np.zeros([dim//seq_length, seq_length, 1])
  print(inputs.shape, labels.shape, dim, new_inputs.shape, new_labels.shape)

  
  idxs = list(np.where(dones==1.0)[0]) + [dones.shape[0]-1] 
  new_inputs = []
  new_labels = []  
  lost_data = 0
  for idx in range(len(idxs)-1): 
    dim = ((idxs[idx+1]-idxs[idx])//seq_length)*seq_length
    lost_data += (idxs[idx+1]-idxs[idx] - dim)
    for i in range(0,dim//seq_length):
      new_inputs.append(inputs[idxs[idx]+i*seq_length:idxs[idx]+i*seq_length+seq_length,:])
      new_labels.append(labels[idxs[idx]+i*seq_length:idxs[idx]+i*seq_length+seq_length,:])
  inputs = np.array(new_inputs)
  labels = np.array(new_labels)
  print(inputs.shape, labels.shape, lost_data)

  # for i in range(0,dim//seq_length):
  #   new_inputs[i,:,:] = inputs[i*seq_length:i*seq_length+seq_length,:]
  #   new_labels[i,:,:] = labels[i*seq_length:i*seq_length+seq_length,:]
  #   if args.stand:
  #     new_dones[i,:,:] = dones[i*seq_length:i*seq_length+seq_length,:]
  
  idx = [i for i in range(inputs.shape[0])]
  np.random.shuffle(idx)

  train_inputs, test_inputs = inputs[idx[:int(len(idx)*0.9)],:,:],inputs[idx[int(len(idx)*0.9):],:,:] 
  train_labels, test_labels = labels[idx[:int(len(idx)*0.9)],:,:],labels[idx[int(len(idx)*0.9):],:,:] 

  # if args.stand:
  #   train_dones, test_dones = new_dones[:int(new_dones.shape[0]*0.9),:,:],new_dones[int(new_dones.shape[0]*0.9):,:,:] 

  print(train_inputs.shape, test_inputs.shape)
  print(train_labels.shape, test_labels.shape)
  if args.stand:
    print(train_dones.shape, test_dones.shape) 
  batch_size = min(batch_size, train_inputs.shape[0])

  for e in range(epochs):

    idx = [i for i in range(train_inputs.shape[0])]
    np.random.shuffle(idx)

    t1 = time.time()
    epoch_loss = []
    # TODO: Stateful training: reset the state at the beginning of every epoch (batches must be contiguous), can't currently do with closed loop data generation.
    # ---------------------------------------------------------
    # Training step: both closed loop and open loop predictions
    # ---------------------------------------------------------
    for batch in range(train_inputs.shape[0]//batch_size):
      b_idx = batch*batch_size
      batch_x, batch_y = train_inputs[idx[b_idx:b_idx+batch_size],:,:], train_labels[idx[b_idx:b_idx+batch_size],:,:]
      if args.stand:
        batch_done = train_dones[idx:idx+batch_size,:,:]
      
      states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      

      # if args.cl and batch%4 == 0:
      # if e < 200:
      if False:
        input_x = batch_x
      else:
        # if False:
        cl_input = batch_x[:,0,:output_size]
        input_x = np.zeros_like(batch_x)
        for j in range(seq_length):
          if args.stand:
            # if batch_done[0,j][0] > 0 or batch_done[1,j][0] > 0:
            # if batch_done[3,j][0] > 0:
            #   print(batch_done[0:4,j])
            #   print(states[0][0:4,:10], states[1][0:4,:10])
            #   print(states[0].shape)
            states = [states[0]*(1-batch_done[:,j]), states[1]*(1-batch_done[:,j])]
            # if batch_done[0,j][0] > 0 or batch_done[1,j][0] > 0:
            # if batch_done[3,j][0] > 0:
            #   print(states[0][0:4,:10], states[1][0:4,:10])

          cl_input = np.concatenate((cl_input.reshape(batch_size,output_size), batch_x[:,j,output_size:]),axis=1)
          input_x[:,j,:] = cl_input.reshape(batch_size,input_size)
          if args.delta:
            cl_input, states = sess.run([rnn.scaled_outputs, rnn.states],feed_dict={rnn.inputs:cl_input.reshape(batch_size,1,input_size), rnn.batch_state_in: states,rnn.batch_size:[batch_size]})
          else:
            cl_input, states = sess.run([rnn.scaled_outputs, rnn.states],feed_dict={rnn.inputs:cl_input.reshape(batch_size,1,input_size), rnn.batch_state_in: states,rnn.batch_size:[batch_size]})

      # print(input_x.shape, batch_y.shape)
      states = [np.zeros([batch_size,lstm_size]), np.zeros([batch_size,lstm_size])]      
      _, loss, train_states = sess.run([rnn.optimise, rnn.loss, rnn.states],feed_dict={rnn.inputs:input_x, rnn.labels:batch_y, rnn.batch_state_in: states,rnn.batch_size:[batch_size], rnn.keep_prob:0.75})
      epoch_loss.append(loss)
    # -----------------------------------------------------------
    # Validation step: both closed loop and open loop predictions
    # -----------------------------------------------------------
    val_loss = []
    val_batch_size = test_inputs.shape[0]
    cl_input = test_inputs[:,0,:output_size]
    cl_inputs = np.zeros_like(test_inputs)
    cl_predictions = np.zeros_like(test_labels)
    val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]
    for j in range(seq_length):
      if args.stand:
        val_states = [val_states[0]*(1-test_dones[:,j]), val_states[1]*(1-test_dones[:,j])]
        # val_states = [val_states[:,0]*(1-batch_done[:,j]), val_states[:,1]*(1-batch_done[:,j])]
      cl_input = np.concatenate((cl_input.reshape(val_batch_size,output_size), test_inputs[:,j,output_size:]),axis=1)
      cl_inputs[:,j,:] = cl_input.reshape(val_batch_size,input_size)
      if args.delta:
        cl_input, outputs, val_states = sess.run([rnn.scaled_outputs, rnn.outputs, rnn.states],feed_dict={rnn.inputs:cl_input.reshape(val_batch_size,1,input_size),rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
      else:
        cl_input, outputs, val_states = sess.run([rnn.scaled_outputs, rnn.outputs, rnn.states],feed_dict={rnn.inputs:cl_input.reshape(val_batch_size,1,input_size),rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
      cl_predictions[:,j,:] = cl_input.reshape(val_batch_size, output_size)
      # cl_predictions[:,j,:] = outputs.reshape(val_batch_size, output_size)
    # Get open loop predictions
    val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
    ol_predictions = sess.run(rnn.outputs,feed_dict={rnn.inputs:test_inputs, rnn.labels:test_labels, rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
    # Get closed loop loss
    val_states = [np.zeros([val_batch_size,lstm_size]), np.zeros([val_batch_size,lstm_size])]    
    loss = sess.run(rnn.loss,feed_dict={rnn.inputs:cl_inputs, rnn.labels:test_labels, rnn.batch_state_in: val_states,rnn.batch_size:[val_batch_size]})
    val_loss.append(loss)
    # -----------------------------------------------------------
    # Display epoch losses. Save best weights, periodically plot things
    # -----------------------------------------------------------
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    if np.mean(val_loss) < best_val_loss and e > 200:
      rnn.save(WEIGHTS_PATH + '/best/')
      val_idx = np.random.randint(0,test_labels.shape[0]-1)

      # print("saved with means, stds, and scalars:")
      # print(rnn.mean.eval(), rnn.std.eval(), rnn.scalar.eval())
      best_val_loss = np.mean(val_loss)
      cl_predictions = np.array(cl_predictions)
      if args.delta:
        scaled_labels = test_labels[val_idx,:,:] + test_inputs[val_idx,:,:output_size]
        # scaled_labels = test_labels[val_idx,:,:]/scale
        y1,y2,y3,y4,y5,y6 = scaled_labels[:,0],scaled_labels[:,1],scaled_labels[:,2],scaled_labels[:,3],scaled_labels[:,4],scaled_labels[:,5]
      else:
        y1,y2,y3,y4,y5,y6 = test_labels[val_idx,:,0],test_labels[val_idx,:,1],test_labels[val_idx,:,2],test_labels[val_idx,:,3],test_labels[val_idx,:,4],test_labels[val_idx,:,5]
      k1,k2,k3,k4,k5,k6 = cl_predictions[val_idx,:,0],cl_predictions[val_idx,:,1],cl_predictions[val_idx,:,2],cl_predictions[val_idx,:,3],cl_predictions[val_idx,:,4],cl_predictions[val_idx,:,5]
      q1,q2,q3,q4,q5,q6 = ol_predictions[val_idx,:,0],ol_predictions[val_idx,:,1],ol_predictions[val_idx,:,2],ol_predictions[val_idx,:,3],ol_predictions[val_idx,:,4],ol_predictions[val_idx,:,5]
      # subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3],[y4,k4,q4],[y5,k5,q5],[y6,k6,q6]], legend=[['target','closed loop pred', 'open loop pred']]*6,PATH=WEIGHTS_PATH + 'best_')
      subplot([[y1,k1],[y2,k2],[y3,k3],[y4,k4],[y5,k5],[y6,k6]], legend=[['target','predictions']]*6,PATH=WEIGHTS_PATH + 'best_')

    if e % 5 == 0:
      rnn.save(WEIGHTS_PATH)      
    if e % 20 == 0:
      val_idx = np.random.randint(0,test_labels.shape[0]-1)


      cl_predictions = np.array(cl_predictions)

      if args.delta:
        scaled_labels = test_labels[val_idx,:,:] + test_inputs[val_idx,:,:output_size]
        # scaled_labels = test_labels[val_idx,:,:]/scale
        y1,y2,y3,y4,y5,y6 = scaled_labels[:,0],scaled_labels[:,1],scaled_labels[:,2],scaled_labels[:,3],scaled_labels[:,4],scaled_labels[:,5]
      else:
        y1,y2,y3,y4,y5,y6 = test_labels[val_idx,:,0],test_labels[val_idx,:,1],test_labels[val_idx,:,2],test_labels[val_idx,:,3],test_labels[val_idx,:,4],test_labels[val_idx,:,5]
      k1,k2,k3,k4,k5,k6 = cl_predictions[val_idx,:,0],cl_predictions[val_idx,:,1],cl_predictions[val_idx,:,2],cl_predictions[val_idx,:,3],cl_predictions[val_idx,:,4],cl_predictions[val_idx,:,5]
      q1,q2,q3,q4,q5,q6 = ol_predictions[val_idx,:,0],ol_predictions[val_idx,:,1],ol_predictions[val_idx,:,2],ol_predictions[val_idx,:,3],ol_predictions[val_idx,:,4],ol_predictions[val_idx,:,5]
      # subplot([[y1,k1,q1],[y2,k2,q2],[y3,k3,q3],[y4,k4,q4],[y5,k5,q5],[y6,k6,q6]], legend=[['target','closed loop pred', 'open loop pred']]*6,PATH=WEIGHTS_PATH)
      subplot([[y1,k1],[y2,k2],[y3,k3],[y4,k4],[y5,k5],[y6,k6]], legend=[['target','predictions']]*6,PATH=WEIGHTS_PATH)
      # plot([test_inputs[0,:,:], test_labels[0,:,:], cl_inputs[0,:,:], cl_predictions[0,:,:], ol_predictions[0,:,:]], legend=['input', 'output', 'cl_input','closed loop pred', 'open loop pred'],PATH='/home/brendan/')
      # plot([test_inputs[0,:,:], cl_predictions[0,:,:], ol_predictions[0,:,:]], legend=['input', 'closed loop', 'open loop'],PATH='/home/brendan/')

