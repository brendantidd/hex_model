from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from baselines.common.distributions import make_pdtype
from baselines.common.mpi_running_mean_std import RunningMeanStd
from baselines.common.mpi_moments import mpi_moments
from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
import scripts.utils as U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from baselines import logger
import time

def get_vars(scope):
  return [x for x in tf.global_variables() if scope in x.name]

class Policy():
  def __init__(self, ob, im, ac, ob_space, im_size, ac_space, sess, vis,  args, normalize=True, hid_size=128):
    # with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
    #   self.model(name, ob, im, ob_space, im_size, ac_space, vis, hid_size)
    #   self.scope = tf.get_variable_scope().name

  # def model(self, name, ob, im, ob_space, im_size, ac_space, vis, hid_size):
    self.ob = ob
    self.im = im
    self.ac = ac
    self.sess = sess
    self.args = args
    self.doa = False
    self.pdtype = pdtype = make_pdtype(ac_space)
    self.vis = vis
    sequence_length = None
    if normalize:
      with tf.variable_scope("obfilter"):
          self.ob_rms = RunningMeanStd(shape=ob_space.shape)
    if vis:
      x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      x = U.flattenallbut0(x)
      self.vis_output = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin',
      kernel_initializer=U.normc_initializer(0.01)))
    if normalize:
      obz = tf.clip_by_value((ob - self.ob_rms.mean) / self.ob_rms.std, -5.0, 5.0)
    else:
      obz = ob
    with tf.variable_scope('vf'):
      last_out = obz
      if vis:
        last_out = tf.concat(axis=1,values=[last_out, self.vis_output])
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
      # self.pre_vpred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:,0]
      self.vpred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:,0]

    # if self.doa:
    #   with tf.variable_scope('doa'):
    #     last_out = obz
    #     if self.vis:
    #       x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
    #       x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
    #       x = U.flattenallbut0(x)
    #       x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin',
    #       kernel_initializer=U.normc_initializer(0.01)))          
    #       last_out = tf.concat(axis=1,values=[last_out, x])
    #     last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
    #     last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
    #     self.doa_pred = tf.sigmoid(tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0)))[:,0]
    # else:
    #   self.doa_pred = tf.constant([0.0])

    with tf.variable_scope('pol'):
      last_out = obz
      if vis:
        last_out = tf.concat(axis=1,values=[last_out, self.vis_output])
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc1', kernel_initializer=U.normc_initializer(1.0)))
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc2', kernel_initializer=U.normc_initializer(1.0)))
      self.mean = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01))
      # self.mean = tf.clip_by_value(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01)),-50,50)
      # self.mean = tf.nn.tanh(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01)))
      # logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer())
      # logstd = tf.clip_by_value(tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer()), -5.0, 0.0)
      # init = tf.constant_initializer([np.log(0.7)]*int(pdtype.param_shape()[0]//2))
      # init = tf.constant_initializer([np.log(1.0)]*int(pdtype.param_shape()[0]//2))
      # init = tf.constant_initializer([np.log(0.5)]*int(pdtype.param_shape()[0]//2))
      # logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2],initializer=init)
      logstd = tf.constant([np.log(0.5)]*int(pdtype.param_shape()[0]//2))
      # logstd = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=init)


    pdparam = tf.concat([self.mean, self.mean * 0.0 + logstd], axis=1)

    self.pd = pdtype.pdfromflat(pdparam)

    self.state_in = []
    self.state_out = []
    self.stochastic = tf.placeholder(dtype=tf.bool, shape=())
    self.action = U.switch(self.stochastic, self.pd.sample(), self.pd.mode())

    with tf.variable_scope('error'):
      last_out = obz
      last_out = tf.concat(axis=1,values=[last_out, tf.stop_gradient(self.ac)])
      if vis:
        last_out = tf.concat(axis=1,values=[last_out, self.vis_output])
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
      self.error = tf.nn.sigmoid(tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0)))[:,0]
      # self.vpred = self.pre_vpred*(1-self.error)

    # self.action = self.pd.sample()
    self.neglogp = self.pd.neglogp(self.action)
    # self.neglogp = self.pd.neglogp(self.ac)
    self.state = tf.constant([])
    self.initial_state = None


  def _evaluate(self, variables, observation, stochastic, im=None, ac=None, **extra_feed):
    sess = self.sess
    if self.vis:
      feed_dict = {self.ob: U.adjust_shape(self.ob, observation),self.im: im, self.stochastic:stochastic}
    else:
      if ac is not None:
        feed_dict = {self.ob: U.adjust_shape(self.ob, observation), self.stochastic:stochastic, self.ac:ac}
      else:
        feed_dict = {self.ob: U.adjust_shape(self.ob, observation), self.stochastic:stochastic}
    for inpt_name, data in extra_feed.items():
      if inpt_name in self.__dict__.keys():
        inpt = self.__dict__[inpt_name]
        if isinstance(inpt, tf.Tensor) and inpt._op.type == 'Placeholder':
          feed_dict[inpt] = U.adjust_shape(inpt, data)
    return sess.run(variables, feed_dict)

  def step(self, observation, image=None, stochastic=False, **extra_feed):
    if self.vis:
      a, v, state, neglogp = self._evaluate([self.action, self.vpred, self.state, self.neglogp], observation, stochastic, image, **extra_feed)
    else:
      if self.args.error:
        a = self._evaluate(self.action, observation, stochastic, **extra_feed)
        v, state, neglogp = self._evaluate([self.vpred, self.state, self.neglogp], observation, stochastic, ac=a, **extra_feed)
      else:
        a, v, state, neglogp = self._evaluate([self.action, self.vpred, self.state, self.neglogp], observation, stochastic, **extra_feed)
    if state.size == 0:
      state = None
    return a, v[0], state, neglogp[0]

  def value(self, ob, *args, **kwargs):
    return self._evaluate(self.vpred, ob, *args, **kwargs)


class Model(Base):
  def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,4], args=None, PATH=None, writer=None, hid_size=128, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=1024):
    self.max_timesteps = max_timesteps
    self.horizon = horizon
    self.learning_rate = lr
    self.env = env

    self.sess = sess = U.get_session()
    comm = MPI.COMM_WORLD
    self.name = name
    high = np.inf*np.ones(ac_size)
    low = -high
    ac_space = spaces.Box(low, high, dtype=np.float32)
    high = np.inf*np.ones(ob_size)
    low = -high
    ob_space = spaces.Box(low, high, dtype=np.float32)
    self.args = args   
    im_size = im_size
    self.PATH = PATH
    self.hid_size = hid_size
    self.writer = writer
    self.env = env
    ob = U.get_placeholder(name="ob_" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
    im = U.get_placeholder(name="im_" + name, dtype=tf.float32, shape=[None] + im_size)
    self.A = A = U.get_placeholder(name="ac_" + name, dtype=tf.float32, shape=[None, ac_size])
    # self.A = A = train_model.pdtype.sample_placeholder([None])

    self.args = args
    if ('vel' in name or 'hex' in name) and not args.vis:
      self.vis = False
    else:
      self.vis = vis
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      # May need some more params when using recurrent
      # act_model = Policy(ob, im, ob_space, im_size, ac_space, sess, self.vis, doa=True, normalize=normalize, hid_size=hid_size)
      train_model = Policy(ob, im, self.A, ob_space, im_size, ac_space, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size)
    Base.__init__(self)

    # CREATE THE PLACEHOLDERS
    self.ADV = ADV = tf.placeholder(tf.float32, [None])
    self.R = R = tf.placeholder(tf.float32, [None])
    # Keep track of old actor
    self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
    # Keep track of old critic
    self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
    self.LR = LR = tf.placeholder(tf.float32, [])
    # Cliprange
    self.CLIPRANGE = CLIPRANGE = tf.placeholder(tf.float32, [])

    neglogpac = train_model.pd.neglogp(A)

    # Calculate the entropy
    # Entropy is used to improve exploration by limiting the premature convergence to suboptimal policy.
    entropy = tf.reduce_mean(train_model.pd.entropy())

    # Get the predicted value
    vf_loss = 0.5*tf.reduce_mean(tf.square(train_model.vpred - R/(1-tf.clip_by_value(tf.math.abs(ADV),0,0.01))))
    # vf_loss = 0.5*tf.reduce_mean(tf.square(train_model.vpred - R))

    # Calculate ratio (pi current policy / pi old policy)
    ratio = tf.exp(OLDNEGLOGPAC - neglogpac)

    # Defining Loss = - J is equivalent to max J
    pg_losses = -ADV * ratio
    pg_losses2 = -ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)

    # Final PG loss
    pg_loss = tf.reduce_mean(tf.maximum(pg_losses, pg_losses2))
    approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - OLDNEGLOGPAC))
    clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), CLIPRANGE)))

    error_loss = 0.5*tf.reduce_mean(tf.square(train_model.error - tf.clip_by_value(tf.math.abs(ADV), 0, 1)))

    loss = pg_loss - entropy * ent_coef + vf_loss + error_loss

    if self.args.ae:
      # Link encoder and decoder
      # z, mean_, std_dev = encoder(input_batch)
      # output = decoder(z)

      # Reshape input and output to flat vectors
      flat_output = tf.reshape(train_model.dec_out, [-1, np.prod(im_size)])
      flat_input = tf.reshape(im, [-1, np.prod(im_size)])

      # img_loss = tf.reduce_sum(flat_input * -tf.log(flat_output) + (1 - flat_input) * -tf.log(1 - flat_output), 1)
      # Sum or mean?
      img_loss = tf.reduce_sum(tf.square(flat_output - flat_input), 1)
      latent_loss = 0.5 * tf.reduce_sum(tf.square(train_model.enc_mean) + tf.square(train_model.enc_std_dev) - tf.log(tf.square(train_model.enc_std_dev)) - 1, 1)
      self.ae_loss = tf.reduce_mean(img_loss + latent_loss)

    # UPDATE THE PARAMETERS USING LOSS
    # 1. Get the model parameters
    params = tf.trainable_variables(name)
    # 2. Build our trainer
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self.trainer = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)

    # 3. Calculate the gradients
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      grads_and_var = self.trainer.compute_gradients(loss, params)
    grads, var = zip(*grads_and_var)
    
    grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
      
    grads_and_var = list(zip(grads, var))
    # zip aggregate each gradient with parameters associated
    # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

    self.grads = grads
    self.var = var
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self._train_op = self.trainer.apply_gradients(grads_and_var)
    
    if self.args.ae:
      ae_params = get_vars(name + '/encoder') + get_vars(name + '/decoder')
      with tf.variable_scope("ae_" + name, reuse=tf.AUTO_REUSE):
        self.ae_trainer = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        ae_grads_and_var = self.ae_trainer.compute_gradients(self.ae_loss, ae_params)
        self.ae_train_op = self.ae_trainer.apply_gradients(ae_grads_and_var)
    
    std = tf.reduce_mean(train_model.pd.std)
    mean_ratio = tf.reduce_mean(ratio)
    mean_adv = tf.reduce_mean(self.ADV)
   
    self.loss_names = ['policy_loss', 'value_loss', 'error_loss', 'policy_entropy', 'approxkl', 
    'clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate']
    self.stats_list = [pg_loss, vf_loss, error_loss, entropy, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR]
  
    self.train_model = train_model
    # self.act_model = act_model
    # Not sure where weights for act_model and train_model are synced. For safety using train_model
    # self.step = train_model.step
    self.value = train_model.value
    self.init_buffer()

  def train(self, epoch, lr, cliprange, obs, imgs, returns, masks, actions, values, neglogpacs,  exp_actions=None, states=None):
    # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
    # Returns = R + yV(s')
    advs = returns - values
    # Normalize the advantages
    advs = (advs - advs.mean()) / (advs.std() + 1e-8)
    td_map = {
      self.train_model.ob : obs,
      self.A : actions,
      self.ADV : advs,
      self.R : returns,
      self.LR : lr,
      self.CLIPRANGE : cliprange,
      self.OLDNEGLOGPAC : neglogpacs,
      self.OLDVPRED : values
    }
    if self.vis:
      td_map[self.train_model.im] = imgs
  
    if states is not None:
      td_map[self.train_model.S] = states
      td_map[self.train_model.M] = masks

    return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]


  def train_ae(self, lr, im, epoch = 0):
    losses, outputs = self.sess.run([self.ae_loss, self.train_model.dec_out, self.ae_train_op], feed_dict={self.LR:lr, self.train_model.im:im})[:-1]
    # print("train ae output", losses, outputs.shape)
    if self.args.display_ae and self.rank == 0 and epoch == 0:
      import cv2
      for orig, i in zip(im, outputs):
        # print(orig.shape, i.shape)
        cv2.imshow('frame', np.hstack([orig, i]))
        cv2.waitKey(1)
    return losses

  def run_train(self, ep_rets, ep_lens):
    max_grad_norm=0.5
    self.train_model.ob_rms.update(self.data['ob'])
    self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
    # lr = lambda f:self.learning_rate*f
    # lrnow = lr(self.cur_lrmult)
    lrnow = self.learning_rate*self.cur_lrmult
    # lrnow = self.learning_rate
    self.lr = lrnow
    #**PPO2 doesn't seem to anneal clipping, may need to try with and without: works better without**#
    # cr = lambda f:0.2*f
    # if self.args.const_clip:
    #   # cliprangenow = cr(1.0)
    cliprangenow = 0.2
    # else:
      # cliprangenow = cr(self.cur_lrmult)
    # cliprangenow = 0.2*self.cur_lrmult
    inds = np.arange(self.n)
    for epoch in range(self.epochs):
      if self.enable_shuffle:
        np.random.shuffle(inds)
      self.loss = [] 
      for start in range(0, self.n, self.batch_size):
        end = start + self.batch_size
        mbinds = inds[start:end]
        slices = (self.data[key][mbinds] for key in self.training_input)
        # if self.args.ae and epoch in [0,1,2]:
        #   self.ae_losses = self.train_ae(lrnow/10, self.data['im'][mbinds], epoch)
        outs = self.train(epoch, lrnow, cliprangenow, *slices)
        self.loss.append(outs[:len(self.loss_names)])
    self.loss = np.mean(self.loss, axis=0)
    self.evaluate(ep_rets, ep_lens)
    self.init_buffer()

  def step(self, ob, im, stochastic=False, multi=False): 
    if not multi:
      actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
      return actions[0], values, self.states, neglogpacs
    else:
      actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
      return actions, values, self.states, neglogpacs        


  def get_advantage(self, last_value, last_done):    
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(self.data['rew'])
    lastgaelam = 0
    for t in reversed(range(len(self.data['rew']))):
      if t == len(self.data['rew']) - 1:
        nextnonterminal = 1.0 - last_done
        nextvalues = last_value
      else:
        nextnonterminal = 1.0 - self.data['done'][t+1]
        nextvalues = self.data['value'][t+1]
      delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    self.data['return'] = advs + self.data['value']
  
  def init_buffer(self):
    # Make sure reward is 'rew' and value/vpred is 'value', for get_advantage function in base.py
    # self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', 'roa_pred', 'roa_done']

    self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac']
    self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
    self.data = {t:[] for t in self.data_input}

  def log_stuff(self):
    # self.env.sample_delay
    pass

  def add_to_buffer(self, data):
    ''' data needs to be a list of lists, same length as self.data'''
    for d,key in zip(data, self.data):
      self.data[key].append(d)

  def finalise_buffer(self, data, last_value, last_done, last_roa_done=None, last_roa=None):
    ''' data must be dict'''
    for key in self.data_input:
      if key == 'done':
        self.data[key] = np.asarray(self.data[key], dtype=np.bool)
      else:
        self.data[key] = np.asarray(self.data[key])
    self.n = next(iter(self.data.values())).shape[0]
    for key in data:
      self.data[key] = data[key]
    self.get_advantage(last_value, last_done)
    if self.rank == 0:
      self.log_stuff()
