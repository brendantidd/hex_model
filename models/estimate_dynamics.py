'''
Load real data. Run in sim, varying dynamics params. Minimise cost of st_real - st_sim
'''
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import numpy as np
from pathlib import Path
home = str(Path.home())
import argparse
from collections import deque
import tensorboardX
import time
from mpi4py import MPI
comm = MPI.COMM_WORLD
from scripts.change_urdf import ChangeURDF
import pybullet as p

class Env():
  rank = comm.Get_rank()    
  ob_size = 7
  ac_size = 3
  im_size = [48,48,4]
  simtimeStep = 1/200
  # simtimeStep = 1/60
  actionRepeat = 1
  delay_length = 3
  episodes = 0

  total_reward = 0
  def __init__(self, render=False, PATH=None):
    self.change_urdf = ChangeURDF('assets/test_leg.urdf', debug=False, normal=True)

    self.args = args
    self.render = render
    self.PATH = PATH

    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot
    self.sim_data = []
    self.exp_sim_data = []

  def load_model(self):
    p.resetSimulation()
    p.loadMJCF("assets/ground.xml")
    urdf_path = "assets/temp_urdf/temp_" + self.args.exp + "_" + str(self.episodes+self.rank) + ".urdf"
    self.change_urdf.write_new_dynamics(urdf_path)
    self.episodes += 1
    self.Id = p.loadURDF(urdf_path)
    os.remove(urdf_path)
    # if self.episodes > 2:
    #   old_urdf_path = "assets/temp_urdf/temp_" + self.args.exp + "_" + str(self.episodes-3+self.rank) + ".urdf"
    #   os.remove(old_urdf_path)
    # objs = p.loadURDF("assets/temp_urdf/test_leg.urdf")
    # self.Id = objs
    objs = p.loadURDF("assets/test_leg.urdf")
    self.exp1_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)

    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      # print("link", j)
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    # self.motor_power =  [5, 10, 5]
    # self.vel_max = [8,11,8]
    self.vel_max = [8,8,8]
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}
    
    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self, exp_joints, exp_joint_vel):
    self.load_model()
    self.exp_joints, self.exp_joint_vel = exp_joints, exp_joint_vel
    self.coxa, self.femur, self.tibia = self.exp_joints
    self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel)
    self.set_position([0,1,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, robot_id=self.exp1_Id)
    
    self.speed = -1.0
    self.step_length = 0.5
    # Number of timesteps per step
    self.total_reward = 0
    self.steps = 0
    self.get_observation()
    self.save_sim_data()
    
    self.torque_history = deque([[0.0 for _ in range(self.ac_size)]]*self.delay_length, maxlen=self.delay_length)

    self.prev_state = self.joints + self.joint_vel
    
    return np.array(self.joints + self.joint_vel + [self.speed])

  def step(self, actions, exp_joints, exp_joint_vel):
    self.exp_joints, self.exp_joint_vel = exp_joints, exp_joint_vel


    self.input_torque = self.actions = actions
    # if self.delay:
    self.torque_history.append(self.input_torque)
    self.real_torque = self.torque_history[0]

    self.prev_state = self.joints + self.joint_vel
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.real_torque)
    self.set_position([0,1,2], [0,0,0,1], self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp1_Id)
    # print(self.actions)
    p.stepSimulation()
    if self.render:
      time.sleep(0.01)
    self.get_observation()
    self.record_sim_data()
    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.total_reward += reward
    self.steps += 1
    return np.array(self.joints + self.joint_vel + [self.speed]), reward, done, None

  def get_reward(self):
    reward = 0
    done = False
    if self.steps == 89990:
    # if self.steps == 10000:
    # if self.steps == 500:
      done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]

  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))       
        np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))       
        self.sim_data = []
        self.exp_sim_data = []
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)
    
    if len(self.sim_data) > 100000: return
    data = [[0,1,2], [0,0,0,1]]
    data.append(self.exp_joints)
    self.exp_sim_data.append(data)


if __name__=="__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--exp', default='test')
  parser.add_argument('--observed_torques',default=False, action='store_true')
  parser.add_argument('--replace_file',default=False, action='store_true')
  parser.add_argument('--std_limit',default=False, action='store_true')

  args = parser.parse_args()

  PATH = home + '/results/hex_model/latest/' + args.exp + '/'
  writer = tensorboardX.SummaryWriter(log_dir=PATH)

  # samples = np.load(home + '/data/iros_data/real/4_2/inputs.npy')
  # samples = np.load('samples/samples.npy')
  # samples = np.load(home + '/data/04_02_2020_clean/good_2020_02_04_19_18_59/inputs.npy')[1000:,:]
  # samples = np.load(home + '/data/iros_data/05_02_2020/baseline/inputs.npy')[1000:,:]
  # samples = np.load(home + '/data/06_02_2020/baseline/inputs.npy')[1000:,:]
  samples = np.load(home + '/data/10_02_2020/test_2020_02_10_10_50_29/inputs.npy')[400:,:]
  
  # samples = np.load(home + '/data/iros_data/real/6_2/inputs.npy')[600:,:]
  # samples = np.load(home + '/data/06_02_2020/test_2020_02_06_16_17_07/inputs.npy')[600:,:]
  # samples = np.load('samples/samples.npy')
  # samples = np.concatenate((samples, more_samples), axis=0)
  print(samples.shape)
  # samples = np.load(home + '/data/iros_data/real/3_2/inputs.npy')
  # samples = np.load('samples/samples.npy')
  # if args.observed_torques:
  torques = np.load(home + '/data/10_02_2020/test_2020_02_10_10_50_29/observed_torques.npy')[400:,:]
  # else:
  #   torques = samples[:,6:]
  print("samples", samples.shape, "torques", torques.shape)

  env = Env(render=args.render, PATH=PATH)

  print("----------------------")
  print("----stepping 1--------")
  print("----------------------")

  # sample_pointer = np.random.randint(torques.shape[0])
  sample_pointer = 0
  # sample_pointer = 600

  exp_joints, exp_joint_vel = samples[sample_pointer,:3], samples[sample_pointer,3:6]
  env.change_urdf.sample_normal()
  new_values = env.change_urdf.get_values()
  ob = env.reset(exp_joints, exp_joint_vel)
  error = 0
  steps = 0
  val_buffer = []
  error_buffer = []
  env.reward_breakdown = {'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100)}
  num_iters = 0
  while True:
    torque = torques[sample_pointer]
    error += np.sum(abs(np.array(exp_joints) - np.array(env.joints)))
    # error += np.mean((np.array(exp_joints) - np.array(env.joints))**2)
    # error += np.mean((np.array(exp_joint_vel) - np.array(env.joint_vel))**2)
    # error += np.mean((np.array(exp_joints)[:2] - np.array(env.joints)[:2])**2)
    # print(np.array(exp_joints)[:2], np.array(env.joints)[:2])
    ob, _, _, _ = env.step(torque, exp_joints, exp_joint_vel)

    sample_pointer += 1
    if sample_pointer > torques.shape[0] - 1:
      sample_pointer = 0
    exp_joints, exp_joint_vel = samples[sample_pointer,:3], samples[sample_pointer,3:6]
    steps += 1
    # print(sample_pointer)
    # if steps % 1000 == 0:
    if steps % 500 == 0:
      
      # Resample params
      env.change_urdf.sample_normal()
      new_values = env.change_urdf.get_values()
      val_buffer.append(new_values)
      # print(new_values)
      # print(steps, error)
      error_buffer.append(error)
      # print(env.delay_length, change_urdf.values['delay'])
      # print(new_values[-3:])
      # print(p.getDynamicsInfo(env.Id,1)[0],p.getDynamicsInfo(env.Id,2)[0], p.getDynamicsInfo(env.Id,3)[0])
      
      error = 0
      # if args.observed_torques:
      sample_pointer = np.random.randint(torques.shape[0] - 1000)
      # else:
      # sample_pointer = 0
      exp_joints, exp_joint_vel = samples[sample_pointer,:3], samples[sample_pointer,3:6]
      # ob = env.reset(exp_joints, exp_joint_vel)
      ob = env.reset(exp_joints, exp_joint_vel)


    # if steps > 20000:
    if steps > 30000:
      # zipped = zip(error_buffer, val_buffer)
      # print([z for z in zipped])
      # zipped = sorted(zipped, key=lambda x: x[0])
      # ent = np.sum(np.log(change_urdf.stds) + .5 * np.log(2.0 * np.pi * np.e), axis=-1)
      # print(ent)
      # error_buffer = np.array(error_buffer)/500 + 0.1*ent
      error_buffer = np.array(error_buffer)
      sorted_idx = np.argsort(error_buffer)
      print()
      print(np.mean(error_buffer), np.array(error_buffer).shape)
      print()
      writer.add_scalar("error", np.mean(error_buffer), num_iters)        
      
      top_vals = np.array(val_buffer)[sorted_idx[:20]]
      # print(top_vals)
      means = np.mean(top_vals, axis=0)
      # s = np.std(top_vals, axis=0)
      stds = np.std(top_vals, axis=0)
      # print(means)
      # print(s)
    
      # if num_iters < 30:
      env.change_urdf.set_params([means,stds], std_limit=args.std_limit)

      error = 0
      error_buffer = []
      steps = 0
      sample_pointer = 0
      exp_joints, exp_joint_vel = samples[sample_pointer,:3], samples[sample_pointer,3:6]
      env.change_urdf.get_means()
      new_values = env.change_urdf.get_values()
      if args.replace_file:
        if num_iters == 0:
          env.change_urdf.write_new_dynamics(PATH + 'new_urdf.urdf', load_from_origin=True)
        else:
          env.change_urdf.write_new_dynamics(PATH + 'new_urdf.urdf', load_from_origin=False)
      else:
        env.change_urdf.write_new_dynamics(PATH + 'new_urdf.urdf', load_from_origin=True)

      val_buffer = [new_values]
      ob = env.reset(exp_joints, exp_joint_vel)
      num_iters += 1
