'''
Base class for all networks.
For now includes training parameters..
'''
# import scripts.utils as U
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
from baselines import logger
from baselines.common.distributions import make_pdtype
import time
import numpy as np
from collections import deque
from mpi4py import MPI
 
class Base():
  epochs = 10
  batch_size = 32
  enable_shuffle = True
  episodes_so_far = 0
  timesteps_so_far = 0
  iters_so_far = 0
  best_reward = 0
  tstart = time.time()
  t1 = time.time()
  all_rewards = []
  lenbuffer = deque(maxlen=100) # rolling buffer for episode lengths
  rewbuffer = deque(maxlen=100) # rolling buffer for episode rewards
  
  def __init__(self):
    vars_with_adam = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_adam if 'Adam' not in v.name]
    # print("these are the variables for ", self.name)
    # print(self.vars)
    self.policy_saver = tf.train.Saver(var_list=self.vars)
    self.t1 = time.time()
    # if self.args.mpi_avail:
    #   from mpi4py import MPI


  def save(self):
    self.policy_saver.save(tf.get_default_session(), self.PATH + 'model.ckpt', write_meta_graph=False)
    print("Saving weights for " + self.name)

  def load(self, WEIGHT_PATH):
    self.policy_saver.restore(tf.get_default_session(), WEIGHT_PATH + 'model.ckpt')
    print("Loaded weights for " + self.name + " module")

  def evaluate(self, rews, lens):

    lrlocal = (lens, rews) # local values
    listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal) # list of tuples
    lens, rews = map(flatten_lists, zip(*listoflrpairs))
    # self.lenbuffer.extend(lens)
    # self.rewbuffer.extend(rews)

    if self.rank == 0:
      logger.log("********** Iteration %i ************"%self.iters_so_far)
      for loss, name in zip(self.loss, self.loss_names):
        logger.record_tabular(name, loss)
        self.writer.add_scalar(name, loss, self.iters_so_far)
      logger.record_tabular("LearningRate", self.lr)
      logger.record_tabular("EpRewMean", np.mean(rews))
      logger.record_tabular("EpLenMean", np.mean(lens))
      logger.record_tabular("EpThisIter", len(lens))
      logger.record_tabular("TimeThisIter", time.time() - self.t1)
      logger.record_tabular("TimeStepsSoFar", self.timesteps_so_far)
      logger.record_tabular("EnvTotalSteps", self.env.total_steps)
      # logger.record_tabular("EnvSampleDelay", self.env.sample_delay)
      # self.writer.add_scalar("sample_delay", self.env.sample_delay, self.iters_so_far)
      logger.record_tabular("Kc", self.env.Kc)
      self.writer.add_scalar("Kc", self.env.Kc, self.iters_so_far)
      self.t1 = time.time()
      self.writer.add_scalar("rewards", np.mean(rews), self.iters_so_far)
      self.writer.add_scalar("lengths", np.mean(lens), self.iters_so_far)
      self.all_rewards.append(np.mean(rews))
      np.save(self.PATH + 'rewards.npy',self.all_rewards)
      # print(np.mean(self.env.reward_breakdown['pos']),np.mean(self.env.reward_breakdown['vel']))
      self.writer.add_scalar("breakdown/pos", np.mean(self.env.reward_breakdown['pos']), self.iters_so_far)        
      self.writer.add_scalar("breakdown/tor", np.mean(self.env.reward_breakdown['tor']), self.iters_so_far)        
      self.writer.add_scalar("breakdown/vel", np.mean(self.env.reward_breakdown['vel']), self.iters_so_far)   
      self.writer.add_scalar("breakdown/tip", np.mean(self.env.reward_breakdown['tip']), self.iters_so_far)   
      self.writer.add_scalar("breakdown/neg", np.mean(self.env.reward_breakdown['neg']), self.iters_so_far)   
      try:
          # if np.mean(rews) > self.best_reward :
          self.save()
      except:
          print("Couldn't save training model")
      logger.dump_tabular()
    self.timesteps_so_far += sum(lens)     
    self.iters_so_far += 1


  def set_training_params(self, max_timesteps, learning_rate, horizon):
    self.max_timesteps = max_timesteps
    self.learning_rate = learning_rate
    self.horizon = horizon

  def log_stuff(self):
    pass

def flatten_lists(listoflists):
    return [el for list_ in listoflists for el in list_]