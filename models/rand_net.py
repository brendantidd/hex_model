# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
from scripts.utils import *

class Net():
  def __init__(self, input_size=1, output_size=1, max_actions=np.ones([1])):
  # def __init__(self, input_size, output_size, max_actions=np.array([7.5,7.5,2])): 
    self.input_size = input_size
    self.output_size = output_size
    self.max_actions = max_actions
    self.dense_size = 32
    self.sess = tf.get_default_session()
    self.inputs = tf.placeholder(tf.float32, [1,input_size])
    self.labels = tf.placeholder(tf.float32, [1,output_size])
    with tf.variable_scope("rand_net"):
      self.model()
    self.vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="rand_net/")
  
  def model(self):
    last_out = self.inputs
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.softsign)
    last_out = tf.layers.dense(last_out, self.dense_size, activation=tf.nn.softsign)
    # self.outputs = tf.layers.dense(last_out, self.output_size, activation=tf.nn.softsign, kernel_initializer=tf.random_normal_initializer)
    self.outputs = tf.layers.dense(last_out, self.output_size, activation=tf.nn.softsign, kernel_initializer=tf.random_uniform_initializer)
    self.scaled_outputs = self.outputs * self.max_actions
    
  def step(self, X):
    output = self.sess.run(self.scaled_outputs,feed_dict={self.inputs:X.reshape(1,self.input_size)})
    return output[0]

  def initialise(self):
    self.sess.run(tf.variables_initializer(self.vars))

if __name__=="__main__":

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  net = Net(9,3,np.array([10,10,1]))
  initialize()

  x = np.arange(9).reshape(1,9)
  print(x.shape)
  print(net.step(x))