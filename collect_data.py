'''
Launch from dsh_sim, run velocity command, save joint data
TODO: 
Add add noise to joints, 
Dynamic reconfigure step params,
option to load different ground plane
'''
from mpi4py import MPI
from pathlib import Path
import argparse
import subprocess 
import os
import time
import roslaunch 
import rospy
from std_srvs.srv import SetBool, Empty, Trigger
from std_msgs.msg import Bool
from geometry_msgs.msg import TwistStamped
import numpy as np
from sensor_msgs.msg import JointState, Imu
from gazebo_msgs.srv import SetModelState,SetModelConfiguration
from gazebo_msgs.msg import ModelState, ModelStates
from std_msgs.msg import Float64
from scripts.utils import *
from glob import glob   
import re 
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD
import pandas as pd

pause_sim = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
unpause_sim = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
  
def pause_unpause(pause=True):
  if pause:
    rospy.wait_for_service('/gazebo/pause_physics')
    pause_sim()
  elif not pause:
    rospy.wait_for_service('/gazebo/unpause_physics')
    unpause_sim()

class Env():
  rank = comm.Get_rank()
  total_rank = comm.Get_size()
  steps = 0
  ob_size = 68
  ac_size = 18 
  im_size = [60,40,1]
  timeStep = 1/60
  def __init__(self, args):
    self.DATA_PATH = home + '/data/'
    self.manual = args.manual
    self.test_with_gz = args.test_with_gz
    if self.test_with_gz:
      self.timeStep = 1/60
    if self.manual or self.test_with_gz:
      locomotion_engine = 'false'
      whole_body_control = 'false'
    else:
      locomotion_engine = 'true'
      whole_body_control = 'true'
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)

    self.robot_name = 'r' + str(1) 

    if args.render:
      cli_args = ['./launch/sim.launch', 'gazebo_gui:=true', 'robot_name:='+ self.robot_name,'locomotion_engine:='+locomotion_engine,'whole_body_control:='+whole_body_control, 'remap_actions:=false']
    else:
      cli_args = ['./launch/sim.launch', 'robot_name:='+ self.robot_name,'locomotion_engine:='+locomotion_engine,'whole_body_control:='+whole_body_control,'remap_actions:=false']
  
    print(cli_args)
    roslaunch_args = cli_args[1:]
    roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
    parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
    parent.start()
    print("ROSLAUNCH STARTED..")

    rospy.init_node(self.robot_name + '_rl_node', anonymous=False) 
    self.rate = rospy.Rate(60)

    time.sleep(2)
    if not (self.manual or self.test_with_gz):
      set_manual_velocity_override = rospy.ServiceProxy('/' + self.robot_name + '/locomotion_engine/command/manual_velocity_override', SetBool())
      rospy.wait_for_service('/' + self.robot_name + '/locomotion_engine/command/manual_velocity_override')
      response = set_manual_velocity_override(True)
      self.manual_vel_pub = rospy.Publisher('/' + self.robot_name + '/locomotion_engine/manual_velocity_command', TwistStamped, queue_size=1)
  
    self.stamped_msg = self.get_speed()

    self.motor_names = ['AL_coxa_joint','AL_femur_joint','AL_tibia_joint']
    self.motor_names += ['AR_coxa_joint','AR_femur_joint','AR_tibia_joint']
    self.motor_names += ['BL_coxa_joint','BL_femur_joint','BL_tibia_joint']
    self.motor_names += ['BR_coxa_joint','BR_femur_joint','BR_tibia_joint']
    self.motor_names += ['CL_coxa_joint','CL_femur_joint','CL_tibia_joint']
    self.motor_names += ['CR_coxa_joint','CR_femur_joint','CR_tibia_joint']
    self.joints = {m:{'pos':0.0,'vel':0.0,'effort':0.0} for m in self.motor_names}
    self.initialise_subscribers()
    self.initialise_joint_subscribers()
    self.samples = []
    self.inputs = []
    self.labels = []
    self.num_saved_samples = 0
    self.joint_effort = [0.0]*self.ac_size
    if self.manual or self.test_with_gz:  
      self.exp_samples = load_data()
      print(self.exp_samples.shape)
      self.sample_pointer = 0
    self.joint_publishers = {}
    for joint in self.motor_names:
      self.joint_publishers[joint] = rospy.Publisher('/' + self.robot_name + '/' + joint + '/command', Float64, queue_size=1)
    time.sleep(2)
    self.t_loop = time.time()
    if self.test_with_gz:
      self.motor_power =  [15, 22, 15]
      self.motor_power += [15, 22, 15]
      self.motor_power += [15, 22, 15]
      self.motor_power += [15, 22, 15]
      self.motor_power += [15, 22, 15]
      self.motor_power += [15, 22, 15]

  def reset(self):
    self.steps = 0
    self.prev_vx = 0
    self.prev_vy = 0
    self.prev_vz = 0
    self.speed = 0
    self.time_of_step = 0
    self.pause_time = 100
    self.ob_dict = {}

    set_joints = rospy.ServiceProxy('/gazebo/set_model_configuration', SetModelConfiguration())
    smc = SetModelConfiguration()
    smc.model_name = '_' + self.robot_name
    smc.urdf_param_name = ''

    # Starting not from the ground was slower??
    smc.joint_names = ['AL_coxa_joint', 'AL_femur_joint', 'AL_tibia_joint']
    smc.joint_positions = [-0.4, 0.0, 1.5]
    smc.joint_names += ['BL_coxa_joint', 'BL_femur_joint', 'BL_tibia_joint']
    smc.joint_positions += [0.0, 0.0, 1.5]
    smc.joint_names += ['CL_coxa_joint', 'CL_femur_joint', 'CL_tibia_joint']
    smc.joint_positions += [0.4, 0.0, 1.5]
    smc.joint_names += ['AR_coxa_joint', 'AR_femur_joint', 'AR_tibia_joint']
    smc.joint_positions += [0.4, 0.0, 1.5]
    smc.joint_names += ['BR_coxa_joint', 'BR_femur_joint', 'BR_tibia_joint']
    smc.joint_positions += [0.0, 0.0, 1.5]
    smc.joint_names += ['CR_coxa_joint', 'CR_femur_joint', 'CR_tibia_joint']
    smc.joint_positions += [-0.4, 0.0, 1.5]
    
    # print("waiting for set model config")
    rospy.wait_for_service('/gazebo/set_model_configuration')
    try:
      response_joints = set_joints(smc.model_name, smc.urdf_param_name, smc.joint_names, smc.joint_positions)
    except:
      print("Failed to reset joints")
  
    set_model = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState())
    sms = ModelState()
    sms.model_name = "_" + self.robot_name
    sms.pose.position.y = 0.0
    sms.pose.position.z = 0.4
    rospy.wait_for_service('/gazebo/set_model_state')
    try:
      response_model = set_model(sms)
    except: 
      print("Failed to reset pose")

    return self.get_observation()

  def step(self, actions=None):
    # print(self.speed)
    if self.steps > self.pause_time:
      self.speed = 0.5
      self.time_of_step = int(-33*self.speed + 35)
      # self.time_of_step = 0.0
    if len(self.samples) > 10000 and not self.test_with_gz: 
      self.num_saved_samples += 1
      if not self.manual:
        self.save_samples()
      self.samples = []  
      self.steps = 0   
      exit()
    if self.num_saved_samples > 20:
      exit()
    if self.manual:
      targets = self.restore_sample(self.exp_samples[self.sample_pointer])
      self.sample_pointer += 1
      self.publish_actions(targets)    
    elif self.test_with_gz:
      # new_actions = actions
      new_actions = []
      new_actions.extend(actions[3:6])
      new_actions.extend(actions[:3])
      new_actions.extend(actions[9:12])
      new_actions.extend(actions[6:9])
      new_actions.extend(actions[15:18])
      new_actions.extend(actions[12:15])
      # print(new_actions)
      torques = [0.] * len(self.motor_names)
      for m, name in enumerate(self.motor_names):
          torques[m] = new_actions[m]*self.motor_power[m]
      # print(torques)
      self.publish_actions(torques)    
    else:
      self.manual_vel_pub.publish(self.stamped_msg)  
      # self.publish_actions(torques)    
    t2 = time.time()

    try:
      self.targets = [self.target_joint_effort[m] for m in self.motor_names]
    except:
      self.targets = None
    if not args.manual and self.targets is not None:
      self.publish_actions(self.targets)    
    pause_unpause(pause=False)    
    t1 = time.time()
    # # Ensure we wait for 1 iteration: 500Hz 
    while time.time() - t1 < self.timeStep:
      time.sleep(0.00001)    
    pause_unpause(pause=True)
    sample = self.get_sample()
    if sample is not None:
      self.samples.append(sample)
      self.inputs.append(self.input)
      self.labels.append(self.label)
      print(len(self.samples))
    self.steps += 1  
    if self.steps > 10000:
      done = True
    else:
      done = False
    return self.get_observation(), 0,done,0

  def publish_actions(self, actions):
    for n,m in enumerate(self.motor_names):
      self.joint_publishers[m].publish(actions[n])
  
  def get_sample(self):
    '''
    This is where we decide what we data we want
    '''  
    try:
      # data = []
      # for n, m in enumerate(self.motor_names):
      #   data.append([self.joints[m]['pos'], self.joints[m]['vel'], self.joints[m]['effort'], self.target_joint_effort[m]])
      # return np.array(data)
      #       
      if self.targets is None:
        # print("targets equal none")
        return None
      # print(self.targets)
      joint_pos = [self.joints[m]['pos'] for m in self.motor_names]
      joint_vel = [self.joints[m]['vel'] for m in self.motor_names]
      joint_effort = [self.joints[m]['effort'] for m in self.motor_names]
      base_pos = [self.pos.x, self.pos.y, self.pos.z]
      base_orn = [self.orn.x, self.orn.y, self.orn.z, self.orn.w]
      base_vel = [self.twist_lin.x, self.twist_lin.y, self.twist_lin.z]
      base_ang_vel = [self.twist_ang.x, self.twist_ang.y, self.twist_ang.z]
      self.input = self.targets
      self.label = joint_effort
      return np.array(joint_pos + joint_vel + joint_effort + base_pos + base_orn + base_vel + base_ang_vel + self.targets + joint_effort)
      # return np.array(joint_effort + self.targets)
    except Exception as e:
      # print(e)
      return None

  def save_samples(self):
    folders = glob(self.DATA_PATH + "*/")
    latest = [f.split('/') for f in folders]
    latest = [q for l in latest for q in l if q not in home]
    l = ''
    for item in latest:
      l += item
    nums = re.findall('(\d+)',l)
    nums = [int(n) for n in nums]
    if nums:
      data_num = max(nums) + 1
    else:
      data_num = 0
    if not os.path.exists(self.DATA_PATH + str(data_num)):
      os.makedirs(self.DATA_PATH + 'data' + str(data_num))
    print("saving:", np.array(self.samples).shape)
    np.save(self.DATA_PATH + 'data' + str(data_num) + '/samples.npy', np.array(self.samples))
    df = pd.DataFrame (np.array(self.inputs))
    df.to_excel(self.DATA_PATH + 'data' + str(data_num) + '/inputs.xlsx', index=False)
    df = pd.DataFrame (np.array(self.labels))
    df.to_excel(self.DATA_PATH + 'data' + str(data_num) + '/labels.xlsx', index=False)

  def get_speed(self):
    stamped_msg = TwistStamped()
    stamped_msg.header.stamp = rospy.get_rostime()
    new_speed = np.random.uniform(0.4, 1.0)
    stamped_msg.twist.linear.x = new_speed
    return stamped_msg

  def initialise_subscribers(self):
    print("initialising subscribers for ", self.robot_name)
    rospy.Subscriber('/' + self.robot_name + '/joint_states', JointState, self.jointStateCallback)
    rospy.Subscriber('/' + self.robot_name + '/imu/data', Imu, self.imuCallback)
    rospy.Subscriber('/gazebo/model_states', ModelStates, self.modelStatesCallback)

  def jointStateCallback(self, data):
    for i,n in enumerate(data.name):
      self.joints[n]['pos'] = data.position[i]
      self.joints[n]['vel'] = data.velocity[i]
      # This is for handling Gazebo sometimes spitting out 0.0 effort 
      if not (data.effort[i] < 0.00001 and data.effort[i] > -0.00001):
        self.joints[n]['effort'] = data.effort[i]
    
  def imuCallback(self, data):
    self.imu_orn = data.orientation 
    self.ang_vel = data.angular_velocity
    self.lin_acc = data.linear_acceleration

  def modelStatesCallback(self, data):
    if "_" + self.robot_name in data.name:
      robot_idx = data.name.index("_" + self.robot_name)
      self.pos = data.pose[robot_idx].position
      self.orn = data.pose[robot_idx].orientation
      self.twist_lin = data.twist[robot_idx].linear
      self.twist_ang = data.twist[robot_idx].angular

  # Callbacks for torque controller ============================================
  def initialise_joint_subscribers(self):
    self.target_joint_effort = {}   
    rospy.Subscriber('/' + self.robot_name + '_exp/AL_coxa_joint/command/', Float64, self.AL_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/AR_coxa_joint/command/', Float64, self.AR_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/BL_coxa_joint/command/', Float64, self.BL_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/BR_coxa_joint/command/', Float64, self.BR_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/CL_coxa_joint/command/', Float64, self.CL_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/CR_coxa_joint/command/', Float64, self.CR_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/AL_femur_joint/command/', Float64, self.AL_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/AR_femur_joint/command/', Float64, self.AR_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/BL_femur_joint/command/', Float64, self.BL_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/BR_femur_joint/command/', Float64, self.BR_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/CL_femur_joint/command/', Float64, self.CL_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/CR_femur_joint/command/', Float64, self.CR_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/AL_tibia_joint/command/', Float64, self.AL_tibiaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/AR_tibia_joint/command/', Float64, self.AR_tibiaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/BL_tibia_joint/command/', Float64, self.BL_tibiaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/BR_tibia_joint/command/', Float64, self.BR_tibiaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/CL_tibia_joint/command/', Float64, self.CL_tibiaCallback)
    rospy.Subscriber('/' + self.robot_name + '_exp/CR_tibia_joint/command/', Float64, self.CR_tibiaCallback)

  def AL_coxaCallback(self, data):
    self.target_joint_effort['AL_coxa_joint'] = data.data

  def AR_coxaCallback(self, data):
    self.target_joint_effort['AR_coxa_joint'] = data.data

  def BL_coxaCallback(self, data):
    self.target_joint_effort['BL_coxa_joint'] = data.data

  def BR_coxaCallback(self, data):
    self.target_joint_effort['BR_coxa_joint'] = data.data

  def CL_coxaCallback(self, data):
    self.target_joint_effort['CL_coxa_joint'] = data.data

  def CR_coxaCallback(self, data):
    self.target_joint_effort['CR_coxa_joint'] = data.data

  def AL_femurCallback(self, data):
    self.target_joint_effort['AL_femur_joint'] = data.data

  def AR_femurCallback(self, data):
    self.target_joint_effort['AR_femur_joint'] = data.data

  def BL_femurCallback(self, data):
    self.target_joint_effort['BL_femur_joint'] = data.data

  def BR_femurCallback(self, data):
    self.target_joint_effort['BR_femur_joint'] = data.data

  def CL_femurCallback(self, data):
    self.target_joint_effort['CL_femur_joint'] = data.data

  def CR_femurCallback(self, data):
    self.target_joint_effort['CR_femur_joint'] = data.data

  def AL_tibiaCallback(self, data):
    self.target_joint_effort['AL_tibia_joint'] = data.data

  def AR_tibiaCallback(self, data):
    self.target_joint_effort['AR_tibia_joint'] = data.data

  def BL_tibiaCallback(self, data):
    self.target_joint_effort['BL_tibia_joint'] = data.data

  def BR_tibiaCallback(self, data):
    self.target_joint_effort['BR_tibia_joint'] = data.data

  def CL_tibiaCallback(self, data):
    self.target_joint_effort['CL_tibia_joint'] = data.data

  def CR_tibiaCallback(self, data):
    self.target_joint_effort['CR_tibia_joint'] = data.data

  def restore_sample(self, sample, expert=False):
    if expert:
      y_offset = 2
    else:
      y_offset = 0
    state = [m + '_pos' for m in self.motor_names]
    state += [m + '_vel' for m in self.motor_names]
    state += ['x','y','z']
    state += ['qx','qy','qz','qw']
    state += ['vx','vy','vz']
    state += ['v_roll','v_pitch','v_yaw']
    state += [m + '_targ' for m in self.motor_names]
    sample_dict = {name: val for name, val in zip(state, sample)}

    joints = [sample_dict[j + '_pos'] for j in self.motor_names]
    joint_vel = [sample_dict[j + '_vel'] for j in self.motor_names]
    pos = [sample_dict['x'], sample_dict['y'] + y_offset, sample_dict['z']+ 0.07]
    orn = [sample_dict['qx'], sample_dict['qy'], sample_dict['qz'], sample_dict['qw']]
    base_lin_vel = [sample_dict['vx'], sample_dict['vy'], sample_dict['vz']]
    base_ang_vel = [sample_dict['v_roll'], sample_dict['v_pitch'], sample_dict['v_yaw']]
    base_vel = base_lin_vel + base_ang_vel
    return [sample_dict[j + '_targ'] for j in self.motor_names]

  def get_observation(self):
    state = [  "AR_coxa_joint_pos","AR_femur_joint_pos","AR_tibia_joint_pos",
                "AL_coxa_joint_pos","AL_femur_joint_pos","AL_tibia_joint_pos",
                "BR_coxa_joint_pos","BR_femur_joint_pos","BR_tibia_joint_pos",
                "BL_coxa_joint_pos","BL_femur_joint_pos","BL_tibia_joint_pos",
                "CR_coxa_joint_pos","CR_femur_joint_pos","CR_tibia_joint_pos",
                "CL_coxa_joint_pos","CL_femur_joint_pos","CL_tibia_joint_pos"]
    state += [ "AR_coxa_joint_vel","AR_femur_joint_vel","AR_tibia_joint_vel",
                "AL_coxa_joint_vel","AL_femur_joint_vel","AL_tibia_joint_vel",
                "BR_coxa_joint_vel","BR_femur_joint_vel","BR_tibia_joint_vel",
                "BL_coxa_joint_vel","BL_femur_joint_vel","BL_tibia_joint_vel",
                "CR_coxa_joint_vel","CR_femur_joint_vel","CR_tibia_joint_vel",
                "CL_coxa_joint_vel","CL_femur_joint_vel","CL_tibia_joint_vel"]
    state += [ "AR_coxa_joint_effort","AR_femur_joint_effort","AR_tibia_joint_effort",
                "AL_coxa_joint_effort","AL_femur_joint_effort","AL_tibia_joint_effort",
                "BR_coxa_joint_effort","BR_femur_joint_effort","BR_tibia_joint_effort",
                "BL_coxa_joint_effort","BL_femur_joint_effort","BL_tibia_joint_effort",
                "CR_coxa_joint_effort","CR_femur_joint_effort","CR_tibia_joint_effort",
                "CL_coxa_joint_effort","CL_femur_joint_effort","CL_tibia_joint_effort"]
    state += ["left_swing", "right_swing"]
    state += ['qx','qy','qz','qw']
    state += ['ax','ay','az']
    state += ['roll_vel','pitch_vel','yaw_vel']
    state += ['com_z']

    self.ob_dict.update({m+'_pos':self.joints[m]['pos'] for m in self.motor_names})
    self.ob_dict.update({m+'_vel':self.joints[m]['vel'] for m in self.motor_names})
    self.ob_dict.update({m+'_effort':self.joints[m]['effort'] for m in self.motor_names})
    self.ob_dict.update({'qx':self.orn.x, 'qy':self.orn.y,'qz':self.orn.z,'qw':self.orn.w}) 
    # self.ob_dict.update({'ax':self.imu.lin_acc.x, 'ay':self.imu.lin_acc.y,'az':self.imu.lin_acc.z}) 
    self.ob_dict.update({'roll_vel':self.twist_ang.x, 'pitch_vel':self.twist_ang.y,'yaw_vel':self.twist_ang.z}) 
    self.ob_dict['com_z'] = self.pos.z
    self.ob_dict['ax'] = (self.twist_lin.x - self.prev_vx)/(self.timeStep)
    self.ob_dict['ay'] = (self.twist_lin.y - self.prev_vy)/(self.timeStep)
    self.ob_dict['az'] = (self.twist_lin.z - self.prev_vz)/(self.timeStep)
    self.prev_vx = self.twist_lin.x
    self.prev_vy = self.twist_lin.y
    self.prev_vz = self.twist_lin.z
    if self.speed == 0:
      self.ob_dict['left_swing'] = False
      self.ob_dict['right_swing'] = False
    elif self.speed > 0 and self.steps > self.pause_time and (self.steps - self.pause_time) % self.time_of_step == 0:
      if self.ob_dict['left_swing']:
        self.ob_dict['left_swing'] = False
        self.ob_dict['right_swing'] = True
      else:
        self.ob_dict['left_swing'] = True
        self.ob_dict['right_swing'] = False
    return np.array([self.ob_dict[s] for s in state] + [self.speed])


if __name__=="__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('--no_gpu', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')  
  parser.add_argument('--manual', default=False, action='store_true')  
  parser.add_argument('--test_with_gz', default=False, action='store_true')  
  args = parser.parse_args()
  
  if args.no_gpu:
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  
  env = Env(args)
  env.reset()
  while not rospy.is_shutdown():
    env.step()
