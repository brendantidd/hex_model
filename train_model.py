import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
from utils import *
from glob import glob   
import re 
import tensorboardX
import time
from pathlib import Path
home = str(Path.home())

class Model():
  def __init__(self, input_size, PATH):
    self.PATH = PATH
    # Place holders for input and label
    self.input = tf.placeholder(dtype=tf.float32, shape=(None,input_size))
    self.label = tf.placeholder(dtype=tf.float32, shape=(None,))
    # Network
    with tf.variable_scope('main'):
      x = tf.nn.softsign(tf.layers.dense(self.input, 32, name="fc1"))
      x = tf.nn.softsign(tf.layers.dense(x, 32, name="fc2"))
      x = tf.nn.softsign(tf.layers.dense(x, 32, name="fc3"))
      self.output = tf.layers.dense(x, 1, name="final")
    # MSE loss
    self.loss = 0.5 * tf.reduce_mean((self.label - self.output)**2)
    # Optimizer
    self.optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
    self.vars = [x for x in tf.global_variables() if 'main' in x.name]
    self.train_op = self.optimizer.minimize(self.loss, var_list=self.vars)
    # Initialize stuff
    self.policy_saver = tf.train.Saver(var_list=self.vars)
    self.sess = tf.Session()
    self.sess.run(tf.global_variables_initializer())

  def step(self, x):
    return self.sess.run(self.output, feed_dict = {self.input: x})

  def train(self, x, y):
    feed_dict = {self.input: x, self.label: y}
    return self.sess.run([self.loss, self.train_op], feed_dict)

  def save(self):
    self.policy_saver.save(self.sess, self.PATH + 'model.ckpt')

  def load(self, WEIGHT_PATH):
    self.policy_saver.restore(self.sess, WEIGHT_PATH + 'model.ckpt')
    print("Loaded weights")

def load_data():
  '''
  Load all the data that is labelled data+num under ~/data/
  '''
  DATA_PATH = home + '/data/'
  folders = glob(DATA_PATH + "*/")
  latest = [f.split('/') for f in folders]
  latest = [q for l in latest for q in l if q not in home]
  l = ''
  for item in latest:
    l += item
  nums = re.findall('(\d+)',l)
  nums = [int(n) for n in nums]
  print("loading data from folder: ", nums)
  data = []
  for num in nums:
    data.extend(np.load(DATA_PATH + 'data' + str(num) + '/samples.npy', allow_pickle=True))
  data = np.array(data)
  
  return data[:,:-1], data[:,-1]

if __name__=="__main__":
  
  parser = argparse.ArgumentParser()
  parser.add_argument('--no_gpu', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  args = parser.parse_args()

  if args.no_gpu:
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"

  PATH = home + '/save/'
  writer = tensorboardX.SummaryWriter(log_dir=PATH)

  motor_names = ['AL_coxa_joint','AL_femur_joint','AL_tibia_joint']
  motor_names += ['AR_coxa_joint','AR_femur_joint','AR_tibia_joint']
  motor_names += ['BL_coxa_joint','BL_femur_joint','BL_tibia_joint']
  motor_names += ['BR_coxa_joint','BR_femur_joint','BR_tibia_joint']
  motor_names += ['CL_coxa_joint','CL_femur_joint','CL_tibia_joint']
  motor_names += ['CR_coxa_joint','CR_femur_joint','CR_tibia_joint']

  input_size = 2
  model = Model(input_size, PATH)
  inputs, labels = load_data()
  print("Total data shape", inputs.shape, labels.shape)
  
  if args.plot:
    # This plot is just to verify the data visually 
    idx = [x for x in range(1500*18,1750*18,18)]
    idx2 = [x+1 for x in range(1500*18,1750*18,18)]
    idx3 = [x+2 for x in range(1500*18,1750*18,18)]
    # subplot([[inputs[idx,2], labels[idx]],[inputs[idx2,2], labels[idx2]],[inputs[idx3,2], labels[idx3]]])
    plot([inputs[idx,2], labels[idx]])
    # exit()

  np.random.seed(42)
  idx = [i for i in range(len(inputs))]
  np.random.shuffle(idx)
  train_idx = idx[:int(0.9*len(idx))]
  val_idx = idx[int(0.9*len(idx)):]

  batch_size = 512
  num_epochs = 100
  losses = []
  t1 = time.time()
  count = 0
  for epoch in range(num_epochs):
    for b in range(0, len(train_idx), batch_size):
      loss = model.train(inputs[train_idx[b:b+batch_size]], labels[train_idx[b:b+batch_size]])
      losses.append(loss[0])
    predictions = model.step(inputs)
    val_loss = np.mean((predictions.reshape(-1,)[val_idx] - labels[val_idx])**2)
    print("loss %.4f val %.4f t: %.4f  epoch %.4d" % (np.mean(losses), val_loss, time.time()-t1, epoch))
    writer.add_scalar("loss/loss", np.mean(losses), count)
    writer.add_scalar("loss/val", val_loss, count)
    count += 1
    t1 = time.time()
    model.save()
