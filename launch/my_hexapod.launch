<!-- -*- xml -*- -->

<launch>

  <env name="ROSCONSOLE_FORMAT" value="[${severity}] [${node}]: ${message}"/>

  <!-- Arguments -->
  <arg name="robot_name" default="r1"/>
  <arg name="init_pose" default="-x 0.0 -y 0.0 -z 0.1 -R 0.0 -P 0.0 -Y 0.0"/>

  <arg name="output" default="screen"/>

  <!-- Input joystick device for teleop node -->
  <arg name="joy_dev" default="/dev/input/js0"/>

  <!-- Locomotion engine arguments -->
  <arg name="locomotion_engine" default="true"/>
  <arg name="use_default_run_parameters" default="true"/>
  <arg name="debug_locomotion_engine" default="false"/>

  <!-- Leg controller arguments -->
  <arg name="whole_body_control" default="true"/>
  <arg name="dynamic_sim" default="true"/>

  <!-- Simulation arguments -->
  <arg name="simulation" default="true"/>
  <arg name="spawn_config" default="grounded_body"/>
  <arg name="fixed" default="false"/>
  <arg name="spin_rate" default="1000"/>

  <group ns="$(arg robot_name)">
    <remap from="/tf" to="tf"/>
    <remap from="/tf_static" to="tf_static"/>

    <!-- DSH1 Model -->
    <group if="$(arg simulation)">
      <include file="$(find darpa_subt_hexapod)/launch/apptronik_minimal_gazebo.launch">
        <arg name="dynamic_sim" value="$(arg dynamic_sim)"/>
        <arg name="enable_lights" value="false"/>
        <arg name="enable_world_odom_br" value="false"/>
        <arg name="fixed" value="$(arg fixed)"/>
        <arg name="gen_payload" value="false"/>
        <arg name="init_pose" value="$(arg init_pose)"/>
        <arg name="launch_scenario" value="false"/>
        <arg name="robot_name" default="$(eval '_'+arg('robot_name') )"/>
        <arg name="spawn_config" value="$(arg spawn_config)"/>
      </include>
    </group>

    <!-- Locomotion Engine -->
    <group if="$(arg locomotion_engine)">
      <param name="locomotion_engine/robot_description" type="str"
	      command="$(find xacro)/xacro $(find darpa_subt_hexapod)/robots/apptronik_minimal.xacro gen_payload:=false"/>
      <rosparam file="$(find locomotion_engine_node)/config/gait_definitions.yaml" command="load"/>
      <rosparam file="$(find darpa_subt_hexapod)/config/DSH1_whole_body_control.yaml" command="load"/>
      <rosparam file="$(find darpa_subt_hexapod)/config/DSH1_locomotion_engine.yaml" subst_value="True" command="load"/>
      <include file="$(find leg_go_teleop)/launch/leg_go_teleop.launch">
        <arg name="joy_dev" value="$(arg joy_dev)"/>
      </include>
      <group if="$(arg debug_locomotion_engine)">
        <param name="locomotion_engine/gait_generator/auto_step_frequency" value="false"/>
        <param name="locomotion_engine/force_zero_load_capacity" value="true" if="$(arg fixed)"/>
        <param name="locomotion_engine/debug_enabled" value="true"/>
        <param name="locomotion_engine/debug_topic_publish_rate/default" value="50"/>
        <param name="locomotion_engine/visualisation_update_rate/default" value="50"/>
        <param name="locomotion_engine/workspace_visualisation_update_period/default" value="5"/>
        <node name="rviz_locomotion" pkg="rviz" type="rviz" respawn="true" args="-d $(find locomotion_engine_node)/config/visualise.rviz">
          <remap from="/tf" to="tf"/>
          <remap from="/tf_static" to="tf_static"/>
        </node>
      </group>
      <param name="locomotion_engine/auto_start" value="walk" if="$(arg simulation)"/>
      <node name="locomotion_engine_node" pkg="locomotion_engine_node" type="locomotion_engine_node" output="$(arg output)"/>
    </group>

    <!-- Whole Body Control -->
    <group if="$(arg whole_body_control)">
      <node name="whole_body_control_node" pkg="whole_body_control_node" type="whole_body_control_node" output="screen">
        <param name="spin_rate" value="$(arg spin_rate)"/>
        <param name="control_params_filename" value="$(find darpa_subt_hexapod)/config/DSH1_whole_body_control.yaml"/>
        <param name="load_urdf_from_file" value="false"/>
        <param name="gazebo_simulation" value="$(arg simulation)"/>
        <param name="dynamic_gazebo" value="$(arg dynamic_sim)"/>
        <param name="no_imu" value="false"/>
        <param name="debug_enabled" value="false"/>
        <param name="debug_publish_rate" value="50"/>
        <param name="urdf_string" type="str"
          command="$(find xacro)/xacro $(find darpa_subt_hexapod)/robots/apptronik_minimal.xacro
            gen_payload:=false"/>
        <remap from="/$(arg robot_name)/AR_coxa_joint/command" to="/$(arg robot_name)_exp/AR_coxa_joint/command"/>
        <remap from="/$(arg robot_name)/BR_coxa_joint/command" to="/$(arg robot_name)_exp/BR_coxa_joint/command"/>
        <remap from="/$(arg robot_name)/CR_coxa_joint/command" to="/$(arg robot_name)_exp/CR_coxa_joint/command"/>
        <remap from="/$(arg robot_name)/AL_coxa_joint/command" to="/$(arg robot_name)_exp/AL_coxa_joint/command"/>
        <remap from="/$(arg robot_name)/BL_coxa_joint/command" to="/$(arg robot_name)_exp/BL_coxa_joint/command"/>
        <remap from="/$(arg robot_name)/CL_coxa_joint/command" to="/$(arg robot_name)_exp/CL_coxa_joint/command"/>
        <remap from="/$(arg robot_name)/AR_femur_joint/command" to="/$(arg robot_name)_exp/AR_femur_joint/command"/>
        <remap from="/$(arg robot_name)/BR_femur_joint/command" to="/$(arg robot_name)_exp/BR_femur_joint/command"/>
        <remap from="/$(arg robot_name)/CR_femur_joint/command" to="/$(arg robot_name)_exp/CR_femur_joint/command"/>
        <remap from="/$(arg robot_name)/AL_femur_joint/command" to="/$(arg robot_name)_exp/AL_femur_joint/command"/>
        <remap from="/$(arg robot_name)/BL_femur_joint/command" to="/$(arg robot_name)_exp/BL_femur_joint/command"/>
        <remap from="/$(arg robot_name)/CL_femur_joint/command" to="/$(arg robot_name)_exp/CL_femur_joint/command"/>
        <remap from="/$(arg robot_name)/AR_tibia_joint/command" to="/$(arg robot_name)_exp/AR_tibia_joint/command"/>
        <remap from="/$(arg robot_name)/BR_tibia_joint/command" to="/$(arg robot_name)_exp/BR_tibia_joint/command"/>
        <remap from="/$(arg robot_name)/CR_tibia_joint/command" to="/$(arg robot_name)_exp/CR_tibia_joint/command"/>
        <remap from="/$(arg robot_name)/AL_tibia_joint/command" to="/$(arg robot_name)_exp/AL_tibia_joint/command"/>
        <remap from="/$(arg robot_name)/BL_tibia_joint/command" to="/$(arg robot_name)_exp/BL_tibia_joint/command"/>
        <remap from="/$(arg robot_name)/CL_tibia_joint/command" to="/$(arg robot_name)_exp/CL_tibia_joint/command"/>
        
      </node>
      <param name="whole_body_control_node/leg_description" type="str"
        command="$(find xacro)/xacro $(find darpa_subt_hexapod)/robots/apptronik_minimal.xacro
          gen_payload:=false"/>
    </group>

  </group>

</launch>
