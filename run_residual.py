import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger


def run(args):

  PATH = home + '/results/hex_model/latest/' + args.exp + '/'
  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/hex_model/latest/' + args.exp + '/'
    # WEIGHTS_PATH = home + '/hpc-home/results/hex_model/latest/weights/' + args.exp + '/'
  else:
    # WEIGHTS_PATH = home + '/results/hex_model/latest/weights/' + args.exp + '/'
    WEIGHTS_PATH = home + '/results/hex_model/latest/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  if not args.test_pol:
    logger.configure(dir=PATH)
    if rank == 0:
      writer = tensorboardX.SummaryWriter(log_dir=PATH)
    else: 
      writer = None 
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from models import residual
  dense_size, lstm_size = 128, 128

  rrnn = residual.ResidualNet(name='residual', input_size=9, output_size=6, lstm_size=lstm_size, sess=sess)
  if args.stand:
    from assets.just_test_stand_rl import Env
  else:
    from assets.just_test_leg_fm_rl import Env

  if args.test_pol:
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test_pol=args.test_pol, record_step=False) 
  else:
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test_pol=args.test_pol, rand_dynamics=args.rand_dynamics) 

  if args.lstm_pol:
    from models.ppo_lstm import Model
  else:
    from models.ppo import Model
  pol = Model("pi", env, ob_size=env.ob_size, ac_size=env.ac_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  
  initialize()
  sync_from_root(sess, pol.vars, comm=comm) #pylint: disable=E1101
  pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  rrnn.load(home + '/data/iros_data/real/6_2/weights_residual_/best/')

  print("residual rnn")      
  print(rrnn.mean.eval())
  print(rrnn.std.eval())

  if args.test_pol: 
    pol.load(WEIGHTS_PATH)


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  tf.get_default_graph().finalize()

  seq_length = 256
  prev_done = True
  ob = env.reset()
  im = np.zeros(env.im_size)
  rrnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  
  env.reward_breakdown = {'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100)}
  
  data_length = 128
  plot_length = 256
  fm_pred =  deque(maxlen=plot_length)
  fm_label = deque(maxlen=plot_length)
  fm_target = deque(maxlen=plot_length)
  pol_pred = deque(maxlen=plot_length)
  pol_des = deque(maxlen=plot_length)
  if args.lstm_pol:
    pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  
  id_pointer = 0
  ep_steps = 0
  if args.test_pol:
    stochastic = False
  else:
    stochastic = True
  training_steps = 0
  action_buffer = deque(maxlen=3)
  
  while True:
    # act = np.zeros(env.ac_size)
    if pol.timesteps_so_far > pol.max_timesteps:
      break 


    if args.lstm_pol:
      act, vpred, pol_states, nlogp = pol.step(ob, im, stochastic=stochastic, states=pol_states)
    else:
      act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
         
    next_ob, rew, done, _ = env.step(act)    
   
    rrnn_output, rrnn_state = rrnn.step(np.array(env.prev_joints + env.prev_joint_vel + list(act)), rrnn_state)       
    next_ob[:6] = np.array(env.joints + env.joint_vel) + rrnn_output

    fm_pred.append(next_ob)  

    next_im = np.zeros(env.im_size)
    
    fm_label.append(env.joints+env.joint_vel)
    fm_target.append(list(env.exp_joints)+list(env.exp_joint_vel))

    pol_pred.append(env.actions)
    pol_des.append(env.exp_torques)
        
    if not args.test_pol:
      pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])

    prev_done = done
    ob = next_ob
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
    
    if not args.test_pol and ep_steps % horizon == 0:
      if args.lstm_pol:
        _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True, states=pol_states)
      else:
        _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
      pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done) 
      pol.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
      training_steps += 1

    if done:    
      if not args.test_pol and rank == 0 and training_steps % 10 == 0:
        if not args.baseline:
          subplot([[np.array(fm_label)[:,0], np.array(fm_pred)[:,0], np.array(fm_target)[:,0]],[np.array(fm_label)[:,1], np.array(fm_pred)[:,1], np.array(fm_target)[:,1]],[np.array(fm_label)[:,2], np.array(fm_pred)[:,2], np.array(fm_target)[:,2]],[np.array(fm_label)[:,3], np.array(fm_pred)[:,3], np.array(fm_target)[:,3]],[np.array(fm_label)[:,4], np.array(fm_pred)[:,4], np.array(fm_target)[:,4]],[np.array(fm_label)[:,5], np.array(fm_pred)[:,5], np.array(fm_target)[:,5]]], legend=[["actual","fm prediction", "target"]]*6, PATH=PATH + 'fm_')
        else:
          subplot([[np.array(fm_label)[:,0], np.array(fm_target)[:,0]],[np.array(fm_label)[:,1], np.array(fm_target)[:,1]],[np.array(fm_label)[:,2], np.array(fm_target)[:,2]],[np.array(fm_label)[:,3], np.array(fm_target)[:,3]],[np.array(fm_label)[:,4], np.array(fm_target)[:,4]],[np.array(fm_label)[:,5], np.array(fm_target)[:,5]]], legend=[["actual", "target"]]*6, PATH=PATH + 'fm_')
        subplot([[np.array(pol_pred)[:,0],np.array(pol_des)[:,0]],[np.array(pol_pred)[:,1],np.array(pol_des)[:,1]],[np.array(pol_pred)[:,2],np.array(pol_des)[:,2]]], legend=[["torque from policy", "desired_torque"]]*3, PATH=PATH)
      fm_pred = deque(maxlen=plot_length)
      fm_label = deque(maxlen=plot_length)
      fm_target = deque(maxlen=plot_length)
      pol_pred = deque(maxlen=plot_length)
      pol_des = deque(maxlen=plot_length)
      action_buffer = deque(maxlen=3)
      ob = env.reset()
      rrnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
      if args.lstm_pol: 
        pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  
      im = np.zeros(env.im_size)
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0        

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--ae', default=False, action='store_true')
  # parser.add_argument('--baseline', default=False, action='store_true')
  parser.add_argument('--baseline', default=True, action='store_false')
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--test_pol', default=False, action='store_true') 
  parser.add_argument('--vis', default=False, action='store_true')  
  parser.add_argument('--set_pos', default=False, action='store_true')  
  parser.add_argument('--obs_torque', default=False, action='store_true')  
  parser.add_argument('--const_std', default=True, action='store_false')  
  parser.add_argument('--lstm_pol', default=False, action='store_true')  
  parser.add_argument('--stand', default=False, action='store_true')  
  parser.add_argument('--new_urdf', default=True, action='store_false')  
  parser.add_argument('--rand_dynamics', default=False, action='store_true')  
  parser.add_argument('--max_ts', default=50e6, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)