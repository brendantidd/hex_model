import gym
import pybulletgym as pg
import numpy as np



# env = gym.make('HumainoidPyBulletEnv-v0')
# Mujoco envs:
mujoco_envs = ['HalfCheetahMuJoCoEnv-v0', 'AntMuJoCoEnv-v0', 'HopperMuJoCoEnv-v0', 'Walker2DMuJoCoEnv-v0', 'HumanoidMuJoCoEnv-v0']
for m in mujoco_envs:
  # env = gym.make('HumanoidMuJoCoEnv-v0')
  env = gym.make(m)
  # env = gym.make('HalfCheetahMuJoCoEnv-v0')

  ob_size, ac_size = env.observation_space.shape[0], env.action_space.shape[0]
  print("ob size ", ob_size, "ac size", ac_size)
  if "Human" in m:
    env.render("human")
  env.reset()
  steps = 0
  while True:
    print(m)
    env.step(2*(np.random.random(ac_size)-0.5))
    if "Human" in m:
      env.render("human")
    steps += 1
    if steps > 1000:
      break
