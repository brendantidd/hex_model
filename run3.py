import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
import scripts.utils as U
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
from pathlib import Path
home = str(Path.home())

def run(args):

  PATH = home + '/results/hex_model/latest/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)

  writer = tensorboardX.SummaryWriter(log_dir=PATH)

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048*3

  # if args.use_model or args.use_sim_model:
  #   from models.rnn import RNN

  if args.use_model:
    # print("what the.. ")
    from models.forward_model import ForwardModel
    model = ForwardModel()
    # pass

    # rnn_length = 128
    # # input_size = 12
    # input_size = 9
    # output_size = 6
    # # input_size = 6
    # # output_size = 3
    # data = tf.placeholder(tf.float32, [None, rnn_length, input_size])
    # target = tf.placeholder(tf.float32, [None, rnn_length, output_size])
    # model = RNN('model', sess, data, target, input_size=input_size, rnn_length=rnn_length)
  else:
    model = None
  if args.use_sim_model:
    rnn_length = 128
    input_size = 12
    data = tf.placeholder(tf.float32, [None, rnn_length, input_size])
    target = tf.placeholder(tf.float32, [None, rnn_length, 3])
    sim_model = RNN('sim_model', sess, data, target, input_size=input_size, rnn_length=rnn_length)
  else:
    sim_model = None

  if args.test_with_gz:
    from collect_data import Env
    env = Env(args)
    args.vis = False
  elif args.test_leg:
    from assets.env_test_leg import Env
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert, act_model=model, sim_model=sim_model)
  else:
    from assets.env import Env
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert, act_model=model, sim_model=sim_model)
  print(args.pd)
  if args.pd:
    from models.torque_net import Model
    pol = Model(input_size=18+6, label_size= 18, PATH=PATH, writer=writer, env=env)
  else:
    from models.ppo import Model
    pol = Model("flat", env, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)

  U.initialize()
  sync_from_root(sess, pol.vars, comm=comm) #pylint: disable=E1101

  if args.use_model:
    if args.test_leg:
      # WEIGHTS_PATH = home + '/data/weights/test_leg_15_11/'
      # WEIGHTS_PATH = home + '/data/test_leg_19_11/weights/'
      # WEIGHTS_PATH = home + '/data/test_leg_real_small/weights/'
      if args.norm:
        WEIGHTS_PATH = home + '/data/test_leg_small/weights_norm/'
      else:
        
        # WEIGHTS_PATH = home + '/data/gz_test_leg_pos/weights_rnn/'
        WEIGHTS_PATH = home + '/data/test_pb_fm/weights_rnn/'
      # WEIGHTS_PATH = home + '/data/test_leg_15_11_pos_vel/weights/'
    else:
      WEIGHTS_PATH = home + '/data/weights/test_leg/'
    # model.load(WEIGHTS_PATH)
    # model.load_weights( home + '/data/test_pb_fm/' + 'weights_/model_weights.hdf5')
    model.load_weights( home + '/data/sixty/' + 'weights_/model_weights.hdf5')
    print("Loaded weights for model")
  
  if args.use_sim_model:
    if args.test_leg:
      # WEIGHTS_PATH = home + '/data/weights/test_leg_15_11/'
      WEIGHTS_PATH = home + '/data/test_leg_20_11/pb_weights/'
      # WEIGHTS_PATH = home + '/data/test_leg_15_11_pos_vel/weights/'
    else:
      WEIGHTS_PATH = home + '/data/weights/test_leg/'
    sim_model.load(WEIGHTS_PATH)

  if args.test or args.test_with_gz:
    try:
      if args.test_with_gz:
        pol.load(home + '/hpc-home/results/elements/latest/hex/hex_cur2/')
      else:
        pol.load(PATH)
    except Exception as e:
      print(e)
      exit()

  prev_done = True
  ob = env.reset()
  im = np.zeros(env.im_size)
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  steps = 0
  env.total_steps = 0
  while True:
    if pol.timesteps_so_far > pol.max_timesteps:
      print("finished, closing")
      break
    act, vpred, _, nlogp = pol.step(ob, im, stochastic=True)
    next_ob, rew, done, _ = env.step(act)
    next_im = np.zeros(env.im_size)
    if args.pd:
      pol.add_to_buffer([ob, env.label])
    else:
      pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
    prev_done = done
    ob = next_ob
    steps += 1
    ep_ret += rew
    ep_len += 1
    env.total_steps += 1

    # if steps % horizon == 0 and not args.test and not args.use_expert and not args.test_with_gz:
    #   _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
    #   if not args.pd:
    #     pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
    #   pol.run_train(ep_rets,ep_lens)
    #   ep_rets = []
    #   ep_lens = []
    # if done or steps % horizon == 0:
    if done:
      print("reseting", done, steps, horizon)
      ob = env.reset()
      exit()
      im = np.zeros(env.im_size)
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
        

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--ae', default=False, action='store_true')
  parser.add_argument('--norm', default=False, action='store_true')
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--use_expert', default=False, action='store_true')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--test_with_gz', default=False, action='store_true')  
  parser.add_argument('--test_leg', default=False, action='store_true')  
  parser.add_argument('--use_model', default=False, action='store_true')  
  parser.add_argument('--use_sim_model', default=False, action='store_true')  
  parser.add_argument('--manual', default=False, action='store_true')  
  parser.add_argument('--pd', default=False, action='store_true')  
  parser.add_argument('--vis', default=False, action='store_true')  
  args = parser.parse_args()
  # os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)