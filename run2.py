import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger


def run(args):

  PATH = home + '/results/hex_model/latest/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  

  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  if args.stand:
    id_input_size = 16
    id_output_size = 3
    fm_input_size = 11
    fm_output_size = 8
  else:
    if args.just_pos:
      id_input_size = 6
      id_output_size = 3
    else:
      id_input_size = 12
      id_output_size = 3
    fm_input_size = 9
    fm_output_size = 6
  if args.train_pol or (args.test_pol and not args.gz):
    from models import simple_rnn, id_rnn
    dense_size, lstm_size = 128, 128

    forward_model = simple_rnn.RNN(name='forward_model', input_size=fm_input_size, output_size=fm_output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess, norm=True, delta=False)
    if args.blank_id:
      id_model = id_rnn.RNN(name='id_model', input_size=id_input_size, output_size=id_output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess, adam=True, norm=False)
    else:
      id_model = id_rnn.RNN(name='id_model', input_size=id_input_size, output_size=id_output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess, adam=args.adam, norm=True)

  if args.train_pol or (args.test_pol and not args.gz):
    if args.stand:
      from assets.just_test_stand import Env
      env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert)
    else:
      from assets.just_test_leg_rl import Env

      env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert, control=args.control, delay=args.delay, rand_dynamics=args.rand_dynamics, test_pol=args.test_pol)
  elif args.gz:
    if args.stand:
      from assets.test_stand_gz import Env
    else:
      from assets.test_leg_gz import Env
    env = Env(render=args.render, PATH=PATH, args=args, delay=args.gz_delay)
    time.sleep(10)
  else:
    if args.stand:
      from assets.just_test_stand import Env
    else:
      from assets.just_test_leg import Env
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert, rand_dynamics=args.rand_dynamics)
  
  if args.train_pol or args.test_pol:
    if args.error:
      from models.ppo_error import Model
    elif args.lstm_pol:
      from models.ppo_lstm import Model
    else:
      from models.ppo import Model
    pol = Model("pi", env, ob_size=env.ob_size, ac_size=env.ac_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
    
    initialize()
    sync_from_root(sess, pol.vars, comm=comm) #pylint: disable=E1101
    if args.blank_id:
      sync_from_root(sess, id_model.vars, comm=comm) #pylint: disable=E1101
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if (args.train_pol or (args.test_pol and not args.gz)) and not args.baseline:
    if args.stand:
      forward_model.load(home + '/data/iros_data/gz_100_delayed_stand/weights_/')
      # Adam
      # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_ft/')
      if not args.blank_id:
        if args.adam:
          id_model.load(home + '/data/iros_data/pb_id_100/weights_adam/')
        else:
          # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_ft/')
          # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_/')
          if args.just_pos:
            id_model.load(home + '/data/iros_data/pb_just_pos/weights_/')
          else:
            id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_32_256/')
    else:
      if args.toms:
        forward_model.load(home + '/data/iros_data/real/300/weights_/best/')
      else:
        forward_model.load(home + '/data/iros_data/real_29_1/double/weights_/best/')
      # Adam
      # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_ft/')
      if not args.blank_id:
        if args.adam:
          id_model.load(home + '/data/iros_data/pb_id_100/weights_adam/')
        else:
          # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_/')
          if args.just_pos:
            id_model.load(home + '/data/iros_data/pb_just_pos/weights_/')
          else:
            if args.toms:
              id_model.load(home + '/data/iros_data/pb_400/toms_ft/weights_/best/') 
            else:
              id_model.load(home + '/data/iros_data/pb_500/ft/weights_/best/') 

    print("forward model mean, std")      
    print(forward_model.mean.eval())
    print(forward_model.std.eval())
    print(forward_model.scalar.eval())
    print("ID model mean, std, scale")
    print(id_model.mean.eval())
    print(id_model.std.eval())
    print(id_model.label_mean.eval())
    print(id_model.label_std.eval())
    # print(id_model.scalar.eval())

  if args.test_pol: 
    if args.hpc:
      pol.load(home + '/hpc-home/results/hex_model/latest/' + args.exp + '/')      
      # pol.load(home + '/hpc-home/results/hex_model/a7/leg_const_std_baseline1/')      
      # pol.load(home + '/hpc-home/results/hex_model/a18/leg_const_std1/')      
      # pol.load(home + '/hpc-home/results/hex_model/latest/leg_const_std1/')    
      # try:
      #   # id_model.load(home + '/hpc-home/results/hex_model/a18/leg_const_std1/')      
      #   # id_model.load(home + '/data/iros_data/pb/weights_/')      
      #   id_model.load(home + '/hpc-home/results/hex_model/latest/' + args.exp + '/')
      # except:
      #   print("no id model to load from test path")
    else:
      pol.load(PATH)
      # try:
      #   id_model.load(PATH)
      # except:
      #   print("no id model to load from test path")
  # pol.load(home + '/hpc-home/results/hex_model/latest/leg_const_std1/')      


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  # seq_length = 64
  seq_length = 256
  prev_done = True
  ob = env.reset()
  im = np.zeros(env.im_size)
  if args.train_pol or (args.test_pol and not args.gz):
    fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
    id_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]   
    # training_states = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  
  env.reward_breakdown = {'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100)}
  y1 = [[],[],[],[],[],[]]
  y2 = [[],[],[],[],[],[]]
  x1 = [[],[],[]]
  x2 = [[],[],[]]
  x3 = [[],[],[]]
  id_x = []
  id_y = []
  id_y_pos = []
  data_length = 128
  id_xs = np.zeros([data_length,seq_length,id_input_size])
  id_ys = np.zeros([data_length,seq_length,id_output_size])
  plot_length = 256
  fm_pred =  deque(maxlen=plot_length)
  fm_label = deque(maxlen=plot_length)
  fm_target = deque(maxlen=plot_length)
  id_pred =  deque(maxlen=plot_length)
  id_label = deque(maxlen=plot_length)
  pol_pred = deque(maxlen=plot_length)
  if args.lstm_pol:
    pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  
  id_pointer = 0
  ep_steps = 0
  full = False
  # id_xs = deque(maxlen=256)
  # id_ys = deque(maxlen=256)
  # desired_next_state = deque(maxlen=seq_length)
  # actual_next_state = deque(maxlen=seq_length)
  id_loss = deque([0.0],maxlen=100)
  if args.test_pol:
    stochastic = False
  else:
    stochastic = True
  while True:
    # act = np.zeros(env.ac_size)
    if args.train_pol or (args.test_pol and not args.gz):
      if pol.timesteps_so_far > pol.max_timesteps:
        break 
      if args.lstm_pol:
        act, vpred, pol_states, nlogp = pol.step(ob, im, stochastic=stochastic, states=pol_states)
      else:
        act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
        # act, vpred, _, nlogp = pol.step(ob, im, stochastic=False)
        # act = (80*(np.array(env.exp_joints)-np.array(env.joints)))
      # if args.train_pol:
      #   if args.cur:
      #     exp_torques = (env.Kp/env.initial_Kp)*(80*(np.array(env.exp_joints)-np.array(env.joints)) - 0.1*np.array(env.joint_vel))
      #     act = [np.clip(e + a,-tm,tm) for e,a,tm in zip(exp_torques, act, env.tor_max)]  
      # torques = act
      if args.baseline:
        torques = act
      else:
        fm_output, fm_state = forward_model.step(np.array(env.joints + env.joint_vel + list(act)), fm_state)   
        prev_id_state = id_state    
        if args.just_pos:
          torques, id_state = id_model.step(np.array(env.joints + list(fm_output[:3])), id_state)
        else:
          torques, id_state = id_model.step(np.array(env.joints + env.joint_vel + list(fm_output)), id_state)
    # elif args.test_pol:
    #   if args.lstm_pol:
    #     act, _, pol_states, _ = pol.step(ob, im, stochastic=stochastic, states=pol_states)
    #   else:
    #     act, _, _, _ = pol.step(ob, im, stochastic=stochastic)
    #   torques = act
      # torques, _, _, _ = pol.step(ob, im, stochastic=True)
      # print(torques)
    else:
      torques = None
    # if args.control == 'position':
    #   next_ob, rew, done, _ = env.step(fm_output)
    # else:
    next_ob, rew, done, _ = env.step(torques)
    # print(rew)
      # if args.id_rew:
      #   # print(np.mean(id_loss))
      #   rew -= 0.5*np.mean(id_loss)
    next_im = np.zeros(env.im_size)
    
    if args.train_pol and not args.baseline:
      if args.just_pos:
        id_x.append(env.prev_joints + env.joints)
        id_torque, _ = id_model.step(np.array(env.prev_joints + env.joints), prev_id_state)
      else:
        id_x.append(env.prev_state + env.joints + env.joint_vel)
        id_torque, _ = id_model.step(np.array(env.prev_state + env.joints + env.joint_vel), prev_id_state)
      id_y.append(torques)
      id_pred.append(id_torque)
      id_label.append(torques)
      fm_pred.append(fm_output)
      fm_label.append(env.joints+env.joint_vel)
      # print(env.exp_joints+env.exp_joint_vel)
      fm_target.append(list(env.exp_joints)+list(env.exp_joint_vel))
      if len(id_x) == seq_length:
        id_xs[id_pointer, :,:] = id_x
        id_ys[id_pointer, :,:] = id_y
        id_pointer += 1
        if id_pointer == data_length:
          id_pointer = 0
          full = True
        id_x = []
        id_y = []
    if args.train_pol:
      # pol_pred.append(act)
      pol_pred.append(env.actions)
        
    if args.train_pol:
      pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])

    prev_done = done
    ob = next_ob
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
    
    if args.train_pol and ep_steps % horizon == 0:
      # print(pol.data['done'])
      # print(pol.data['rew'])
      # print(pol.data['value'])
      if not args.baseline:
        # batch_size = min(32,len(id_xs))
        if full:
          batch_size = data_length//4
          num_batches = data_length//batch_size
          train_length = data_length
          # idx = random.sample(range(data_length), data_length)
        else:
          batch_size = max(1,id_pointer//4)
          num_batches = id_pointer//batch_size
          train_length = data_length
          # idx = random.sample(range(id_pointer), id_pointer)
        
        # train_x = id_xs[idx,:,:]
        # train_y = id_ys[idx,:,:]
        if args.train_id:

          for e in range(5):    
            # print(train_x.shape)
            idx = [i for i in range(train_length)]
            np.random.shuffle(idx)
            # for start in range(0,num_batches,batch_size):
            for batch in range(train_length//batch_size):
              b_idx = batch*batch_size
              batch_x, batch_y = id_xs[idx[b_idx:b_idx+batch_size],:,:], id_ys[idx[b_idx:b_idx+batch_size],:,:]
              # end = start + batch_size
              # mbinds = idx[start:end]
              # print(start, end, len(id_xs), len(idx)//batch_size, train_x[idx[start:end],:,:].shape, train_y[idx[start:end],:,:].shape)
                # loss, _, _ = id_model.train(train_x[idx[start:end],:,:],train_y[idx[start:end],:,:],batch_size=batch_size,seq_length=seq_length)
              loss, _, _ = id_model.train(batch_x,batch_y,batch_size=batch_size,seq_length=seq_length)
                # loss = id_model.get_loss(train_x[idx[start:end],:,:],train_y[idx[start:end],:,:],batch_size=batch_size,seq_length=seq_length)     
              id_loss.append(loss)      

        else:
          batch_x, batch_y = id_xs[0,:,:], id_ys[0,:,:]
          id_loss = id_model.get_loss(batch_x,batch_y,batch_size=1,seq_length=seq_length)
        if rank == 0:
          if args.train_id:
            id_model.save(PATH)
          writer.add_scalar("id_loss", np.mean(id_loss), pol.iters_so_far)        
          logger.record_tabular("Id loss", np.mean(id_loss))  
          logger.record_tabular("Id pointer", id_pointer)  
          subplot([[np.array(fm_label)[:,0], np.array(fm_pred)[:,0], np.array(fm_target)[:,0]],[np.array(fm_label)[:,1], np.array(fm_pred)[:,1], np.array(fm_target)[:,1]],[np.array(fm_label)[:,2], np.array(fm_pred)[:,2], np.array(fm_target)[:,2]],[np.array(fm_label)[:,3], np.array(fm_pred)[:,3], np.array(fm_target)[:,3]],[np.array(fm_label)[:,4], np.array(fm_pred)[:,4], np.array(fm_target)[:,4]],[np.array(fm_label)[:,5], np.array(fm_pred)[:,5], np.array(fm_target)[:,5]]], legend=[["actual","fm prediction", "target"]]*6, PATH=PATH + 'fm_')
          subplot([[np.array(id_label)[:,0], np.array(id_pred)[:,0]],[np.array(id_label)[:,1], np.array(id_pred)[:,1]],[np.array(id_label)[:,2], np.array(id_pred)[:,2]]], legend=[["label from sim","pred from id model"]]*3, PATH=PATH + 'id_')
          # subplot([[np.array(id_label)[:,0], np.array(id_pred)[:,0],np.array(pol_pred)[:,0]],[np.array(id_label)[:,1], np.array(id_pred)[:,1],np.array(pol_pred)[:,1]],[np.array(id_label)[:,2], np.array(id_pred)[:,2],np.array(pol_pred)[:,2]]], legend=[["label from sim","pred from id model", "real torque"]]*3, PATH=PATH + 'id_')
      else:
        if not args.test_pol and rank == 0:
        # print(np.array(pol_pred).shape)
          subplot([[np.array(pol_pred)[:,0]],[np.array(pol_pred)[:,1]],[np.array(pol_pred)[:,2]]], legend=[["torque"]]*3, PATH=PATH)
      if args.lstm_pol:
        _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True, states=pol_states)
      else:
        _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
      pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
      # print(pol.data['rew'])
      pol.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
    if done:    
      # print("reseting", done, steps, horizon)
      ob = env.reset()
      if args.train_pol or (args.test_pol and not args.gz):
        fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
        id_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
        # training_states = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]  
        id_x = []
        id_y = []
      if args.lstm_pol: 
        pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  
      # exit()
      im = np.zeros(env.im_size)
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0        

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--ae', default=False, action='store_true')
  parser.add_argument('--baseline', default=False, action='store_true')
  parser.add_argument('--error', default=False, action='store_true')
  parser.add_argument('--norm', default=False, action='store_true')
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--use_expert', default=False, action='store_true')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--rnn', default=False, action='store_true')
  parser.add_argument('--stand', default=False, action='store_true')
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--test_with_gz', default=False, action='store_true')  
  parser.add_argument('--test_leg', default=False, action='store_true')  
  parser.add_argument('--use_act_model', default=False, action='store_true')  
  parser.add_argument('--use_sim_model', default=False, action='store_true')  
  parser.add_argument('--manual', default=False, action='store_true')  
  parser.add_argument('--train_pol', default=False, action='store_true')  
  parser.add_argument('--test_pol', default=False, action='store_true') 
  parser.add_argument('--pd', default=False, action='store_true')  
  parser.add_argument('--vis', default=False, action='store_true')  
  parser.add_argument('--train_id', default=False, action='store_true')  
  parser.add_argument('--gz', default=False, action='store_true')    
  parser.add_argument('--const_std', default=True, action='store_false')  
  parser.add_argument('--control', default="torque")  
  parser.add_argument('--adam', default=False, action='store_true')  
  parser.add_argument('--toms', default=False, action='store_true')  
  parser.add_argument('--delay', default=False, action='store_true')  
  parser.add_argument('--just_pos', default=False, action='store_true')  
  parser.add_argument('--gz_delay', default=True, action='store_false')  
  parser.add_argument('--blank_id', default=False, action='store_true')  
  parser.add_argument('--id_rew', default=False, action='store_true')  
  parser.add_argument('--lstm_pol', default=False, action='store_true')  
  parser.add_argument('--rand_dynamics', default=False, action='store_true')  
  parser.add_argument('--max_ts', default=1e8, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)