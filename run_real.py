import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
import random
# from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from assets.test_leg_real import Env

def run(args):


  PATH = home + '/results/hex_model/latest/' + args.exp + '/'
  if args.hpc:
    # WEIGHTS_PATH = home + '/hpc-home/results/hex_model/latest/weights/' + args.exp + '/'
    WEIGHTS_PATH = home + '/hpc-home/results/hex_model/latest/' + args.exp + '/'
  else:
    # WEIGHTS_PATH = home + '/results/hex_model/latest/weights/' + args.exp + '/'
    WEIGHTS_PATH = home + '/results/hex_model/latest/' + args.exp + '/'

  # comm = MPI.COMM_WORLD
  # rank = comm.Get_rank()
  comm = None
  rank = 0
  myseed = int(time.time()) + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
    
  # if rank == 0:
  #   writer = tensorboardX.SummaryWriter(log_dir=PATH)
  # else: 
  writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  env = Env(PATH=PATH, args=args, controller_type=args.controller_type)

  time.sleep(2)

  if args.controller_type == 'policy':
    args.test_pol = True
  
  if args.test_pol:
    t1 = time.time()
    print("loading ppo module")
    if args.lstm_pol:
      from models.ppo_lstm import Model
    else:
      from models.ppo import Model
    print("ppo module loaded", time.time()-t1)

    pol = Model("pi", env, ob_size=env.ob_size, ac_size=env.ac_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)

    initialize()
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)
    pol.load(WEIGHTS_PATH)
    print()
    print("testing a policy")
    print("*** loaded weights from ", WEIGHTS_PATH, "***")

  print()
  print("*** about to start ***")
  print()

  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  # seq_length = 64
  seq_length = 256
  prev_done = True
  ob = env.reset()
  if args.history:
    act = np.array([0,0,0])
    state_buffer = deque([0]*env.ob_size, maxlen=env.ob_size)
  im = np.zeros(env.im_size)
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  
  env.reward_breakdown = {'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100)}

  if args.lstm_pol:
    pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  

  stochastic = False
  while True:
    if args.test_pol:
      if args.lstm_pol:
        act, vpred, pol_states, nlogp = pol.step(ob, im, stochastic=stochastic, states=pol_states)
      else:
        if args.history:
          state_buffer.extend(np.concatenate((ob, act)))
          obs = np.array(state_buffer)
          act, vpred, _, nlogp = pol.step(obs, im, stochastic=stochastic)
        else:
          act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
        cmd_torque = act*env.max_torque
    else:
      cmd_torque = None

    # Go to nice starting point in the first couple of seconds 
    if cmd_torque is not None and env.steps < 400:
      cmd_torque = None
      
    next_ob, rew, done, _ = env.step(cmd_torque)
    prev_done = done
    ob = next_ob
    ep_ret += rew
    ep_len += 1

    if done:    
      ob = env.reset()
      if args.history:
        act = np.array([0,0,0])
        state_buffer = deque([0]*env.ob_size, maxlen=env.ob_size)
      if args.lstm_pol: 
        pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  
      im = np.zeros(env.im_size)
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
  
  env.close()

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--exp', default="test")
  parser.add_argument('--test_pol', default=False, action="store_true")
  parser.add_argument('--sanity_check', default=False, action="store_true")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--vis', default=False, action="store_true")
  parser.add_argument('--ae', default=False, action="store_true")
  parser.add_argument('--controller_type', default="test")
  parser.add_argument('--hpc', default=False, action="store_true")
  parser.add_argument('--rand', default=False, action="store_true")
  parser.add_argument('--mpi_avail', default=False, action="store_true")
  parser.add_argument('--lstm_pol', default=False, action="store_true")
  parser.add_argument('--const_std', default=True, action="store_false")
  parser.add_argument('--render', default=True, action="store_false")
  parser.add_argument('--history', default=True, action="store_false")
  parser.add_argument('--max_ts', default=1e8, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)