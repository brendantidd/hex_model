import numpy as np
import pybullet as p

class Obstacles():
  def __init__ (self):
    self.box_id = []
    self.positions = []
    self.sizes = []
    self.colours = []
    
  def remove_obstacles(self):
    if self.box_id:
      for b in self.box_id:
        p.removeBody(b)
      self.box_id = []
      self.positions = []
      self.sizes = []
      self.colours = []
  
  def insert_base(self):
    self.add_box([0.12,0.0,-0.05], [0,0,0], size=[0.15,0.15,0.1], colour=[0.3,0.8,0.3,1])
    self.add_box([0.12,1.0,-0.05], [0,0,0], size=[0.15,0.15,0.1], colour=[0.3,0.8,0.3,1])

  def insert_block(self, height=0.01):
    # Block
    position = [0.12,0.075,-0.04 + height]
    size = [0.1,0.05,0.1]
    self.add_box(position, [0,0,0], size=size, colour=[0.8,0.3,0.3,1])
    return position

  def add_box(self, pos, orn, size, colour=[0.8,0.3,0.3,1]):
    # print(pos, orn, size)
    colBoxId = p.createCollisionShape(p.GEOM_BOX, halfExtents=size)
    visBoxId = p.createVisualShape(p.GEOM_BOX, halfExtents=size, rgbaColor=colour)
    q = p.getQuaternionFromEuler(orn)
    box_id = p.createMultiBody(baseMass=0, baseCollisionShapeIndex=colBoxId, baseVisualShapeIndex=visBoxId, basePosition=pos, baseOrientation=q)
    self.box_id.append(box_id)
    self.positions.append(pos+orn)
    self.sizes.append(size)
    self.colours.append(colour)

  def get_box_info(self):
    return [self.box_id, self.positions, self.sizes, self.colours]

if __name__=="__main__":
  from env import Env
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--display_hm', default=False, action='store_true')
  args = parser.parse_args()

  env = Env(render=args.render, display_hm=args.display_hm)
  # env = Env(render=False, display_hm=True)
  # physicsClientId = p.connect(p.GUI)
  obstacles = Obstacles()
  # obstacles.stairs(['down','up','down','up','down'])
  # obstacles.stairs(['up' for _ in range(10)])
  # obstacles.doughnut(size=0.5,width=0.5)
  # obstacles.path(path=[[1,1,0.5,1],[1.1,1,0.5,1],[1,1,0.5,1]])
  obstacles.path(difficulty=4)
  # obstacles.adversarial_world()
  # obstacles.test_world()
  # obstacles.path(difficulty=10)
  # obstacles.stairs(['up','up','up','up','up','up','up','up','up','down','down','down','down','down','down'], height_coeff=0.07)

  env.reset()
  env.world_map = env.get_world_map(obstacles.get_box_info())


  while True:
    env.step(np.random.random(env.ac_size))
    env.get_hm()
