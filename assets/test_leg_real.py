import numpy as np
import rospy
import os
from std_msgs.msg import Float64, Empty
from sensor_msgs.msg import JointState, Imu
from gazebo_msgs.srv import SetModelState,SetModelConfiguration
from gazebo_msgs.msg import ModelState, ModelStates
from dynamic_reconfigure.srv import Reconfigure
from dynamic_reconfigure.msg import Config, DoubleParameter
from leg_go_ros_msgs.msg import TipStates
import time
from scipy.spatial.transform import Rotation as R
import copy
import roslaunch
from std_srvs.srv import SetBool, Trigger
import pandas as pd
from collections import deque
import random
from pathlib import Path
home = str(Path.home())
from models import rand_net
import pybullet as p
from datetime import datetime
import shutil  
from scripts.utils import *
from assets.playback_env import PlayBackEnv

class Env():
  ac_size = 3
  ob_size = 9
  im_size = [48,48,1]
  steps = 0
  rewards = 0
  # timestep = 1/800
  timestep = 1/200
  steps_per_sec = 200
  done = True
  def __init__(self, PATH=None, args=None, controller_type='test'):


    self.args = args
    if self.args.history:
      self.ob_size = 60
    else:
      self.ob_size = 9

    self.sanity_check = args.sanity_check
    if self.sanity_check:
      self.sanity_env = PlayBackEnv(args)
      # self.Kp = np.array([40,40,10]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([0.5,0.5,0.5])
      # self.Kp = np.array([15,20,10]); self.Ki = np.array([0.05,0.05,0.05]); self.Kd = np.array([1.5,1.0,1.0])
      self.Kp = np.array([30,30,20]); self.Ki = np.array([0.05,0.05,0.05]); self.Kd = np.array([1.5,1.0,1.0])
      # self.Kp = np.array([80,80,20]); self.Ki = np.array([0.0,0.0,0.0]); self.Kd = np.array([0.0,0.0,0.0])
      self.max_torque = np.array([10,10,3])

      # self.rand_net_max_torque = np.array([1,1,0.5]) 
      # self.rand_net_max_pos = np.array([0.25,0.25,0.1]) 
      # self.rand_net_max_pos = np.array([0.5,0.5,0.2]) 
      # self.rand_net_max_pos = np.array([0.6,0.4,0.3]) 
      self.rand_net_max_pos = np.array([0.3,0.3,0.2]) 
      # self.rand_net_max_pos = np.array([0.2,0.2,0.1]) 
      # self.rand_net_max_pos = np.array([0.1,0.1,0.1]) 

      self.rand_net = rand_net.Net(input_size=6, output_size=3, max_actions=self.rand_net_max_pos)
      # self.rand_net_max_freq = np.array([5,5,5]) 
      # self.rand_net = rand_net.Net(input_size=6, output_size=3, max_actions=self.rand_net_max_freq)

    else:
      # self.Kp = np.array([6,7.5,2]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([0.75,0.75,0.5])
      # self.Kp = np.array([6,8,3]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([0.75,0.75,0.5])
      # self.Kp = np.array([10,15,5]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([0.75,0.75,0.5])
      # self.Kp = np.array([20,15,5]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([1.5,1.0,0.25])
      # increase femur 20, 1.5. inc tib 5,0.5
      # self.Kp = np.array([30,30,4]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([1.75,1.75,0.4])
      self.Kp = np.array([25,25,4]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([1.75,1.75,0.4])
      # self.Kp = np.array([20,20,4]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([1.75,1.75,0.4])
      self.max_torque = np.array([10,10,3])
      # self.rand_net_max_torque = np.array([1.0,1.0,0.25]) 
      # self.rand_net_max_pos = np.array([0.6,0.4,0.3]) 
      self.rand_net_max_pos = np.array([0.3,0.3,0.2]) 
      # self.rand_net_max_pos = np.array([0.1,0.1,0.075]) 
      # self.rand_net_max_pos = np.array([0.08,0.08,0.05]) 
      self.rand_net = rand_net.Net(input_size=6, output_size=3, max_actions=self.rand_net_max_pos)
      # self.rand_net = rand_net.Net(input_size=6, output_size=3, max_actions=self.rand_net_max_freq)
      
  

    self.controller_type = controller_type

    self.motor_names = ['TEST_coxa','TEST_femur','TEST_tibia']
    self.real_motor_names = ['TEST_coxa','TEST_tibia','TEST_femur']

    self.joint_dict = {n:{'pos':0.0,'vel':0.0,'effort':0.0} for n in self.motor_names}

    self.samples = np.load('samples/samples.npy')
    # self.torque_samples = np.load('samples/torque_samples.npy')
    self.torque_samples = self.samples[:,6:]
    self.sample_size = self.torque_samples.shape[0]
    self.entry_count = 0
    self.seq_length = 256
    if self.controller_type == 'policy':
      self.desired_sim_length = 1500
    else:
      self.desired_sim_length = (10000//self.seq_length)*self.seq_length
      # self.desired_sim_length = (2000//self.seq_length)*self.seq_length
    print("collecting ", self.desired_sim_length, " data points")

    self.record_step = True  
    self.robot_name = 'r1' 
    
    # wait until publishing
    if not args.sanity_check:
      rospy.init_node(self.robot_name + '_rl_node', anonymous=False) 
      self.initialise_subscribers()
      self.initialise_publishers()
      time.sleep(2) 
    
    self.PATH = PATH
    self.DATA_PATH = '/home/brendan/data/actuator_all/'
    # self.DATA_PATH = '/home/brendan/data/actuator_expert/'
    self.args = args
    self.robot_number = 1
    self.vel_max = [8,11,8]
    self.tor_max = [80,112,80]
        
    # # State from PB
    self.state = ["TEST_coxa_joint_pos","TEST_femur_joint_pos","TEST_tibia_joint_pos"]
    self.state += ["TEST_coxa_joint_vel","TEST_femur_joint_vel","TEST_tibia_joint_vel"]
    
    self.episodes = 0
    self.total_steps = 0
    
    self.eth_input_buf = {i:deque([0.0]*15,maxlen=15) for i in self.motor_names}
    self.eth_inputs = []
    self.eth_labels = []
    self.inputs = []
    self.targets = []
    self.torques = []
    self.labels = []
    self.dones = []
    
    # Max torques, max positions
    # From .yaml file
    # kinematic_limits_upper: [  0.9,   0.6,   2.4]
    # kinematic_limits_lower: [ -0.9,  -0.98,  0.4]
    
    self.coxa_min, self.coxa_max  = -0.8, 0.8
    self.femur_min, self.femur_max  = -0.8, 0.5
    self.tibia_min, self.tibia_max = 1.0, 2.3

    self.rand_limits_min = np.array([-0.6, -0.7, 1.2])
    self.rand_limits_max = np.array([0.6, 0.3, 2.2])
    
    # self.tibia_min, self.tibia_max = 1.4, 1.8
    # self.coxa_max, self.coxa_min = 0.3, -0.3
    # self.femur_max, self.femur_min = 0.0, -0.4
    # self.tibia_max, self.tibia_min = 1.8, 1.3     
  
    self.t1 = time.time() 

  def close(self):
    self.publish_actions([0.0,0.0,0.0])

  def initialise_publishers(self):
    print("initialising publishers for ", self.robot_name)

    self.joint_publisher = rospy.Publisher('/input_joint_commands', JointState, queue_size=1)
    self.publish_actions([0.0,0.0,0.0])

  # def estopCallback(self):
  #   self.e_stopped = True

  def initialise_subscribers(self):
    print("initialising subscribers for ", self.robot_name)
    rospy.Subscriber('/output_joint_states', JointState, self.jointStateCallback)

  # ROS interface stuff ======================================================
  def publish_actions(self, actions):
    
    # Trained in the order of "coxa", "femur", "tibia". Real order is "coxa", "tibia", "femur"
    my_actions = {m:a for a,m in zip(actions, self.motor_names)}
    joint_cmd = JointState()
    for i, m in enumerate(self.real_motor_names): 
      joint_cmd.name.append(m)
      joint_cmd.position.append(0.0)
      joint_cmd.velocity.append(0.0)
      joint_cmd.effort.append(my_actions[m])
    self.joint_publisher.publish(joint_cmd)

  def reset(self):
    # print("============= resetting  =============================")
    self.joint_dict = {n:{'pos':0.0,'vel':0.0,'effort':0.0} for n in self.motor_names}
    self.action_buffer = deque(maxlen=5)
    
    self.controller_state = 'goto'
    # if self.controller_type == 'explore':
    self.rand_net.initialise()         
    if self.controller_type == 'test':
      # self.simple_targets = [[0.15,-0.2,1.7],[0.3,-0.4,1.8],[0.3,-0.4,1.8],[0.15,-0.2,1.7],[0.0,0.0,1.5707],[0.0,0.0,1.5707],[-0.15,-0.2,1.7],[-0.3,-0.4,1.8],[-0.3,-0.4,1.8],[-0.15,-0.2,1.7],[0.0,0.0,1.5707],[0.0,0.0,1.5707],[0.15,0.15,1.4],[0.3,0.3,1.3],[0.3,0.3,1.3],[0.15,0.15,1.4],[0.0,0.0,1.5707],[0.0,0.0,1.5707],[-0.15,0.15,1.4],[-0.3,0.3,1.3],[-0.3,0.3,1.3],[-0.15,0.15,1.4],[-0.0,0.0,1.5707]]
      # self.simple_targets = [[1,1,1],[0,0,0],[-1,1,1],[0,0,0],[1,-1,-1],[0,0,0],[-1,-1,-1],[0,0,0]]
      self.simple_targets = [[0.0,0.0,1.5707],[0.3,-0.4,1.8],[0.0,0.0,1.5707],[-0.3,-0.4,1.8],[0.0,0.0,1.5707],[0.3,0.4,1.2],[0.0,0.0,1.5707],[-0.3,0.4,1.2],[0.0,0.0,1.5707]]
    
    self.sample_pointer = 70
    temp = self.samples[self.sample_pointer,:]
    self.exp_torque = self.torque_samples[self.sample_pointer,:]
    self.zero_position = [0.0,0.0,1.5707]

    self.steps = 0
    self.prev_steps = 400%600
    self.episodes += 1   
    self.sum_error = 0 
    # self.explore_time = 0
    self.get_observation()
    self.initial_position = self.joints
    self.target_joints, self.target_joint_vel = self.initial_position, [0,0,0]
    self.prev_actions = np.zeros(3)
    self.prev_target = np.zeros(3)

    self.metric = deque(maxlen=self.sample_size)
    self.metrics = []
    self.pos_metric = deque(maxlen=self.sample_size)
    self.pos_metrics = []
    self.torque_metric = deque(maxlen=self.sample_size)
    self.torque_metrics = []


    if self.sanity_check:
      self.joints, self.joint_vel = self.sanity_env.reset()
    return np.array(list(self.joints) + list(self.joint_vel) + list(self.initial_position))
    
  def step(self, actions=None):
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints
    self.prev_joint_vel = self.joint_vel

    if actions is not None:
      torques = actions
    else:
      if self.controller_type == 'expert':
        error = (np.array(self.target_joints) - np.array(self.joints))  
        # torques = self.Kp*error + self.Ki*self.sum_error - self.Kd*np.array(self.joint_vel)
        # torques = self.Kp*error - self.Kd*np.array(self.joint_vel) + np.array([np.random.uniform(-0.5,0.5),np.random.uniform(-0.5,0.5),np.random.uniform(-0.25,0.25)])
        # if self.steps >= 400:
        #   torques = self.Kp*error - self.Kd*np.array(self.joint_vel) + self.rand_net.step(np.array(self.joints+self.joint_vel))
        # else:
        torques = self.Kp*error - self.Kd*np.array(self.joint_vel)

        # self.sum_error += error
        # self.sum_error = np.clip(error + self.sum_error, -10, 10)
        if self.steps % 600 == 0 or (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
          # self.Kp = np.array([25,25,4]); self.Ki = np.array([0.02,0.02,0.02]); self.Kd = np.array([1.75,1.75,0.4])
          # self.Kp = (np.random.uniform(15,35),np.random.uniform(15,35), np.random.uniform(3,5))
          self.Kp = (np.random.uniform(15,30),np.random.uniform(15,30), np.random.uniform(3,5))
          # self.Kp = (35, 35, 5)
          # self.Kp = (15, 15, 3)
          
          self.rand_net.initialise()  
          # self.offset = np.array([np.random.uniform(-0.1, 0.1),np.random.uniform(-0.2, 0.2),np.random.uniform(-0.15, 0.15)])
          # self.scale = np.array([np.random.choice([-1,1])*np.random.uniform(0.7, 1.1),np.random.uniform(0.7, 1.05),np.random.uniform(0.7, 1.1)])
          # self.scale = np.array([np.random.uniform(0.7, 1.05),np.random.uniform(0.7, 1.05),np.random.uniform(0.7, 1.1)])
        
          # print(self.scale, self.offset)
          
          # if self.steps > 400:            
          #   if self.steps % 600 == 0:
          #     print("checkpoint")
          #   if self.joints[0] < self.coxa_min: print(self.coxa_min)
          #   if self.joints[0] > self.coxa_max: print(self.coxa_max)
          #   if self.joints[1] < self.femur_min: print( self.femur_min)
          #   if self.joints[1] > self.femur_max: print( self.femur_max)
          #   if self.joints[2] < self.tibia_min: print( self.tibia_min)
          #   if self.joints[2] > self.tibia_max: print( self.tibia_max)
          #   print("new initialisation")

      elif self.controller_type == 'explore_sin':
        if self.steps < 400:
          error = (np.array(self.target_joints) - np.array(self.joints))
          torques = self.Kp*error + self.Ki*self.sum_error - self.Kd*np.array(self.joint_vel)
          self.sum_error += error
        else:
          dx = 2*np.pi*(self.steps%600/600)   
          # print(dx)
          rand = self.rand_net.step(np.array(self.joints + self.joint_vel))      
          coxa = 5*np.sin(10*dx + self.prev_steps) - 3.0*np.sin(rand[0]*dx + self.prev_steps)
          femur = 5*np.sin(7.5*dx + self.prev_steps) - 2.0*np.sin(rand[1]*dx + self.prev_steps) - 2
          tibia = 1.5*np.sin(7.5*dx + self.prev_steps) - 0.5*np.sin(rand[2]*dx + self.prev_steps)
          torques = [coxa, femur, tibia]
          # print(torques)

          if self.steps % 600 == 0 or (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
            self.rand_net.initialise()  
            # self.offset = np.array([np.random.uniform(-0.1, 0.1),np.random.uniform(-0.2, 0.2),np.random.uniform(-0.15, 0.15)])
            # self.scale = np.array([np.random.choice([-1,1])*np.random.uniform(0.7, 1.1),np.random.uniform(0.7, 1.05),np.random.uniform(0.7, 1.1)])
            # self.scale = np.array([np.random.uniform(0.7, 1.05),np.random.uniform(0.7, 1.05),np.random.uniform(0.7, 1.1)])
          
            # print(self.scale, self.offset)
            self.prev_dx = (self.steps%600/600)
            if self.steps > 400:            
              if self.steps % 600 == 0:
                print("checkpoint")
              if self.joints[0] < self.coxa_min: print(self.coxa_min)
              if self.joints[0] > self.coxa_max: print(self.coxa_max)
              if self.joints[1] < self.femur_min: print( self.femur_min)
              if self.joints[1] > self.femur_max: print( self.femur_max)
              if self.joints[2] < self.tibia_min: print( self.tibia_min)
              if self.joints[2] > self.tibia_max: print( self.tibia_max)
              print("new initialisation")

      else:
        if self.controller_state == 'explore':
          torques = self.rand_net.step(np.array(self.joints+self.joint_vel))
          # print(self.steps, torques)
          torques[:2] = np.clip(torques[:2], np.array(self.prev_actions)[:2]-0.1, np.array(self.prev_actions)[:2]+0.1)

          error = 1.5707 - self.joints[2]
          torques[2] = self.Kp[2]*error - self.Kd[2]*self.joint_vel[2]
          
          # print(error, self.Kp, self.sum_error)
          self.sum_error += error


          # if self.explore_time > 500 or (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
          if (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
            self.controller_state = 'goto'

            print("was exploring now goingto")
            if self.joints[0] < self.coxa_min: print(self.coxa_min)
            if self.joints[0] > self.coxa_max: print(self.coxa_max)
            if self.joints[1] < self.femur_min: print( self.femur_min)
            if self.joints[1] > self.femur_max: print( self.femur_max)
            if self.joints[2] < self.tibia_min: print( self.tibia_min)
            if self.joints[2] > self.tibia_max: print( self.tibia_max)
            self.sum_error = 0
            self.current_position = self.joints
            self.current_steps = self.steps
            self.sample = self.samples[np.random.randint(0,self.samples.shape[0]),:3]


        if self.controller_state == 'goto':
          error = (np.array(self.target_joints) - np.array(self.joints))
          # print(self.target_joints, error)
          # error = (np.array([0,0,1.5707]) - np.array(self.joints))
          torques = self.Kp*error + self.Ki*self.sum_error - self.Kd*np.array(self.joint_vel)
          # print(error, self.Kp, self.sum_error)
          self.sum_error += error
          self.sum_error = np.clip(error + self.sum_error, -20, 20)
          if self.controller_type == 'explore' and self.steps >= 400:
            target_error = (np.array(self.sample) - np.array(self.joints))
            # print(target_error)
            # if (abs(target_error)<0.2).all() and not (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
            if (abs(target_error)<0.2).all():
              self.controller_state = 'explore'
              print("starting explore")
              self.rand_net.initialise()  
    
              # self.explore_time = 0
            # else:
            #   print(target_error, self.sample)

              # if self.joints[0] < self.coxa_min: print(self.coxa_min)
              # if self.joints[0] > self.coxa_max: print(self.coxa_max)
              # if self.joints[1] < self.femur_min: print( self.femur_min)
              # if self.joints[1] > self.femur_max: print( self.femur_max)
              # if self.joints[2] < self.tibia_min: print( self.tibia_min)
              # if self.joints[2] > self.tibia_max: print( self.tibia_max)

      # torques = np.clip(torques, np.array(self.prev_actions)-0.05, np.array(self.prev_actions)+0.05)
      self.prev_actions = torques
      # self.action_buffer.append(np.clip(torques, np.array(self.prev_actions)-0.1, np.array(self.prev_actions)+0.1))
      # torques = np.mean(self.action_buffer, axis=0)
      # self.action_buffer.append(torques)

    self.input_torque = np.clip(torques, -self.max_torque, self.max_torque) 
    # print(self.input_torque)
    if not self.sanity_check:
      self.publish_actions(self.input_torque)
    self.step_sim()
    if not self.sanity_check:
      self.get_observation()

    if self.steps >= 400:
      self.metric.append(np.mean((np.array(self.joints) - np.array(self.target_joints))**2) + np.mean((np.array(self.joint_vel) - np.array(self.target_joint_vel))**2))
      self.pos_metric.append(np.mean((np.array(self.joints) - np.array(self.target_joints))**2))
      self.torque_metric.append(np.sum(abs(self.input_torque)))
      # print(np.mean((np.array(self.joints) - np.array(self.target_joints))**2))

    if len(self.metric) == self.sample_size:
      self.metrics.append(np.mean(self.metric))
      self.metric = deque(maxlen=self.sample_size)
      self.pos_metrics.append(np.mean(self.pos_metric))
      self.pos_metric = deque(maxlen=self.sample_size)
      self.torque_metrics.append(np.mean(self.torque_metric))
      self.torque_metric = deque(maxlen=self.sample_size)

    reward, self.done = self.get_reward(self.input_torque)

   
    if self.steps >= 400:
      if self.controller_type == 'explore':
        if self.controller_state == 'goto':
          time_to_target = 100  
          if self.steps - self.current_steps < time_to_target:
            self.target_joints = list(((self.steps - self.current_steps)%time_to_target)*((np.array(self.sample) - np.array(self.current_position))/time_to_target) + np.array(self.current_position))
          else:
            self.target_joints = list(self.sample)
      elif self.controller_type == 'policy':
        self.sample_pointer += 1
        if self.sample_pointer > self.sample_size - 1:
          self.sample_pointer = 0
        temp = self.samples[self.sample_pointer,:]
        self.target_joints, self.target_joint_vel = list(temp[:3]), list(temp[3:6])
      elif self.controller_type == 'test':
        time_to_target = 100        
        if self.steps % time_to_target == 0:
          self.simple_target = self.simple_targets[self.sample_pointer]
          self.sample_pointer += 1
          if self.sample_pointer == len(self.simple_targets):
            self.save_sim_data()
        self.target_joints = list((self.steps%time_to_target)*((np.array(self.simple_targets[self.sample_pointer]) - np.array(self.simple_targets[self.sample_pointer-1]))/time_to_target) + np.array(self.simple_targets[self.sample_pointer-1]))
        # print(self.target_joints)
      elif self.controller_type == 'expert':
        # time_to_target = 100    
        # if self.steps < (400 + time_to_target):    
        #   if self.steps == 400:
        #     self.simple_target = self.samples[self.sample_pointer][:3]
        #   self.target_joints = list((((self.steps-400)%time_to_target)*(np.array(self.simple_target) - np.array(self.zero_position))/time_to_target) + np.array(self.zero_position))
        #   self.target_joint_vel = [0,0,0]
        # else:
        self.sample_pointer += 1
        if self.sample_pointer > self.sample_size - 1:
          self.sample_pointer = 0
        # self.rand = False
        # self.rand = True
        if self.args.rand:
          self.target_joints = np.clip(np.array(self.samples[self.sample_pointer][:3]) + self.rand_net.step(np.array(self.joints+self.joint_vel)), self.rand_limits_min, self.rand_limits_max)
          # self.target_joints = np.clip(self.target_joints, self.prev_target - 0.01, self.prev_target + 0.01)
          self.target_joints = np.clip(self.target_joints, self.prev_target - 0.035, self.prev_target + 0.035)
          # print(self.target_joints - self.prev_target)
          # self.prev_target = self.target_joints
          # self.target_joints = self.scale*np.array(self.samples[self.sample_pointer][:3]) + self.rand_net.step(np.array(self.joints+self.joint_vel)) + self.offset
        else:
          self.target_joints = np.array(self.samples[self.sample_pointer][:3])
        self.target_joint_vel = np.array(self.samples[self.sample_pointer][3:6])
        # print(np.array(self.target_joints) - self.prev_target)
    elif self.steps < 400 and self.steps >= 200:
      time_to_target = 100        
      self.sample = np.array([0.0,0.0,1.5707])
      if (self.steps - 200) < time_to_target:
        self.target_joints = list(((self.steps-200)%time_to_target)*((np.array(self.sample) - np.array(self.initial_position))/time_to_target) + np.array(self.initial_position))
      else:
         self.target_joints = list(self.sample)
      self.target_joint_vel = [0,0,0]
      if self.steps == 399:
        self.controller_state = 'explore'
      else:
        self.controller_state = 'goto'
    elif self.steps < 200:
      self.sample = list(self.initial_position)
      self.target_joints = self.sample
      self.target_joint_vel = [0,0,0]
      self.controller_state = 'goto'
    self.prev_target = np.array(self.target_joints)
    

    self.rewards += reward
    self.steps += 1
    self.total_steps += 1
    if self.entry_count % 1000 == 0:
      print("closeness to expert: ", np.mean(self.metric))
      # print(self.metric)
      print(np.array(self.inputs).shape, np.array(self.labels).shape)
      print(self.entry_count)



    if self.record_step and self.entry_count > self.desired_sim_length:
      self.save_sim_data()

    if self.sanity_check:
      self.joints, self.joint_vel, reward, self.done = self.sanity_env.step(self.input_torque, self.target_joints)

    if self.record_step: 
      self.record_sim_data()
    return np.array(list(self.joints) + list(self.joint_vel) + list(self.target_joints)), reward, self.done, None

  def step_sim(self):
    while time.time() - self.t1 < self.timestep:
      time.sleep(0.00001)
    self.t1 = time.time()

  def get_reward(self, actions):
    reward = 1
    done = False

    # if not self.args.test_pol and self.steps > self.seq_length:
    #   done = True
    # if self.args.test_pol and self.steps > 1000:
    #   done = True
    return reward, done

  def get_observation(self):
    '''
    Robot state
    '''   
    self.joints = [self.joint_dict[key]['pos'] for key in self.motor_names]
    self.joint_vel = [self.joint_dict[key]['vel'] for key in self.motor_names]
    self.joint_effort = [self.joint_dict[key]['effort'] for key in self.motor_names]

  def jointStateCallback(self, data):
    for i, n in enumerate(data.name):
      self.joint_dict[n]['pos'] = data.position[i]
      self.joint_dict[n]['vel'] = data.velocity[i]
      self.joint_dict[n]['effort'] = data.effort[i]

  def save_sim_data(self):
    # path = self.PATH
    if not self.sanity_check:
      now = datetime.now()
      self.base_directory = home + '/data/' + now.strftime("%d_%m_%Y") + '/'
      self.batch_name = self.args.exp + '_' + now.strftime("%Y_%m_%d_%H_%M_%S")
      self.save_path = os.path.join(self.base_directory, self.batch_name)
      os.makedirs(self.save_path)
      print("saving to ", self.save_path)
      latest = os.path.join(self.base_directory, 'latest')
      try:
        if os.readlink(latest):
          os.remove(latest)
      except OSError:
        pass
      try:
        os.symlink(self.batch_name, latest)
      except OSError:
        pass
      path = self.save_path 
      try:
        np.save(path + "/inputs.npy", np.array(self.inputs))
        np.save(path + "/observed_torques.npy", np.array(self.torques))
        np.save(path + "/labels.npy", np.array(self.labels))
        
        np.save(path + "/eth_inputs.npy", np.array(self.eth_inputs))
        np.save(path + "/eth_labels.npy", np.array(self.eth_labels))

      except Exception as e:
        print("Save sim data error:")
        print(e)

    self.nice_finish()  

  def record_sim_data(self):

    for m,j,v,it,dt in zip(self.motor_names, self.prev_joints, self.prev_joint_vel, self.input_torque, self.joint_effort):
      self.eth_input_buf[m].extend([j,v,it])
      self.eth_inputs.append(np.array(self.eth_input_buf[m]))
      self.eth_labels.append(dt)

    # self.input_buf.append(self.prev_state + list(self.input_torque))
    # self.label_buf.append(self.joints + self.joint_vel)
    self.targets.append(list(self.target_joints) + list(self.target_joint_vel))
    self.inputs.append(self.prev_state + list(self.input_torque))
    self.torques.append(list(self.joint_effort))
    self.labels.append(self.joints + self.joint_vel)
    self.entry_count += 1

  def nice_finish(self):
    print()
    print("*** going to safe place and turning torques to zero ***") 
    print()
    sum_error = 0
    time_to_target = 100
    start_joints = self.joints
    for steps in range(time_to_target):
      target_joints = (steps*(np.array([0,0.2,1.2]) - np.array(start_joints))/time_to_target + np.array(start_joints))
      error = target_joints - np.array(self.joints)
      torques = self.Kp*error + self.Ki*sum_error - self.Kd*np.array(self.joint_vel)
      sum_error = np.clip(error + sum_error, -10, 10)
      torques = np.clip(torques, -self.max_torque, self.max_torque)
      # print(target_joints, torques)

      if self.sanity_check:
        sanity_joints, sanity_joint_vel, reward, done = self.sanity_env.step(torques, target_joints)
        self.joints, self.joint_vel = sanity_joints, sanity_joint_vel
      else:
        self.publish_actions(torques)
      self.step_sim()
      self.get_observation()
      # print(torques)
            
    print()

    print("setting velocity to zero")
    if self.sanity_check:
      sanity_env_state = self.sanity_env.step([0.0,0.0,0.0], [0,0.2,1.2])
    else:
      self.publish_actions([0.0,0.0,0.0])

    # Open some plots? Playback torques/positions in sanity env?
    inputs = np.array(self.inputs)
    torques = np.array(self.torques)
    targets = np.array(self.targets)
    input_pos = np.array(self.labels)[:,:6]
    input_torque = inputs[:,6:]

    print()
    print("=================== results ===================== ") 
    print("mean", np.mean(self.metrics))
    print("pos mean", np.mean(self.pos_metrics))
    print("torque mean", np.mean(self.torque_metrics))
    print("all: ")
    print(self.metrics)
    print(self.pos_metrics)
    if not self.sanity_check:
      np.save(self.save_path + '/metrics.npy', np.array(self.metrics))
      np.save(self.save_path + '/pos_metrics.npy', np.array(self.pos_metrics))
      np.save(self.save_path + '/torque_metrics.npy', np.array(self.torque_metrics))
    print("================================================= ") 

    print()

    print("plotting")
    # subplot([[inputs[:,0]],[inputs[:,1]],[inputs[:,2]],[inputs[:,3]],[inputs[:,4]],[inputs[:,5]],[inputs[:,6]],[inputs[:,7]],[inputs[:,8]]], legend=[['coxa_pos'],['femur_pos'],['tibia_pos'],['coxa_vel'],['femur_vel'],['tibia_vel'],['coxa_torque'],['femur_torque'],['tibia_torque']], PATH=home + '/Desktop/')

    if not self.sanity_check:
      subplot([[inputs[:,0],targets[:,0]],[inputs[:,1],targets[:,1]],[inputs[:,2],targets[:,2]],[inputs[:,3],targets[:,3]],[inputs[:,4],targets[:,4]],[inputs[:,5],targets[:,5]],[inputs[:,6], torques[:,0]],[inputs[:,7],torques[:,1]],[inputs[:,8],torques[:,2]]], legend=[['coxa_pos','target'],['femur_pos','target'],['tibia_pos','target'],['coxa_vel','target'],['femur_vel','target'],['tibia_vel','target'],['coxa_torque','observed_torque'],['femur_torque','observed_torque'],['tibia_torque','observed_torque']], PATH=self.save_path + '/')
      self.sanity_env = PlayBackEnv(self.args)
    else:
      subplot([[inputs[:,0],targets[:,0]],[inputs[:,1],targets[:,1]],[inputs[:,2],targets[:,2]],[inputs[:,3],targets[:,3]],[inputs[:,4],targets[:,4]],[inputs[:,5],targets[:,5]],[inputs[:,6], torques[:,0]],[inputs[:,7],torques[:,1]],[inputs[:,8],torques[:,2]]], legend=[['coxa_pos','target'],['femur_pos','target'],['tibia_pos','target'],['coxa_vel','target'],['femur_vel','target'],['tibia_vel','target'],['coxa_torque','observed_torque'],['femur_torque','observed_torque'],['tibia_torque','observed_torque']], PATH=home + '/Desktop/')

    self.sanity_env.play(input_torque, input_pos, self.initial_position)

    exit()
