import pybullet as p
import numpy as np
import os   
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from models import rand_net
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD

def normalise(inputs, inputs_max, inputs_min):
  return (inputs - inputs_min)/(inputs_max - inputs_min)

def denormalise(inputs, inputs_max, inputs_min):
  # x = (max - min) * z + min
  return ((inputs_max - inputs_min)*inputs) + inputs_min

class Env():
  rank = comm.Get_rank()
  # ob_size = 18
  ob_size = 3
  # ob_size = 18*3
  ac_size = 3
  im_size = [48,48,4]
  # simtimeStep = 1/800
  simtimeStep = 1/100
  steps_per_sec = 1/simtimeStep
  # simtimeStep = 1/60
  actionRepeat = 1
  rew_buffer = deque(maxlen=5)
  episodes = -1

  def __init__(self, render=False, PATH=None, args=None, horizon=500, display_expert=True, record_step=True, cur=False, test=False, use_expert=False, act_model=None, sim_model=None):
      
    # data_path, data_type = home + '/data/iros_data/gz_sample/', ''
    # data_path, data_type = home + '/data/iros_data/gz_fm_100_real_rand/', ''
    # self.samples = np.array(pd.read_excel(data_path + 'inputs.xlsx'))[1:,:]
    self.samples = np.load('samples/samples.npy')
    self.torque_samples = np.load('/samples/torque_samples.npy')
    # self.sample_size = self.samples.shape[0]
    self.sample_size = self.torque_samples.shape[0]
    print("Loaded reference data", self.samples.shape)
    self.desired_sim_length = 100000

    self.act_model = act_model
    self.args = args
    # if self.act_model is not None:
    #   self.rnn_length = self.act_model.rnn_length
    self.sim_model = sim_model
    self.model_breakdown = True
    # self.model_breakdown = False
    self.render = render
    self.PATH = PATH
    self.display_expert = display_expert
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.use_expert = use_expert
    # self.all_legs = True
    self.all_legs = False
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot

    self.load_model()
    self.sim_data = []
    self.inputs = []
    self.labels = []

  def load_model(self):
    p.loadMJCF("./assets/ground.xml")
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.Id = objs
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.exp1_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)

    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      print(jname)
      self.jdict[jname] = j
    self.exp1_jdict = {}
    for j in range( p.getNumJoints(self.exp1_Id) ):
      info = p.getJointInfo(self.exp1_Id, j)
      link_name = info[12].decode("ascii")
      # self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      print(jname)
      # lower, upper = (info[8], info[9])
      # self.ordered_joints.append( (j, lower, upper) )
      self.exp1_jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]
    self.exp1_motors = [self.exp1_jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    # self.vel_max = np.array([20,20,20])
    self.vel_max = np.array([8,11,8])
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}
    
    self.collect_data = True
    # self.collect_data = False
    # Disable motors to use torque control:
    if self.collect_data:
      p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp1_Id, self.exp1_motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    
    # data_path, data_type = '/home/brendan/data/real_test_leg/', ''
    # data_path, data_type = '/home/brendan/results/hex_model/latest/gz_data/', ''
    # self.test_inputs = np.array(pd.read_excel(data_path + 'inputs.xlsx'))[1:,:]
    # test_labels = np.array(pd.read_excel(data_path + 'test_' + data_type + 'labels.xlsx'))
    # self.test_labels = test_labels - self.test_inputs[:,:6]
    # self.test_labels = test_labels
    # print("expert size", self.test_inputs.shape)

    self.rand_net = rand_net.Net(input_size=6, output_size=3, max_actions=np.array([7.5,7.5,2]))
    # self.rand_net = rand_net.Net(6, 3, max_actions=np.array(7.5,7.5,2))
    # self.rand_net = rand_net.Net()

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self):
    
    self.rand_net.initialise()

    self.freq = 1
    self.coxa_speed, self.femur_speed, self.tibia_speed = 1,1,1
    self.coxa_limit, self.femur_limit, self.tibia_limit = False, False, False
    self.sample_pointer = 0
    temp = self.samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torque = temp[:3], temp[3:6], self.exp_torque_samples[self.sample_pointer]
    self.exp_joints, self.exp_joint_vel = temp[:3], temp[3:6]
    self.exp_torque = self.torque_samples[self.sample_pointer,:]
    self.coxa, self.femur, self.tibia = self.exp_joints
    # self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel)
    # self.set_position([0,1,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, robot_id=self.exp1_Id)
    self.coxa_max, self.coxa_min = 0.8, -0.8
    self.femur_max, self.femur_min = 0.5, -1.0
    self.tibia_max, self.tibia_min = 2.5, 0.5

    # self.set_position([0,0,2], [0,0,0,1], [np.random.uniform(self.coxa_min, self.coxa_max),np.random.uniform(self.femur_min, self.femur_max),np.random.uniform(self.tibia_min, self.tibia_max)], joint_vel=[0.0,0.0,0.0])
    self.set_position([0,0,2], [0,0,0,1], [np.random.uniform(self.coxa_min, self.coxa_max),np.random.uniform(self.femur_min, self.femur_max),np.random.uniform(self.tibia_min, self.tibia_max)], joint_vel=[0.0,0.0,0.0])
    self.set_position([0,1,2], [0,0,0,1], [0.0,0.0,1.5], joint_vel=[0.0,0.0,0.0], robot_id=self.exp1_Id)
    
    self.coxa_x, self.femur_x, self.tibia_x = 0,0,0
    self.input_torque = np.zeros(self.ac_size)
    self.input_sequence = deque([[0]*9]*32,maxlen=32)
    self.sim_model_input = deque([[0]*12]*32,maxlen=32)
    # self.sim_model_input = deque([[0]*9]*1,maxlen=1)
    self.data_size = 100
    self.all_data  = {n:deque([[0]*3]*self.data_size,maxlen=self.data_size) for n in ['input','output','exp_input','exp_output']}
    self.sample_pointer = 0
    # self.exp_joints, self.exp_joint_vel = self.test_inputs[self.sample_pointer,:3],self.test_inputs[self.sample_pointer,3:6]
    # self.set_position([0,0,2], [0,0,0,1], self.exp_joints, joint_vel = self.exp_joint_vel)
    # self.set_position([0,1,2], [0,0,0,1], self.exp_joints, joint_vel = self.exp_joint_vel, robot_id=self.exp1_Id)
    # self.set_position([0,0,2], [0,0,0,1], [0.0,-0.4,1.85])
    # self.set_position([0,1,2], [0,0,0,1], [0.0,0.0,1.5], robot_id=self.exp1_Id)

    self.episodes += 1
    self.total_reward = 0
    self.steps = 0
    jointStates = p.getJointStates(self.exp1_Id,self.ordered_joint_indices)
    self.exp_joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp_joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    self.get_observation()
    self.prev_state = self.joints + self.joint_vel
    self.prev_exp_state = self.joints + self.joint_vel
    if self.record_step:
      self.save_sim_data()
    self.desired_torque = [0,0,0]
    # return np.array(self.desired_torque + self.exp_joints + self.exp_joint_vel+list(self.history))
    # return np.array(self.desired_torque + self.joints + self.joint_vel)
    return np.array(self.desired_torque)

  def step(self, actions):
    # delta_torque = np.array([np.random.uniform(-0.75,0.75),np.random.uniform(-0.5,0.5),np.random.uniform(-0.15,0.15)])
    # # des_pos = np.array([np.random.uniform(-0.8,0.8),np.random.uniform(-1.0,0.5),np.random.uniform(0.5,2.5)])
    # if (self.joints[0] < -0.5 and delta_torque[0] < 0) or (self.joints[0] > 0.5 and delta_torque[0] > 0):
    #   delta_torque[0] *= -1
    # if (self.joints[1] < -0.7 and delta_torque[1] < 0) or (self.joints[1] > 0.2 and delta_torque[1] > 0):
    #   delta_torque[1] *= -1
    # if (self.joints[2] < 0.8 and delta_torque[2] < 0) or (self.joints[2] > 2.1 and delta_torque[2] > 0):
    #   delta_torque[2] *= -1
    # self.input_torque += delta_torque    
    # self.input_torque = np.clip(self.input_torque, -np.array([5,3,1]),np.array([5,3,1]))
    # print(self.input_torque)
    # print()
    # if self.steps % 10 == 0:
    
    # scale*np.sin(self.steps%)
    torques = self.rand_net.step(np.array(self.joints+self.joint_vel))
    # print(torques)
    self.input_torque = torques
    # self.input_torque = self.get_torques()

    # if self.steps == 0 or self.sample_pointer == 0:
    #   # sign, scale, frequency.
    #   self.scale = np.array([np.random.choice((-1,1))*np.random.uniform(0.1,1),1*np.random.uniform(0.1,1),np.random.choice((-1,1))*np.random.uniform(0.1,1)]) 
    #   self.freq = np.random.choice((0.5, 0.75, 1, 1.5, 2),p=[0.15,0.2,0.3,0.2,0.15])
    #   # self.scale = np.array([0.8,0.8,0.8]) 
    #   # self.freq = 1.0
    #   # print(self.scale, self.freq)

    #   # self.means = [np.random.uniform(-7.5,7.5),np.random.uniform(-7.5,7.5),np.random.uniform(-2,2)] 
    #   # print(self.means)
    # self.input_torque = self.scale * self.exp_torque
    # self.input_torque = [np.random.normal(self.means[0],2.5),np.random.normal(self.means[1],2.5),np.random.normal(self.means[2],1)]
    # self.input_torque = [np.random.uniform(-5,5),np.random.uniform(-3,3),np.random.uniform(-1,1)]
    # self.input_torque = [np.random.normal(0,5),np.random.normal(-2,3),np.random.normal(0,1.25)]
    # self.input_torque = self.exp_torque
    self.prev_state = self.joints + self.joint_vel    
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel

    # des_pos = np.array([np.random.uniform(-0.8,0.8),np.random.uniform(-1.0,0.5),np.random.uniform(0.5,2.5)])
    # max_pos = np.array(self.joints) + self.simtimeStep * self.vel_max
    # min_pos = np.array(self.joints) - self.simtimeStep * self.vel_max
    # des_pos = np.clip(des_pos, min_pos, max_pos)
    # pos_error = des_pos - np.array(self.joints)
    # print(pos_error)

    # pos_error = np.clip(pos_error, -0.1,0.1)
    # pos_error = np.clip(pos_error, -0.4,0.4)
    # print(pos_error)
    # self.input_torque = np.clip(1000*pos_error - 0.1*np.array(self.joint_vel), -np.array(self.tor_max), np.array(self.tor_max))
    # # print(self.input_torque)
    # des_pos = self.get_des_joint_pos()
    # pos_error = (des_pos - np.array(self.joints)) + np.array([np.random.uniform(-0.2,0.2), np.random.uniform(-0.2,0.2), np.random.uniform(-0.2,0.2)])
    # # self.input_torque = np.clip(40*pos_error - 0.1*np.array(self.joint_vel), -np.array(self.tor_max), np.array(self.tor_max))
    # # self.input_torque = np.clip(100000*pos_error , -np.array(self.tor_max), np.array(self.tor_max))
    # self.input_torque = np.array([10*pos_error[0], 10*pos_error[1], 2*pos_error[2]])
    # self.input_torque = [50*pos_error[0], 80*pos_error[1], 10*pos_error[2]]
    # self.input_torque = [100*pos_error[0], 100*pos_error[1], 10*pos_error[2]]
    # self.input_torque = [np.random.normal(0,5),np.random.normal(-2,3),np.random.normal(0,1.25)]
    # Clip to some value close to previous value (don't want to go crazy)
    
    # p.setJointMotorControlArray(self.exp1_Id, self.exp1_motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque)
  

    self.set_position([0,1,2], [0,0,0,1], self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp1_Id)
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque)
    p.stepSimulation()
    # if not self.collect_data:
    jointStates = p.getJointStates(self.exp1_Id,self.ordered_joint_indices)
    self.exp_joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp_joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
      # self.all_data['exp_output'].append(np.array(self.exp_joints + self.exp_joint_vel) - np.array(self.prev_exp_state))
      # self.all_data['output'].append(net_out)

    if self.render:
      time.sleep(0.01)
    if len(self.inputs) % 1000 == 0: 
      print(len(self.inputs))
    self.get_observation()
    if self.record_step: 
      self.record_sim_data()
    reward, done = self.get_reward()
    self.total_reward += reward
    self.steps += 1


    self.sample_pointer += int(8*self.freq)
    if self.sample_pointer > self.sample_size - 8*self.freq:
      self.sample_pointer = 0
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel = temp[:3], temp[3:6]
    self.exp_torque = self.torque_samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torque = temp[:3], temp[3:6], self.exp_torque_samples[self.sample_pointer]

    return np.array(self.desired_torque), reward, done, None

  def get_reward(self):
    reward = 0
    done = False

    # self.coxa_max, self.coxa_min = 0.35, -0.35
    # self.femur_max, self.femur_min = 0.0, -0.7
    # self.tibia_max, self.tibia_min = 2.2, 1.5
    self.coxa_max, self.coxa_min = 0.8, -0.8
    self.femur_max, self.femur_min = 0.5, -1.0
    self.tibia_max, self.tibia_min = 2.5, 0.5

    # if self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joint_vel[0] < -self.vel_max[0] or self.joint_vel[0] > self.vel_max[0]:
    if self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max:
      done = True
    if self.joints[1] < self.femur_min or self.joints[1] > self.femur_max:
      done = True
    if self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max:
      done = True

    if self.steps == 2000:
    # if self.steps == 2999:
    # if self.steps == 29999:

      done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    # print(jointStates)
    # self.ob_dict = {self.state[i]:jointStates[j[0]][0] for i,j in enumerate(self.ordered_joints[:int(18)])}
    # self.ob_dict.update({self.state[i+18]:jointStates[j[0]][1] for i,j in enumerate(self.ordered_joints[:int(18)])})
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    # print(self.joints, s)

  def get_torques(self):
    # self.coxa_middle, self.femur_middle, self.tibia_middle = 0.0, -2, 0.0
    self.coxa_middle, self.femur_middle, self.tibia_middle = 0.0, 0.0, 0.0
    # if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.coxa_speed) == 0:
      # coxa_max, coxa_min, self.coxa_middle = 7, -7, 0.0
    #   self.coxa_speed = np.random.uniform(0.1,1.25)
    #   self.coxa_scale  = np.random.uniform(0,1.0)*(coxa_max - coxa_min)/2
    #   self.coxa_x = 0.0

    # if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.femur_speed) == 0:
    #   femur_max, femur_min, self.femur_middle = 2, -5, -2.5
    #   self.femur_speed = np.random.uniform(0.1,1.25)
    #   self.femur_scale = np.random.uniform(0,1.0)*(femur_max - femur_min)/2
    #   self.femur_x = 0.0

    # if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.tibia_speed) == 0:
    #   tibia_max, tibia_min, self.tibia_middle = 1.5, -1.5, 0.0
    #   self.tiba_speed = np.random.uniform(0.1,1.25)
    #   self.tibia_scale = np.random.uniform(0,1.0)*(tibia_max - tibia_min)/2
      # self.tibia_x = 0.0
    # self.coxa_speed, self.femur_speed, self.tibia_speed = 0.25, 0.25, 0.25
    self.coxa_speed, self.femur_speed, self.tibia_speed = 1.0,1.0,1.0
    self.coxa_scale, self.femur_scale, self.tibia_scale = 0.2,0.0,0.0
    self.coxa_x += 2*np.pi/(self.steps_per_sec/self.coxa_speed)    
    self.coxa = self.coxa_scale*np.sin(self.coxa_x) + self.coxa_middle
    self.femur_x += 2*np.pi/(self.steps_per_sec/self.femur_speed)    
    self.femur = self.femur_scale*np.sin(self.femur_x) + self.femur_middle
    self.tibia_x += 2*np.pi/(self.steps_per_sec/self.tibia_speed)    
    self.tibia = self.tibia_scale*np.sin(self.tibia_x) + self.tibia_middle
    self.coxa = 5*np.sin(self.steps*0.1)
    self.femur = 5*np.sin(self.steps*0.1) - 2
    self.tibia = 2*np.sin(self.steps*0.1) 
    # self.femur = 5*np.sin(self.steps*0.2)
    
    print([self.coxa, self.femur, self.tibia])


    return [self.coxa, self.femur, self.tibia]

  # def get_torques(self):
  #   # Limits of real controller:
  #   # --------------------   
  #   # self.coxa_max, self.coxa_min = 0.35, -0.35
  #   # self.femur_max, self.femur_min = 0.0, -0.7
  #   # self.tibia_max, self.tibia_min = 2.2, 1.5
  #   # Conservative limits
  #   self.coxa_max, self.coxa_min = 0.0, 0.0
  #   self.femur_max, self.femur_min = -0.35, -0.35
  #   self.tibia_max, self.tibia_min = 1.8,1.8
  #   # --------------------
  #   # Actual joint limits:
  #   # --------------------   
  #   # coxa_max, coxa_min = 0.8, -0.8
  #   # femur_max, femur_min = 0.5, -1.0
  #   # tibia_max, tibia_min = 2.5, 0.5
  #   scale = 0.01
  #   if np.random.random() < 0.2 or self.steps == 0:
  #     self.coxa_dt = 0.02*(np.random.random()-0.5) 
  #     self.coxa_limit = np.random.uniform(0,2)

  #     # self.coxa_limit = np.random.uniform(0,5)
  #     # print("coxa", self.coxa_dt)
  #   if np.random.random() < 0.2 or self.steps == 0:
  #     self.femur_dt = 0.02*(np.random.random()-0.5) 
  #     self.femur_limit = np.random.uniform(0,2)

  #     # print("femur", self.femur_dt)
  #   if np.random.random() < 0.2 or self.steps == 0:
  #     self.tibia_dt = 0.02*(np.random.random()-0.5) 
  #     self.tibia_limit = np.random.uniform(0,1)
  #     # print("tibia", self.tibia_dt)
    
  #   if self.coxa > self.coxa_limit:
  #     self.coxa_dt = abs(self.coxa_dt)*-1
  #   if self.coxa < -self.coxa_limit:
  #     self.coxa_dt = abs(self.coxa_dt)
  #   if self.femur > self.femur_limit:
  #     self.femur_dt = abs(self.femur_dt)*-1
  #   if self.femur < -self.femur_limit:
  #     self.femur_dt = abs(self.femur_dt)
  #   if self.femur > self.femur_limit:
  #     self.femur_dt = abs(self.femur_dt)*-1
  #   if self.femur < -self.femur_limit:
  #     self.femur_dt = abs(self.femur_dt)


  #   # max_dt = 0.01
  #   if self.joints[0] > self.coxa_max:
  #     self.coxa_dt = abs(self.coxa_dt)*-1
  #     # self.coxa_dt = -max_dt
  #   if self.joints[0] < self.coxa_min:
  #     self.coxa_dt = abs(self.coxa_dt)
  #     # self.coxa_dt = max_dt
  #   if self.joints[1] > self.femur_max:
  #     # print("femur max", self.joints[1], self.femur_max, self.femur_dt, self.femur)
  #     self.femur_dt = abs(self.femur_dt)*-1
  #     # self.femur_dt = -max_dt
  #   if self.joints[1] < self.femur_min:
  #     self.femur_dt = abs(self.femur_dt)
  #     # self.femur_dt = max_dt
  #     # print("femur min", self.joints[1], self.femur_min, self.femur_dt, self.femur)
  #   if self.joints[2] > self.tibia_max:
  #     self.tibia_dt = abs(self.tibia_dt)*-1
  #     # self.tibia_dt = -max_dt
  #   if self.joints[2] < self.tibia_min:
  #     self.tibia_dt = abs(self.tibia_dt)
  #     # self.tibia_dt = max_dt

  #   # if self.joints[0] > self.coxa_max or self.joint_vel[0] > self.vel_max[0]:
  #   #   self.coxa_dt = abs(self.coxa_dt)*-1
  #   # if self.joints[0] < self.coxa_min or self.joint_vel[0] < -self.vel_max[0]:
  #   #   self.coxa_dt = abs(self.coxa_dt)
  #   # if self.joints[1] > self.femur_max or self.joint_vel[1] > self.vel_max[1]:
  #   #   self.femur_dt = abs(self.femur_dt)*-1
  #   # if self.joints[1] < self.femur_min or self.joint_vel[1] < -self.vel_max[1]:
  #   #   self.femur_dt = abs(self.femur_dt)
  #   # if self.joints[2] > self.tibia_max or self.joint_vel[2] > self.vel_max[2]:
  #   #   self.tibia_dt = abs(self.tibia_dt)*-1
  #   # if self.joints[2] < self.tibia_min or self.joint_vel[2] < -self.vel_max[2]:
  #   #   self.tibia_dt = abs(self.tibia_dt)

  #   self.coxa  = np.clip(self.coxa+ self.coxa_dt ,-self.tor_max[0], self.tor_max[0])
  #   self.femur = np.clip(self.femur+self.femur_dt,-self.tor_max[1], self.tor_max[1])
  #   self.tibia = np.clip(self.tibia+self.tibia_dt,-self.tor_max[2], self.tor_max[2])
  #   print(self.coxa, self.femur, self.tibia)
  #   return [self.coxa, self.femur, self.tibia]

  # def get_torques(self):
  #   if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.coxa_speed) == 0:
  #     coxa_max, coxa_min, self.coxa_middle = 7, -7, 0.0
  #     self.coxa_speed = np.random.uniform(0.1,1.25)
  #     self.coxa_scale  = np.random.uniform(0,1.0)*(coxa_max - coxa_min)/2
  #     self.coxa_x = 0.0

  #   if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.femur_speed) == 0:
  #     femur_max, femur_min, self.femur_middle = 2, -5, -2.5
  #     self.femur_speed = np.random.uniform(0.1,1.25)
  #     self.femur_scale = np.random.uniform(0,1.0)*(femur_max - femur_min)/2
  #     self.femur_x = 0.0

  #   if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.tibia_speed) == 0:
  #     tibia_max, tibia_min, self.tibia_middle = 1.5, -1.5, 0.0
  #     self.tiba_speed = np.random.uniform(0.1,1.25)
  #     self.tibia_scale = np.random.uniform(0,1.0)*(tibia_max - tibia_min)/2
  #     self.tibia_x = 0.0
  #   # self.coxa_speed, self.femur_speed, self.tibia_speed = 0.25, 0.25, 0.25
  #   # self.coxa_speed, self.femur_speed, self.tibia_speed = 1.25, 1.25, 1.25
  #   self.coxa_x += 2*np.pi/(self.steps_per_sec/self.coxa_speed)    
  #   self.coxa = self.coxa_scale*np.sin(self.coxa_x) + self.coxa_middle
  #   self.femur_x += 2*np.pi/(self.steps_per_sec/self.femur_speed)    
  #   self.femur = self.femur_scale*np.sin(self.femur_x) + self.femur_middle
  #   self.tibia_x += 2*np.pi/(self.steps_per_sec/self.tibia_speed)    
  #   self.tibia = self.tibia_scale*np.sin(self.tibia_x) + self.tibia_middle
  #   return [self.coxa, self.femur, self.tibia]

  def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))
        # print("episodes", self.episodes)
        if len(self.sim_data) > self.desired_sim_length:
          # print(np.array(self.inputs).shape, np.array(self.labels).shape)
          df = pd.DataFrame (np.array(self.inputs))  
          df.to_excel(path + '/inputs.xlsx', index=False)
          
          df = pd.DataFrame (np.array(self.labels))  
          df.to_excel(path + '/labels.xlsx', index=False)
          exit()
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    # self.inputs.append(self.prev_state + self.input_torque)
    # self.labels.append(self.joints + self.joint_vel)

    # self.inputs.append(self.prev_state + self.input_torque)
    # self.labels.append(self.joints + self.joint_vel)
    
    self.inputs.append(self.prev_state + self.joints + self.joint_vel)
    self.labels.append(self.input_torque)
    
    # self.inputs.append(self.prev_joints + self.joints)
    # self.labels.append(self.input_torque)

    # self.inputs.append(self.exp_joints + self.exp_joint_vel + self.exp_torque)
    # self.labels.append(self.input_torque)

    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)
