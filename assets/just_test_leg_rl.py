import pybullet as p
import numpy as np
import os   
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from scripts.change_urdf import ChangeURDF
import cv2
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD

class Env():
  rank = comm.Get_rank()
  # ob_size = 9
  # ob_size = 6
  ac_size = 3
  # im_size = [48,48,4]
  im_size = [20,20,1]

  # simtimeStep = 1/100
  simtimeStep = 1/200
  episodes = 0
  Kp = 20
  initial_Kp = 20
  Kd = 0.97
  # Params 11_2 Kd = 0.99, Kc = 0.001
  # vel = -self.Kc*0.02*np.mean(np.array(self.joint_vel)**2)
  # tor = -self.Kc*0.01*np.mean(np.array(self.actions)**2)
  # neg = -self.Kc*0.2*np.mean((np.array(self.actions) - np.array(self.prev_actions))**2) 
  Kc = 0.001
  total_reward = 0
  total_steps = 0
  delay_length = 1
  def __init__(self, render=False, PATH=None, args=None, horizon=500, record_step=True, cur=False, test=False, test_pol=False, rand_dynamics=False, history=False):
    if history:
      self.ob_size = 60  
    else:
      self.ob_size = 9

    self.max_disturbance = 200

    self.test_pol = test_pol
    self.render = render
    self.PATH = PATH
    self.args = args
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.rand_dynamics = rand_dynamics

    self.samples = np.load('samples/samples.npy')
    self.sample_size = self.samples.shape[0]
    print("Loaded reference data", self.samples.shape)


    # if self.render:
    #   self.physicsClientId = p.connect(p.GUI)
    #   self.load_model()
    
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT)
    
    if self.rand_dynamics:
      if self.args.new_urdf:
        try:
          if self.args.obs_torque:
            # print('***using obs torque urdf ************')
            self.change_urdf = ChangeURDF('assets/obs_torque_new_urdf.urdf', debug=False, normal=False)
          else:
            # print('***using new urdf ************')
            self.change_urdf = ChangeURDF('assets/new_urdf.urdf', debug=False, normal=False)
        except:
            # print('***using new urdf ************')
            self.change_urdf = ChangeURDF('assets/new_urdf.urdf', debug=False, normal=False)
      else:
        # print('***using old urdf ************')
        self.change_urdf = ChangeURDF('assets/test_leg.urdf', debug=False, normal=False)

    self.load_model()

    self.sim_data = []
    self.exp_sim_data = []
    self.exp1_sim_data = []
    self.inputs = []
    self.labels = []
    self.rew_buffer = deque(maxlen=5)
    
    self.coxa_min, self.coxa_max  = -0.5, 0.5
    self.femur_min, self.femur_max  = 0.0, 0.4
    self.tibia_min, self.tibia_max = 1.0, 1.9

  def load_model(self):
    p.resetSimulation()
    p.loadMJCF("assets/ground.xml")

    if self.rand_dynamics:
    
      urdf_path = "assets/temp_urdf_" + self.args.exp + "_" + str(self.episodes+self.rank) + ".urdf"
      if self.rank == 0:
        print("loading urdf from ", urdf_path)
      self.change_urdf.write_new_dynamics(urdf_path)
      self.Id = p.loadURDF(urdf_path)
      if self.rank == 0:
        print([p.getDynamicsInfo(self.Id, i)[0] for i in [1,2,3]])
      os.remove(urdf_path)
    else:
      if self.args.new_urdf:
        if self.args.obs_torque:
          if self.rank == 0:
            print("*********using obs_torque new urdf**********")
          urdf_path = "assets/obs_torque_new_urdf.urdf"
        else:
          if self.rank == 0:
            print("*********using new urdf**********")
          urdf_path = "assets/new_urdf.urdf"

      else:
        urdf_path = "assets/test_leg.urdf"

      objs = p.loadURDF(urdf_path)
      self.Id = objs
      if self.rank == 0:
        print("loading urdf from ", urdf_path)
        print([p.getDynamicsInfo(self.Id, i)[0] for i in [1,2,3]])
        
    objs = p.loadURDF(urdf_path)
    self.exp_Id = objs

    objs = p.loadURDF(urdf_path)
    self.exp1_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)


    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    self.joint_links = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      exp_info = p.getJointInfo(self.exp_Id, j)
      link_name = info[12].decode("ascii")
      exp_link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      self.joint_links.append(j)
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    # self.motor_power =  [5, 10, 5]
    # self.vel_max = [8,11,8]
    self.vel_max = [8,8,8]
    self.max_torque = np.array([10,10,3])
    # self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    
    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp1_Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    
  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self):


    self.Kc = self.Kc**(self.Kd)
    self.action_buffer = deque(maxlen=5)

    if self.rand_dynamics:
      self.load_model()

    # self.action_buffer.append([0,0,0])
    
    self.coxa_speed, self.femur_speed, self.tibia_speed = 1,1,1
    self.coxa_x, self.femur_x, self.tibia_x = 0,0,0
    self.sample_pointer = np.random.randint(0,self.sample_size)
    # self.sample_pointer = 0
    temp = self.samples[self.sample_pointer,:]
    # self.coxa, self.femur, self.tibia = [np.random.uniform(self.coxa_min,self.coxa_max),np.random.uniform(self.femur_min,self.femur_max),np.random.uniform(self.tibia_min,self.tibia_max)] 
    # self.coxa, self.femur, self.tibia = [np.random.uniform(-0.1,0.1),np.random.uniform(0.2,0.4),np.random.uniform(1.0,1.7)] 
    
    self.coxa, self.femur, self.tibia = temp[:3]
    self.initial_position  = [self.coxa, self.femur, self.tibia]
    self.exp_joints, self.exp_joint_vel, self.exp_torques = self.initial_position, temp[3:6], temp[6:]
    self.coxa_vel, self.femur_vel, self.tibia_vel = self.exp_joint_vel
    self.exp_joints = self.initial_position
    # if self.render:
    #   self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel])
    #   self.set_position([0,1,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel], robot_id=self.exp_Id)
    #   self.get_observation()
    # else:
    #   self.joints, self.joint_vel = self.initial_position, [0,0,0]
    
    self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel])
    self.set_position([0,1,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel], robot_id=self.exp_Id)
    self.set_position([0,2,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel], robot_id=self.exp1_Id)
    self.get_observation()
    
    self.episodes += 1
    self.rew_buffer.append(self.total_reward)
    
    self.total_reward = 0
    self.steps = 0
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel
    if self.record_step:
      self.save_sim_data()
      # if self.episodes > 1:
      #   exit()
    self.desired_torque = [0,0,0]
    self.prev_actions = [0.0,0.0,0.0]
    self.prev_prev_torque = self.prev_torque = [0.0,0.0,0.0]

    self.control_bit = 0
    return np.array(self.joints + self.joint_vel + self.exp_joints)
    

  def step(self, actions, next_state=None, cmd_torque=None):
    self.phase = self.sample_pointer/self.sample_size
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel

    self.original_actions = actions
    # actions = np.clip(actions, -self.max_torque, self.max_torque)
    if cmd_torque is not None:
      self.actions = cmd_torque
    else:
      self.actions = actions

    if not self.test_pol and self.args.add_dist and np.random.random() < 0.05:
      self.add_disturbance()  

    if self.args.fm:
      self.set_position([0,0,2],[0,0,0,1], joints=next_state[:self.ac_size],joint_vel=next_state[self.ac_size:self.ac_size*2], robot_id=self.Id)
      p.setJointMotorControlArray(self.exp1_Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=actions)
      self.set_position([0,1,2],[0,0,0,1], joints=self.exp_joints, robot_id=self.exp_Id)
      self.joints, self.joint_vel = list(next_state[:self.ac_size]), list(next_state[self.ac_size:self.ac_size*2])
      # print(self.joints, self.joint_vel)
    else:
      p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=actions)  
      p.setJointMotorControlArray(self.exp1_Id, self.motors, controlMode=p.TORQUE_CONTROL, forces=self.exp_torques)
      self.set_position([0,1,2],[0,0,0,1], joints=self.exp_joints, robot_id=self.exp_Id)
      p.stepSimulation()
      self.get_observation()

  
    # for n,m in enumerate(self.motors):
    #   p.setJointMotorControl2(self.Id, m,controlMode=p.POSITION_CONTROL, targetPosition=next_state[n], targetVelocity=next_state[n+self.ac_size], maxVelocity=7)
  
    
    if self.render:
      time.sleep(0.005)
    # print(self.joints, self.joint_vel)
    if self.record_step: 
      self.record_sim_data()
    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.action_buffer.append(self.actions)
    self.total_reward += reward
    

    # if self.steps >= 400:
    if True:
      self.sample_pointer += 1
      if self.sample_pointer > self.sample_size-1:
        self.sample_pointer = 0
      temp = self.samples[self.sample_pointer,:]
      self.exp_joints, self.exp_joint_vel, self.exp_torques = list(temp[:3]), list(temp[3:6]), temp[6:]
    elif self.steps < 200:
      self.exp_joints, self.exp_joint_vel = self.initial_position, [0,0,0]
    elif self.steps >= 200 and self.steps < 400:
      self.exp_joints, self.exp_joint_vel = [0.0,0.0,1.5707], [0,0,0]

    self.steps += 1
    self.total_steps += 1

    return np.array(self.joints + self.joint_vel + self.exp_joints), reward, done, None

  def get_reward(self):
    self.tips = p.getLinkState(self.Id, self.foot_link, computeLinkVelocity=True)
    self.tip_pos, self.tip_vel = self.tips[0], self.tips[-2]
    foot = np.array(p.getLinkState(self.Id, self.foot_link)[0])
    exp_foot = p.getLinkState(self.exp_Id, self.foot_link)[0]
    exp_foot = np.array([exp_foot[0], exp_foot[1]-1, exp_foot[2]])
    reward = 0
    # if self.steps < 400:
    pos = np.exp(-2*np.sum(abs(np.array(self.exp_joints) - np.array(self.joints))))
    # tor = np.exp(-5*np.sum(abs(np.array(self.exp_torques) - np.array(self.actions))))
    # print(self.joint_vel)
    # vel = np.exp(-0.1*np.sum(abs(np.array(self.exp_joint_vel) - np.array(self.joint_vel))))

    self.Kc = 0.001
    vel = -self.Kc*0.01*np.mean(np.array(self.joint_vel)**2)
    tor = -self.Kc*0.005*np.mean(np.array(self.actions)**2)
    neg = -self.Kc*0.1*np.mean((np.array(self.actions) - np.array(self.prev_actions))**2) 

    # vel = -self.Kc*0.008*np.mean(np.array(self.joint_vel)**2)
    # tor = -self.Kc*0.001*np.mean(np.array(self.actions)**2)
    # neg = -self.Kc*0.08*np.mean((np.array(self.actions) - np.array(self.prev_actions))**2) 

    # tip = np.exp(-20*np.sum(abs(foot - exp_foot)))
    tip = 0
    # tip = 0
    # neg = np.exp(-5*np.sum(abs(np.array(self.actions) - np.array(self.prev_actions)))) + 0.1*np.exp(-0.5*np.sum(abs(np.array(self.actions))))
    # if self.args.neg_rew:
      # neg = np.exp(-3*np.sum(abs(np.array(self.actions) - np.array(self.prev_actions)))) + np.exp(-0.5*np.sum(abs(np.array(self.actions))))
      # neg = np.exp(-5*np.sum(abs(np.array(self.actions) - np.array(self.prev_actions)))) + 0.1*np.exp(-0.5*np.sum(abs(np.array(self.actions))))
      # neg = max(-0.02*np.sum(abs(np.array(self.actions) - np.array(self.prev_actions))) - 0.005*np.sum(abs(np.array(self.actions))), -1)
      # neg = max(-0.01*np.sum(abs(np.array(self.actions) - np.array(self.prev_actions))) - 0.005*np.sum(abs(np.array(self.actions))), -1)
    # else:
      # neg = 0.0
    # else:
    #   pos = np.exp(-2*np.sum(abs(np.array(self.exp_torques) - np.array(self.actions))))
    #   vel = 0
    #   tip = 0
    #   neg = 0
    if not self.args.forward_residual:
      self.reward_breakdown['pos'].append(pos)
      self.reward_breakdown['tor'].append(tor)
      self.reward_breakdown['vel'].append(vel)
      self.reward_breakdown['neg'].append(neg)
      self.reward_breakdown['tip'].append(tip)
    # reward = 0.6*pos + 0.1*vel + 0.2*neg + 0.1*tip
    # reward = 0.7*pos + 0.1*vel + 0.2*neg
    reward = pos + vel + neg + tor
    # reward = 0.6*pos + 0.1*tor + 0.1*vel + 0.2*neg
      
    done = False
    if ((self.steps + 1) % 16 == 0 and self.steps != 0) and self.steps > 8*256-1:
      done = True
    return reward, done
 
  def get_observation(self):
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    # if self.test_pol:
    #   self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    #   self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]))
    # else:
    #   # self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]])+ np.random.uniform(-0.05,0.05,self.ac_size))
    #   # self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.uniform(-0.5,0.5,self.ac_size))
    #   self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]])+ np.random.normal(0,0.05,self.ac_size))
    #   self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.normal(0,0.5,self.ac_size))

    self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]))

    jointStates = p.getJointStates(self.exp1_Id,self.ordered_joint_indices)
    self.exp1_joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    
  def add_disturbance(self):
    for j in self.joint_links:
      force_x = (np.random.random() - 0.5)*self.max_disturbance
      force_y = (np.random.random() - 0.5)*self.max_disturbance
      force_z = (np.random.random() - 0.5)*self.max_disturbance
      # print("applying forces", force_x, force_y, force_z)
      
      p.applyExternalForce(self.Id,j,[force_x,force_y,force_z],[0,0,0],p.LINK_FRAME)

  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id  
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        # print("setting joints to ", joints, joint_vel)
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))       
        np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))       
        np.save(path + 'exp1_sim_data.npy', np.array(self.exp1_sim_data))       
        self.sim_data = []
        self.exp_sim_data = []
        self.exp1_sim_data = []
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):

    data = [[0,0,2], [0,0,0,1], self.joints]
    self.sim_data.append(data)

    data = [[0,1,2], [0,0,0,1], self.exp_joints]
    self.exp_sim_data.append(data)

    data = [[0,2,2], [0,0,0,1], self.exp1_joints]
    self.exp1_sim_data.append(data)

  def get_hm(self):
    return np.zeros(self.im_size)
