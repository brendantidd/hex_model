import numpy as np
import rospy
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState, Imu
from gazebo_msgs.srv import SetModelState,SetModelConfiguration
from gazebo_msgs.msg import ModelState, ModelStates
from dynamic_reconfigure.srv import Reconfigure
from dynamic_reconfigure.msg import Config, DoubleParameter
from leg_go_ros_msgs.msg import TipStates
import time
from scipy.spatial.transform import Rotation as R
import copy
import roslaunch
from std_srvs.srv import SetBool, Empty, Trigger
import pandas as pd
from collections import deque
import random
from pathlib import Path
home = str(Path.home())
from models import rand_net


class Env():
  ac_size = 3
  ob_size = 7
  im_size = [48,48,1]
  steps = 0
  total_steps = 0
  rewards = 0
  Kp = 400
  # timestep = 1/800
  timestep = 1/100
  steps_per_sec = 100
  def __init__(self, render=False, PATH=None, args=None, record_samples=False, horizontal=False):


    # data_path, data_type = '/home/brendan/data/my_rnn_real/', ''
    # self.samples = np.array(pd.read_excel(data_path + 'test_inputs.xlsx'))[1:,:]
    # self.exp_torque_samples = np.array(pd.read_excel(data_path + 'test_labels.xlsx'))[1:,:]
    self.samples = np.load(home + '/data/iros_data/my_rnn_real/samples_stand.npy')
    self.torque_samples = np.load(home + '/data/iros_data/my_rnn_real/torque_samples_stand.npy')
    # self.sample_size = self.samples.shape[0]
    self.sample_size = self.torque_samples.shape[0]
    print("Loaded reference data", self.samples.shape)
    self.desired_sim_length = 10000
    
    self.record_step = True
    self.rank = 0
    port = 1
    # os.environ["ROS_MASTER_URI"] = 'http://localhost:1131' + str(port)
    # os.environ["GAZEBO_MASTER_URI"] = 'http://localhost:1134' + str(port)
    terrain = 'flat'          
    robot_name = 'r1' 
    # roscore = subprocess.Popen(['roscore', '-p 1131' + str(port)])

    print("ROSCORE STARTED..")
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    collect_data = False
    # if collect_data:
    #   cli_args = ['./launch/test_leg_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=true']
    # else:
    #   cli_args = ['./launch/test_leg_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=false']
    cli_args = ['./launch/test_stand_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=false', 'whole_body_control:=false', 'locomotion_engine:=false']

    print(cli_args)
    roslaunch_args = cli_args[1:]
    roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
    parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
    parent.start()
    print("ROSLAUNCH STARTED..")

    # wait until publishing
    rospy.init_node('r1_rl_node', anonymous=False) 
    time.sleep(7.5)

    # if rank == 0:
    self.pause_sim = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
    self.unpause_sim = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)

    # rospy.wait_for_service('/gazebo/unpause_physics')
    # unpause_sim()
    # self.eth = True
    self.eth = False
        
    self.torque = True
    self.num_saved_samples = 0
    # self.samples = []     
    self.horizontal = horizontal
    self.pause_time = 100
    self.PATH = PATH
    self.DATA_PATH = '/home/brendan/data/actuator_all/'
    # self.DATA_PATH = '/home/brendan/data/actuator_expert/'
    self.record_samples = record_samples
    self.args = args
    self.robot_name = robot_name
    self.robot_number = 1
    self.y_offset = 0
    self.ob_dict = {}
    self.motor_names = ['TEST_coxa_joint','TEST_femur_joint','TEST_tibia_joint']
    # self.motor_power =  [15, 22, 15]
    self.motor_power =  [5, 10, 5]
    self.vel_max = [8,11,8]
    self.tor_max = [80,112,80]
    
    #
    # # State from PB
    self.state = ["TEST_coxa_joint_pos","TEST_femur_joint_pos","TEST_tibia_joint_pos"]
    self.state += ["TEST_coxa_joint_vel","TEST_femur_joint_vel","TEST_tibia_joint_vel"]
    # self.state += ["BR_coxa_joint_effort","BR_femur_joint_effort","BR_tibia_joint_effort"]

    # self.tip_names = ['AL', 'AR', 'BL', 'BR', 'CL', 'CR']    
    self.episodes = 0
    self.tip_states = {}
    self.initialise_subscribers()
    self.initialise_publishers()
    self.expert_scale = 0.1
    self.prev_torques = np.zeros(18)
    self.prev_acts = np.zeros(self.ac_size)
    self.joint_effort_data = np.zeros(self.ac_size)
    self.inputs = []
    self.labels = []
    self.dones = []
    self.my_inputs = []
    self.my_labels = []
    self.rand_net = rand_net.Net(input_size=6, output_size=3, max_actions=np.array([7.5,7.5,2]))

  
  def reset(self):
    self.sample_pointer = np.random.randint(0,self.sample_size)
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel, self.exp_torques = temp[:3], temp[4:7], temp[8:]
    # vertical slide lower"0.05" upper="0.7
    self.coxa, self.femur, self.tibia = self.exp_joints[0],self.exp_joints[1],self.exp_joints[2]
    self.exp_slide = temp[3]   
    # self.coxa_x, self.femur_x, self.tibia_x = 0,0,0

    self.joint_effort_data = [0.0,0.0,0.0]
    self.rewards = 0
    self.steps = 0
    self.episodes += 1
    
    self.pos_error_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.vel_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.label_torques = {n:0.0 for n in ['coxa','femur','tibia']}
    
    self.desired_torque_buf = deque([0.0]*3*5,maxlen=5*3)
    self.prev_state_buf = deque([0.0]*6*5,maxlen=5*6)

    set_joints = rospy.ServiceProxy('/gazebo/set_model_configuration', SetModelConfiguration())
    smc = SetModelConfiguration()
    smc.model_name = self.robot_name
    smc.urdf_param_name = ''
    smc.joint_names = ['TEST_coxa_joint', 'TEST_femur_joint', 'TEST_tibia_joint', 'vertical_slide_joint']
    smc.joint_positions = [self.coxa, self.femur, self.tibia, self.exp_slide]

    # print("waiting for set model config")
    rospy.wait_for_service('/gazebo/set_model_configuration')
    try:
      response_joints = set_joints(smc.model_name, smc.urdf_param_name, smc.joint_names, smc.joint_positions)
    except:
      print("Failed to reset joints")

    # set_model = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState())
    # sms = ModelState()
    # sms.model_name = self.robot_name
    # sms.pose.position.y = self.y_offset
    # # sms.pose.position.z = -0.045
    # # sms.pose.position.z = 1.0

    # # print("waiting for set model state")
    # rospy.wait_for_service('/gazebo/set_model_state')
    # try:
    #   response_model = set_model(sms)
    # except: 
    #   print("Failed to reset pose")

    self.step_sim()
    self.get_observation()

    # got_observation = False
    # while not got_observation and not rospy.is_shutdown():
    #   try:
    #     self.get_observation()
    #     got_observation = True
    #   except:
    #     print("trying to get observation")

    self.speed = 1.0
    self.step_length = 0.5
    if self.record_step and self.total_steps > 100000:
      self.save_sim_data()
      if self.episodes > 1: exit()
    # self.coxa, self.femur, self.tibia = 0,0,0
    self.prev_prev_torque = self.prev_torque = [0.0,0.0,0.0]
    self.actual_torque = self.prev_torq = [0.0,0.0,0.0]
    
    self.timesteps_per_step = 50
    # return np.array([self.ob_dict[s] for s in self.state] + [self.speed])
    # print(self.joints, self.joint_vel)
    # return np.array(self.joints + self.joint_vel + [self.speed, self.step_length])
    self.phase = self.sample_pointer/self.sample_size
    print("resetting.. ")

    return np.array(self.joints + self.joint_vel + [self.phase])

  def step(self, actions=None):
    # self.actions = [m*a for m,a in zip(self.motor_power, actions)]
    self.prev_state = self.joints + self.joint_vel
    if self.steps % (self.steps_per_sec*3) == 0:
      self.rate = np.random.randint(1,5)
      self.impulse = np.random.randint(10,20)
      print("new rate and impulse", self.rate, self.impulse)
      

    if (self.rate*self.steps)%self.steps_per_sec < self.impulse:
      self.input_torque = [0.0, 60, -40]
    else:
      max_dt = 0.5                                                                        
      self.maxes = np.array([5,3,1.25])*0.3
      self.input_torque = np.array([1*-self.joints[0], 80*np.clip(-self.joints[1],-max_dt,max_dt), 1*np.clip((2.0-self.joints[2]),-max_dt, max_dt)]) + 0.1*-np.array(self.joint_vel)[:3] + (np.random.random()-0.5)*self.maxes
    
    # self.input_torque = self.exp_torques

    if actions is not None:
      self.input_torque = actions
    
    self.delay = True
    
    if self.delay:
      self.publish_actions(self.prev_prev_torque)
      self.prev_prev_torque = self.prev_torque
      self.prev_torque = self.input_torque    
    else:
      self.publish_actions(self.input_torque)

    self.step_sim()
    self.get_observation()
    
    # Create artificial delay of 2 timesteps
    self.actual_torque = self.prev_torq
    self.prev_torq = self.input_torque

    reward, self.done = self.get_reward(self.input_torque)

    if self.record_step: 
      self.record_sim_data()
    
    self.rewards += reward
    self.steps += 1
    self.total_steps += 1
    # print(self.total_steps, self.total_steps%1000, self.total_steps% 1000 == 0)
    if self.total_steps % 1000 == 0:
      # print("why is this not printing?")
      print(len(self.inputs))
      print(self.total_steps)
    self.sample_pointer += 1
    if self.sample_pointer > self.sample_size-1:
      self.sample_pointer = 0
    self.phase = self.sample_pointer/self.sample_size
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel, self.exp_torques = temp[:3], temp[4:7], temp[8:]
    self.exp_slide = temp[3]
    # return np.array([self.ob_dict[s] for s in self.state] + [self.speed]), reward, done, self.ob_dict
    # return np.array(self.joints + self.joint_vel + [self.speed, self.step_length]), reward, done, self.ob_dict
    return np.array(self.joints + self.joint_vel + [self.phase]), reward, self.done, None

  def get_reward(self, actions):
    reward = 1; done = False
    # if self.total_steps > 100000 or self.joints[3] < 0.0475 or self.joints[2] < 0.7:
    if self.total_steps > 100000 or self.joints[3] < 0.0525 or self.steps > 2000:
      # print(self.joints)
    # if self.steps > 3000:
      done = True
    return reward, done

  def get_observation(self):
    '''
    Robot state
    '''   
    # pass
    self.joints = list(self.joint_pos_data)
    self.joint_vel = list(self.joint_vel_data)
    self.joint_effort = list(self.joint_effort_data)
    # self.ob_dict.update({j + '_pos': pos for j, pos in zip(self.motor_names, self.joint_pos)})
    # self.ob_dict.update({j + '_vel': vel for j, vel in zip(self.motor_names, self.joint_vel)})
    # self.ob_dict.update({j + '_effort': effort for j, effort in zip(self.motor_names, self.joint_effort)})
    # self.ob_dict.update({j + '_prev_act': a for j, a in zip(self.motor_names, self.prev_actions)})

  def get_des_joint_pos(self):
    # Pick new speed every 3 seconds
    if self.steps % (self.steps_per_sec*3) == 0:
      self.coxa_speed = np.random.uniform(0,0.75)
      self.femur_speed = np.random.uniform(0,0.75)
      self.tibia_speed = np.random.uniform(0,0.75)
      self.coxa_scale = np.random.uniform(0.1,1)
      self.femur_scale = np.random.uniform(0.1,1)
      self.tibia_scale = np.random.uniform(0.1,1)
      self.coxa_speed  = 0.5
      self.femur_speed = 0.5
      self.tibia_speed = 0.5
      self.coxa_scale  = 1
      self.femur_scale = 1
      self.tibia_scale = 1


      # print("new speeds", self.coxa_speed, self.femur_speed, self.tibia_speed, self.coxa_scale, self.femur_scale, self.tibia_scale)

    positions = []
    # Fraction of 1 second (max speed will be 2pi in 1 second)
    # steps = self.steps % (self.steps_per_sec)
    # --------------------
    # Limits of real controller:
    # --------------------   
    # coxa_max, coxa_min = 0.35, -0.35
    # femur_max, femur_min = 0.0, -0.7
    # tibia_max, tibia_min = 2.2, 1.5
    # --------------------
    # Actual joint limits:
    # --------------------   
    # coxa_max, coxa_min = 0.8, -0.8
    # femur_max, femur_min = 0.5, -1.0
    # tibia_max, tibia_min = 2.5, 0.5
    # --------------------   
    coxa_max, coxa_min = 0.5, -0.5
    femur_max, femur_min = 0.1, -0.9
    tibia_max, tibia_min = 2.3, 1.0
    coxa_scale = self.coxa_scale*(coxa_max - coxa_min)/2
    coxa_middle = (coxa_max + coxa_min)/2
    # self.coxa = coxa_scale*np.sin(self.coxa_speed*(self.steps*2*np.pi/(self.steps_per_sec)) + self.coxa_start) + coxa_middle
    # self.coxa = coxa_scale*np.sin(self.coxa_speed*(self.steps*2*np.pi/(self.steps_per_sec))) + coxa_middle
    self.coxa_x += self.coxa_speed*2*np.pi/(self.steps_per_sec)    
    self.coxa = coxa_scale*np.sin(self.coxa_x) + coxa_middle
    femur_scale = self.femur_scale*(femur_max - femur_min)/2
    femur_middle = (femur_max + femur_min)/2
    # self.femur = femur_scale*np.sin(self.femur_speed*(self.steps*2*np.pi/(self.steps_per_sec)) + self.femur_start) + femur_middle
    # self.femur = femur_scale*np.sin(self.femur_speed*(self.steps*2*np.pi/(self.steps_per_sec))) + femur_middle
    self.femur_x += self.femur_speed*2*np.pi/(self.steps_per_sec)
    self.femur = femur_scale*np.sin(self.femur_x) + femur_middle
    tibia_scale = self.tibia_scale*(tibia_max - tibia_min)/2
    tibia_middle = (tibia_max + tibia_min)/2
    # self.tibia = tibia_scale*np.sin(self.tibia_speed*(self.steps*2*np.pi/(self.steps_per_sec)) + self.tibia_start) + tibia_middle
    # self.tibia = tibia_scale*np.sin(self.tibia_speed*(self.steps*2*np.pi/(self.steps_per_sec))) + tibia_middle
    self.tibia_x += self.tibia_speed*2*np.pi/(self.steps_per_sec)
    self.tibia = tibia_scale*np.sin(self.tibia_x) + tibia_middle
    # self.prev_coxa_dt = self.coxa_speed*2*np.pi/(self.steps_per_sec)
    # self.prev_femur_dt = self.femur_speed*2*np.pi/(self.steps_per_sec)
    # self.prev_tibia_dt = self.tibia_speed*2*np.pi/(self.steps_per_sec)
    positions = [self.coxa, self.femur, self.tibia]
    # print(self.steps*2*np.pi/(self.steps_per_sec), positions)
    return positions


  def get_desired_joints(self):
    positions = []
    delta_step = self.steps % self.timesteps_per_step
    lift_off_time = 25 - 2.5
    lift_stale_time = (lift_off_time/2) + 2.5
    coxa_stance_dt = 0.7/25
    coxa_swing_dt = 0.7/25
    femur_up_dt = 0.7/10
    femur_down_dt = 0.7/10
    tibia_dt = 0.2
    coxa_max = 0.35
    coxa_min = -coxa_max
    femur_max = 0.0
    femur_min = -0.7
    tibia_max = 2.2
    tibia_min = 1.5
    # Lift off
    if delta_step < lift_off_time/2:
      self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
      self.femur = max(self.femur - femur_up_dt, femur_min)
      self.tibia = min(self.tibia + tibia_dt, tibia_max)
    elif delta_step < lift_stale_time:
      self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
      self.femur = femur_min
      self.tibia = min(self.tibia + tibia_dt, tibia_max)
    elif delta_step < lift_off_time:
      self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
      self.femur = min(self.femur + femur_down_dt, femur_max)
      self.tibia = max(self.tibia - tibia_dt, tibia_min)
    else:
      self.coxa = min(self.coxa + coxa_stance_dt, coxa_max)
      self.femur = femur_max
      self.tibia = max(self.tibia - tibia_dt, tibia_min)
    # print(delta_step, self.coxa, self.femur)
    positions = [self.coxa, self.femur, self.tibia]
    return positions

  def step_sim(self):
    self.pause_unpause(pause=False)
    t1 = time.time()
    while time.time() - t1 < self.timestep:
        time.sleep(0.00001)
    self.pause_unpause(pause=True)

  def pause_unpause(self, pause=True):
    if pause:
      rospy.wait_for_service('/gazebo/pause_physics')
      self.pause_sim()
    elif not pause:
      rospy.wait_for_service('/gazebo/unpause_physics')
      self.unpause_sim()

  # ROS interface stuff ======================================================
  def publish_actions(self, actions):
    for act, joint in zip(actions, self.joint_publishers):
      self.joint_publishers[joint].publish(np.clip(act, -75, 75))

  def initialise_publishers(self):
    self.joint_publishers = []
    self.joint_publishers = {}
    for joint in self.motor_names:
      # self.joint_publishers[joint] = rospy.Publisher('/' + self.robot_name + '/' + joint + '/command', Float64, queue_size=1)
      self.joint_publishers[joint] = rospy.Publisher('/' + joint + '/command', Float64, queue_size=1)

  def initialise_subscribers(self):
    print("initialising subscribers for ", self.robot_name)
    # rospy.Subscriber('/' + self.robot_name + '/joint_states', JointState, self.jointStateCallback)
    # rospy.Subscriber('/' + self.robot_name + '/tip_states', TipStates, self.tipStateCallback)
    # rospy.Subscriber('/' + self.robot_name + '/imu/data', Imu, self.imuCallback)
    rospy.Subscriber('/joint_states', JointState, self.jointStateCallback)
    # rospy.Subscriber('/tip_states', TipStates, self.tipStateCallback)
    # rospy.Subscriber('/imu/data', Imu, self.imuCallback)
    # rospy.Subscriber('/gazebo/model_states', ModelStates, self.modelStatesCallback)


    # Callbacks -------------------------------------------------------------- 
  def jointStateCallback(self, data):
    self.joint_pos_data = data.position
    self.joint_vel_data = data.velocity
    # Handle when joint_effort is 0.0.
    # Not sure if a gazebo thing, or a sample rate issue, but if 0.0, will wreck predictions
    if self.steps > 0:
      for i in range(self.ac_size):  
        if not (data.effort[i] < 0.00001 and data.effort[i] > -0.00001):
          self.joint_effort[i] = data.effort[i]        
      # else:
      #   print("gotta 0.0", data.effort[i], self.joint_effort[i])

  # def tipStateCallback(self, data):
  #   for i, name in enumerate(data.name):
  #     self.tip_states[name] = data.state[i]
  #         # print(self.tip_states)
  #         # print(self.tip_states[name].twist.linear.z, self.ob_dict[name + '_foot_link_left_ground'])

  # def imuCallback(self, data):
  #   self.imu_orn = data.orientation 
  #   self.ang_vel = data.angular_velocity
  #   self.lin_acc = data.linear_acceleration

  # def modelStatesCallback(self, data):
  #     # Ground = 0, expert = 1
  #     num = [i for i, name in enumerate(data.name) if name == self.robot_name][0]
  #     self.pos = data.pose[num].position
  #     self.orn = data.pose[num].orientation
  #     self.twist_lin = data.twist[num].linear
  #     self.twist_ang = data.twist[num].angular


  # def save_samples(self):
  #   import os   
  #   from glob import glob   
  #   import re 
  #   folders = glob(self.DATA_PATH + "*/")
  #   latest = [f.split('/') for f in folders]
  #   latest = [q for l in latest for q in l]
  #   l = ''
  #   for item in latest:
  #       l += item
  #   nums = re.findall('(\d+)',l)
  #   nums = [int(n) for n in nums]
  #   if nums:
  #       data_num = max(nums) + 1
  #   else:
  #       data_num = 0
  #   if not os.path.exists(self.DATA_PATH + str(data_num)):
  #       os.makedirs(self.DATA_PATH + 'data' + str(data_num))
  #   print("saving:", np.array(self.samples).shape)
  #   np.save(self.DATA_PATH + 'data' + str(data_num) + '/samples.npy', np.array(self.samples))

  def initialise_joint_subscribers(self):
    self.target_joint_effort = {}   
    rospy.Subscriber('/' + self.robot_name + '/TEST_coxa_joint/command/', Float64, self.TEST_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '/TEST_femur_joint/command/', Float64, self.TEST_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '/TEST_tibia_joint/command/', Float64, self.TEST_tibiaCallback)

  def TEST_coxaCallback(self, data):
    self.target_joint_effort['TEST_coxa_joint'] = data.data

  def TEST_femurCallback(self, data):
    self.target_joint_effort['TEST_femur_joint'] = data.data

  def TEST_tibiaCallback(self, data):
    self.target_joint_effort['TEST_tibia_joint'] = data.data
  
  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        # print(np.array(self.my_inputs).shape)
        df = pd.DataFrame (np.array(self.inputs))  
        df.to_excel(path + '/inputs.xlsx', index=False)
        df = pd.DataFrame (np.array(self.labels))  
        df.to_excel(path + '/labels.xlsx', index=False)
        df = pd.DataFrame (np.array(self.dones))  
        df.to_excel(path + '/dones.xlsx', index=False)
        # df = pd.DataFrame (np.array(self.my_inputs))  
        # df.to_excel(path + '/my_inputs.xlsx', index=False)
        # df = pd.DataFrame (np.array(self.my_labels))  
        # df.to_excel(path + '/my_labels.xlsx', index=False)
        
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    # for n in ['coxa', 'femur', 'tibia']:
    #   self.inputs.append(np.array(list(self.pos_error_buf[n]) + list(self.vel_buf[n])))
    #   self.labels.append(self.label_torques[n])
    # self.my_inputs.append(list(self.prev_state_buf) + list(self.desired_torque_buf))
    # self.my_labels.append(self.joints + self.joint_vel)
    self.inputs.append(self.prev_state + list(self.input_torque))
    self.labels.append(self.joints + self.joint_vel)
    self.dones.append(self.done)