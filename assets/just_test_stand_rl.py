import pybullet as p
import numpy as np
import os   
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from scripts.change_urdf import ChangeURDF
import cv2
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD

class Env():
  rank = comm.Get_rank()
  ob_size = 9
  # ob_size = 6
  ac_size = 3
  im_size = [20,20,1]
  # simtimeStep = 1/100
  simtimeStep = 1/200
  episodes = 0
  Kp = 20
  initial_Kp = 20
  Kc = 0.001
  Kd = 0.97
  total_reward = 0
  total_steps = 0
  delay_length = 1
  height_coeff = 0.01
  def __init__(self, render=False, PATH=None, args=None, horizon=500, record_step=True, cur=False, test=False, test_pol=False, rand_dynamics=False, history=False, obstacle_type=None, display_hm=False):
    self.test_pol = test_pol
    self.render = render
    self.PATH = PATH
    self.args = args
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.rand_dynamics = rand_dynamics
    self.history = history
    self.obstacle_type = obstacle_type
    self.display_hm = display_hm


    self.samples = np.load('samples/samples.npy')
    self.sample_size = self.samples.shape[0]
    print("Loaded reference data", self.samples.shape)


    # if self.render:
    #   self.physicsClientId = p.connect(p.GUI)
    #   self.load_model()
    
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT)
    
    if self.rand_dynamics:
      if self.args.new_urdf:
        self.change_urdf = ChangeURDF('assets/new_test_stand.urdf', debug=False, normal=False)
      else:
        self.change_urdf = ChangeURDF('assets/test_stand.urdf', debug=False, normal=False)

    self.load_model()

    self.sim_data = []
    self.exp_sim_data = []
    self.exp1_sim_data = []
    self.inputs = []
    self.labels = []
    self.rew_buffer = deque(maxlen=5)
    self.max_disturbance = 25

    self.tor_max = np.array([10,10,3])

    self.coxa_min, self.coxa_max  = -0.5, 0.5
    self.femur_min, self.femur_max  = 0.0, 0.4
    self.tibia_min, self.tibia_max = 1.0, 1.9

    self.prev_max_height = 0
    
    if self.obstacle_type is None: 
      self.z_offset = 0
      self.world_map = None
    else:
      from assets.obstacles import Obstacles
      self.obstacles = Obstacles()
      self.obstacles.insert_base()
      if self.obstacle_type == 'block':
        self.block_pos = self.obstacles.insert_block(self.height_coeff)
      # elif self.obstacle_type == 'stairs':
      #   self.obstacles.stairs(order=['flat'] + [np.random.choice(['up','down'],p=[0.5,0.5]) for _ in range(30)], height_coeff=self.height_coeff) 
      # self.world_map = self.get_world_map(self.obstacles.get_box_info())  


  def load_model(self):
    p.resetSimulation()
    p.loadMJCF("assets/ground.xml")

    if self.rand_dynamics:
      urdf_path = "assets/temp_urdf_" + self.args.exp + "_" + str(self.episodes+self.rank) + ".urdf"
      # print("loading urdf from ", urdf_path)
      self.change_urdf.write_new_dynamics(urdf_path)
      self.Id = p.loadURDF(urdf_path)
      # print([p.getDynamicsInfo(self.Id, i)[0] for i in [1,2,3]])
      os.remove(urdf_path)
    else:
      if self.args.new_urdf:
        print("using new urdf")
        urdf_path = "assets/test_stand.urdf"
      else:
        urdf_path = "assets/test_stand.urdf"
      objs = p.loadURDF(urdf_path)
      self.Id = objs
  
    objs = p.loadURDF(urdf_path)
    self.exp_Id = objs

    # objs = p.loadURDF(urdf_path)
    # self.exp1_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)


    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    self.joint_links = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      exp_info = p.getJointInfo(self.exp_Id, j)
      link_name = info[12].decode("ascii")
      exp_link_name = info[12].decode("ascii")
      if info[2] == p.JOINT_PRISMATIC:
        jname = info[1].decode("ascii")
        print("prism ", jname, info[8], info[9])
        self.slide = j
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      self.joint_links.append(j)
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    # self.motor_power =  [5, 10, 5]
    # self.vel_max = [8,11,8]
    self.vel_max = [8,8,8]
    # self.max_torque = np.array([10,10,3])
    self.max_torque = np.array([80,112,80])
    
    forces = np.ones(len(self.motors))*240
    # self.actions = {key:0.0 for key in self.motor_names}
    
    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp_Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    # p.setJointMotorControlArray(self.exp1_Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    
    p.setJointMotorControl2(self.Id, self.slide,controlMode=p.VELOCITY_CONTROL, force=0.0)
    p.setJointMotorControl2(self.exp_Id, self.slide,controlMode=p.VELOCITY_CONTROL, force=0.0)
    # p.setJointMotorControl2(self.exp1_Id, self.slide,controlMode=p.VELOCITY_CONTROL, force=0.0)
    
    
  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self):
    self.hop_count = 0
    self.Kc = self.Kc**(self.Kd)
    
    if self.rand_dynamics:
      self.load_model()

    if self.obstacle_type is not None:
      # self.obstacles.remove_obstacles()
      self.obstacles.insert_base()
      if self.obstacle_type == 'block':
        self.block_pos = self.obstacles.insert_block(self.height_coeff)
      self.world_map = self.get_world_map(self.obstacles.get_box_info())

    self.tip_left_ground = 0
    self.exp_tip_left_ground = 0
    self.exp1_tip_left_ground = 0


    self.coxa, self.femur, self.tibia = 0,0,1.75

    self.initial_position  = [self.coxa, self.femur, self.tibia]
    self.coxa_vel, self.femur_vel, self.tibia_vel = [0,0,0]

    p.resetJointState(self.Id, self.slide, targetValue=0.2, targetVelocity=0.0)
    p.resetJointState(self.exp_Id, self.slide, targetValue=0.2, targetVelocity=0.0)
    # p.resetJointState(self.exp1_Id, self.slide, targetValue=0.2, targetVelocity=0.0)

    self.set_position([0,0,0.0], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel])
    self.set_position([0,1,0.0], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel], robot_id=self.exp_Id)
    # self.set_position([0,0,-0.045], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel])
    # self.set_position([0,1,-0.045], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel], robot_id=self.exp_Id)
    # self.set_position([0,2,-0.045], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel], robot_id=self.exp1_Id)
    
    self.episodes += 1
    self.rew_buffer.append(self.total_reward)
    
    self.total_reward = 0
    self.steps = 0

    self.get_observation()

    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel


    if self.record_step:
      self.save_sim_data()
      # if self.episodes > 1:
      #   exit()
    self.desired_torque = [0,0,0]
    self.prev_actions = [0.0,0.0,0.0]
    self.prev_prev_torque = self.prev_torque = [0.0,0.0,0.0]

    self.control_bit = 0
    return np.array(self.joints + self.joint_vel + self.initial_position)
    

  def step(self, actions, next_state=None):

    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel

    actions = 5*actions

    if self.exp_tip_left_ground or self.steps == 0:
      self.time_in_contact = 0
    else:
      self.time_in_contact += 1
    # if self.time_in_contact > 5 and self.joints[1] < 0.3:
    # if self.time_in_contact > 20:
    if self.time_in_contact > 5 and self.exp_joints[1] < 0.0:
      self.exp_torques = [0.0, 30, -15]
      # print(self.exp_joints[2])
    # elif self.exp_tip_left_ground:
    #   if self.hop_count == 0:
    #     self.exp_coxa = 0.2
    #   else: 
    #     self.exp_coxa = 0.0
    #   max_dt = 0.1                                                                        
    #   self.exp_torques = list(([12*(self.exp_coxa-self.exp_joints[0]), 30*np.clip((0.0-self.exp_joints[1]),-max_dt,max_dt), 8*np.clip((2.0-self.exp_joints[2]),-max_dt, max_dt)]) - 0.1*np.array(self.exp_joint_vel)[:self.ac_size])
    else:
      max_dt = 0.1                                                                        
      self.exp_torques = list(([1*(0.0-self.exp_joints[0]), 30*np.clip((0.0-self.exp_joints[1]),-max_dt,max_dt), 8*np.clip((2.0-self.exp_joints[2]),-max_dt, max_dt)]) - 0.1*np.array(self.exp_joint_vel)[:self.ac_size])


    actions = np.clip(actions, -self.max_torque, self.max_torque)
    self.actions = actions

    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=actions)
    p.setJointMotorControlArray(self.exp_Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.exp_torques)
    # p.setJointMotorControlArray(self.exp1_Id, self.motors, controlMode=p.TORQUE_CONTROL, forces=self.exp_torques)

    p.stepSimulation()
    
    if self.render:
      time.sleep(0.005)
    self.get_observation()
    # print(self.joints, self.joint_vel)
    if self.record_step: 
      self.record_sim_data()

    self.target_joints = self.exp_joints

    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.total_reward += reward
    
    self.steps += 1
    self.total_steps += 1


    return np.array(self.joints + self.joint_vel + self.target_joints), reward, done, None

  def get_reward(self):
    self.tips = p.getLinkState(self.Id, self.foot_link, computeLinkVelocity=True)
    self.tip_pos, self.tip_vel = self.tips[0], self.tips[-2]
    # Just match the x of the foot

    foot = np.array(p.getLinkState(self.Id, self.foot_link)[0])[0]

    exp_foot = p.getLinkState(self.exp_Id, self.foot_link)[0]
    exp_foot = np.array([exp_foot[0], exp_foot[1]-1, exp_foot[2]])[0]
    
    # pos = np.exp(-2*np.sum(abs(np.array(self.exp_joints) - np.array(self.joints)))) + np.exp(-0.5*np.sum(abs(np.array(self.exp_torques) - np.array(self.actions))))
    # femur and tibia pos and vel, just x in the tip, 
    pos = np.exp(-2*np.sum(abs(np.array(self.exp_joints)[1:] - np.array(self.joints)[1:])))
    vel = np.exp(-0.1*np.sum(abs(np.array(self.exp_joint_vel)[1:] - np.array(self.joint_vel)[1:])))
    tip = np.exp(-20*np.sum(abs(foot - exp_foot)))
    if self.hop_count == 1:
      neg = np.exp(-10*np.sum(abs(self.exp_height - self.height + self.height_coeff)))
      pos += np.exp(-2*(abs(0.3 - np.array(self.joints)[0])))
    else:
      neg = np.exp(-10*np.sum(abs(self.exp_height - self.height)))
    reward = 0.6*pos + 0.1*vel + 0.2*neg + 0.1*tip
    
    # For when we use a curriculum for rewards
    # self.Kc = 0.001
    # vel = -self.Kc*0.01*np.mean(np.array(self.joint_vel)**2)
    # tor = -self.Kc*0.005*np.mean(np.array(self.actions)**2)
    # neg = -self.Kc*0.1*np.mean((np.array(self.actions) - np.array(self.prev_actions))**2) 

    if self.height < 0.175:
      reward -= 0.25
    
    # reward -= 0.05*abs(self.joints[1] - self.exp_joints[1])

    # print(self.height)
    self.reward_breakdown['pos'].append(pos)
    self.reward_breakdown['vel'].append(vel)
    self.reward_breakdown['neg'].append(neg)
    self.reward_breakdown['tip'].append(tip)
    
    done = False
    if self.steps == (self.horizon-2):
    # if self.steps == (self.horizon-2):
      done = True
    return reward, done
 
  def get_observation(self):
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    # if self.test_pol:
    self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.prev_tip_left_ground = self.tip_left_ground
    self.tip_left_ground = not len(p.getContactPoints(self.Id, -1, self.foot_link, -1))>0

    slide = p.getJointState(self.Id, self.slide)
    self.height, self.height_vel = slide[0], slide[1]

    # else:
    #   # self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]])+ np.random.uniform(-0.05,0.05,self.ac_size))
    #   # self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.uniform(-0.5,0.5,self.ac_size))
    #   self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]])+ np.random.normal(0,0.05,self.ac_size))
    #   self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.normal(0,0.5,self.ac_size))

    jointStates = p.getJointStates(self.exp_Id,self.ordered_joint_indices)
    self.exp_joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.exp_joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.exp_height = p.getJointState(self.exp_Id, self.slide)[0]
    self.prev_exp_tip_left_ground = self.exp_tip_left_ground
    self.exp_tip_left_ground = not len(p.getContactPoints(self.exp_Id, -1, self.foot_link, -1))>0
    if self.prev_exp_tip_left_ground and not self.exp_tip_left_ground:
      self.hop_count += 1
      if self.hop_count > 1:
        self.hop_count = 0
      # print(self.hop_count)
    

    # jointStates = p.getJointStates(self.exp1_Id,self.ordered_joint_indices)
    # self.exp1_joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    # self.exp1_joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]))
    # self.exp1_height = p.getJointState(self.exp1_Id, self.slide)[0]
    # self.prev_exp1_tip_left_ground = self.exp1_tip_left_ground
    # self.exp1_tip_left_ground = not len(p.getContactPoints(self.exp1_Id, -1, self.foot_link, -1))>0


  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None, slide=None):
    if robot_id is None:
      robot_id = self.Id  
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])
    if slide is not None:
      p.resetJointState(robot_id, self.slide, targetValue=slide)

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))       
        np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))       
        # np.save(path + 'exp1_sim_data.npy', np.array(self.exp1_sim_data))       
        self.sim_data = []
        self.exp_sim_data = []
        # self.exp1_sim_data = []
        if self.obstacle_type is not None:
          box_info = self.obstacles.get_box_info()
          np.save(path + 'box_info.npy', np.array(box_info))
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):

    data = [[0,0,0.0], [0,0,0,1], self.joints + [self.height]]
    self.sim_data.append(data)

    data = [[0,1,0.0], [0,0,0,1], self.exp_joints + [self.exp_height]]
    self.exp_sim_data.append(data)

    # data = [[0,2,-0.045], [0,0,0,1], self.exp1_joints + [self.exp1_height]]
    # self.exp1_sim_data.append(data)

  def get_hm(self):

    # if self.vis and self.world_map is not None:
    if self.world_map is not None:
      # x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
      x,y,yaw = 0.12/self.grid_size, 0.0/self.grid_size, 0.0
      try:
        x_pix = int(x+(-self.min_x+1)/self.grid_size)
        y_pix = int(y+(-self.min_y+1)/self.grid_size)
        ordered_pix = np.sort(self.world_map[x_pix-5:x_pix+5, y_pix-5:y_pix+5], axis=None)
        self.z_offset = max(ordered_pix[-1], 0.5)
      except Exception as e:
        pass
        # print(e)
        # print("We're off the map harry!")
        self.z_offset = 0.5
      self.hm = np.ones([self.dx_back+self.dx_forward, 2*self.dy], dtype=np.float32)*-1
      t1 = time.time()
      hm_pts = self.transform_rot_and_add(1*yaw, [x+(-self.min_x+1)/self.grid_size, y+(-self.min_y+1)/self.grid_size], self.ij)
      hm_pts[:,0] = np.clip(hm_pts[:,0], 0, self.world_map.shape[0]-1)
      hm_pts[:,1] = np.clip(hm_pts[:,1], 0, self.world_map.shape[1]-1)
      for ij_pts, pts in zip(self.hm_ij, hm_pts[:,:2]):
        try:
          self.hm[ij_pts[0], ij_pts[1]] = self.world_map[pts[0], pts[1]]
        except Exception as e:
          print(e)
          print(ij_pts, pts, self.hm.shape, self.world_map.shape)
      # print(time.time() - t1)    
      self.hm = np.clip(self.hm - self.z_offset, -1, 1).reshape(self.im_size)
      # Normalise
      self.hm = (self.hm+1)/2
    else:
      self.hm = np.zeros(self.im_size, dtype=np.float32)
    if self.display_hm:
      self.display()        
    return self.hm
  
  def display(self):
    
    # dx_forward, dx_back, dy = 40, 20, 20
    x1, y1, x2, y2, x3, y3, x4, y4 = self.dx_forward, self.dy, -self.dx_back, self.dy, -self.dx_back, -self.dy, self.dx_forward, -self.dy
    
    # dx_forward_col, dx_back_col, dy_col = 30, 12, 12
    dx_forward_col, dx_back_col, dy_col = 10, 10, 10
    x1_col, y1_col, x2_col, y2_col, x3_col, y3_col, x4_col, y4_col = dx_forward_col, dy_col, -dx_back_col, dy_col, -dx_back_col, -dy_col, dx_forward_col, -dy_col
    if self.world_map is not None:
      norm_wm = (np.min(self.world_map) - self.world_map)/(np.max(self.world_map) - np.min(self.world_map))
    else:
      norm_wm = np.zeros([100,100])
    wm_col = cv2.cvtColor(norm_wm,cv2.COLOR_GRAY2RGB)

    # x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
    x,y,yaw = 0.12/self.grid_size, 0.0/self.grid_size, 0.0
    rect_pts = np.array([[y1,x1,1],[y2,x2,1],[y3,x3,1],[y4,x4,1]])
    pts = self.transform_rot_and_add(-1*yaw, [y+ (-self.min_y+1)/self.grid_size, x+ (-self.min_x+1)/self.grid_size], rect_pts)        
    pts = pts[:, :2]
    cv2.polylines(wm_col, [pts], True, (0,255,0), 1)
    cv2.circle(wm_col,(int(y + (-self.min_y+1)/self.grid_size), int(x +(-self.min_x+1)/self.grid_size)), 3, (0,0,255), -1)
    
    norm_hm = np.array(self.hm*255.0, dtype=np.uint8)
    px,py = self.hm.shape[0], self.hm.shape[1]

    if self.rank == 0:
      cv2.imshow('world_map', wm_col)
      cv2.waitKey(1)
      cv2.imshow('local_map', norm_hm)
      cv2.waitKey(1)

  # def get_world_map(self, box_info=None, grid_size=0.025):
  def get_world_map(self, box_info=None, grid_size=0.015):
    gz = False
    if gz:
      self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0]/2)
      self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1]/2)
      max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0]/2)
      max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1]/2)
    else:
      self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0])
      self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1])
      max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0])
      max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1])
    world_shape = [int((max_x - self.min_x + 2)/grid_size), int((max_y - self.min_y + 2)/grid_size)]

    hm = np.ones([world_shape[0], world_shape[1]]).astype(np.float32)*-1
    if box_info is not None:
      positions = box_info[1]
      sizes = box_info[2]
      for pos, size in zip(positions, sizes):
        x, y, z = pos[0]/grid_size, pos[1]/grid_size, pos[2]
        if gz:
          x_size, y_size, z_size = (size[0]/2)/grid_size, (size[1]/2)/grid_size, size[2]/2
        else:
          x_size, y_size, z_size = (size[0])/grid_size, (size[1])/grid_size, size[2]
        ij = np.array([[i,j,1] for i in range(int(0-x_size), int(0+x_size)) for j in range(int(0-y_size), int(0+y_size))])

        hm_pts = self.transform_rot_and_add(1*pos[5], [x+(-self.min_x+1)/grid_size, y+(-self.min_y+1)/grid_size], ij)
        for pt in hm_pts:
          try:
            if hm[pt[0], pt[1]] < (z + z_size):
            # if hm[pt[0], pt[1]] == 0:
              hm[pt[0], pt[1]] = (z + z_size)
          except Exception as e:
            print(e)
            print(pt)

    self.grid_size = grid_size              
    # self.dx_forward, self.dx_back, self.dy = 36, 24, 20        
    self.dx_forward, self.dx_back, self.dy = 10, 10, 10        
    self.ij = np.array([[i,j,1] for i in range(-self.dx_back, self.dx_forward) for j in range(-self.dy, self.dy)])
    self.hm_ij = np.array([[i,j] for i in range(0, self.dx_back + self.dx_forward) for j in range(0, self.dy + self.dy)])
    return hm

  def transform_rot_and_add(self, yaw, pos, points):
    rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw), pos[0]],
            [np.sin(yaw), np.cos(yaw), pos[1]],
            [0, 0, 1]])
    return np.dot(rot_mat,points.T).T.astype(np.int32)