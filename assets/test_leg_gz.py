import numpy as np
import rospy
import os
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState, Imu
from gazebo_msgs.srv import SetModelState,SetModelConfiguration
from gazebo_msgs.msg import ModelState, ModelStates
from dynamic_reconfigure.srv import Reconfigure
from dynamic_reconfigure.msg import Config, DoubleParameter
from leg_go_ros_msgs.msg import TipStates
import time
from scipy.spatial.transform import Rotation as R
import copy
import roslaunch
from std_srvs.srv import SetBool, Empty, Trigger
import pandas as pd
from collections import deque
import random
from pathlib import Path
home = str(Path.home())
from models import rand_net

class Env():
  ac_size = 3
  ob_size = 7
  im_size = [48,48,1]
  steps = 0
  rewards = 0
  Kp = 400
  # timestep = 1/800
  timestep = 1/200
  steps_per_sec = 200
  def __init__(self, render=False, PATH=None, args=None, record_samples=False, horizontal=False, delay=True):
    self.delay = delay

    self.samples = np.load('samples/samples.npy')
    self.torque_samples = np.load('samples/torque_samples.npy')
    # self.sample_size = self.samples.shape[0]
    self.sample_size = self.torque_samples.shape[0]
    print("Loaded reference data", self.samples.shape)
    idx = [i for i in range(0,self.sample_size,self.sample_size//10)]
    self.target_set = self.samples[idx,:3]
    self.desired_sim_length = 300000
    # self.desired_sim_length = 1000
    self.entry_count = 0

    self.record_step = True
    self.rank = 0
    port = 1
    # os.environ["ROS_MASTER_URI"] = 'http://localhost:1131' + str(port)
    # os.environ["GAZEBO_MASTER_URI"] = 'http://localhost:1134' + str(port)
    # terrain = 'flat'          
    robot_name = 'r1' 
    # # roscore = subprocess.Popen(['roscore', '-p 1131' + str(port)])
    
    # print("ROSCORE STARTED..")
    # uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    # collect_data = False
    # if collect_data:
    #   cli_args = ['./launch/test_leg_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=true']
    # else:
      # cli_args = ['./launch/test_leg_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=false']
    # cli_args = ['./launch/test_leg_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=false', 'whole_body_control:=false', 'locomotion_engine:=false']
    # cli_args = ['./launch/my_test_stand_gazebo.launch', 'spawn_config:=grounded_body']
    # parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
    # parent.start()
    if False:
      uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
      roslaunch.configure_logging(uuid)
      # cli_args = ['darpa_subt_hexapod', 'test_stand_gazebo.launch', 'fixed:=true']
      cli_args = ['darpa_subt_hexapod', 'test_stand_gazebo.launch']
      print(cli_args)
      roslaunch_args = cli_args[1:]
      roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
      parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
      parent.start()
      print("ROSLAUNCH STARTED..")
    else:
      try:
        print("""running --> gnome-terminal -- bash -c "roslaunch darpa_subt_hexapod test_stand_gazebo.launch" """)
        os.system("""gnome-terminal -- bash -c -i "title test_stand_rt.launch; roslaunch darpa_subt_hexapod test_stand_gazebo.launch; exit; " """)  
      except Exception as e:
        print("ERROR: launch_leg:", e)

    # wait until publishing
    rospy.init_node(robot_name + '_rl_node', anonymous=False) 
    time.sleep(7.5)

    # if rank == 0:
    self.pause_sim = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
    self.unpause_sim = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)

    # rospy.wait_for_service('/gazebo/unpause_physics')
    # unpause_sim()
        
    self.torque = True
    self.num_saved_samples = 0
    # self.samples = []     
    self.horizontal = horizontal
    self.pause_time = 100
    self.PATH = PATH
    self.DATA_PATH = '/home/brendan/data/actuator_all/'
    # self.DATA_PATH = '/home/brendan/data/actuator_expert/'
    self.record_samples = record_samples
    self.args = args
    self.robot_name = robot_name
    self.robot_number = 1
    self.y_offset = 0
    self.ob_dict = {}
    # self.motor_names = ['TEST_coxa_joint','TEST_femur_joint','TEST_tibia_joint', 'vertical_slide_joint']
    self.motor_names = ['TEST_coxa_joint','TEST_femur_joint','TEST_tibia_joint']
    # self.motor_power =  [15, 22, 15]
    self.motor_power =  [5, 10, 5]
    self.vel_max = [8,11,8]
    self.tor_max = [80,112,80]
    
    #
    # # State from PB
    self.state = ["TEST_coxa_joint_pos","TEST_femur_joint_pos","TEST_tibia_joint_pos"]
    self.state += ["TEST_coxa_joint_vel","TEST_femur_joint_vel","TEST_tibia_joint_vel"]
    # self.state += ["BR_coxa_joint_effort","BR_femur_joint_effort","BR_tibia_joint_effort"]

    # self.tip_names = ['AL', 'AR', 'BL', 'BR', 'CL', 'CR']    
    self.episodes = 0
    self.tip_states = {}
    self.initialise_subscribers()
    self.initialise_publishers()
    self.expert_scale = 0.1
    self.prev_torques = np.zeros(18)
    self.prev_acts = np.zeros(self.ac_size)
    self.joint_effort_data = np.zeros(self.ac_size)
    self.total_steps = 0
    
    # if self.eth: 
    self.eth_input_buf = {i:deque([0.0]*15,maxlen=15) for i in self.motor_names}
    self.eth_inputs = []
    self.eth_labels = []
    self.inputs = []
    self.labels = []
    self.dones = []
    # Max torques, max positions
    self.max_torques = np.array([15,15,4])
    # self.coxa_max, self.coxa_min = 0.8, -0.8
    # self.femur_max, self.femur_min = 0.5, -1.0
    # self.tibia_max, self.tibia_min = 2.5, 0.5
    self.coxa_max, self.coxa_min = 0.6, -0.6
    self.femur_max, self.femur_min = 0.3, -0.8
    self.tibia_max, self.tibia_min = 2.3, 0.3
    self.rand_net = rand_net.Net(input_size=6, output_size=3, max_actions=self.max_torques)
    self.goto_ref = False
  
    self.t1 = time.time()


  
  def reset(self):
    # print("============= resetting  =============================")
    self.delay_length = 3
    self.torque_history = deque([[0.0 for _ in range(self.ac_size)]]*self.delay_length, maxlen=self.delay_length)

    self.rand_net.initialise()
    self.done = True

    self.coxa_speed, self.femur_speed, self.tibia_speed = 1,1,1
    self.coxa_x, self.femur_x, self.tibia_x = 0,0,0
    self.sample_pointer = np.random.randint(0,self.sample_size)
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel = temp[:3], temp[3:6]
    self.exp_torque = self.torque_samples[self.sample_pointer,:]
    # self.exp_torques = self.exp_torque_samples[self.sample_pointer,:]
    self.coxa, self.femur, self.tibia = self.exp_joints
    self.vertical_slide = 0.7
    # self.coxa, self.femur, self.tibia = [0.0,0.0,1.5]
    # self.coxa_x, self.femur_x, self.tibia_x = 0,0,0

    self.pos_error_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.vel_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.label_torques = {n:0.0 for n in ['coxa','femur','tibia']}
    
    self.real_torque_buf = deque([0.0]*3*5,maxlen=5*3)
    self.prev_state_buf = deque([0.0]*6*5,maxlen=5*6)

    if self.episodes > 0:
      self.inputs.append(np.array(self.input_buf))
      self.labels.append(np.array(self.label_buf))
      self.input_buf = []
      self.label_buf = []
    else:
      self.input_buf = []
      self.label_buf = []
      set_joints = rospy.ServiceProxy('/gazebo/set_model_configuration', SetModelConfiguration())
      smc = SetModelConfiguration()
      # smc.model_name = "_" + self.robot_name
      smc.model_name = "r1" 
      smc.urdf_param_name = ''
      smc.joint_names = ['TEST_coxa_joint', 'TEST_femur_joint', 'TEST_tibia_joint', 'vertical_slide_joint']
      smc.joint_positions = [self.coxa, self.femur, self.tibia, self.vertical_slide]
      rospy.wait_for_service('/gazebo/set_model_configuration')
      try:
        response_joints = set_joints(smc.model_name, smc.urdf_param_name, smc.joint_names, smc.joint_positions)
      except:
        print("Failed to reset joints")

    self.step_sim()
    
    self.joint_effort_data = [0.0,0.0,0.0]
    self.rewards = 0
    self.steps = 0
    self.episodes += 1
    self.freq = 1    
    self.get_observation()

    # got_observation = False
    # t1 = time.time() 
    # while not got_observation and not rospy.is_shutdown():
    #   if time.time() - t1 > 0.1:
    #     try:
    #       self.get_observation()
    #       got_observation = True
    #     except:
    #       print("trying to get observation")

    self.speed = 1.0
    self.step_length = 0.5
    if self.record_step:
      self.save_sim_data()
    # self.coxa, self.femur, self.tibia = 0,0,0
    self.actual_torque = [0.0,0.0,0.0]
    self.prev_prev_torque = self.prev_torque = [0.0,0.0,0.0]
    
    # self.timesteps_per_step = 50
    self.phase = self.sample_pointer/self.sample_size

    # return np.array([self.ob_dict[s] for s in self.state] + [self.speed])
    # print(self.joints, self.joint_vel)
    # return np.array(self.joints + self.joint_vel + [self.speed, self.step_length])
    # return np.array(self.joints + self.joint_vel)
    return np.array(self.joints + self.joint_vel + [self.phase])

  def step(self, actions=None):
    # print("stepng", self.steps)
    self.prev_done = self.done
    # self.actions = [m*a for m,a in zip(self.motor_power, actions)]
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints
    self.prev_joint_vel = self.joint_vel
    # des_pos = self.get_desired_joints()
    # des_pos = [np.random.uniform(-0.8,0.8),np.random.uniform(-1.0,0.5),np.random.uniform(0.5,2.5)]
    des_pos = self.exp_joints
    # Kp = np.array([5,10,5])
    Kp = np.array([10,20,5])
    Kv = np.array([0.1,0.1,0.1])
    # Kp = np.array([80,80,20])
    pos_error = np.array(des_pos) - np.array(self.joints)
    vel_error = np.array(self.exp_joint_vel) - np.array(self.joint_vel)
    # self.input_torque = np.clip(Kp*pos_error + Kv*vel_error, -np.array(self.tor_max), np.array(self.tor_max))
    # if self.steps%200 == 0:
    #   self.scale = np.array([np.random.choice((-1,1)),1,np.random.choice((-1,1))]) + (np.random.random(self.ac_size)-0.5)*0.3
    #   # print("new scale", self.scale)
    #   self.maxes = np.array([5,3,1.25])*0.3
    if actions is not None:
      self.input_torque = actions
    # else:
    #   if self.steps % 10 == 0:
    #     self.means = [np.random.uniform(-10.0,10.0),np.random.uniform(-10.0,10.0),np.random.uniform(-2,2)] 
    #     # print(self.means)
    #   self.input_torque = [np.random.normal(self.means[0],1.5),np.random.normal(self.means[1],1.5),np.random.normal(self.means[2],1)]
    
    else:
      if self.steps == 0 or self.sample_pointer == 0:
        # sign, scale, frequency.
        self.scale = np.array([np.random.choice((-1,1))*np.random.uniform(0.1,1),1*np.random.uniform(0.1,1),np.random.choice((-1,1))*np.random.uniform(0.1,1)]) 
        self.freq = np.random.choice((0.5, 0.75, 1, 1.5, 2),p=[0.15,0.2,0.3,0.2,0.15])

        # self.scale = np.array([1.0,1.0,1.0]) 
        # self.freq = 1.0
        # print(self.scale, self.freq)

        # self.means = [np.random.uniform(-7.5,7.5),np.random.uniform(-7.5,7.5),np.random.uniform(-2,2)] 
        # print(self.means)
      # self.input_torque = self.scale * self.exp_torque
      # self.input_torque = self.get_torques()
    # Kp = np.array([1.0]); Kd = np.array([0.75])
    Kp = np.array([15,15,5]); Kd = np.array([0.8,0.8,0.4])

  
    if self.steps < 64 or (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
      if not self.goto_ref:
        self.goto_ref = True
        # self.target = self.target_set[np.argmin((np.sum((self.target_set - np.array(self.joints))**2,axis=1))),:]
        # self.target = self.samples[np.random.randint(0,self.sample_size),:self.ac_size]
        self.sum_error = 0
      # print(self.target)
    if self.goto_ref:
      error = (np.array(self.exp_joints) - np.array(self.joints))
      # torques = 0.5*self.max_torques*error + 0.1*self.sum_error - 0.25*np.array(self.joint_vel)
      torques = Kp*error + 0.1*self.sum_error - Kd*np.array(self.joint_vel)
      self.sum_error += error
      # print(torques)
      if (abs(error)<0.3).all():
        self.goto_ref = False
    else:
      torques = self.rand_net.step(np.array(self.joints+self.joint_vel))

      # if np.random.random() < 0.9:
      # else:
      #   torques = [np.random.uniform(-3,3),np.random.uniform(-3,0),np.random.uniform(-0.5,0.5)]
      # print(self.steps, torques)      

    self.input_torque = np.clip(torques, -self.max_torques, self.max_torques) 
#     t1 = time.time()
#     if not self.goto_ref and (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
#       self.goto_ref = True
#       self.goto_timeout = 0
#       self.target = self.target_set[np.argmin((np.sum((self.target_set - np.array(self.joints))**2,axis=1))),:]
#       errors = abs(self.target - np.array(self.joints))
#       scales = errors/np.max(errors)
#       self.K = np.random.uniform(0.5,0.8)*self.max_torques*scales
#     if self.goto_ref:
#       error = (np.array(self.target) - np.array(self.joints))
#       if (abs(error)<0.3).all():
#         self.goto_ref = False
#       else:      
#         self.goto_timeout += 1
#         if self.goto_timeout > 50:
#           torques = self.max_torques*error
#         else:
#           torques = self.K*error
#     # print(time.time())
#     if not self.goto_ref:
#       torques = self.rand_net.step(np.array(self.joints+self.joint_vel))
#     self.input_torque = np.clip(torques, -self.max_torques, self.max_torques) 
#     # print(time.time() - t1, self.goto_ref)

    # self.input_torque = self.scale*np.array(self.exp_torques) + (np.random.random()-0.5)*self.maxes
    # print(self.input_torque)
    
    # self.input_torque = [np.random.normal(0,5),np.random.normal(-2,3),np.random.normal(0,1.25)]

    # des_pos = self.get_des_joint_pos()
    # pos_error = (des_pos - np.array(self.joints)) + np.array([np.random.uniform(-0.2,0.2), np.random.uniform(-0.2,0.2), np.random.uniform(-0.2,0.2)])
    # self.input_torque = np.clip(np.array([10*pos_error[0], 10*pos_error[1], 2*pos_error[2]]), -np.array(self.tor_max), np.array(self.tor_max))
    
    # self.prev_state = self.joints + self.joint_vel
    # self.actions = [np.random.normal(0,5),np.random.normal(-2.5,3),np.random.normal(0,2)]
    # self.real_torque = self.input_torque
    # for n,name in enumerate(['coxa', 'femur','tibia']):
    #   self.pos_error_buf[name].append(pos_error[n])
    #   self.vel_buf[name].append(self.joint_vel[n])
    #   self.label_torques[name] = self.actual_torque[n]
    
    # self.real_torque_buf.extend(self.real_torque)
    # self.prev_state_buf.extend(self.prev_state)

    # self.publish_actions(self.actual_torque)
    # print(actions)
    # self.delay = False
    # t1 = time.time()
    if self.delay:
      self.torque_history.append(self.input_torque)
      self.real_torque = self.torque_history[0]
      # self.real_torque = self.prev_prev_torque
      # self.prev_prev_torque = self.prev_torque
      # self.prev_torque = self.input_torque    
    else:
      self.real_torque = self.input_torque
    self.publish_actions(self.real_torque)
    # self.publish_actions([75.0,75.0,75.0])


    self.step_sim()
    self.get_observation()
    
    if self.record_step: 
      self.record_sim_data()
    
    # print(time.time()-t1)
    # # Create artificial delay of 2 timesteps
    # self.actual_torque = self.prev_torq
    # self.prev_torq = self.real_torque

    reward, self.done = self.get_reward(self.real_torque)
    self.rewards += reward
    self.steps += 1
    self.total_steps += 1
    if self.entry_count % 1000 == 0:
      print(self.entry_count)
    self.sample_pointer += int(4*self.freq)
    if self.sample_pointer > self.sample_size - 4*self.freq:
      self.sample_pointer = 0
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel = temp[:3], temp[3:6]
    self.exp_torque = self.torque_samples[self.sample_pointer,:]
    self.phase = self.sample_pointer/self.sample_size
    # self.exp_torques = self.exp_torque_samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torques = temp[:3], temp[3:6], temp[6:]
    # return np.array([self.ob_dict[s] for s in self.state] + [self.speed]), reward, self.done, self.ob_dict
    # return np.array(self.joints + self.joint_vel + [self.speed, self.step_length]), reward, self.done, self.ob_dict
    return np.array(self.joints + self.joint_vel + [self.phase]), reward, self.done, None

  def step_sim(self):
    # self.pause_unpause(pause=False)
    # t1 = time.time()
    while time.time() - self.t1 < self.timestep:
      time.sleep(0.00001)
    # print(time.time() - self.t1, self.timestep)
    # self.pause_unpause(pause=True)
    self.t1 = time.time()

  def pause_unpause(self, pause=True):
    if pause:
      rospy.wait_for_service('/gazebo/pause_physics')
      self.pause_sim()
    elif not pause:
      rospy.wait_for_service('/gazebo/unpause_physics')
      self.unpause_sim()

  # ROS interface stuff ======================================================
  def publish_actions(self, actions):
    # actions = list(actions) + [75]
    for act, joint in zip(actions, self.joint_publishers):
      # print("publishing ", joint, act)
      self.joint_publishers[joint].publish(np.clip(act, -75, 75))

  def get_reward(self, actions):
    reward = 1
    done = False
    self.coxa_max, self.coxa_min = 0.8, -0.8
    self.femur_max, self.femur_min = 0.5, -1.0
    self.tibia_max, self.tibia_min = 2.5, 0.5

    # if self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joint_vel[0] < -self.vel_max[0] or self.joint_vel[0] > self.vel_max[0]:
    # # if self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max:
    #   done = True
    # if self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joint_vel[1] < -self.vel_max[1] or self.joint_vel[1] > self.vel_max[1]:
    #   done = True
    # if self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max or self.joint_vel[2] < -self.vel_max[2] or self.joint_vel[2] > self.vel_max[2]:
    #   done = True
    # if self.steps > 255:
    if self.steps == 512:
    # if self.steps == 500:
    # if self.steps > 3000:
      done = True
    return reward, done

  def get_observation(self):
    '''
    Robot state
    '''   
    # pass
    self.joints = list(self.joint_pos_data)[:self.ac_size]
    self.joint_vel = list(self.joint_vel_data)[:self.ac_size]
    self.joint_effort = list(self.joint_effort_data)[:self.ac_size]
    # self.ob_dict.update({j + '_pos': pos for j, pos in zip(self.motor_names, self.joint_pos)})
    # self.ob_dict.update({j + '_vel': vel for j, vel in zip(self.motor_names, self.joint_vel)})
    # self.ob_dict.update({j + '_effort': effort for j, effort in zip(self.motor_names, self.joint_effort)})
    # self.ob_dict.update({j + '_prev_act': a for j, a in zip(self.motor_names, self.prev_actions)})

  def get_torques(self):
    if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.coxa_speed) == 0:
      coxa_max, coxa_min, self.coxa_middle = 7, -7, 0.0
      self.coxa_speed = np.random.uniform(0.1,1.25)
      self.coxa_scale  = np.random.uniform(0,1.0)*(coxa_max - coxa_min)/2
      self.coxa_x = 0.0

    if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.femur_speed) == 0:
      femur_max, femur_min, self.femur_middle = 2, -5, -2.5
      self.femur_speed = np.random.uniform(0.1,1.25)
      self.femur_scale = np.random.uniform(0,1.0)*(femur_max - femur_min)/2
      self.femur_x = 0.0

    if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.tibia_speed) == 0:
      tibia_max, tibia_min, self.tibia_middle = 1.5, -1.5, 0.0
      self.tiba_speed = np.random.uniform(0.1,1.25)
      self.tibia_scale = np.random.uniform(0,1.0)*(tibia_max - tibia_min)/2
      self.tibia_x = 0.0
    # self.coxa_speed, self.femur_speed, self.tibia_speed = 0.25, 0.25, 0.25
    # self.coxa_speed, self.femur_speed, self.tibia_speed = 1.25, 1.25, 1.25
    self.coxa_x += 2*np.pi/(self.steps_per_sec/self.coxa_speed)    
    self.coxa = self.coxa_scale*np.sin(self.coxa_x) + self.coxa_middle
    self.femur_x += 2*np.pi/(self.steps_per_sec/self.femur_speed)    
    self.femur = self.femur_scale*np.sin(self.femur_x) + self.femur_middle
    self.tibia_x += 2*np.pi/(self.steps_per_sec/self.tibia_speed)    
    self.tibia = self.tibia_scale*np.sin(self.tibia_x) + self.tibia_middle
    return [self.coxa, self.femur, self.tibia]


  def get_des_joint_pos(self):
    # Pick new speed every 3 seconds
    if self.steps % (self.steps_per_sec*3) == 0:
      self.coxa_speed = np.random.uniform(0,0.75)
      self.femur_speed = np.random.uniform(0,0.75)
      self.tibia_speed = np.random.uniform(0,0.75)
      self.coxa_scale = np.random.uniform(0.1,1)
      self.femur_scale = np.random.uniform(0.1,1)
      self.tibia_scale = np.random.uniform(0.1,1)
      self.coxa_speed  = 0.5
      self.femur_speed = 0.5
      self.tibia_speed = 0.5
      self.coxa_scale  = 1
      self.femur_scale = 1
      self.tibia_scale = 1


      # print("new speeds", self.coxa_speed, self.femur_speed, self.tibia_speed, self.coxa_scale, self.femur_scale, self.tibia_scale)

    positions = []
    # Fraction of 1 second (max speed will be 2pi in 1 second)
    # steps = self.steps % (self.steps_per_sec)
    # --------------------
    # Limits of real controller:
    # --------------------   
    # coxa_max, coxa_min = 0.35, -0.35
    # femur_max, femur_min = 0.0, -0.7
    # tibia_max, tibia_min = 2.2, 1.5
    # --------------------
    # Actual joint limits:
    # --------------------   
    # coxa_max, coxa_min = 0.8, -0.8
    # femur_max, femur_min = 0.5, -1.0
    # tibia_max, tibia_min = 2.5, 0.5
    # --------------------   
    coxa_max, coxa_min = 0.5, -0.5
    femur_max, femur_min = 0.1, -0.9
    tibia_max, tibia_min = 2.3, 1.0
    coxa_scale = self.coxa_scale*(coxa_max - coxa_min)/2
    coxa_middle = (coxa_max + coxa_min)/2
    # self.coxa = coxa_scale*np.sin(self.coxa_speed*(self.steps*2*np.pi/(self.steps_per_sec)) + self.coxa_start) + coxa_middle
    # self.coxa = coxa_scale*np.sin(self.coxa_speed*(self.steps*2*np.pi/(self.steps_per_sec))) + coxa_middle
    self.coxa_x += self.coxa_speed*2*np.pi/(self.steps_per_sec)    
    self.coxa = coxa_scale*np.sin(self.coxa_x) + coxa_middle
    femur_scale = self.femur_scale*(femur_max - femur_min)/2
    femur_middle = (femur_max + femur_min)/2
    # self.femur = femur_scale*np.sin(self.femur_speed*(self.steps*2*np.pi/(self.steps_per_sec)) + self.femur_start) + femur_middle
    # self.femur = femur_scale*np.sin(self.femur_speed*(self.steps*2*np.pi/(self.steps_per_sec))) + femur_middle
    self.femur_x += self.femur_speed*2*np.pi/(self.steps_per_sec)
    self.femur = femur_scale*np.sin(self.femur_x) + femur_middle
    tibia_scale = self.tibia_scale*(tibia_max - tibia_min)/2
    tibia_middle = (tibia_max + tibia_min)/2
    # self.tibia = tibia_scale*np.sin(self.tibia_speed*(self.steps*2*np.pi/(self.steps_per_sec)) + self.tibia_start) + tibia_middle
    # self.tibia = tibia_scale*np.sin(self.tibia_speed*(self.steps*2*np.pi/(self.steps_per_sec))) + tibia_middle
    self.tibia_x += self.tibia_speed*2*np.pi/(self.steps_per_sec)
    self.tibia = tibia_scale*np.sin(self.tibia_x) + tibia_middle
    # self.prev_coxa_dt = self.coxa_speed*2*np.pi/(self.steps_per_sec)
    # self.prev_femur_dt = self.femur_speed*2*np.pi/(self.steps_per_sec)
    # self.prev_tibia_dt = self.tibia_speed*2*np.pi/(self.steps_per_sec)
    positions = [self.coxa, self.femur, self.tibia]
    # print(self.steps*2*np.pi/(self.steps_per_sec), positions)
    return positions


  def get_desired_joints(self):
    positions = []
    delta_step = self.steps % self.timesteps_per_step
    lift_off_time = 25 - 2.5
    lift_stale_time = (lift_off_time/2) + 2.5
    coxa_stance_dt = 0.7/25
    coxa_swing_dt = 0.7/25
    femur_up_dt = 0.7/10
    femur_down_dt = 0.7/10
    tibia_dt = 0.2
    coxa_max = 0.35
    coxa_min = -coxa_max
    femur_max = 0.0
    femur_min = -0.7
    tibia_max = 2.2
    tibia_min = 1.5
    # Lift off
    if delta_step < lift_off_time/2:
      self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
      self.femur = max(self.femur - femur_up_dt, femur_min)
      self.tibia = min(self.tibia + tibia_dt, tibia_max)
    elif delta_step < lift_stale_time:
      self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
      self.femur = femur_min
      self.tibia = min(self.tibia + tibia_dt, tibia_max)
    elif delta_step < lift_off_time:
      self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
      self.femur = min(self.femur + femur_down_dt, femur_max)
      self.tibia = max(self.tibia - tibia_dt, tibia_min)
    else:
      self.coxa = min(self.coxa + coxa_stance_dt, coxa_max)
      self.femur = femur_max
      self.tibia = max(self.tibia - tibia_dt, tibia_min)
    # print(delta_step, self.coxa, self.femur)
    positions = [self.coxa, self.femur, self.tibia]
    return positions

  def initialise_publishers(self):
    self.joint_publishers = []
    self.joint_publishers = {}
    for joint in self.motor_names:
      # self.joint_publishers[joint] = rospy.Publisher('/' + self.robot_name + '/' + joint + '/command', Float64, queue_size=1)
      self.joint_publishers[joint] = rospy.Publisher('/' + joint + '/command', Float64, queue_size=1)

  def initialise_subscribers(self):
    print("initialising subscribers for ", self.robot_name)
    # rospy.Subscriber('/' + self.robot_name + '/joint_states', JointState, self.jointStateCallback)
    # rospy.Subscriber('/' + self.robot_name + '/tip_states', TipStates, self.tipStateCallback)
    # rospy.Subscriber('/' + self.robot_name + '/imu/data', Imu, self.imuCallback)
    rospy.Subscriber('/joint_states', JointState, self.jointStateCallback)
    # rospy.Subscriber('/tip_states', TipStates, self.tipStateCallback)
    # rospy.Subscriber('/imu/data', Imu, self.imuCallback)
    # rospy.Subscriber('/gazebo/model_states', ModelStates, self.modelStatesCallback)


    # Callbacks -------------------------------------------------------------- 
  def jointStateCallback(self, data):
    self.joint_pos_data = data.position
    self.joint_vel_data = data.velocity
    # Handle when joint_effort is 0.0.
    # Not sure if a gazebo thing, or a sample rate issue, but if 0.0, will wreck predictions
    if self.steps > 0:
      for i in range(self.ac_size):  
        if not (data.effort[i] < 0.00001 and data.effort[i] > -0.00001):
          self.joint_effort[i] = data.effort[i]        
      # else:
      #   print("gotta 0.0", data.effort[i], self.joint_effort[i])

  # def tipStateCallback(self, data):
  #   for i, name in enumerate(data.name):
  #     self.tip_states[name] = data.state[i]
  #         # print(self.tip_states)
  #         # print(self.tip_states[name].twist.linear.z, self.ob_dict[name + '_foot_link_left_ground'])

  # def imuCallback(self, data):
  #   self.imu_orn = data.orientation 
  #   self.ang_vel = data.angular_velocity
  #   self.lin_acc = data.linear_acceleration

  # def modelStatesCallback(self, data):
  #     # Ground = 0, expert = 1
  #     num = [i for i, name in enumerate(data.name) if name == self.robot_name][0]
  #     self.pos = data.pose[num].position
  #     self.orn = data.pose[num].orientation
  #     self.twist_lin = data.twist[num].linear
  #     self.twist_ang = data.twist[num].angular


  # def save_samples(self):
  #   import os   
  #   from glob import glob   
  #   import re 
  #   folders = glob(self.DATA_PATH + "*/")
  #   latest = [f.split('/') for f in folders]
  #   latest = [q for l in latest for q in l]
  #   l = ''
  #   for item in latest:
  #       l += item
  #   nums = re.findall('(\d+)',l)
  #   nums = [int(n) for n in nums]
  #   if nums:
  #       data_num = max(nums) + 1
  #   else:
  #       data_num = 0
  #   if not os.path.exists(self.DATA_PATH + str(data_num)):
  #       os.makedirs(self.DATA_PATH + 'data' + str(data_num))
  #   print("saving:", np.array(self.samples).shape)
  #   np.save(self.DATA_PATH + 'data' + str(data_num) + '/samples.npy', np.array(self.samples))

  def initialise_joint_subscribers(self):
    self.target_joint_effort = {}   
    rospy.Subscriber('/' + self.robot_name + '/BR_coxa_joint/command/', Float64, self.BR_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '/BR_femur_joint/command/', Float64, self.BR_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '/BR_tibia_joint/command/', Float64, self.BR_tibiaCallback)

  def BR_coxaCallback(self, data):
    self.target_joint_effort['BR_coxa_joint'] = data.data

  def BR_femurCallback(self, data):
    self.target_joint_effort['BR_femur_joint'] = data.data

  def BR_tibiaCallback(self, data):
    self.target_joint_effort['BR_tibia_joint'] = data.data
  
  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        if self.entry_count > self.desired_sim_length:
          np.save(path + "/inputs.npy", np.array(self.inputs))
          np.save(path + "/labels.npy", np.array(self.labels))
          
          np.save(path + "/eth_inputs.npy", np.array(self.eth_inputs))
          np.save(path + "/eth_labels.npy", np.array(self.eth_labels))

        # if len(self.inputs) > self.desired_sim_length:
        #   # print(np.array(self.inputs).shape, np.array(self.labels).shape)
        
        #   df = pd.DataFrame (np.array(self.inputs))  
        #   df.to_excel(path + '/inputs.xlsx', index=False)
          
        #   df = pd.DataFrame (np.array(self.labels))  
        #   df.to_excel(path + '/labels.xlsx', index=False)

        #   df = pd.DataFrame (np.array(self.dones))  
        #   df.to_excel(path + '/dones.xlsx', index=False)
          exit()
        
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):

    # self.inputs.append(self.prev_state + list(self.input_torque))
    # self.labels.append(self.joints + self.joint_vel)
    # self.dones.append(self.prev_done)
    # t1 = time.time()
    for m,j,v,it,dt in zip(self.motor_names, self.prev_joints, self.prev_joint_vel, self.input_torque, self.real_torque):
      self.eth_input_buf[m].extend([j,v,it])
      self.eth_inputs.append(np.array(self.eth_input_buf[m]))
      self.eth_labels.append(dt)

    self.input_buf.append(self.prev_state + list(self.input_torque))
    self.label_buf.append(self.joints + self.joint_vel)
    self.entry_count += 1
    # print(time.time() - t1)
