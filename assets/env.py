import pybullet as p
import numpy as np
import os   
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD
  
class Env():
  rank = comm.Get_rank()
  # ob_size = 18
  ob_size = 24
  # ob_size = 18*3
  ac_size = 18
  im_size = [48,48,4]
  # Gazebo data has been collected at 60Hz
  # simtimeStep = 1/240
  # # simtimeStep = 1/500
  # timeStep = 1/60
  # actionRepeat = 4
  simtimeStep = 0.001
  actionRepeat = 1
  rew_buffer = deque(maxlen=5)
  episodes = -1
  sample_delay = 100
  # Kp = {'coxa_fs':2000, 'coxa_g':2000, 'femur_fs': 10,'femur_g': 2000, 'tibia_fs': 2000, 'tibia_g': 2000}
  # Kd = {'coxa_fs':1, 'coxa_g':1, 'femur_fs': 1,'femur_g': 1, 'tibia_fs': 1, 'tibia_g': 1}
  Kp = {'coxa_fs':1000, 'coxa_g':1000, 'femur_fs': 1000,'femur_g': 1000, 'tibia_fs': 1000, 'tibia_g': 1000}
  # Ki   = {'coxa_fs':0, 'coxa_g':0, 'femur_fs':0,'femur_g': 0, 'tibia_fs': 0, 'tibia_g': 0}
  Kd = {'coxa_fs':5, 'coxa_g':5, 'femur_fs': 5,'femur_g': 5, 'tibia_fs': 5, 'tibia_g': 5}
  errors = deque([[0.0]*ac_size],maxlen=10)
  def __init__(self, render=False, PATH=None, horizon=500, display_expert=True, record_step=True, cur=False, test=False, use_expert=False, act_model=None, sim_model=None):
    self.render = render
    self.PATH = PATH
    self.display_expert = display_expert
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.act_model = act_model
    self.sim_model = sim_model
    self.use_expert = use_expert
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot
    self.load_model()
    self.sim_data = []
    self.exp_sim_data = []
    self.inputs = []
    self.labels = []
    self.pos_and_vel = []

  def load_model(self):
    p.loadMJCF("./assets/ground.xml")
    objs = p.loadURDF("./assets/robot.urdf")
    self.Id = objs
    if self.display_expert:
      objs = p.loadURDF("./assets/robot.urdf")
      self.exp_Id = objs
    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)
    self.feet_dict = {}
    self.feet = ["AR_foot_link","AL_foot_link","BR_foot_link",
                  "BL_foot_link","CR_foot_link","CL_foot_link"]
    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name in self.feet: self.feet_dict[link_name] = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    if self.display_expert:
      self.exp_jdict = {}
      for j in range( p.getNumJoints(self.exp_Id) ):
        info = p.getJointInfo(self.exp_Id, j)
        link_name = info[12].decode("ascii")
        if info[2] != p.JOINT_REVOLUTE: continue
        jname = info[1].decode("ascii")
        lower, upper = (info[8], info[9])
        self.exp_jdict[jname] = j

    self.motor_names = [
                        "AR_coxa_joint","AR_femur_joint","AR_tibia_joint",
                        "AL_coxa_joint","AL_femur_joint","AL_tibia_joint",
                        "BR_coxa_joint","BR_femur_joint","BR_tibia_joint",
                        "BL_coxa_joint","BL_femur_joint","BL_tibia_joint",
                        "CR_coxa_joint","CR_femur_joint","CR_tibia_joint",
                        "CL_coxa_joint","CL_femur_joint","CL_tibia_joint"]


    self.motors = [self.jdict[n] for n in self.motor_names]
    if self.display_expert:
      self.exp_motors = [self.exp_jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    self.motor_power += [15, 22, 15]
    self.motor_power += [15, 22, 15]
    self.motor_power += [15, 22, 15]
    self.motor_power += [15, 22, 15]
    self.motor_power += [15, 22, 15]
    self.vel_max = [8,11,8]
    self.vel_max += [8,11,8]
    self.vel_max += [8,11,8]
    self.vel_max += [8,11,8]
    self.vel_max += [8,11,8]
    self.vel_max += [8,11,8]
    self.tor_max =  [80,112,80]
    self.tor_max += [80,112,80]
    self.tor_max += [80,112,80]
    self.tor_max += [80,112,80]
    self.tor_max += [80,112,80]
    self.tor_max += [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}
    
    # Disable motors to use torque control:
    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

    # Increase the friction on the feet.
    for key in self.feet_dict:
      p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9, rollingFriction=0.1)
    self.DATA_PATH = home + '/data/'
    # self.samples = load_data(self.DATA_PATH + 'eth/')
    # self.samples = np.load(self.DATA_PATH + 'data1/samples.npy', allow_pickle=True)
    self.samples = pd.read_csv(self.DATA_PATH + 'full_hex_samples.csv', header=None).as_matrix()
    # self.samples = pd.read_excel(self.DATA_PATH + 'data/samples.xlsx', header=None).as_matrix()
    print("Length of samples: ", self.samples.shape)

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self):
    self.reward_breakdown = {'pos':[], 'vel':[]}

    self.episodes += 1
    self.history = deque([0]*self.ac_size*3*10, maxlen=self.ac_size*3*10)
    if self.episodes > 0:
      self.rew_buffer.append(self.total_reward)
    if self.rank == 0:
      print(self.rew_buffer)
    if len(self.rew_buffer) == 5 and np.all(np.array(self.rew_buffer) > -12000):
      if self.cur:
        self.Kp = self.Kp*0.75
        self.Kd = self.Kd*0.75
        if self.Kp < 10:
          self.Kp = self.Kd = 0
          self.cur = False
      # else:
      #   if np.all(np.array(self.rew_buffer) > -600):
      #     self.sample_delay = min(self.sample_delay + 10, 200)
      self.rew_buffer = deque(maxlen=5)
    # self.sample_delay = 50
    self.total_reward = 0
    self.steps = 16000
    # self.sample_pointer = np.random.randint(0,4000)
    self.sample_pointer = 16000
    self.initial_pointer = self.sample_pointer
    self.restore_sample(self.samples[self.sample_pointer], expert=False)
    self.desired_torque = self.restore_sample(self.samples[self.sample_pointer], expert=True)
    self.get_observation()
    if self.record_step:
      self.save_sim_data()
      self.sim_data = []
      self.exp_sim_data = []
      self.inputs = []
      self.labels = []
      self.pos_and_vel = []
      if self.episodes > 0:
        exit()
      
    # return np.array(self.desired_torque + self.exp_joints + self.exp_joint_vel+list(self.history))
    # return np.array(self.desired_torque + self.joints + self.joint_vel)
    return np.array(self.desired_torque + [self.ob_dict[f + "_left_ground"] for f in self.feet_dict])

  def step(self, actions):
    self.history.extend(self.desired_torque + self.exp_joints + self.exp_joint_vel)
    torques = [0.] * len(self.motor_names)
    # if self.cur:
    # guide_torque = self.Kp*(np.array(self.exp_joints) - np.array(self.joints)) + self.Kd*(np.array(self.exp_joint_vel) - np.array(self.joint_vel))
    # for m, name in enumerate(self.motor_names):
    #   if self.cur:
    #     torques[m] = actions[m]*self.motor_power[m] + guide_torque[m]
    #   else:
    #     torques[m] = actions[m]*self.motor_power[m] 
    #   torques[m] = np.clip(torques[m], -self.tor_max[m], self.tor_max[m])
    # ***************************
    self.pd_torque = []
    # prev_errors = self.errors[-1]
    errors = []
    vel_errors = []
    # sum_errors = np.sum(np.array(self.errors), axis=0)
    # print(sum_errors)
    for n,m in enumerate(self.motor_names):
      name = m.split('_')
      config = name[1]
      if self.ob_dict[name[0] + '_foot_link_left_ground']:
        config += '_fs'
      else:
        config += '_g'
      # print(config)
      # error = self.exp_joints[n] - self.joints[n]
      # self.pd_torque.append(self.Kp[config]*error + self.Ki[config]*(error + sum_errors[n]) + self.Kd[config]*(error - prev_errors[1]))
      # self.pd_torque.append(self.Kp[config]*error + self.Ki[config]*(error + sum_errors[n]) + self.Kd[config]*(error - prev_errors[1]))
      
      self.pd_torque.append(self.Kp[config]*(self.exp_joints[n] - self.joints[n]) + self.Kd[config]*(np.clip(self.exp_joint_vel[n] - self.joint_vel[n],-0.5,0.5)))
      errors.append(self.exp_joints[n] - self.joints[n])
      vel_errors.append(np.clip(self.exp_joint_vel[n] - self.joint_vel[n],-1,1))
    # print("pos", errors)
    # print("vel",vel_errors)
    # self.errors.append(errors)

    # self.pd_torque = self.Kp*(np.array(self.exp_joints) - np.array(self.joints)) + self.Kd*(np.array(self.exp_joint_vel) - np.array(self.joint_vel))    
    for n,m in enumerate(self.tor_max):
      self.pd_torque = np.clip(self.pd_torque, -m, m)
    # ***************************
    if self.use_expert:
      # torques = self.desired_torque
      # torques = []
      # for n,m in enumerate((self.motor_names)):
      #   if 'coxa' in m:
      #     torques.append(self.desired_torque[n]*10)
      #   elif 'femur' in m:
      #     torques.append(self.desired_torque[n]*1.2)
      #   else:
      #     torques.append(self.desired_torque[n]*1.5)
      # torques = self.Kp*(np.array(self.exp_joints) - np.array(self.joints)) + self.Kd*(np.array(self.exp_joint_vel) - np.array(self.joint_vel))
      self.label = self.Kp*(np.array(self.exp_joints) - np.array(self.joints)) + self.Kd*(np.array(self.exp_joint_vel) - np.array(self.joint_vel))
      # print(torques)
    torques = self.pd_torque
    if self.act_model is not None:
      torques = self.act_model(self.input_torque)
    for _ in range(self.actionRepeat):
      p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=torques)  
      p.stepSimulation()
    # if self.render:
      # time.sleep(0.01)
    self.get_observation()
    reward, done = self.get_reward()
    self.total_reward += reward
    if self.record_step: 
      self.record_sim_data()
    self.steps += 1
    # if (self.sample_pointer-self.initial_pointer) % self.sample_delay == 0:
      # if not self.test and not self.use_expert:
    # self.restore_sample(self.samples[self.sample_pointer], expert=False)
    self.sample_pointer += 1
    p.resetBasePositionAndOrientation(self.Id, [0,0,1.5],[0,0,0,1])

    print(self.sample_pointer)
    self.desired_torque = self.restore_sample(self.samples[self.sample_pointer], expert=True)
    # return np.array(self.desired_torque + self.joints + self.joint_vel), reward, done, None
    # print(np.array(self.desired_torque + [self.ob_dict[f + "_left_ground"] for f in self.feet_dict]))
    return np.array(self.desired_torque + [self.ob_dict[f + "_left_ground"] for f in self.feet_dict]), reward, done, None

  def get_reward(self):
    reward = 0
    reward -= 20*np.sum(np.array(self.exp_joints) - np.array(self.joints))**2
    reward -= np.clip(0.001*np.sum(np.array(self.exp_joint_vel) - np.array(self.joint_vel))**2, 0, 0.25)
    self.reward_breakdown['pos'].append(20*np.sum(np.array(self.exp_joints) - np.array(self.joints))**2)
    self.reward_breakdown['vel'].append(np.clip(0.001*np.sum(np.array(self.exp_joint_vel) - np.array(self.joint_vel))**2, 0, 0.25))
    # print(np.mean(self.reward_breakdown['pos']))
    done = False
    # if self.steps > self.horizon:
    # if self.steps > 200:
    # if self.sample_pointer == 14999:
    # print(self.steps, self.sample_pointer)
    if self.steps > 20000:
      done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    # self.ob_dict = {self.state[i]:jointStates[j[0]][0] for i,j in enumerate(self.ordered_joints[:int(18)])}
    # self.ob_dict.update({self.state[i+18]:jointStates[j[0]][1] for i,j in enumerate(self.ordered_joints[:int(18)])})
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    for n in self.feet_dict:
      self.ob_dict[n + '_left_ground']= not len(p.getContactPoints(self.Id, -1, self.feet_dict[n], -1))>0
    self.contacts = [self.ob_dict[m + '_left_ground'] for m in self.feet_dict]
  

  def restore_sample(self, sample, expert=False):
    if expert:
      y_offset = 4
    else:
      y_offset = 0
    gz_motor_names = ['AL_coxa_joint','AL_femur_joint','AL_tibia_joint']
    gz_motor_names += ['AR_coxa_joint','AR_femur_joint','AR_tibia_joint']
    gz_motor_names += ['BL_coxa_joint','BL_femur_joint','BL_tibia_joint']
    gz_motor_names += ['BR_coxa_joint','BR_femur_joint','BR_tibia_joint']
    gz_motor_names += ['CL_coxa_joint','CL_femur_joint','CL_tibia_joint']
    gz_motor_names += ['CR_coxa_joint','CR_femur_joint','CR_tibia_joint']
    # return np.array(joint_pos + joint_vel + joint_effort + base_pos + base_orn + base_vel + base_ang_vel + self.targets + joint_effort)
    state = [m + '_pos' for m in gz_motor_names]
    state += [m + '_vel' for m in gz_motor_names]
    state += [m + '_effort' for m in gz_motor_names]
    state += ['x','y','z']
    state += ['qx','qy','qz','qw']
    state += ['vx','vy','vz']
    state += ['v_roll','v_pitch','v_yaw']
    state += [m + '_targ' for m in gz_motor_names]
    sample_dict = {name: val for name, val in zip(state, sample)}

    joints = [sample_dict[j + '_pos'] for j in self.motor_names]
    joint_vel = [sample_dict[j + '_vel'] for j in self.motor_names]
    pos = [sample_dict['x'], sample_dict['y'] + y_offset, sample_dict['z']+ 0.07]
    orn = [sample_dict['qx'], sample_dict['qy'], sample_dict['qz'], sample_dict['qw']]
    base_lin_vel = [sample_dict['vx'], sample_dict['vy'], sample_dict['vz']]
    base_ang_vel = [sample_dict['v_roll'], sample_dict['v_pitch'], sample_dict['v_yaw']]
    base_vel = base_lin_vel + base_ang_vel
    if expert:
      self.set_position(pos, orn, joints, base_vel, joint_vel, robot_id=self.exp_Id)   
      self.exp_joints = joints
      self.exp_joint_vel = joint_vel
    else:
      self.set_position(pos, orn, joints, base_vel, joint_vel, robot_id=self.Id)
    self.input_torque = [sample_dict[j + '_targ'] for j in self.motor_names]
    return self.input_torque

  def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))
        np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))
        df = pd.DataFrame (np.array(self.inputs))  
        df.to_excel(path + '/inputs.xlsx', index=False)
        df = pd.DataFrame (np.array(self.labels))  
        df.to_excel(path + '/labels.xlsx', index=False)
        df = pd.DataFrame (np.array(self.pos_and_vel))  
        df.to_excel(path + '/pos_and_vel.xlsx', index=False)
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    # print(type(self.input_torque), type(self.contacts), type(list(self.pd_torque)))
    self.inputs.append(self.input_torque + self.contacts)
    self.labels.append(list(self.pd_torque))
    self.pos_and_vel.append(self.exp_joints + self.exp_joint_vel + self.joints + self.joint_vel)
    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)
    if len(self.exp_sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.exp_Id)
    exp_data = [pos, orn]
    joints = p.getJointStates(self.exp_Id, self.motors)
    exp_data.append([i[0] for i in joints])
    self.exp_sim_data.append(exp_data)
