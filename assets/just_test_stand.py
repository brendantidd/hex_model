import pybullet as p
import numpy as np
import os   
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD

def normalise(inputs, inputs_max, inputs_min):
  return (inputs - inputs_min)/(inputs_max - inputs_min)

def denormalise(inputs, inputs_max, inputs_min):
  # x = (max - min) * z + min
  return ((inputs_max - inputs_min)*inputs) + inputs_min

class Env():
  rank = comm.Get_rank()
  # ob_size = 18
  ob_size = 9
  # ob_size = 18*3
  ac_size = 3
  im_size = [48,48,4]
  # simtimeStep = 1/800
  simtimeStep = 1/100
  steps_per_sec = 1/simtimeStep
  # simtimeStep = 1/60
  actionRepeat = 1
  rew_buffer = deque(maxlen=5)
  episodes = -1
  Kp = 20
  def __init__(self, render=False, PATH=None, args=None, horizon=500, display_expert=True, record_step=True, cur=False, test=False, use_expert=False, act_model=None, sim_model=None):
      
    self.samples = np.load(home + '/data/my_rnn_real/stand_samples.npy')
    self.sample_size = self.samples.shape[0]
    print("Loaded reference data", self.samples.shape)

    self.act_model = act_model
    self.args = args
    # if self.act_model is not None:
    #   self.rnn_length = self.act_model.rnn_length
    self.sim_model = sim_model
    self.model_breakdown = True
    # self.model_breakdown = False
    self.render = render
    self.PATH = PATH
    self.display_expert = display_expert
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.use_expert = use_expert
    # self.all_legs = True
    self.all_legs = False
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot

    self.load_model()
    self.sim_data = []
    self.exp_sim_data = []
    self.inputs = []
    self.labels = []

  def load_model(self):
    p.loadMJCF("./assets/ground.xml")
    # objs = p.loadURDF("./assets/test_leg.urdf")
    objs = p.loadURDF("./assets/test_stand.urdf")
    self.Id = objs
    objs = p.loadURDF("./assets/test_stand.urdf")
    self.exp_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)

    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      self.ordered_joint_indices.append(j)
      if link_name == 'BR_foot_link': self.foot_link = j
      if info[2] == p.JOINT_PRISMATIC:
        jname = info[1].decode("ascii")
        print("prism ", jname, info[8], info[9])
        self.slide = j
        self.slide_min = info[8]
        self.slide_max = info[9]

      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    self.exp1_jdict = {}
    for j in range( p.getNumJoints(self.exp_Id) ):
      info = p.getJointInfo(self.exp_Id, j)
      link_name = info[12].decode("ascii")
      # self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      print(jname)
      # lower, upper = (info[8], info[9])
      # self.ordered_joints.append( (j, lower, upper) )
      self.exp1_jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.slide_name = ["vertical_slide_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]
    self.exp1_motors = [self.exp1_jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    self.vel_max = np.array([8,11,8])
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}
    
    self.collect_data = True
    # self.collect_data = False
    # Disable motors to use torque control:
    if self.collect_data:
      p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp_Id, self.exp1_motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    
    # data_path, data_type = '/home/brendan/data/real_test_leg/', ''
    # data_path, data_type = '/home/brendan/results/hex_model/latest/gz_data/', ''
    # self.test_inputs = np.array(pd.read_excel(data_path + 'inputs.xlsx'))[1:,:]
    # test_labels = np.array(pd.read_excel(data_path + 'test_' + data_type + 'labels.xlsx'))
    # self.test_labels = test_labels - self.test_inputs[:,:6]
    # self.test_labels = test_labels
    # print("expert size", self.test_inputs.shape)

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self):
    self.tip_left_ground = 0
    # self.sample_pointer = np.random.randint(0,self.sample_size)
    self.sample_pointer = 0
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel, self.exp_torque = temp[:3], temp[4:7], temp[8:]
    self.exp_slide = temp[3]
    self.coxa, self.femur, self.tibia = self.exp_joints

    self.set_position([0,0,-0.045], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, slide=self.exp_slide)
    self.set_position([0,1,-0.045], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel, robot_id=self.exp_Id,slide=self.exp_slide)

    # self.coxa, self.femur, self.tibia = [0.0,0.0,2.0]
    # self.exp_slide = 0.2
    # self.set_position([0,0,-0.045], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[0.0,0.0,0.0,0.0], slide=self.exp_slide)
    # self.set_position([0,1,-0.045], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[0.0,0.0,0.0,0.0], robot_id=self.exp_Id,slide=self.exp_slide)
    

    self.coxa_x, self.femur_x, self.tibia_x = 0,0,0
    self.input_torque = np.zeros(self.ac_size)
    self.input_sequence = deque([[0]*9]*32,maxlen=32)
    self.sim_model_input = deque([[0]*12]*32,maxlen=32)
    # self.sim_model_input = deque([[0]*9]*1,maxlen=1)
    self.data_size = 100
    self.all_data  = {n:deque([[0]*3]*self.data_size,maxlen=self.data_size) for n in ['input','output','exp_input','exp_output']}
    self.sample_pointer = 0
    # self.exp_joints, self.exp_joint_vel = self.test_inputs[self.sample_pointer,:3],self.test_inputs[self.sample_pointer,3:6]
    # self.set_position([0,0,2], [0,0,0,1], self.exp_joints, joint_vel = self.exp_joint_vel)
    # self.set_position([0,1,2], [0,0,0,1], self.exp_joints, joint_vel = self.exp_joint_vel, robot_id=self.exp_Id)
    # self.set_position([0,0,2], [0,0,0,1], [0.0,-0.4,1.85])
    # self.set_position([0,1,2], [0,0,0,1], [0.0,0.0,1.5], robot_id=self.exp_Id)

    self.episodes += 1
    self.total_reward = 0
    self.steps = 0
    jointStates = p.getJointStates(self.exp_Id,self.ordered_joint_indices)
    self.exp_joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp_joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    self.get_observation()
    self.prev_state = self.joints + self.joint_vel
    self.prev_exp_state = self.joints + self.joint_vel
    if self.record_step:
      self.save_sim_data()
    self.desired_torque = [0,0,0]
    # return np.array(self.desired_torque + self.exp_joints + self.exp_joint_vel+list(self.history))
    # return np.array(self.desired_torque + self.joints + self.joint_vel)
    self.phase = self.sample_pointer/self.sample_size

    return np.array(self.joints + self.joint_vel + [self.phase])

  def step(self, actions):
    
    # des_pos = self.get_des_joint_pos()
    rate = 2
    impulse = 20
    # if 
    # if (rate*self.steps)%self.steps_per_sec < impulse and not self.tip_left_ground:
    if self.tip_left_ground or self.steps == 0:
      self.time_in_contact = 0
    else:
      self.time_in_contact += 1

    # if (rate*self.steps)%self.steps_per_sec < impulse and not self.tip_left_ground:
    # if (rate*self.steps)%self.steps_per_sec < impulse:
    if self.time_in_contact > 10 and self.joints[1] < 0.3:
      self.input_torque = [0.0, 40, -25]
    else:
      max_dt = 0.1                                                                        
      self.input_torque = list(([1*(0.0-self.joints[0]), 30*np.clip((0.0-self.joints[1]),-max_dt,max_dt), 8*np.clip((2.0-self.joints[2]),-max_dt, max_dt)]) - 0.1*np.array(self.joint_vel)[:self.ac_size])

    # if self.
    # self.input_torque = 100*(np.array(des_pos) - np.array(self.joints))
    # self.input_torque = 100*(np.array([0.0,0.0,1.9]) - np.array(self.joints))
    # self.input_torque = [np.random.uniform(-5,5),np.random.uniform(-3,3),np.random.uniform(-1,1)]
    # self.input_torque = [np.random.normal(0,5),np.random.normal(-2,3),np.random.normal(0,1.25)]

    self.prev_state = self.joints + self.joint_vel    

    # p.setJointMotorControlArray(self.exp_Id, self.exp1_motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque)
    if actions is not None:
      torques = actions
    self.actions = torques

    self.set_position([0,1,-0.045], [0,0,0,1], self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp_Id, slide=self.exp_slide)
    
    # self.set_position([0,1,1], [0,0,0,1], vertical=self.vertical_slide)
    # p.setJointMotorControl2(self.Id, self.slide_joint,controlMode=p.POSITION_CONTROL, targetPosition=self.slide_joint_max)
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=torques)
    p.stepSimulation()
    # if not self.collect_data:
    jointStates = p.getJointStates(self.exp_Id,self.ordered_joint_indices)
    self.exp_joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp_joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
  
    if self.render:
      time.sleep(0.01)
    # if self.steps % 1000 == 0: 
    #   print(self.steps)
    self.get_observation()
    if self.record_step: 
      self.record_sim_data()
    reward, done = self.get_reward()
    self.total_reward += reward
    self.steps += 1
    self.sample_pointer += 1
    if self.sample_pointer > self.sample_size-1:
      self.sample_pointer = 0
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel, self.exp_torque = temp[:3], temp[4:7], temp[8:]
    self.exp_slide = temp[3]
    self.phase = self.sample_pointer/self.sample_size
    return np.array(self.joints + self.joint_vel + [self.phase]), reward, done, None
    # return np.array(self.desired_torque), reward, done, None

  def get_reward(self):
    
    reward = 0
    foot = np.array(p.getLinkState(self.Id, self.foot_link)[0])
    exp_foot = p.getLinkState(self.exp_Id, self.foot_link)[0]
    exp_foot = np.array([exp_foot[0], exp_foot[1]-1, exp_foot[2]])
    print(self.exp_torque, self.actions)
    pos = np.exp(-2*np.sum(abs(np.array(self.exp_joints) - np.array(self.joints)))) + np.exp(-40*np.sum(abs(np.array(self.exp_torque) - np.array(self.actions))))
    vel = np.exp(-0.1*np.sum(abs(np.array(self.exp_joint_vel) - np.array(self.joint_vel))))
    tip = np.exp(-40*np.sum(abs(foot - exp_foot)))
    
    neg = 0
    self.reward_breakdown['pos'].append(pos)
    self.reward_breakdown['vel'].append(vel)
    self.reward_breakdown['neg'].append(neg)
    self.reward_breakdown['tip'].append(tip)
    reward = 0.7*pos + 0.1*vel + neg + 0.2*tip
    # reward -= 0.5*abs(self.exp_tip_left_ground - self.tip_left_ground)
    # reward -= 2*abs(self.joints[3] - self.exp_joints[3])
    done = False
    # print(self.joints[3])
    if self.steps == 2000 or self.joints[3] < 0.08:
    # if self.steps == 500:
    # if self.steps == 29999:

      done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    slide = p.getJointState(self.Id, self.slide)
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]] + [slide[0]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]] + [slide[1]]
    
    exp_jointStates = p.getJointStates(self.exp_Id,self.ordered_joint_indices)
    exp_slide = p.getJointState(self.exp_Id, self.slide)
    self.exp_joints = [exp_jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]] + [exp_slide[0]]
    self.exp_joint_vel = [exp_jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]] + [exp_slide[1]]
    
    
    self.prev_tip_left_ground = self.tip_left_ground
    self.tip_left_ground = not len(p.getContactPoints(self.Id, -1, self.foot_link, -1))>0
    self.exp_tip_left_ground = not len(p.getContactPoints(self.exp_Id, -1, self.foot_link, -1))>0


  def get_des_joint_pos(self):
    # Pick new speed every 3 seconds
    # print(self.steps)
    if self.steps % (self.steps_per_sec*3) == 0:
      self.femur_speed = np.random.uniform(0.25,0.35)
      self.tibia_speed = np.random.uniform(0,0.25)
      self.femur_scale = np.random.uniform(0.5,1)
      self.tibia_scale = np.random.uniform(0.5,1)
      
      self.femur_speed = 0.01
      self.tibia_speed = 0.01
      self.femur_scale = 0.5 
      self.tibia_scale = 0.5
      # print("new speeds", self.coxa_speed, self.femur_speed, self.tibia_speed, self.coxa_scale, self.femur_scale, self.tibia_scale)
    # if self.tip_left_ground:
    if False:
      positions = [0.0, 0.0, 1.9]
    else:
      positions = []
      # Fraction of 1 second (max speed will be 2pi in 1 second)
      # steps = self.steps % (self.steps_per_sec)
      # --------------------
      # Limits of real controller:
      # --------------------   
      # coxa_max, coxa_min = 0.35, -0.35
      # femur_max, femur_min = 0.0, -0.7
      # tibia_max, tibia_min = 2.2, 1.5
      # --------------------
      # Actual joint limits:
      # --------------------   
      # coxa_max, coxa_min = 0.8, -0.8
      # femur_max, femur_min = 0.5, -1.0
      # tibia_max, tibia_min = 2.5, 0.5
      # --------------------   
      femur_max, femur_min = 0.0, -0.5
      tibia_max, tibia_min = 2.0, 1.7
      self.coxa = 0.0
      femur_scale = self.femur_scale*(femur_max - femur_min)/2
      femur_middle = (femur_max + femur_min)/2
      self.femur_x += self.femur_speed*2*np.pi/(self.steps_per_sec)
      self.femur = femur_scale*np.sin(self.femur_x) + femur_middle
      tibia_scale = self.tibia_scale*(tibia_max - tibia_min)/2
      tibia_middle = (tibia_max + tibia_min)/2
      self.tibia_x += self.tibia_speed*2*np.pi/(self.steps_per_sec) + np.pi/2
      self.tibia = tibia_scale*np.sin(self.tibia_x) + tibia_middle
      positions = [self.coxa, self.femur, self.tibia]
    return positions

  def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None, robot_id=None, slide=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])
    if slide is not None:
      p.resetJointState(robot_id, self.slide, targetValue=slide)
      

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))
        np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))
        self.sim_data = []
        self.exp_sim_data = []
        # print(np.array(self.inputs).shape, np.array(self.labels).shape)
        # df = pd.DataFrame (np.array(self.inputs))  
        # df.to_excel(path + '/inputs.xlsx', index=False)
        
        # df = pd.DataFrame (np.array(self.labels))  
        # df.to_excel(path + '/labels.xlsx', index=False)
        # print("episodes", self.episodes)
        # if self.episodes > 0:
        #   exit()
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    # self.inputs.append(self.prev_state + self.input_torque)
    # self.labels.append(self.joints + self.joint_vel)

    self.inputs.append(self.prev_state + self.input_torque)
    self.labels.append(self.joints + self.joint_vel)
    
    # self.inputs.append(self.prev_state + self.joints + self.joint_vel)
    # self.labels.append(self.input_torque)

    # self.inputs.append(self.exp_joints + self.exp_joint_vel + self.exp_torque)
    # self.labels.append(self.input_torque)

    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    data.append(self.joints)
    self.sim_data.append(data)

    if len(self.sim_data) > 100000: return
    data = [[0,1,-0.045], [0,0,0,1]]
    data.append(self.exp_joints)
    self.exp_sim_data.append(data)