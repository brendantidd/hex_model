import pybullet as p
import numpy as np
import os   
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from scripts.change_urdf import ChangeURDF
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD

class Env():
  rank = comm.Get_rank()
  ob_size = 9
  # ob_size = 6
  ac_size = 3
  im_size = [48,48,4]
  # simtimeStep = 1/100
  simtimeStep = 1/200
  # simtimeStep = 1/800
  # simtimeStep = 1/60
  # action_repeat = 2
  episodes = 0
  Kp = 20
  initial_Kp = 20
  total_reward = 0
  total_steps = 0
  delay_length = 1
  def __init__(self, render=False, PATH=None, args=None, horizon=500, record_step=True, cur=False, test=False, use_expert=False, control='torque', delay=False, rand_dynamics=False, test_pol=False):
    self.delay = delay
    self.rand_dynamics = rand_dynamics
    self.test_pol = test_pol
    self.control = control
    self.model_breakdown = True
    # self.model_breakdown = False
    self.render = render
    self.PATH = PATH
    self.args = args
    if self.render:
      self.display_expert = True
    else:
      self.display_expert = False
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.use_expert = use_expert
    # self.all_legs = True
    self.all_legs = False

    self.samples = np.load('samples/samples.npy')
    self.sample_size = self.samples.shape[0]
    print("Loaded reference data", self.samples.shape)

    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot

    if self.rand_dynamics:
      self.change_urdf = ChangeURDF('assets/new_urdf.urdf', debug=False, normal=False)
    
    self.load_model()
    self.sim_data = []
    self.exp_sim_data = []
    self.inputs = []
    self.labels = []
    self.rew_buffer = deque(maxlen=5)
    self.max_disturbance = 25

  def load_model(self):
    p.resetSimulation()
    p.loadMJCF("assets/ground.xml")
  
    if self.rand_dynamics:
      
      urdf_path = "assets/temp_urdf_" + self.args.exp + "_" + str(self.episodes+self.rank) + ".urdf"
      print("loading urdf from ", urdf_path)
      self.change_urdf.write_new_dynamics(urdf_path)
      self.Id = p.loadURDF(urdf_path)
      print([p.getDynamicsInfo(self.Id, i)[0] for i in [1,2,3]])
      os.remove(urdf_path)
    else:
      if self.args.new_urdf:
        print("using new urdf")
        urdf_path = "assets/new_urdf.urdf"
      else:
        urdf_path = "assets/test_leg.urdf"
      objs = p.loadURDF(urdf_path)
      self.Id = objs
    
    # if self.display_expert:
    objs = p.loadURDF(urdf_path)
    self.exp_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)


    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    self.joint_links = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      exp_info = p.getJointInfo(self.exp_Id, j)
      link_name = info[12].decode("ascii")
      exp_link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      self.joint_links.append(j)
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    # self.motor_power =  [5, 10, 5]
    # self.vel_max = [8,11,8]
    self.vel_max = [8,8,8]
    # self.tor_max =  [80,112,80]
    self.tor_max = np.array([10,10,3])
    
    self.coxa_max, self.coxa_min = 0.4, -0.4
    self.femur_max, self.femur_min = 0.1, -0.6
    self.tibia_max, self.tibia_min = 1.9, 0.0

    forces = np.ones(len(self.motors))*240
    # self.actions = {key:0.0 for key in self.motor_names}
    if self.control == 'torque':
      p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    
    # # Set max velocity of joints..
    # for link in self.joint_links:
    #   p.changeDynamics(self.Id, link, maxJointVelocity=8)
  
  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self):
    self.action_buffer = deque(maxlen=3)
    self.action_buffer.append([0,0,0])
    if self.rand_dynamics:
      self.load_model()

      # for l in self.joint_links:
      #   print(p.getDynamicsInfo(self.Id, l)[0])

    # Things to randomise: 
    # External disturbance
    # Need to consider stiction
    # - delay length (1-3)
    # - timestep 
    # from urdf
    # - joint position
    # - link com position
    # - link com value
    # pybullet dynamics
    # - joint damping
    # - contact friction (for stand)
    # Other
    # - Observation noise (0,0.05, 0,0.5)
    
    if self.delay:
      # self.action_repeat = np.random.randint(3,6)
      self.delay_length = np.random.randint(1,5)
      self.torque_history = deque([[0.0 for _ in range(self.ac_size)]]*self.delay_length, maxlen=self.delay_length)
    # for link in self.joint_links:
    #   p.changeDynamics(self.Id, link, jointDamping=np.random.uniform(0,0.1))
    
    self.coxa_speed, self.femur_speed, self.tibia_speed = 1,1,1
    self.coxa_x, self.femur_x, self.tibia_x = 0,0,0
    # self.sample_pointer = np.random.randint(0,self.sample_size)
    self.sample_pointer = 75
    temp = self.samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torques = temp[:3], temp[3:6], temp[6:]
    # self.coxa, self.femur, self.tibia = self.exp_joints + np.random.uniform(-0.2,0.2,3)
    self.coxa, self.femur, self.tibia = [np.random.uniform(self.coxa_min,self.coxa_max),np.random.uniform(self.femur_min,self.femur_max),np.random.uniform(self.tibia_min,self.tibia_max)] 

    self.initial_position  = [self.coxa, self.femur, self.tibia]
    self.exp_joints, self.exp_joint_vel, self.exp_torques = self.initial_position, [0,0,0], temp[6:]
    self.coxa_vel, self.femur_vel, self.tibia_vel = self.exp_joint_vel
    # self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=self.exp_joint_vel)
  
    # self.coxa, self.femur, self.tibia = np.random.uniform(-0.8,0.8),np.random.uniform(0.5,-1.0),np.random.uniform(2.5,0.5)
    # self.coxa_vel, self.femur_vel, self.tibia_vel = np.random.uniform(-8,8),np.random.uniform(-8,8),np.random.uniform(-8,8)

    self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel])
    self.set_position([0,1,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[self.coxa_vel, self.femur_vel, self.tibia_vel], robot_id=self.exp_Id)
    self.input_buffers = {n:{m:deque([0.0]*3, maxlen=3) for m in ['coxa','femur','tibia']} for n in ['torq', 'vel']}
    
    self.pos_error_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.vel_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.label_torques = {n:0.0 for n in ['coxa','femur','tibia']}
    self.desired_torque_buf = deque([0.0]*3*5,maxlen=5*3)
    self.prev_state_buf = deque([0.0]*6*5,maxlen=5*6)

    self.speed = -1.0
    self.step_length = 0.5
    # Number of timesteps per step
    self.episodes += 1
    self.rew_buffer.append(self.total_reward)
    # if self.Kp > 0 and len(self.rew_buffer) > 3 and np.all(np.array(self.total_reward) > 6400):
    # if self.Kp > 0 and len(self.rew_buffer) > 3 and np.all(np.array(self.total_reward) > 5800):
    if self.Kp > 0 and len(self.rew_buffer) > 4:
      self.Kp -= 1
      self.rew_buffer = deque(maxlen=5)
    self.total_reward = 0
    self.steps = 0
    self.get_observation()
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel
    if self.record_step:
      self.save_sim_data()
      # if self.episodes > 1:
      #   exit()
    self.desired_torque = [0,0,0]
    self.prev_actions = [0.0,0.0,0.0]
    self.prev_prev_torque = self.prev_torque = [0.0,0.0,0.0]
    # return np.array(self.desired_torque + self.exp_joints + self.exp_joint_vel+list(self.history))
    # return np.array(self.desired_torque + self.joints + self.joint_vel)
    self.phase = self.sample_pointer/self.sample_size

    # Control bit: 0 for hold position, 1 for go to zero, 2 for active
    self.control_bit = 0

    # return np.array(self.joints + self.joint_vel + [self.phase, self.control_bit])
    return np.array(self.joints + self.joint_vel + self.initial_position)
    # return np.array(self.joints + self.joint_vel)

  def step(self, actions):
    # if self.steps < 200:
    #   self.control_bit = 0
    # elif self.steps >= 200 and self.steps < 400:
    #   self.control_bit = 1
    # else:
    #   self.control_bit = 2     

    # if np.random.random() < 0.2:

    # if self.rand_dynamics:
    #   self.add_disturbance()  

    self.phase = self.sample_pointer/self.sample_size
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel
    # print(self.phase)
    # if self.steps % 3000 == 0:
    #   sign = 1 
    #   # sign = 1 if np.random.random() < 0.5 else -1
    #   self.speed = np.random.choice([0.0,0.25,0.5,0.75, 1.0]) * sign
    #   # self.speed = 0
    #   if abs(self.speed) == 1: 
    #     self.timesteps_per_step = 300
    #   elif abs(self.speed) == 0.75: 
    #     self.timesteps_per_step = 400
    #   elif abs(self.speed) == 0.5: 
    #     self.timesteps_per_step = 500
    #   elif abs(self.speed) == 0.25: 
    #     self.timesteps_per_step = 600
    #   else:
    #     self.timesteps_per_step = 0
    #   if self.rank == 0:
    #     print("new speed", self.speed)
        # self.delay = False

    self.actions = actions

    actions = np.clip(actions, np.array(self.prev_actions)-0.1, np.array(self.prev_actions)+0.1)
    self.prev_actions = actions
    # actions = np.mean(self.action_buffer, axis=0)
    # print(self.actions)

    if self.steps < 200:
      self.exp_joints, self.exp_joint_vel = self.initial_position, [0,0,0]
    elif self.steps >= 200 and self.steps < 400:
      # self.desired_joints =  [0.0,0.0,1.5707]
      self.exp_joints, self.exp_joint_vel = [0.0,0.0,1.5707], [0,0,0]
    # elif self.control_bit == 2:     
    #   self.desired_joints = list(self.exp_joints)
    self.set_position([0,1,2], [0,0,0,1], self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp_Id)

      # p.setJointMotorControlArray(self.exp_Id, self.exp1_motors,controlMode=p.TORQUE_CONTROL, forces=torques)
    if self.control == 'torque':
      # exp_torques = (self.Kp/self.initial_Kp)*(80*(np.array(self.exp_joints)-np.array(self.joints)) - 0.1*np.array(self.joint_vel))
     
      if self.delay:
        self.torque_history.append(actions)
        torques = self.torque_history[0]
        # torques = self.prev_prev_torque
        # self.prev_prev_torque = self.prev_torque
        # self.prev_torque = actions
      else:
        torques = actions
        # print(torques)
      # torques = [np.clip(e + a,-tm,tm) for e,a,tm in zip(exp_torques, torques, self.tor_max)]  
      torques = np.clip(torques, -self.tor_max, self.tor_max)
      p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=torques)
    else:
      vels = np.clip(np.array(actions[self.ac_size:self.ac_size*2]), -np.array(self.vel_max), np.array(self.vel_max))
      p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=actions[:self.ac_size], targetVelocities=vels,forces=self.tor_max)
    # for _ in range(self.action_repeat):
    p.stepSimulation()
    if self.render:
      time.sleep(0.005)
    self.get_observation()
    if self.record_step: 
      self.record_sim_data()
    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.action_buffer.append(self.actions)
    self.total_reward += reward
    self.steps += 1
    self.total_steps += 1

    # self.sample_pointer += 8
    # self.sample_pointer += 4
    if self.steps >= 400:
      self.sample_pointer += 1
      if self.sample_pointer > self.sample_size-1:
        self.sample_pointer = 0
      temp = self.samples[self.sample_pointer,:]
      self.exp_joints, self.exp_joint_vel, self.exp_torques = list(temp[:3]), list(temp[3:6]), temp[6:]

    # print(self.steps)
    return np.array(self.joints + self.joint_vel + self.exp_joints), reward, done, None
    # return np.array(self.joints + self.joint_vel), reward, done, None

  def get_reward(self):
    self.tips = p.getLinkState(self.Id, self.foot_link, computeLinkVelocity=True)
    self.tip_pos, self.tip_vel = self.tips[0], self.tips[-2]
    self.des_y = -0.47 
    self.des_z = 0.69
    # print(self.tip_pos[0])
    # print(self.tip_pos, self.tip_vel)
    # swing, stance.. 
    reward = 0
    # reward += np.exp(-2*((self.des_z - self.tip_pos[2])**2))
    # reward += np.exp(-10*((self.speed - np.sqrt(self.tip_vel[0]**2 + self.tip_vel[2]**2))**2))
    # reward -= 2*((self.des_z - self.tip_pos[2])**2)
    # reward -= 10*((self.speed - np.sqrt(self.tip_vel[0]**2 + self.tip_vel[2]**2))**2)
    # reward -= 5*(self.des_y - self.tip_pos[1])**2
    # print(self.tip_pos, np.sqrt(self.tip_vel[0]**2 + self.tip_vel[2]**2))
    # reward += np.exp(-2*np.sum((np.array(self.exp_joints) - np.array(self.joints))**2))
    foot = np.array(p.getLinkState(self.Id, self.foot_link)[0])
    exp_foot = p.getLinkState(self.exp_Id, self.foot_link)[0]
    exp_foot = np.array([exp_foot[0], exp_foot[1]-1, exp_foot[2]])
    # print(foot)
    # print(exp_foot)
    # print()
    # Not sure if interpreting correctly, but in the deep mimic paper the error terms are:
    # sumi(||ei||2), for each ith element. Meaning that it is the sqrt of the square of each error term, e.i just the absolute value of each error term summed.  
    # pos = np.exp(-2*np.sum((np.array(self.exp_joints) - np.array(self.joints))**2))
    # vel = np.exp(-0.1*np.sum((np.array(self.exp_joint_vel) - np.array(self.joint_vel))**2))
    # tip = np.exp(-40*np.sum((foot - exp_foot)**2))
    pos = np.exp(-2*np.sum(abs(np.array(self.exp_joints) - np.array(self.joints))))
    vel = np.exp(-0.1*np.sum(abs(np.array(self.exp_joint_vel) - np.array(self.joint_vel))))
    # vel = 0.0
    tip = np.exp(-40*np.sum(abs(foot - exp_foot)))

    # vel = 0.0
    # pos = np.exp(-2*np.sum((np.array([0.0,0.0,1.5]) - np.array(self.joints))**2))
    # neg = 0.25*np.exp(-0.01*sum(abs(np.array(self.prev_actions)-self.actions))) + 0.25*np.exp(-0.1*sum(abs(self.actions)))
    # neg = -0.005*sum(abs(np.array(self.prev_actions)-self.actions)) - 0.0005*sum(abs(self.actions)) - abs(self.exp_joints[1] - self.joints[1])
    # neg = -0.01*sum(abs(np.array(self.prev_actions)-self.actions)) - 0.0005*sum(abs(self.actions)) 
    
    # neg = -0.01*(sum(abs(np.array(self.prev_actions)-self.actions))) - 0.0001*sum(abs(self.actions)) 
    # neg = -0.005*(sum(abs(np.array(self.prev_actions)-self.actions))) - 0.00005*sum(abs(self.actions)) 
    # if (abs(np.array(self.prev_actions)-self.actions)>1.0).any():

    # errors = np.clip(abs(np.array(self.prev_actions)-np.array(self.actions)), None, 0.5)
    # errors = abs(np.array(self.prev_actions)-np.array(self.actions))
    # errors[errors < 0.1] = 0.0
    # mean_errors = (np.mean(self.action_buffer, axis=0)-np.array(self.actions))**2
    # fact = max(self.total_steps/500000, 1.0)
    # neg = -(fact*sum(errors) + fact*sum(mean_errors))
    # else:
    neg = 0
    # print(self.reward_breakdown, neg)
    self.reward_breakdown['pos'].append(pos)
    self.reward_breakdown['vel'].append(vel)
    self.reward_breakdown['neg'].append(neg)
    self.reward_breakdown['tip'].append(tip)

    # if self.control_bit == 0:
    #   reward = -np.sum((np.array(self.joints) - np.array(self.initial_position))**2)
    #   reward -= 0.01*np.sum((np.array(self.joint_vel) - np.array([0.0,0.0,0.0]))**2)
    # elif self.control_bit == 1:
    #   reward = -np.sum((np.array(self.joints) - np.array([0.0,0.0,1.5707]))**2)
    #   reward -= 0.01*np.sum((np.array(self.joint_vel) - np.array([0.0,0.0,0.0]))**2)
    # elif self.control_bit == 2:
    reward = 0.7*pos + 0.1*vel + neg + 0.2*tip
    # print(self.control_bit, reward)
    # reward += np.exp(-10*np.sum((np.array(self.exp_joint_vel) - np.array(self.joint_vel))**2))
    # Keep tip height constant, match reverence velocity with tip
    # Step height (tip z pos?), step length (tip x pos), step width (tip y pos)
    # for jv, mv in zip(self.joint_vel, self.vel_max):
    #   if abs(jv) > mv:
    #     reward -= 0.5
    # self.steps % self.timesteps_per_step
    done = False
    # if (np.any(abs(self.exp_joints - self.joints) > 0.85)):
    # if (np.any(abs(self.exp_joints - self.joints) > 1.5)):
    # if (np.sum((np.array(self.exp_joints) - np.array(self.joints))**2) > 1.0 or abs(self.exp_joints[1] - self.joints[1]) > 0.5):
    # if not args.test_pol and (sum((next_ob[:3] - samples[sample_pointer+1][:3])**2) > 1.0 or abs(next_ob[1] - samples[sample_pointer+1][1]) > 0.5):
      # done = True
    # if self.steps == 2000:
    if self.steps == (self.horizon-1):
    # if self.steps == self.sample_size-1:
      done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    if self.test_pol:
      self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
      self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]))
    else:
      self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]])+ np.random.uniform(-0.05,0.05,self.ac_size))
      # self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.normal(0,0.5,self.ac_size))
      self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.uniform(-0.5,0.5,self.ac_size))
    # print(self.joints, s)

  def add_disturbance(self):
    for j in self.joint_links:
      force_x = (np.random.random() - 0.5)*self.max_disturbance
      force_y = (np.random.random() - 0.5)*self.max_disturbance
      force_z = (np.random.random() - 0.5)*self.max_disturbance
      # print("applying forces", force_x, force_y, force_z)
      
      p.applyExternalForce(self.Id,j,[force_x,force_y,force_z],[0,0,0],p.LINK_FRAME)

  def get_des_joint_pos(self):
    positions = []
    if self.timesteps_per_step == 0:
      self.coxa, self.femur, self.tibia = 0.0, 0.0, 1.5
    else:
      delta_step = self.steps % self.timesteps_per_step
      step_time = self.timesteps_per_step/2
      # lift_off_time = step_time - (step_time/8)
      lift_off_time = step_time
      # lift_stale_time = (lift_off_time/2) + (step_time/8)
      coxa_stance_dt = 0.7/step_time/2
      coxa_swing_dt = 0.7/step_time/2
      # femur_up_dt = 0.5/((step_time/2) - (step_time/8))
      femur_up_dt = 0.5/((step_time/2))
      femur_down_dt = 0.5/(step_time/2)
      tibia_dt = 0.015
      coxa_max = 0.35
      coxa_min = -coxa_max
      femur_max = 0.0
      femur_min = -0.7
      tibia_max = 2.2
      tibia_min = 1.5
      # Lift off
      if delta_step < lift_off_time/2:
        if np.sign(self.speed) < 0:
          self.coxa = min(self.coxa + coxa_swing_dt, coxa_max)
        else:
          self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
        self.femur = max(self.femur - femur_up_dt, femur_min)
        self.tibia = min(self.tibia + tibia_dt, tibia_max)
      # elif delta_step < lift_stale_time:
      #   if np.sign(self.speed) < 0:
      #     self.coxa = min(self.coxa + coxa_swing_dt, coxa_max)
      #   else:
      #     self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
      #   self.femur = femur_min
      #   self.tibia = min(self.tibia + tibia_dt, tibia_max)
      elif delta_step < lift_off_time:
        if np.sign(self.speed) < 0:
          self.coxa = min(self.coxa + coxa_swing_dt, coxa_max)
        else:
          self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
        self.femur = min(self.femur + femur_down_dt, femur_max)
        self.tibia = max(self.tibia - tibia_dt, tibia_min)
      else:
        if np.sign(self.speed) < 0:
          self.coxa = max(self.coxa - coxa_swing_dt, coxa_min)
        else:
          self.coxa = min(self.coxa + coxa_stance_dt, coxa_max)
        self.femur = femur_max
        self.tibia = max(self.tibia - tibia_dt, tibia_min)
      # print(delta_step, self.coxa, self.femur)
    positions = [self.coxa, self.femur, self.tibia]
    return positions

  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))       
        np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))       
        self.sim_data = []
        self.exp_sim_data = []
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)
    
    if len(self.sim_data) > 100000: return
    data = [[0,1,2], [0,0,0,1]]
    data.append(self.exp_joints)
    self.exp_sim_data.append(data)
