import numpy as np
import rospy
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState, Imu
from gazebo_msgs.srv import SetModelState,SetModelConfiguration
from gazebo_msgs.msg import ModelState, ModelStates
from dynamic_reconfigure.srv import Reconfigure
from dynamic_reconfigure.msg import Config, DoubleParameter
from leg_go_ros_msgs.msg import TipStates
import time
from scipy.spatial.transform import Rotation as R
import copy
import roslaunch
from std_srvs.srv import SetBool, Empty, Trigger
import pandas as pd
from collections import deque
import random

class Env():
  ac_size = 3
  ob_size = 7
  im_size = [48,48,1]
  steps = 0
  rewards = 0
  Kp = 400
  # timestep = 1/800
  timestep = 1/100
  steps_per_sec = 100
  def __init__(self, render=False, PATH=None, args=None, record_samples=False, horizontal=False):

    data_path, data_type = '/home/brendan/data/my_rnn_real/', ''
    # self.samples = np.array(pd.read_excel(data_path + 'test_inputs.xlsx'))[1:,:]
    # self.exp_torque_samples = np.array(pd.read_excel(data_path + 'test_labels.xlsx'))[1:,:]
    self.samples = np.load('samples/samples.npy')
    self.sample_size = self.samples.shape[0]
    print("Loaded reference data", self.samples.shape)

    self.record_step = True
    self.rank = 0
    port = 1
    # os.environ["ROS_MASTER_URI"] = 'http://localhost:1131' + str(port)
    # os.environ["GAZEBO_MASTER_URI"] = 'http://localhost:1134' + str(port)
    terrain = 'flat'          
    robot_name = 'r1' 
    # roscore = subprocess.Popen(['roscore', '-p 1131' + str(port)])

    print("ROSCORE STARTED..")
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    collect_data = False
    # if collect_data:
    #   cli_args = ['./launch/test_leg_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=true']
    # else:
      # cli_args = ['./launch/test_leg_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=false']
    cli_args = ['./launch/test_leg_sim.launch', 'paused:=false', 'gui:=false', 'collect_data:=false', 'whole_body_control:=false', 'locomotion_engine:=false']

    print(cli_args)
    roslaunch_args = cli_args[1:]
    roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
    parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
    parent.start()
    print("ROSLAUNCH STARTED..")

    # wait until publishing
    rospy.init_node(robot_name + '_rl_node', anonymous=False) 
    time.sleep(7.5)

    # if rank == 0:
    self.pause_sim = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
    self.unpause_sim = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)

    self.torque = True
    self.num_saved_samples = 0
    # self.samples = []     
    self.horizontal = horizontal
    self.pause_time = 100
    self.PATH = PATH
    self.DATA_PATH = '/home/brendan/data/actuator_all/'
    # self.DATA_PATH = '/home/brendan/data/actuator_expert/'
    self.record_samples = record_samples
    self.args = args
    self.robot_name = robot_name
    self.robot_number = 1
    self.y_offset = 0
    self.ob_dict = {}
    self.motor_names = ['BR_coxa_joint','BR_femur_joint','BR_tibia_joint']
    # self.motor_power =  [15, 22, 15]
    self.motor_power =  [5, 10, 5]
    self.vel_max = [8,11,8]
    self.tor_max = [80,112,80]
    
    #
    # # State from PB
    self.state = ["BR_coxa_joint_pos","BR_femur_joint_pos","BR_tibia_joint_pos"]
    self.state += ["BR_coxa_joint_vel","BR_femur_joint_vel","BR_tibia_joint_vel"]
    # self.state += ["BR_coxa_joint_effort","BR_femur_joint_effort","BR_tibia_joint_effort"]

    # self.tip_names = ['AL', 'AR', 'BL', 'BR', 'CL', 'CR']    
    self.episodes = 0
    self.tip_states = {}
    self.initialise_subscribers()
    self.initialise_publishers()
    self.expert_scale = 0.1
    self.prev_torques = np.zeros(18)
    self.prev_acts = np.zeros(self.ac_size)
    self.joint_effort_data = np.zeros(self.ac_size)
    self.total_steps = 0
    self.inputs = []
    self.labels = []
    self.my_inputs = []
    self.my_labels = []
  
  def reset(self):
    self.sample_pointer = np.random.randint(0,self.sample_size)
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel = temp[:3], temp[3:6]
    # self.exp_torques = self.exp_torque_samples[self.sample_pointer,:]
    # self.coxa, self.femur, self.tibia = self.exp_joints
    self.coxa, self.femur, self.tibia = [0.0,0.0,1.5]
    # self.coxa_x, self.femur_x, self.tibia_x = 0,0,0

    self.joint_effort_data = [0.0,0.0,0.0]
    self.rewards = 0
    self.steps = 0
    self.episodes += 1
    
    self.pos_error_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.vel_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
    self.label_torques = {n:0.0 for n in ['coxa','femur','tibia']}
    
    self.desired_torque_buf = deque([0.0]*3*5,maxlen=5*3)
    self.prev_state_buf = deque([0.0]*6*5,maxlen=5*6)

    set_joints = rospy.ServiceProxy('/gazebo/set_model_configuration', SetModelConfiguration())
    smc = SetModelConfiguration()
    smc.model_name = "_" + self.robot_name
    smc.urdf_param_name = ''
    smc.joint_names = ['BR_coxa_joint', 'BR_femur_joint', 'BR_tibia_joint']
    smc.joint_positions = [self.coxa, self.femur, self.tibia]

    # print("waiting for set model config")
    rospy.wait_for_service('/gazebo/set_model_configuration')
    try:
      response_joints = set_joints(smc.model_name, smc.urdf_param_name, smc.joint_names, smc.joint_positions)
    except:
      print("Failed to reset joints")

    set_model = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState())
    sms = ModelState()
    sms.model_name = "_" + self.robot_name
    sms.pose.position.y = self.y_offset
    sms.pose.position.z = 0.1

    # print("waiting for set model state")
    rospy.wait_for_service('/gazebo/set_model_state')
    try:
      response_model = set_model(sms)
    except: 
      print("Failed to reset pose")
    self.step_sim()
    
    # self.get_observation()
    got_observation = False
    while not got_observation and not rospy.is_shutdown():
      try:
        self.get_observation()
        got_observation = True
      except:
        print("trying to get observation")

    self.speed = 1.0
    self.step_length = 0.5
    if self.record_step:
      self.save_sim_data()
      if self.episodes > 1: exit()
    # self.coxa, self.femur, self.tibia = 0,0,0
    self.actual_torque = [0.0,0.0,0.0]
    self.prev_prev_torque = self.prev_torque = [0.0,0.0,0.0]
    
    self.timesteps_per_step = 50
    self.phase = self.sample_pointer/self.sample_size

    return np.array(self.joints + self.joint_vel + [self.phase])

  def step(self, actions=None):

    self.prev_state = self.joints + self.joint_vel

    if actions is not None:
      self.input_torque = actions

    self.delay = False

    if self.delay:
      self.publish_actions(self.prev_prev_torque)
      self.prev_prev_torque = self.prev_torque
      self.prev_torque = self.input_torque    
    else:
      self.publish_actions(self.input_torque)
    
    self.step_sim()
    self.get_observation()
    
    reward, done = self.get_reward(self.input_torque)
    self.rewards += reward
    self.steps += 1
    if self.steps % 1000 == 0:
      print(self.steps)
    self.total_steps += 1
    self.sample_pointer += 8
    if self.sample_pointer > self.sample_size-9:
      self.sample_pointer = 0
    self.phase = self.sample_pointer/self.sample_size
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel = temp[:3], temp[3:6]

    return np.array(self.joints + self.joint_vel + [self.phase]), reward, done, None

  def get_reward(self, actions):
    reward = 1; done = False
    if self.steps > 100000:
    # if self.steps > 3000:
      done = True
    return reward, done

  def get_observation(self):
    '''
    Robot state
    '''   
    self.joints = list(self.joint_pos_data)
    self.joint_vel = list(self.joint_vel_data)
    self.joint_effort = list(self.joint_effort_data)

  def step_sim(self):
    self.pause_unpause(pause=False)
    t1 = time.time()
    while time.time() - t1 < self.timestep:
      time.sleep(0.00001)
    self.pause_unpause(pause=True)

  def pause_unpause(self, pause=True):
    if pause:
      rospy.wait_for_service('/gazebo/pause_physics')
      self.pause_sim()
    elif not pause:
      rospy.wait_for_service('/gazebo/unpause_physics')
      self.unpause_sim()

  # ROS interface stuff ======================================================
  def publish_actions(self, actions):
    for act, joint in zip(actions, self.joint_publishers):
      self.joint_publishers[joint].publish(np.clip(act, -75, 75))

  def initialise_publishers(self):
    self.joint_publishers = []
    self.joint_publishers = {}
    for joint in self.motor_names:
      # self.joint_publishers[joint] = rospy.Publisher('/' + self.robot_name + '/' + joint + '/command', Float64, queue_size=1)
      self.joint_publishers[joint] = rospy.Publisher('/r1/' + joint + '/command', Float64, queue_size=1)

  def initialise_subscribers(self):
    print("initialising subscribers for ", self.robot_name)
    # rospy.Subscriber('/' + self.robot_name + '/joint_states', JointState, self.jointStateCallback)
    # rospy.Subscriber('/' + self.robot_name + '/tip_states', TipStates, self.tipStateCallback)
    # rospy.Subscriber('/' + self.robot_name + '/imu/data', Imu, self.imuCallback)
    rospy.Subscriber('/r1/joint_states', JointState, self.jointStateCallback)
    # rospy.Subscriber('/tip_states', TipStates, self.tipStateCallback)
    # rospy.Subscriber('/imu/data', Imu, self.imuCallback)
    # rospy.Subscriber('/gazebo/model_states', ModelStates, self.modelStatesCallback)


    # Callbacks -------------------------------------------------------------- 
  def jointStateCallback(self, data):
    self.joint_pos_data = data.position
    self.joint_vel_data = data.velocity
    # Handle when joint_effort is 0.0.
    # Not sure if a gazebo thing, or a sample rate issue, but if 0.0, will wreck predictions
    if self.steps > 0:
      for i in range(self.ac_size):  
        if not (data.effort[i] < 0.00001 and data.effort[i] > -0.00001):
          self.joint_effort[i] = data.effort[i]        
      # else:
      #   print("gotta 0.0", data.effort[i], self.joint_effort[i])



  def initialise_joint_subscribers(self):
    self.target_joint_effort = {}   
    rospy.Subscriber('/' + self.robot_name + '/BR_coxa_joint/command/', Float64, self.BR_coxaCallback)
    rospy.Subscriber('/' + self.robot_name + '/BR_femur_joint/command/', Float64, self.BR_femurCallback)
    rospy.Subscriber('/' + self.robot_name + '/BR_tibia_joint/command/', Float64, self.BR_tibiaCallback)

  def BR_coxaCallback(self, data):
    self.target_joint_effort['BR_coxa_joint'] = data.data

  def BR_femurCallback(self, data):
    self.target_joint_effort['BR_femur_joint'] = data.data

  def BR_tibiaCallback(self, data):
    self.target_joint_effort['BR_tibia_joint'] = data.data

if __name__=="__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    PATH = home + '/results/hex_model/latest/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  if args.stand:
    id_input_size = 16
    id_output_size = 3
    fm_input_size = 11
    fm_output_size = 8
  else:
    if args.just_pos:
      id_input_size = 6
      id_output_size = 3
    else:
      id_input_size = 12
      id_output_size = 3
    fm_input_size = 9
    fm_output_size = 6
  if args.train_pol or (args.test_pol and not args.gz) and not args.rl_model:
    from models import simple_rnn, id_rnn
    dense_size, lstm_size = 128, 128

    forward_model = simple_rnn.RNN(name='forward_model', input_size=fm_input_size, output_size=fm_output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess)
    id_model = id_rnn.RNN(name='id_model', input_size=id_input_size, output_size=id_output_size, lstm_size=lstm_size, dense_size=dense_size, sess=sess, adam=args.adam)

  if args.train_pol or (args.test_pol and not args.gz):
    if args.stand:
      from assets.just_test_stand import Env
    else:
      from assets.just_test_leg_rl import Env
    # env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert)
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert, control=args.control)
  elif args.gz:
    if args.stand:
      from assets.test_stand_gz import Env
    else:
      from assets.test_leg_gz import Env
    env = Env(render=args.render, PATH=PATH, args=args)
    time.sleep(5)
  else:
    if args.stand:
      from assets.just_test_stand import Env
    else:
      from assets.just_test_leg import Env
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert)
  
  if args.train_pol or args.test_pol:
    if args.error:
      from models.ppo_error import Model
    else:
      from models.ppo import Model
    pol = Model("pi", env, ob_size=env.ob_size, ac_size=env.ac_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
    if args.rl_model:
      rl_model = Model("rl_model", env, ob_size=9, ac_size=3, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)

    initialize()
    sync_from_root(sess, pol.vars, comm=comm) #pylint: disable=E1101
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if (args.train_pol or (args.test_pol and not args.gz)) and not args.baseline:
    if args.rl_model:
      # rl_model.load(home + '/results/hex_model/latest/boobies3/')
      weights = tf.get_default_graph().get_tensor_by_name('rl_model/obfilter/runningsum:0')
      print(weights.eval(session=sess))
      rl_model.load(home + '/data/iros_data/rl_model/')
      print(weights.eval(session=sess))

    else:
      if args.stand:
        forward_model.load(home + '/data/iros_data/gz_100_delayed_stand/weights_/')
        # Adam
        # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_ft/')
        if not args.blank_id:
          if args.adam:
            id_model.load(home + '/data/iros_data/pb_id_100/weights_adam/')
          else:
            # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_ft/')
            # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_/')
            if args.just_pos:
              id_model.load(home + '/data/iros_data/pb_just_pos/weights_/')
            else:
              id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_32_256/')
      else:
        forward_model.load(home + '/data/iros_data/gz_100_delayed/weights_/')
        # Adam
        # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_ft/')
        if not args.blank_id:
          if args.adam:
            id_model.load(home + '/data/iros_data/pb_id_100/weights_adam/')
          else:
            # id_model.load(home + '/data/iros_data/pb_id_100_rand_norm/weights_/')
            if args.just_pos:
              id_model.load(home + '/data/iros_data/pb_just_pos/weights_/')
            else:
              id_model.load(home + '/data/iros_data/pb_id_150_rand_norm/weights_/')
      print("forward model mean, std")      
      print(forward_model.mean.eval())
      print(forward_model.std.eval())
      print(forward_model.scalar.eval())
      print("ID model mean, std, scale")
      print(id_model.mean.eval())
      print(id_model.std.eval())
      print(id_model.scalar.eval())

  if args.test_pol:
    if args.hpc:
      pol.load(home + '/hpc-home/results/hex_model/latest/' + args.exp + '/')      
      # pol.load(home + '/hpc-home/results/hex_model/a7/leg_const_std_baseline1/')      
      # pol.load(home + '/hpc-home/results/hex_model/latest/leg_const_std1/')      
    else:
      pol.load(PATH)
  
  # Throw an error if the graph grows (shouldn't once everything is initialised)
  tf.get_default_graph().finalize()
  # seq_length = 64
  seq_length = 256
  prev_done = True
  ob = env.reset()
  im = np.zeros(env.im_size)
  if args.train_pol or (args.test_pol and not args.gz) and not args.rl_model:
    fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
    id_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]   
    # training_states = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  steps = 0
  env.total_steps = 0
  env.reward_breakdown = {'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100)}
  plot_len = 200
  y1 = [[],[],[],[],[],[]]
  y2 = [[],[],[],[],[],[]]
  x1 = [[],[],[]]
  x2 = [[],[],[]]
  x3 = [[],[],[]]
  id_x = []
  id_y = []
  id_y_pos = []
  data_length = 256
  id_xs = np.zeros([data_length,seq_length,id_input_size])
  id_ys = np.zeros([data_length,seq_length,id_output_size])
  fm_pred =  deque(maxlen=data_length)
  fm_label = deque(maxlen=data_length)
  id_pred =  deque(maxlen=data_length)
  id_label = deque(maxlen=data_length)
  id_pointer = 0
  full = False
  # id_xs = deque(maxlen=256)
  # id_ys = deque(maxlen=256)
  # desired_next_state = deque(maxlen=seq_length)
  # actual_next_state = deque(maxlen=seq_length)
  id_loss = deque(maxlen=100)
  while True:
    # act = np.zeros(env.ac_size)
    if args.train_pol or (args.test_pol and not args.gz):
      if pol.timesteps_so_far > pol.max_timesteps:
        break 
      if args.test_pol:
        act, vpred, _, nlogp = pol.step(ob, im, stochastic=False)
        # act, vpred, _, nlogp = pol.step(ob, im, stochastic=True)
      else:
        act, vpred, _, nlogp = pol.step(ob, im, stochastic=True)
        # act = (80*(np.array(env.exp_joints)-np.array(env.joints)))
      if args.train_pol:
        if args.cur:
          exp_torques = (env.Kp/env.initial_Kp)*(80*(np.array(env.exp_joints)-np.array(env.joints)) - 0.1*np.array(env.joint_vel))
          act = [np.clip(e + a,-tm,tm) for e,a,tm in zip(exp_torques, act, env.tor_max)]  

      # print(act)
      # torques = act
      # t1 = time.time()
      if args.rl_model:
        torques, _,_,_ = rl_model.step(np.array(env.joints + env.joint_vel + list(act)), im, stochastic=False)
        # print(torques)1
        # print("torques from rl model", torques)
      elif args.baseline:
        torques = act
      else:
        fm_output, fm_state = forward_model.step(np.array(env.joints + env.joint_vel + list(act)), fm_state)   
        prev_id_state = id_state    
        if args.just_pos:
          torques, id_state = id_model.step(np.array(env.joints + list(fm_output[:3])), id_state)
        else:
          torques, id_state = id_model.step(np.array(env.joints + env.joint_vel + list(fm_output)), id_state)
    elif args.test_pol:
      torques, _, _, _ = pol.step(ob, im, stochastic=False)
      # torques, _, _, _ = pol.step(ob, im, stochastic=True)
      # print(torques)
    else:
      torques = None
    if args.do_plot:
      for a in range(3):
        # y3[a].append(fm_output[a])
        x1[a].append(act[a])
        x2[a].append(torques[a])
        x3[a].append(env.exp_torques[a])
      if len(x1[0]) > plot_len:
        subplot([[x1[0],x2[0],x3[0]],[x1[1],x2[1],x3[1]],[x1[2],x2[2],x3[2]]], legend=[["policy (real)", "sim", "exp torque"]]*3)
        # subplot([[x1[0],x3[0]],[x1[1],x3[1]],[x1[2],x3[2]]], legend=[["pred", "ground truth"]]*3)
    # print(time.time()-t1)
    # print(act)
    # print(torques)
    # print()
    # print("testing")
    # torques = fm_output
    if args.control == 'position':
      next_ob, rew, done, _ = env.step(fm_output)
    else:
      next_ob, rew, done, _ = env.step(torques)
    next_im = np.zeros(env.im_size)
    
    if args.train_pol:
    #   # ID model needs to be trained with on-policy data
      # desired_next_state.append(fm_output)
      # actual_next_state.append(env.joints + env.joint_vel)
      if args.just_pos:
        id_x.append(env.joints + env.joints)
        id_torque, _ = id_model.step(np.array(env.prev_joints + env.joints), prev_id_state)
      else:
        id_x.append(env.prev_state + env.joints + env.joint_vel)
        id_torque, _ = id_model.step(np.array(env.prev_state + env.joints + env.joint_vel), prev_id_state)
      id_y.append(torques)
      id_pred.append(id_torque)
      id_label.append(torques)
      fm_pred.append(fm_output)
      fm_label.append(env.joints+env.joint_vel)
      if len(id_x) == seq_length:
        id_xs[id_pointer, :,:] = id_x
        id_ys[id_pointer, :,:] = id_y
        id_pointer += 1
        if id_pointer == data_length:
          id_pointer = 0
          full = True
        # id_xs.append(id_x)
        # id_ys.append(id_y)
        id_x = []
        id_y = []
        
    #   # if env.steps != 0 and env.steps%seq_length == 0 and len(id_loss) < 6:
    #   if env.steps != 0 and env.steps%seq_length == 0:
    #     id_x = np.array(id_x)
    #     id_y = np.array(id_y)
    #     loss, id_pred, training_states = id_model.train(id_x,id_y,training_states,batch_size=1,seq_length=seq_length)
    #     id_loss.append(loss)
    #     if rank == 0 and env.steps == seq_length:
    #       subplot([[id_y[:,0], id_pred[0,:,0]],[id_y[:,1], id_pred[0,:,1]],[id_y[:,2], id_pred[0,:,2]]], legend=[["label","pred"]]*3, PATH=PATH)
    #       # id_model.save(PATH)
    #     id_x = []
    #     id_y = []
        

    if args.train_pol:
      pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
    prev_done = done
    ob = next_ob

    if done:    
      # print("reseting", done, steps, horizon)
      ob = env.reset()
      if args.train_pol or (args.test_pol and not args.gz) and not args.rl_model:
        fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]      
        id_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
        # training_states = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]  
        id_x = []
        id_y = []
      # exit()
      im = np.zeros(env.im_size)
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
        