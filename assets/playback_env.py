import pybullet as p
import numpy as np
import time

class PlayBackEnv():
  timeStep = 1/200
  ac_size = 3
  def __init__(self, args=None):
    self.args = args
    if args.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT)
    p.resetSimulation()
    p.loadMJCF("assets/ground.xml")
    objs = p.loadURDF("assets/new_urdf.urdf")
    self.Id = objs
    objs = p.loadURDF("assets/new_urdf.urdf")
    self.exp_Id = objs
    p.setTimeStep(self.timeStep)
    p.setGravity(0,0,-9.8)
    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    self.joint_links = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      # exp_info = p.getJointInfo(self.exp_Id, j)
      link_name = info[12].decode("ascii")
      # exp_link_name = info[12].decode("ascii")
      if link_name == 'BR_foot_link': self.foot_link = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      self.joint_links.append(j)
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    self.tor_max = np.array([10,10,3])

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]

    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

  def reset(self, initial_position=None, initial_velocity=None):
    self.steps = 0
    self.set_position([0,0,2], [0,0,0,1], joints=initial_position, joint_vel=initial_velocity)
    self.set_position([0,1,2], [0,0,0,1], joints=initial_position, joint_vel=initial_velocity, robot_id=self.exp_Id)
    self.get_observation()
    self.joints = initial_position
    self.joint_vel = initial_velocity
    self.prev_joints = self.joints
    self.prev_joint_vel = self.joint_vel
    return self.joints, self.joint_vel

  def step(self, actions, desired_joints):
    self.prev_joints = self.joints
    self.prev_joint_vel = self.joint_vel
    self.set_position([0,1,2], [0,0,0,1], desired_joints, robot_id=self.exp_Id)
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=actions)
    p.stepSimulation()
    self.get_observation()
    self.steps += 1
    time.sleep(0.005)
    reward, done = 0, False
    # if not self.args.test_pol and self.steps > 255:
    #   done = True
    return self.joints, self.joint_vel, reward, done

  def get_observation(self):
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]))

  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def play(self, input_torque, input_pos, initial_position):
    for _ in range(10):
      self.reset(initial_position)
      for torque, pos in zip(input_torque, input_pos):
        self.step(torque, pos)

if __name__=="__main__":
  import os,sys,inspect
  currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
  parentdir = os.path.dirname(currentdir)
  sys.path.insert(0,parentdir) 
  from pathlib import Path
  from scripts.utils import *
  import argparse
  from collections import deque
  home = str(Path.home())
  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--exp', default='')
  args = parser.parse_args()

  # path = home + '/data/30_01_2020/leg_baseline_09_49_56_30_01_2020/'
  # path = home + '/data/30_01_2020/leg_baseline_09_49_56_30_01_2020/'
  if args.exp == 'pid':
    path = home + '/Desktop/data/06_02_2020/baseline/'
  elif args.exp == 'baseline':
    path = home + '/Desktop/data/12_02_2020/leg_baseline_2020_02_12_08_06_49/'
  elif args.exp == 'eth':
    # path = home + '/Desktop/data/12_02_2020/leg_eth_2020_02_12_09_02_34/'
    path = home + '/Desktop/data/12_02_2020/leg_eth_2020_02_12_08_08_48/'
    # path = home + '/Desktop/data/12_02_2020/leg_eth_2020_02_12_08_10_51/'
  elif args.exp == 'eth_rnn' or args.exp == 'sim2real':
    # path = home + '/Desktop/data/12_02_2020/leg_eth_rnn_2020_02_12_09_04_20/'
    path = home + '/Desktop/data/12_02_2020/leg_eth_rnn_2020_02_12_08_25_08/'
  # elif args.exp == 'eth':
  #   path = home + '/Desktop/data/12_02_2020/leg_eth_2020_02_12_09_02_34/'
  # elif args.exp == 'eth':
  #   path = home + '/Desktop/data/12_02_2020/leg_eth_2020_02_12_09_02_34/'

  start_idx, stop_idx = 450, 1000

  samples = np.load('samples/samples.npy')
  inputs = np.load(path + 'inputs.npy')[450:4000,:]
  labels = np.load(path + 'labels.npy')[450:4000,:]
  if args.exp == 'sim2real':
    torques = np.load(path + 'observed_torques.npy')[450:4000,:]
  else:
    torques = inputs[:,6:9]
  exp_torques = samples[:,6:9]

  playback_env = PlayBackEnv(args)
  source = []
  target = []
  tor_source = []
  tor_target = []
  sample_size = inputs.shape[0]
  # sample_pointer = 125
  sample_pointer = 120
  playback_env.reset(inputs[0,:3],inputs[0,3:6])
  # print(playback_env.joints, inputs[sample_pointer,:3])
  # print(inputs.shape, sample_size)
  # x = [i for i in range(inputs.shape[0])]
  error = deque(maxlen=sample_size)
  errors = []
  torq = deque(maxlen=sample_size)
  torqs = []
  # for i in range(450, inputs.shape[0]):
  best = 1000
  best_inputs = deque(maxlen=sample_size)
  best_samples = deque(maxlen=sample_size)

  while True:
    playback_env.step(torques[0,:], [0,0,1.5707])


  for i in range(0, inputs.shape[0]):
    # source.append(playback_env.joints)
    # print(i)
    # print(playback_env.joints)
    # source.append(playback_env.joints)
    if args.exp == 'sim2real':
      # playback_env.step(torques[i,:], samples[sample_pointer,:])
      playback_env.step(torques[i,:], [0,0,1.5707])
      source.append(playback_env.prev_joints)
      target.append(inputs[i,:])
      if i == 0:
        print(source[0], target[0])
    else:
      source.append(inputs[i,:])
      target.append(samples[sample_pointer,:])
    tor_source.append(torques[i,:])
    tor_target.append(exp_torques[sample_pointer,:])
    

    # self.metric.append(np.mean((np.array(self.joints) - np.array(self.target_joints))**2) + np.mean((np.array(self.joint_vel) - np.array(self.target_joint_vel))**2))
    # self.pos_metric.append(np.mean((np.array(self.joints) - np.array(self.target_joints))**2))
    # self.torque_metric.append(np.sum(abs(self.input_torque)))
    # error.append(np.mean((np.array(inputs[i,:2]) - np.array(samples[sample_pointer,:2 ]))**2))
    # torq.append(np.sum(abs(torques[i,:])))
    
    error.append(np.mean((np.array(inputs[i,:3]) - np.array(samples[sample_pointer,:3]))**2))
    
    # error.append(np.sqrt(sum((np.array(inputs[i,:3]) - np.array(samples[sample_pointer,:3]))**2)))
    # torq.append(np.sqrt(sum(torques[i,:]**2)))
    torq.append(np.mean((np.array(torques[i,:3]) - np.array(exp_torques[sample_pointer,:3]))**2))

    if len(error) == sample_size:
      if np.mean(error) < best:
        best_run = [inputs[i-170:i,:3], list(samples[120:,:3]) + list(samples[:120,:3])]
        # print(np.array(best_run[1]).shape)
      errors.append(np.mean(error))
      torqs.append(np.mean(torq))

    sample_pointer += 1
    if sample_pointer > samples.shape[0] - 1:
      sample_pointer = 0      

  print(args.exp)
  print("pos error", np.mean(errors))
  print("torque error", np.mean(torqs))

  target = np.array(target)
  source = np.array(source)
  # source = np.array(best_run[0])
  # target = np.array(best_run[1])
  # print(target.shape, source.shape)
  # subplot([[target[:,0],source[:,0]],[target[:,1],source[:,1]],[target[:,2],source[:,2]]], legend=[['coxa_pos'],['femur_pos'],['tibia_pos']], PATH=home + '/Dropbox/PhD/iros/2020/pid_')
  if args.exp == 'sim2real':
    subplot([[target[:,0],source[:,0]],[target[:,1],source[:,1]],[target[:,2],source[:,2]]], PATH=home + '/Dropbox/PhD/iros/2020/' + args.exp + '_', legend=[['real', 'sim'],['real', 'sim'],['real', 'sim']], title=['Coxa Position','Femur Position','Tibia Position'])
  else:
    subplot([[target[:,0],source[:,0]],[target[:,1],source[:,1]],[target[:,2],source[:,2]]], PATH=home + '/Dropbox/PhD/iros/2020/' + args.exp + '_', legend=[['target', 'actual'],['target', 'actual'],['target', 'actual']], title=['Coxa Position','Femur Position','Tibia Position'])


  target = np.array(tor_target)
  source = np.array(tor_source)

  subplot([[target[:,0],source[:,0]],[target[:,1],source[:,1]],[target[:,2],source[:,2]]], PATH=home + '/Dropbox/PhD/iros/2020/torq_' + args.exp + '_', legend=[['target', 'actual'],['target', 'actual'],['target', 'actual']], title=['Coxa Position','Femur Position','Tibia Position'])
