import pybullet as p
import numpy as np
import os   
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from models import rand_net
from pathlib import Path
from scripts.change_urdf import ChangeURDF
home = str(Path.home())
comm = MPI.COMM_WORLD

def normalise(inputs, inputs_max, inputs_min):
  return (inputs - inputs_min)/(inputs_max - inputs_min)

def denormalise(inputs, inputs_max, inputs_min):
  # x = (max - min) * z + min
  return ((inputs_max - inputs_min)*inputs) + inputs_min

class Env():
  rank = comm.Get_rank()
  # ob_size = 18
  ob_size = 3
  # ob_size = 18*3
  ac_size = 3
  im_size = [48,48,4]
  # simtimeStep = 1/800
  simtimeStep = 1/200
  steps_per_sec = 1/simtimeStep
  # simtimeStep = 1/60
  actionRepeat = 1
  rew_buffer = deque(maxlen=5)
  episodes = 0

  def __init__(self, render=False, PATH=None, args=None, horizon=500, display_expert=True, record_step=True, cur=False, test=False, use_expert=False, act_model=None, sim_model=None, rand_dynamics=False):
    self.rand_dynamics = rand_dynamics
    # self.samples = np.load(home + '/data/iros_data/real/200/inputs_orig.npy')[100:270,:]
    # np.save("samples/samples.npy",self.samples)
    # np.save("samples/torque_samples.npy",self.torque_samples)

    # self.samples = np.load(home + "/data/data_12_39_32_29_01_2020/inputs.npy",allow_pickle=True)[0]
    # print(self.samples[0].shape)
    # self.torque_samples = self.samples[:,6:]
    # print("loading samples from ", home + "/data/data_12_39_32_29_01_2020/inputs.npy")
    self.samples = np.load("samples/samples.npy")
    self.torque_samples = self.samples[:,6:9]   
    
    # self.sample_size = self.samples.shape[0]
    self.sample_size = self.samples.shape[0]
    print("Loaded reference data", self.samples.shape)
    idx = [i for i in range(0,self.sample_size,self.sample_size//10)]
    self.target_set = self.samples[idx,:3]
    # self.desired_sim_length = 400000
    self.desired_sim_length = 200000
    self.entry_count = 0

    self.act_model = act_model
    self.args = args
    # if self.act_model is not None:
    #   self.rnn_length = self.act_model.rnn_length
    self.sim_model = sim_model
    self.model_breakdown = True
    # self.model_breakdown = False
    self.render = render
    self.PATH = PATH
    self.display_expert = display_expert
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.use_expert = use_expert
    # self.all_legs = True
    self.all_legs = False
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot

    if self.rand_dynamics:
      self.change_urdf = ChangeURDF('assets/test_leg.urdf', debug=False, normal=False, rank=self.rank)
      self.max_disturbance = 25


    # Max torques, max positions
    # self.max_torques = np.array([10,10,3])
    self.max_torques = np.array([15,15,5])
    # self.coxa_max, self.coxa_min = 0.8, -0.8
    # self.femur_max, self.femur_min = 0.5, -1.0
    # self.tibia_max, self.tibia_min = 2.5, 0.5
    # self.coxa_max, self.coxa_min = 0.6, -0.6
    # self.femur_max, self.femur_min = 0000.3, -0.8
    # self.tibia_max, self.tibia_min = 2.3, 0.3

    self.coxa_max, self.coxa_min = 0.4, -0.4
    self.femur_max, self.femur_min = 0.1, -0.6
    self.tibia_max, self.tibia_min = 1.9, 0.0

    self.rand_net = rand_net.Net(input_size=6, output_size=3, max_actions=self.max_torques)
    self.goto_ref = False
    if not self.rand_dynamics:
      self.load_model()
    self.sim_data = []
    self.inputs = []
    self.labels = []
    self.dones = []

  def load_model(self):
    p.resetSimulation()
    p.loadMJCF("./assets/ground.xml")
    if self.rand_dynamics:
      urdf_path = "assets/temp_urdf_" + self.args.exp + "_" + str(self.episodes+self.rank) + ".urdf"
      self.change_urdf.write_new_dynamics(urdf_path)
      self.Id = p.loadURDF(urdf_path)
      os.remove(urdf_path)
    else:
      objs = p.loadURDF("./assets/test_leg.urdf")
      self.Id = objs
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.exp1_Id = objs

    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)

    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    self.joint_links = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.joint_links.append(j)
      self.jdict[jname] = j
    self.exp1_jdict = {}
    for j in range( p.getNumJoints(self.exp1_Id) ):
      info = p.getJointInfo(self.exp1_Id, j)
      link_name = info[12].decode("ascii")
      # self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      # print(jname)
      # lower, upper = (info[8], info[9])
      # self.ordered_joints.append( (j, lower, upper) )
      self.exp1_jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
    self.motors = [self.jdict[n] for n in self.motor_names]
    self.exp1_motors = [self.exp1_jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    # self.vel_max = np.array([20,20,20])
    self.vel_max = np.array([8,11,8])
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}
    
    self.collect_data = True
    # self.collect_data = False
    # Disable motors to use torque control:
    if self.collect_data:
      p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp1_Id, self.exp1_motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    
    # # Limit joint velocity
    # print("joint links", self.joint_links)
    # for link in self.joint_links:
    #   p.changeDynamics(self.Id, link, maxJointVelocity=8)



  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self):
    self.controller = np.random.choice(['random','expert', 'rand_net'], p=[0.3,0.1,0.6])
    self.sum_error = 0
    # print(self.controller)
    self.rand_net.initialise()
    if self.rand_dynamics:
      self.load_model()
    self.sample_pointer = np.random.randint(0,self.sample_size)
    temp = self.samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torque = temp[:3], temp[3:6], self.exp_torque_samples[self.sample_pointer]
    self.exp_joints, self.exp_joint_vel = temp[:3], temp[3:6]
    self.exp_torque = self.torque_samples[self.sample_pointer,:]

    if self.episodes > 0:
      self.inputs.append(np.array(self.input_buf))
      self.labels.append(np.array(self.label_buf))
      self.input_buf = []
      self.label_buf = []
    else:
      self.input_buf = []
      self.label_buf = []

      self.freq = 1
      self.coxa_speed, self.femur_speed, self.tibia_speed = 1,1,1
      self.coxa_limit, self.femur_limit, self.tibia_limit = False, False, False
      if self.controller == 'random':
        self.coxa, self.femur, self.tibia = [np.random.uniform(self.coxa_min,self.coxa_max),np.random.uniform(self.femur_min,self.femur_max),np.random.uniform(self.tibia_min,self.tibia_max)] 
      elif self.controller == 'expert':
        self.coxa, self.femur, self.tibia = self.exp_joints
      else:
        self.coxa, self.femur, self.tibia = [0,0,1.5707]

      self.set_position([0,0,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[0,0,0])
      self.set_position([0,1,2], [0,0,0,1], [self.coxa,self.femur,self.tibia], joint_vel=[0,0,0], robot_id=self.exp1_Id)
      self.coxa_max, self.coxa_min = 0.8, -0.8
      self.femur_max, self.femur_min = 0.5, -1.0
      self.tibia_max, self.tibia_min = 2.5, 0.5

      # self.set_position([0,0,2], [0,0,0,1], [np.random.uniform(self.coxa_min, self.coxa_max),np.random.uniform(self.femur_min, self.femur_max),np.random.uniform(self.tibia_min, self.tibia_max)], joint_vel=[0.0,0.0,0.0])
      # self.set_position([0,0,2], [0,0,0,1], [np.random.uniform(self.coxa_min, self.coxa_max),np.random.uniform(self.femur_min, self.femur_max),np.random.uniform(self.tibia_min, self.tibia_max)], joint_vel=[0.0,0.0,0.0])
      # self.set_position([0,1,2], [0,0,0,1], [0.0,0.0,1.5], joint_vel=[0.0,0.0,0.0], robot_id=self.exp1_Id)
      
    self.coxa_x, self.femur_x, self.tibia_x = 0,0,0
    self.input_torque = np.zeros(self.ac_size)
    self.input_sequence = deque([[0]*9]*32,maxlen=32)
    self.sim_model_input = deque([[0]*12]*32,maxlen=32)
    # self.sim_model_input = deque([[0]*9]*1,maxlen=1)
    self.data_size = 100
    self.all_data  = {n:deque([[0]*3]*self.data_size,maxlen=self.data_size) for n in ['input','output','exp_input','exp_output']}
    self.sample_pointer = 0
    # self.exp_joints, self.exp_joint_vel = self.test_inputs[self.sample_pointer,:3],self.test_inputs[self.sample_pointer,3:6]
    # self.set_position([0,0,2], [0,0,0,1], self.exp_joints, joint_vel = self.exp_joint_vel)
    # self.set_position([0,1,2], [0,0,0,1], self.exp_joints, joint_vel = self.exp_joint_vel, robot_id=self.exp1_Id)
    # self.set_position([0,0,2], [0,0,0,1], [0.0,-0.4,1.85])
    # self.set_position([0,1,2], [0,0,0,1], [0.0,0.0,1.5], robot_id=self.exp1_Id)

    self.episodes += 1
    self.total_reward = 0
    self.steps = 0
    jointStates = p.getJointStates(self.exp1_Id,self.ordered_joint_indices)
    self.exp_joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp_joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    self.get_observation()
    self.prev_state = self.joints + self.joint_vel
    self.prev_exp_state = self.joints + self.joint_vel
    if self.record_step:
      self.save_sim_data()
    self.desired_torque = [0,0,0]
    self.prev_actions = [0,0,0]
    self.action_buffer = deque(maxlen=5)
    self.done = True
    # return np.array(self.desired_torque + self.exp_joints + self.exp_joint_vel+list(self.history))
    # return np.array(self.desired_torque + self.joints + self.joint_vel)
    return np.array(self.desired_torque)

  def step(self, actions):
    # delta_torque = np.array([np.random.uniform(-0.75,0.75),np.random.uniform(-0.5,0.5),np.random.uniform(-0.15,0.15)])
    # # des_pos = np.array([np.random.uniform(-0.8,0.8),np.random.uniform(-1.0,0.5),np.random.uniform(0.5,2.5)])
    # if (self.joints[0] < -0.5 and delta_torque[0] < 0) or (self.joints[0] > 0.5 and delta_torque[0] > 0):
    #   delta_torque[0] *= -1
    # if (self.joints[1] < -0.7 and delta_torque[1] < 0) or (self.joints[1] > 0.2 and delta_torque[1] > 0):
    #   delta_torque[1] *= -1
    # if (self.joints[2] < 0.8 and delta_torque[2] < 0) or (self.joints[2] > 2.1 and delta_torque[2] > 0):
    #   delta_torque[2] *= -1
    # self.input_torque += delta_torque    
    # self.input_torque = np.clip(self.input_torque, -np.array([5,3,1]),np.array([5,3,1]))
    # print(self.input_torque)
    # print()
    # if self.steps % 10 == 0:
    # print(self.joint_vel)
    self.prev_done = self.done
    # scale*np.sin(self.steps%)
    # if near limit, take some time, get back to closest point in ref trajectory.

    # if self.steps < 128:
    #   error = (np.array(self.exp_joints) - np.array(self.joints))
    #   torques = 0.5*self.max_torques*error + 0.1*self.sum_error - 0.25*np.array(self.joint_vel)
    #   self.sum_error += error
    # else:
      # if self.steps == 0 or (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
    # Kp = 20; Kd = 0.5
    
    if self.rand_dynamics:
      self.add_disturbance()  

    
    Kp = np.array([15,15,5]); Kd = np.array([0.5,0.5,0.25])    
    
    if self.controller == 'random':
      torques = [np.random.uniform(-self.max_torques[0], self.max_torques[0]),np.random.uniform(-self.max_torques[1], self.max_torques[1]),np.random.uniform(-self.max_torques[2], self.max_torques[2])]
    elif self.controller == 'expert':
      error = (np.array(self.exp_joints) - np.array(self.joints))
      # torques = 0.5*self.max_torques*error + 0.1*self.sum_error - 0.25*np.array(self.joint_vel)
      torques = Kp*error + 0.1*self.sum_error - Kd*np.array(self.joint_vel)
      self.sum_error += error
    
    elif self.controller == 'rand_net':

      if not self.goto_ref and (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
        self.goto_ref = True
        self.sum_error = 0
      if self.goto_ref:
        # error = (np.array(self.exp_joints) - np.array(self.joints))
        error = (np.array([0,0,1.5707]) - np.array(self.joints))
        torques = Kp*error + 0.02*self.sum_error - Kd*np.array(self.joint_vel)
        self.sum_error += error
        if (abs(error)<0.5).all():
          self.goto_ref = False
          self.rand_net.initialise()         
      else:
        torques = self.rand_net.step(np.array(self.joints+self.joint_vel))
        # print(self.steps, torques)    
      # print(self.steps, torques)
      # if np.random.random() < 0.9:
      # else:
      #   torques = [np.random.uniform(-3,3),np.random.uniform(-3,0),np.random.uniform(-0.5,0.5)]
      # print(self.steps, torques)      
    
    # self.action_buffer.append(np.clip(torques, np.array(self.prev_actions)-0.2, np.array(self.prev_actions)+0.2))
    # torques = np.mean(self.action_buffer, axis=0)
    # torques = np.clip(torques, -self.max_torques, self.max_torques) 
    # self.prev_actions = torques
    # self.action_buffer.append(torques)

    # Smoother: closest point, scaled for distance
    # # ============================================================================================================================================
    # # clever explore?
    # # ============================================================================================================================================
    # if not self.goto_ref and (self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max):
    #   # t1 = time.time()
    #   self.goto_ref = True
    #   self.goto_timeout = 0
    #   self.target = self.target_set[np.argmin((np.sum((self.target_set - np.array(self.joints))**2,axis=1))),:]
    #   errors = abs(self.target - np.array(self.joints))
    #   scales = errors/np.max(errors)
    #   self.K = np.random.uniform(0.5,0.8)*self.max_torques*scales
    #   # print(time.time() - t1)
    # if self.goto_ref:
    #   error = (np.array(self.target) - np.array(self.joints))
    #   if (abs(error)<0.3).all():
    #     self.goto_ref = False
    #   else:      
    #     self.goto_timeout += 1
    #     if self.goto_timeout > 50:
    #       torques = self.max_torques*error
    #     else:
    #       torques = self.K*error
    # if not self.goto_ref:
    #   torques = self.rand_net.step(np.array(self.joints+self.joint_vel))
    # torques = np.clip(torques, -self.max_torques, self.max_torques) 
    # # ============================================================================================================================================
      # if self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max:
    #   done = True
    # if self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joint_vel[1] < -self.vel_max[1] or self.joint_vel[1] > self.vel_max[1]:
    #   done = True
    # if self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max or self.joint_vel[2] < -self.vel_max[2] or self.joint_vel[2] > self.vel_max[2]:
    #   done = True

    # print(torques)
    self.input_torque = torques
    # self.input_torque = self.get_torques()

    # if self.steps == 0 or self.sample_pointer == 0:
    #   # sign, scale, frequency.
    #   self.scale = np.array([np.random.choice((-1,1))*np.random.uniform(0.1,1),1*np.random.uniform(0.1,1),np.random.choice((-1,1))*np.random.uniform(0.1,1)]) 
    #   self.freq = np.random.choice((0.5, 0.75, 1, 1.5, 2),p=[0.15,0.2,0.3,0.2,0.15])
    #   # self.scale = np.array([0.8,0.8,0.8]) 
    #   # self.freq = 1.0
    #   # print(self.scale, self.freq)

    #   # self.means = [np.random.uniform(-7.5,7.5),np.random.uniform(-7.5,7.5),np.random.uniform(-2,2)] 
    #   # print(self.means)
    # self.input_torque = self.scale * self.exp_torque
    # self.input_torque = [np.random.normal(self.means[0],2.5),np.random.normal(self.means[1],2.5),np.random.normal(self.means[2],1)]
    # self.input_torque = [np.random.uniform(-5,5),np.random.uniform(-3,3),np.random.uniform(-1,1)]
    # self.input_torque = [np.random.normal(0,5),np.random.normal(-2,3),np.random.normal(0,1.25)]
    # self.input_torque = self.exp_torque
    self.prev_state = self.joints + self.joint_vel    
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel

    # des_pos = np.array([np.random.uniform(-0.8,0.8),np.random.uniform(-1.0,0.5),np.random.uniform(0.5,2.5)])
    # max_pos = np.array(self.joints) + self.simtimeStep * self.vel_max
    # min_pos = np.array(self.joints) - self.simtimeStep * self.vel_max
    # des_pos = np.clip(des_pos, min_pos, max_pos)
    # pos_error = des_pos - np.array(self.joints)
    # print(pos_error)

    # pos_error = np.clip(pos_error, -0.1,0.1)
    # pos_error = np.clip(pos_error, -0.4,0.4)
    # print(pos_error)
    # self.input_torque = np.clip(1000*pos_error - 0.1*np.array(self.joint_vel), -np.array(self.tor_max), np.array(self.tor_max))
    # # print(self.input_torque)
    # des_pos = self.get_des_joint_pos()
    # pos_error = (des_pos - np.array(self.joints)) + np.array([np.random.uniform(-0.2,0.2), np.random.uniform(-0.2,0.2), np.random.uniform(-0.2,0.2)])
    # # self.input_torque = np.clip(40*pos_error - 0.1*np.array(self.joint_vel), -np.array(self.tor_max), np.array(self.tor_max))
    # # self.input_torque = np.clip(100000*pos_error , -np.array(self.tor_max), np.array(self.tor_max))
    # self.input_torque = np.array([10*pos_error[0], 10*pos_error[1], 2*pos_error[2]])
    # self.input_torque = [50*pos_error[0], 80*pos_error[1], 10*pos_error[2]]
    # self.input_torque = [100*pos_error[0], 100*pos_error[1], 10*pos_error[2]]
    # self.input_torque = [np.random.normal(0,5),np.random.normal(-2,3),np.random.normal(0,1.25)]
    # Clip to some value close to previous value (don't want to go crazy)
    
    # p.setJointMotorControlArray(self.exp1_Id, self.exp1_motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque)
  

    self.set_position([0,1,2], [0,0,0,1], self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp1_Id)
    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque)
    p.stepSimulation()
    # if not self.collect_data:
    jointStates = p.getJointStates(self.exp1_Id,self.ordered_joint_indices)
    self.exp_joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.exp_joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
      # self.all_data['exp_output'].append(np.array(self.exp_joints + self.exp_joint_vel) - np.array(self.prev_exp_state))
      # self.all_data['output'].append(net_out)

    if self.render:
      time.sleep(0.005)
      # time.sleep(0.00001)
    if self.entry_count % 1000 == 0 and self.entry_count != 0: 
      print(self.entry_count)
    self.get_observation()
    if self.record_step: 
      self.record_sim_data()
    reward, self.done = self.get_reward()
    self.total_reward += reward
    self.steps += 1

    self.sample_pointer += 1
    if self.sample_pointer > self.sample_size - 1:
      self.sample_pointer = 0
    temp = self.samples[self.sample_pointer,:]
    self.exp_joints, self.exp_joint_vel = temp[:3], temp[3:6]
    self.exp_torque = self.torque_samples[self.sample_pointer,:]
    # self.exp_joints, self.exp_joint_vel, self.exp_torque = temp[:3], temp[3:6], self.exp_torque_samples[self.sample_pointer]

    return np.array(self.desired_torque), reward, self.done, None

  def get_reward(self):
    reward = 0
    done = False

    # self.coxa_max, self.coxa_min = 0.35, -0.35
    # self.femur_max, self.femur_min = 0.0, -0.7
    # self.tibia_max, self.tibia_min = 2.2, 1.5
    

    # if self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max or self.joint_vel[0] < -self.vel_max[0] or self.joint_vel[0] > self.vel_max[0]:
    # # if self.joints[0] < self.coxa_min or self.joints[0] > self.coxa_max:
    #   done = True
    # if self.joints[1] < self.femur_min or self.joints[1] > self.femur_max or self.joint_vel[1] < -self.vel_max[1] or self.joint_vel[1] > self.vel_max[1]:
    #   done = True
    # if self.joints[2] < self.tibia_min or self.joints[2] > self.tibia_max or self.joint_vel[2] < -self.vel_max[2] or self.joint_vel[2] > self.vel_max[2]:
    #   done = True

    # if self.steps == 255:
    if self.steps == 8*256-1:
    # if self.steps == 2999:
    # if self.steps == 2999999:

      done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    # print(jointStates)
    # self.ob_dict = {self.state[i]:jointStates[j[0]][0] for i,j in enumerate(self.ordered_joints[:int(18)])}
    # self.ob_dict.update({self.state[i+18]:jointStates[j[0]][1] for i,j in enumerate(self.ordered_joints[:int(18)])})
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    # print(self.joints, s)

  def add_disturbance(self):
    for j in self.joint_links:
      force_x = (np.random.random() - 0.5)*self.max_disturbance
      force_y = (np.random.random() - 0.5)*self.max_disturbance
      force_z = (np.random.random() - 0.5)*self.max_disturbance
      # print("applying forces", force_x, force_y, force_z)
      
      p.applyExternalForce(self.Id,j,[force_x,force_y,force_z],[0,0,0],p.LINK_FRAME)

  def get_torques(self):
    # self.coxa_middle, self.femur_middle, self.tibia_middle = 0.0, -2, 0.0
    self.coxa_middle, self.femur_middle, self.tibia_middle = 0.0, 0.0, 0.0
    # if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.coxa_speed) == 0:
      # coxa_max, coxa_min, self.coxa_middle = 7, -7, 0.0
    #   self.coxa_speed = np.random.uniform(0.1,1.25)
    #   self.coxa_scale  = np.random.uniform(0,1.0)*(coxa_max - coxa_min)/2
    #   self.coxa_x = 0.0

    # if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.femur_speed) == 0:
    #   femur_max, femur_min, self.femur_middle = 2, -5, -2.5
    #   self.femur_speed = np.random.uniform(0.1,1.25)
    #   self.femur_scale = np.random.uniform(0,1.0)*(femur_max - femur_min)/2
    #   self.femur_x = 0.0

    # if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.tibia_speed) == 0:
    #   tibia_max, tibia_min, self.tibia_middle = 1.5, -1.5, 0.0
    #   self.tiba_speed = np.random.uniform(0.1,1.25)
    #   self.tibia_scale = np.random.uniform(0,1.0)*(tibia_max - tibia_min)/2
      # self.tibia_x = 0.0
    # self.coxa_speed, self.femur_speed, self.tibia_speed = 0.25, 0.25, 0.25
    self.coxa_speed, self.femur_speed, self.tibia_speed = 1.0,1.0,1.0
    self.coxa_scale, self.femur_scale, self.tibia_scale = 0.2,0.0,0.0
    self.coxa_x += 2*np.pi/(self.steps_per_sec/self.coxa_speed)    
    self.coxa = self.coxa_scale*np.sin(self.coxa_x) + self.coxa_middle
    self.femur_x += 2*np.pi/(self.steps_per_sec/self.femur_speed)    
    self.femur = self.femur_scale*np.sin(self.femur_x) + self.femur_middle
    self.tibia_x += 2*np.pi/(self.steps_per_sec/self.tibia_speed)    
    self.tibia = self.tibia_scale*np.sin(self.tibia_x) + self.tibia_middle
    self.coxa = 5*np.sin(self.steps*0.1)
    self.femur = 5*np.sin(self.steps*0.1) - 2
    self.tibia = 2*np.sin(self.steps*0.1) 
    # self.femur = 5*np.sin(self.steps*0.2)
    
    print([self.coxa, self.femur, self.tibia])


    return [self.coxa, self.femur, self.tibia]

  # def get_torques(self):
  #   # Limits of real controller:
  #   # --------------------   
  #   # self.coxa_max, self.coxa_min = 0.35, -0.35
  #   # self.femur_max, self.femur_min = 0.0, -0.7
  #   # self.tibia_max, self.tibia_min = 2.2, 1.5
  #   # Conservative limits
  #   self.coxa_max, self.coxa_min = 0.0, 0.0
  #   self.femur_max, self.femur_min = -0.35, -0.35
  #   self.tibia_max, self.tibia_min = 1.8,1.8
  #   # --------------------
  #   # Actual joint limits:
  #   # --------------------   
  #   # coxa_max, coxa_min = 0.8, -0.8
  #   # femur_max, femur_min = 0.5, -1.0
  #   # tibia_max, tibia_min = 2.5, 0.5
  #   scale = 0.01
  #   if np.random.random() < 0.2 or self.steps == 0:
  #     self.coxa_dt = 0.02*(np.random.random()-0.5) 
  #     self.coxa_limit = np.random.uniform(0,2)

  #     # self.coxa_limit = np.random.uniform(0,5)
  #     # print("coxa", self.coxa_dt)
  #   if np.random.random() < 0.2 or self.steps == 0:
  #     self.femur_dt = 0.02*(np.random.random()-0.5) 
  #     self.femur_limit = np.random.uniform(0,2)

  #     # print("femur", self.femur_dt)
  #   if np.random.random() < 0.2 or self.steps == 0:
  #     self.tibia_dt = 0.02*(np.random.random()-0.5) 
  #     self.tibia_limit = np.random.uniform(0,1)
  #     # print("tibia", self.tibia_dt)
    
  #   if self.coxa > self.coxa_limit:
  #     self.coxa_dt = abs(self.coxa_dt)*-1
  #   if self.coxa < -self.coxa_limit:
  #     self.coxa_dt = abs(self.coxa_dt)
  #   if self.femur > self.femur_limit:
  #     self.femur_dt = abs(self.femur_dt)*-1
  #   if self.femur < -self.femur_limit:
  #     self.femur_dt = abs(self.femur_dt)
  #   if self.femur > self.femur_limit:
  #     self.femur_dt = abs(self.femur_dt)*-1
  #   if self.femur < -self.femur_limit:
  #     self.femur_dt = abs(self.femur_dt)


  #   # max_dt = 0.01
  #   if self.joints[0] > self.coxa_max:
  #     self.coxa_dt = abs(self.coxa_dt)*-1
  #     # self.coxa_dt = -max_dt
  #   if self.joints[0] < self.coxa_min:
  #     self.coxa_dt = abs(self.coxa_dt)
  #     # self.coxa_dt = max_dt
  #   if self.joints[1] > self.femur_max:
  #     # print("femur max", self.joints[1], self.femur_max, self.femur_dt, self.femur)
  #     self.femur_dt = abs(self.femur_dt)*-1
  #     # self.femur_dt = -max_dt
  #   if self.joints[1] < self.femur_min:
  #     self.femur_dt = abs(self.femur_dt)
  #     # self.femur_dt = max_dt
  #     # print("femur min", self.joints[1], self.femur_min, self.femur_dt, self.femur)
  #   if self.joints[2] > self.tibia_max:
  #     self.tibia_dt = abs(self.tibia_dt)*-1
  #     # self.tibia_dt = -max_dt
  #   if self.joints[2] < self.tibia_min:
  #     self.tibia_dt = abs(self.tibia_dt)
  #     # self.tibia_dt = max_dt

  #   # if self.joints[0] > self.coxa_max or self.joint_vel[0] > self.vel_max[0]:
  #   #   self.coxa_dt = abs(self.coxa_dt)*-1
  #   # if self.joints[0] < self.coxa_min or self.joint_vel[0] < -self.vel_max[0]:
  #   #   self.coxa_dt = abs(self.coxa_dt)
  #   # if self.joints[1] > self.femur_max or self.joint_vel[1] > self.vel_max[1]:
  #   #   self.femur_dt = abs(self.femur_dt)*-1
  #   # if self.joints[1] < self.femur_min or self.joint_vel[1] < -self.vel_max[1]:
  #   #   self.femur_dt = abs(self.femur_dt)
  #   # if self.joints[2] > self.tibia_max or self.joint_vel[2] > self.vel_max[2]:
  #   #   self.tibia_dt = abs(self.tibia_dt)*-1
  #   # if self.joints[2] < self.tibia_min or self.joint_vel[2] < -self.vel_max[2]:
  #   #   self.tibia_dt = abs(self.tibia_dt)

  #   self.coxa  = np.clip(self.coxa+ self.coxa_dt ,-self.tor_max[0], self.tor_max[0])
  #   self.femur = np.clip(self.femur+self.femur_dt,-self.tor_max[1], self.tor_max[1])
  #   self.tibia = np.clip(self.tibia+self.tibia_dt,-self.tor_max[2], self.tor_max[2])
  #   print(self.coxa, self.femur, self.tibia)
  #   return [self.coxa, self.femur, self.tibia]

  # def get_torques(self):
  #   if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.coxa_speed) == 0:
  #     coxa_max, coxa_min, self.coxa_middle = 7, -7, 0.0
  #     self.coxa_speed = np.random.uniform(0.1,1.25)
  #     self.coxa_scale  = np.random.uniform(0,1.0)*(coxa_max - coxa_min)/2
  #     self.coxa_x = 0.0

  #   if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.femur_speed) == 0:
  #     femur_max, femur_min, self.femur_middle = 2, -5, -2.5
  #     self.femur_speed = np.random.uniform(0.1,1.25)
  #     self.femur_scale = np.random.uniform(0,1.0)*(femur_max - femur_min)/2
  #     self.femur_x = 0.0

  #   if int(self.steps*2*np.pi) % int(self.steps_per_sec/self.tibia_speed) == 0:
  #     tibia_max, tibia_min, self.tibia_middle = 1.5, -1.5, 0.0
  #     self.tiba_speed = np.random.uniform(0.1,1.25)
  #     self.tibia_scale = np.random.uniform(0,1.0)*(tibia_max - tibia_min)/2
  #     self.tibia_x = 0.0
  #   # self.coxa_speed, self.femur_speed, self.tibia_speed = 0.25, 0.25, 0.25
  #   # self.coxa_speed, self.femur_speed, self.tibia_speed = 1.25, 1.25, 1.25
  #   self.coxa_x += 2*np.pi/(self.steps_per_sec/self.coxa_speed)    
  #   self.coxa = self.coxa_scale*np.sin(self.coxa_x) + self.coxa_middle
  #   self.femur_x += 2*np.pi/(self.steps_per_sec/self.femur_speed)    
  #   self.femur = self.femur_scale*np.sin(self.femur_x) + self.femur_middle
  #   self.tibia_x += 2*np.pi/(self.steps_per_sec/self.tibia_speed)    
  #   self.tibia = self.tibia_scale*np.sin(self.tibia_x) + self.tibia_middle
  #   return [self.coxa, self.femur, self.tibia]

  def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))
        # print("episodes", self.episodes)
        if self.entry_count > self.desired_sim_length:
          np.save(path + "/inputs.npy", np.array(self.inputs))
          np.save(path + "/labels.npy", np.array(self.labels))
          # print(np.array(self.inputs).shape, np.array(self.labels).shape)
          # df = pd.DataFrame (np.array(self.inputs))  
          # df.to_excel(path + '/inputs.xlsx', index=False)
          
          # df = pd.DataFrame (np.array(self.labels))  
          # df.to_excel(path + '/labels.xlsx', index=False)

          # df = pd.DataFrame (np.array(self.dones))  
          # df.to_excel(path + '/dones.xlsx', index=False)
          exit()
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    # self.inputs.append(self.prev_state + self.input_torque)
    # self.labels.append(self.joints + self.joint_vel)

    # self.inputs.append(self.prev_state + self.input_torque)
    # self.labels.append(self.joints + self.joint_vel)
    
    # self.inputs.append(self.prev_state + self.joints + self.joint_vel)
    # self.labels.append(self.input_torque)
    # self.dones.append(self.prev_done)

    self.input_buf.append(self.prev_state + self.joints + self.joint_vel)
    self.label_buf.append(self.input_torque)
    self.entry_count += 1

    # self.inputs.append(self.prev_joints + self.joints)
    # self.labels.append(self.input_torque)

    # self.inputs.append(self.exp_joints + self.exp_joint_vel + self.exp_torque)
    # self.labels.append(self.input_torque)

    if len(self.sim_data) > 1000000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)
