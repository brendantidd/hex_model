import pybullet as p
import numpy as np
import os   
import time
from mpi4py import MPI
from collections import deque
import pandas as pd
from scripts.utils import *
from pathlib import Path
home = str(Path.home())
comm = MPI.COMM_WORLD
  
class Env():
  rank = comm.Get_rank()
  # ob_size = 18
  ob_size = 3
  # ob_size = 18*3
  ac_size = 3
  im_size = [48,48,4]
  # Gazebo data has been collected at 500Hz
  # simtimeStep = 1/800
  simtimeStep = 1/60
  # simtimeStep = 1/200
  # timeStep = 1/800
  actionRepeat = 1
  rew_buffer = deque(maxlen=5)
  episodes = -1
  sample_delay = 10000000
  # sample_delay = 1
  # sample_delay = 10
  # Kp = {'coxa_fs':160, 'coxa_g':400, 'femur_fs': 300,'femur_g': 400, 'tibia_fs': 300, 'tibia_g': 400}
  # Kd = {'coxa_fs':0.5, 'coxa_g':1, 'femur_fs': 1,'femur_g': 1, 'tibia_fs': 0.01, 'tibia_g': 1}
  # Kp = np.array([70,200,50])
  # Kd = np.array([8,10,4])
  Kp = np.array([40,80,6])
  Kd = np.array([6,6,2])
  # Kp = np.array([0,0,0])
  # Kd = np.array([0,0,0])
  # Kd = {'coxa_fs':0.5, 'coxa_g':1, 'femur_fs': 1,'femur_g': 1, 'tibia_fs': 0.01, 'tibia_g': 1}

  def __init__(self, render=False, PATH=None, args=None, horizon=500, display_expert=True, record_step=True, cur=False, test=False, use_expert=False, act_model=None, sim_model=None):
    self.act_model = act_model
    self.args = args
    # if self.act_model is not None:
    #   self.rnn_length = self.act_model.rnn_length
    self.sim_model = sim_model
    # if self.sim_model is not None:
    #   self.sim_rnn_length = self.sim_model.rnn_length
    self.model_breakdown = True
    # self.model_breakdown = False
    self.render = render
    self.PATH = PATH
    self.display_expert = display_expert
    self.horizon = horizon
    self.record_step = record_step
    self.cur = cur
    self.test = test
    self.use_expert = use_expert
    # self.all_legs = True
    self.all_legs = False
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot
    self.DATA_PATH = home + '/data/'
    # self.samples = load_data(self.DATA_PATH + 'test/')
    # self.samples = np.load(home + '/data/test_leg_15_11/sample.npy')
    # self.samples = np.load(home + '/data/test_leg_15_11/sample.npy')
    # self.samples = np.load(home + '/results/hex_model/latest/pb_fm/sim_data.npy',allow_pickle=True)
    # self.samples = pd.read_csv(self.DATA_PATH + 'test_leg_samples.csv', header=None).as_matrix()



    # inputs = np.array(pd.read_excel(self.DATA_PATH + 'test_pb_fm/test_inputs.xlsx'))
    # labels = np.array(pd.read_excel(self.DATA_PATH + 'test_pb_fm/test_labels.xlsx'))
    inputs = np.array(pd.read_excel(self.DATA_PATH + 'sixty/train_inputs.xlsx'))
    labels = np.array(pd.read_excel(self.DATA_PATH + 'sixty/train_labels.xlsx'))
    self.samples = np.concatenate((inputs, labels, labels),axis=1)

    print("Length of samples: ", self.samples.shape)
    self.load_model()
    self.sim_data = []
    self.exp_sim_data = []
    self.inputs = []
    self.labels = []
    self.delayed = []

  def load_model(self):
    p.loadMJCF("./assets/ground.xml")
    objs = p.loadURDF("./assets/test_leg.urdf")
    self.Id = objs
    if self.display_expert:
      # for e in self.num_expert:
      objs = p.loadURDF("./assets/test_leg.urdf")
      self.exp1_Id = objs
    if self.model_breakdown:
      objs = p.loadURDF("./assets/test_leg.urdf")
      self.exp2_Id = objs
    if self.all_legs:
      objs = p.loadURDF("./assets/test_leg.urdf")
      self.exp2_Id = objs
      objs = p.loadURDF("./assets/test_leg.urdf")
      self.exp3_Id = objs
      objs = p.loadURDF("./assets/test_leg.urdf")
      self.exp4_Id = objs
    p.setTimeStep(self.simtimeStep)
    p.setGravity(0,0,-9.8)
    self.feet_dict = {}
    self.feet = ["AR_foot_link","AL_foot_link","BR_foot_link",
                  "BL_foot_link","CR_foot_link","CL_foot_link"]
    numJoints = p.getNumJoints(self.Id)
    self.jdict = {}
    self.ordered_joints = []
    self.ordered_joint_indices = []
    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name in self.feet: self.feet_dict[link_name] = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    if self.display_expert:
      self.exp1_jdict = {}
      for j in range( p.getNumJoints(self.exp1_Id) ):
        info = p.getJointInfo(self.exp1_Id, j)
        link_name = info[12].decode("ascii")
        if info[2] != p.JOINT_REVOLUTE: continue
        jname = info[1].decode("ascii")
        lower, upper = (info[8], info[9])
        self.exp1_jdict[jname] = j
    if self.model_breakdown:
      self.exp2_jdict = {}      
      for j in range( p.getNumJoints(self.exp2_Id) ):
        info = p.getJointInfo(self.exp2_Id, j)
        link_name = info[12].decode("ascii")
        if info[2] != p.JOINT_REVOLUTE: continue
        jname = info[1].decode("ascii")
        lower, upper = (info[8], info[9])
        self.exp2_jdict[jname] = j
    if self.all_legs:
      self.exp2_jdict = {}      
      for j in range( p.getNumJoints(self.exp2_Id) ):
        info = p.getJointInfo(self.exp2_Id, j)
        link_name = info[12].decode("ascii")
        if info[2] != p.JOINT_REVOLUTE: continue
        jname = info[1].decode("ascii")
        lower, upper = (info[8], info[9])
        self.exp2_jdict[jname] = j
      self.exp3_jdict = {}      
      for j in range( p.getNumJoints(self.exp3_Id) ):
        info = p.getJointInfo(self.exp3_Id, j)
        link_name = info[12].decode("ascii")
        if info[2] != p.JOINT_REVOLUTE: continue
        jname = info[1].decode("ascii")
        lower, upper = (info[8], info[9])
        self.exp3_jdict[jname] = j
      self.exp4_jdict = {}      
      for j in range( p.getNumJoints(self.exp4_Id) ):
        info = p.getJointInfo(self.exp4_Id, j)
        link_name = info[12].decode("ascii")
        if info[2] != p.JOINT_REVOLUTE: continue
        jname = info[1].decode("ascii")
        lower, upper = (info[8], info[9])
        self.exp4_jdict[jname] = j

    self.motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]


    self.motors = [self.jdict[n] for n in self.motor_names]
    self.exp1_motors = [self.exp1_jdict[n] for n in self.motor_names]
    if self.model_breakdown:
      self.exp2_motors = [self.exp2_jdict[n] for n in self.motor_names]
    if self.all_legs:
      self.exp2_motors = [self.exp2_jdict[n] for n in self.motor_names]
      self.exp3_motors = [self.exp3_jdict[n] for n in self.motor_names]
      self.exp4_motors = [self.exp4_jdict[n] for n in self.motor_names]

    self.motor_power =  [15, 22, 15]
    self.vel_max = [8,11,8]
    self.tor_max =  [80,112,80]

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}
    
    # Disable motors to use torque control:

    self.get_data = False
    # self.get_data = True
    if self.get_data:
      p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp1_Id, self.exp1_motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    p.setJointMotorControlArray(self.exp2_Id, self.exp2_motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
    if self.all_legs:
      p.setJointMotorControlArray(self.exp2_Id, self.exp2_motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
      p.setJointMotorControlArray(self.exp3_Id, self.exp3_motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))
      p.setJointMotorControlArray(self.exp4_Id, self.exp4_motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

    # Increase the friction on the feet.
    for key in self.feet_dict:
      p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9, rollingFriction=0.1)

  def seed(self, seed=None):
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def close(self):
    print("closing")
  
  def reset(self):
    # if self.act_model is not None:
    #   self.input_torques = deque([[0]*self.act_model.input_size]*self.act_model.rnn_length,maxlen=self.act_model.rnn_length)
    #   self.all_torques  = {n:deque([[0]*self.act_model.input_size]*self.act_model.rnn_length,maxlen=self.act_model.rnn_length) for n in ['input','output','pd','rnn']}
    # if self.sim_model is not None:
    #   self.sim_model_input = deque([[0]*self.sim_model.input_size]*self.sim_model.rnn_length,maxlen=self.sim_model.rnn_length)
    self.desired_torque = [0]*self.ac_size
    self.exp_joints = [0]*self.ac_size
    self.exp_joint_vel = [0]*self.ac_size
    self.input_torque = [0]*self.ac_size
    self.prev_torque = [0]*self.ac_size

    self.reward_breakdown = {'pos':[], 'vel':[]}

    self.episodes += 1
    self.history = deque([0]*self.ac_size*3*10, maxlen=self.ac_size*3*10)
    if self.episodes > 0:
      self.rew_buffer.append(self.total_reward)
    if self.rank == 0:
      print(self.rew_buffer)
    if len(self.rew_buffer) == 5 and np.all(np.array(self.rew_buffer) > -12000):
      if self.cur:
        self.Kp = self.Kp*0.75
        self.Kd = self.Kd*0.75
        if self.Kp < 10:
          self.Kp = self.Kd = 0
          self.cur = False
      # else:
      #   if np.all(np.array(self.rew_buffer) > -600):
      #     self.sample_delay = min(self.sample_delay + 10, 200)
      self.rew_buffer = deque(maxlen=5)
    # self.sample_delay = 50
    self.total_reward = 0
    self.steps = 0
    # self.sample_pointer = np.random.randint(0,4000)
    # self.sample_pointer = 160000
    # self.sample_pointer = self.initial_pointer = 50000
    # self.sample_pointer = self.initial_pointer = 100000
    self.sample_pointer = self.initial_pointer = 0
    self.get_sample(self.samples[self.sample_pointer])
    self.reset_to_sample(leg_num=0)
    self.reset_to_sample(leg_num=1)
    if self.model_breakdown:
      self.reset_to_sample(leg_num=2)
    if self.all_legs:
      self.reset_to_sample(leg_num=2)
      self.reset_to_sample(leg_num=3)
      self.reset_to_sample(leg_num=4)
    self.get_observation()
    if self.record_step:
      self.save_sim_data()
      self.sim_data = []
      self.exp_sim_data = []
      self.inputs = []
      self.labels = []
      self.pb_inputs = []
      self.pb_labels = []
      self.delayed = []
      self.pos_and_vel = []

    # return np.array(self.desired_torque + self.exp_joints + self.exp_joint_vel+list(self.history))
    # return np.array(self.desired_torque + self.joints + self.joint_vel)
    return np.array(self.desired_torque)

  def step(self, actions):
    # if (self.sample_pointer-self.initial_pointer) % self.sample_delay == 0:
    #   # self.sample_pointer = np.random.randint(0,3000)
    #   self.sample_pointer += 1
    #   # if not self.test and not self.use_expert:
    #     # self.restore_sample(self.samples[self.sample_pointer], expert=False)
    # else:
    # self.sample_pointer += int(1000*self.timeStep)
    self.sample_pointer += 1
    print(self.sample_pointer)
    self.prev_exp_state = self.exp_joints + self.exp_joint_vel
    self.get_sample(self.samples[self.sample_pointer])

    # self.history.extend(self.desired_torque + self.exp_joints + self.exp_joint_vel)
    torques = [0.] * len(self.motor_names)
    # if self.cur:
    # self.pd_torque = []
    # for n,m in enumerate(self.motor_names):
    #   name = m.split('_')
    #   config = name[1]
    #   if self.ob_dict[name[0] + '_foot_link_left_ground']:
    #     config += '_fs'
    #   else:
    #     config += '_g'
      # self.pd_torque.append(self.Kp[config]*(self.exp_joints[n] - self.joints[n]) + self.Kd[config]*(self.exp_joint_vel[n] - self.joint_vel[n]))
    # if self.sample_pointer < 75000:
    self.pd_torque = [np.random.normal(0,5),np.random.normal(-2,3),np.random.normal(0,1.25)]
    # print(self.pd_torque)
    
    # self.pd_torque = np.random.normal(0,1, self.ac_size)*10
    # else:  
      # self.pd_torque = self.Kp*(np.array(self.exp_joints) - np.array(self.joints)) + self.Kd*(np.array(self.exp_joint_vel) - np.array(self.joint_vel))
      
    # for m, name in enumerate(self.motor_names):
    #   if self.cur:
    #     torques[m] = actions[m]*self.motor_power[m] + self.pd_torque[m]
    #   else:
    #     torques[m] = actions[m]*self.motor_power[m] 
    #   torques[m] = np.clip(torques[m], -self.tor_max[m], self.tor_max[m])
    # ***************************
    # self.label_torque = self.Kp*(np.array(self.exp_joints) - np.array(self.joints)) + self.Kd*(np.array(self.exp_joint_vel) - np.array(self.joint_vel))    
    # ***************************
    if self.use_expert:
      # torques = self.desired_torque
      # torques = []
      # for n,m in enumerate((self.motor_names)):
      #   if 'coxa' in m:
      #     torques.append(self.desired_torque[n]*10)
      #   elif 'femur' in m:
      #     torques.append(self.desired_torque[n]*1.2)
      #   else:
      #     torques.append(self.desired_torque[n]*1.5)
      # torques = self.Kp*(np.array(self.exp_joints) - np.array(self.joints)) + self.Kd*(np.array(self.exp_joint_vel) - np.array(self.joint_vel))
      self.label = self.Kp*(np.array(self.exp_joints) - np.array(self.joints)) + self.Kd*(np.array(self.exp_joint_vel) - np.array(self.joint_vel))
      # print(torques)
    self.prev_state = self.joints + self.joint_vel
    
    # torques = self.pd_torque
    #   torques = self.label_torque
    # print(self.input_torque)
    # self.input_torques.append(self.input_torque)
    if self.act_model is not None:
      
      # rnn_mean = np.array([-0.07622801, -2.80571822, -0.3558065])
      # rnn_std = np.array([4.06731155, 2.19501669, 0.63668901])
      # self.input_torques.append(self.input_torque)
      if self.model_breakdown:
        jointStates = p.getJointStates(self.exp2_Id,self.ordered_joint_indices)
        joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
        joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
      # self.input_torques.append(self.input_torque + self.exp_joints + self.exp_joint_vel)
      # print(self.joint_vel, joint_vel)
      # self.input_torque = [20,-20,20]
      # print(self.joints, joints)
      # print(self.joint_vel, joint_vel)
      
      # input_mean = np.array([ 1.97126336e-03, -1.23687695e-01,  1.69440854e+00, -7.45739415e-04,
      # 1.39266610e-04, -1.00977116e-04, -6.45365770e-02, -2.79975955e+00,
      # -3.70322215e-01])
      # label_mean = np.array([-6.15864993e-07,  1.12945690e-07, -1.49176520e-07, -1.30020945e-05,
      # 1.96499778e-06, -5.78451391e-06])
      # input_std = np.array([0.3340376,  0.13628615, 0.15371701, 2.82549676, 1.93697143, 2.5725009,
      # 3.87782059, 2.14239459, 0.61925939])
      # label_std = np.array([0.00388251, 0.0026709,  0.00356653, 0.05931397, 0.05691614, 0.09089013])
      
      # input mean:
      # [ 2.11000012e-03 -1.23517703e-01  1.69018638e+00 -1.60546026e-03
      # 1.11078712e-04 -7.09673307e-04 -7.20106148e-02 -2.80358587e+00
      # -3.59110503e-01]
      # label mean:
      # [-2.20775283e-06 -2.09489040e-21 -7.94167111e-07 -2.62319987e-06
      # -1.04405924e-06 -4.57488516e-06]
      # stds
      # input std:
      # [0.34448658 0.13729068 0.15727606 2.92362317 1.96778483 2.65309085
      # 4.02805686 2.17855077 0.63311685]
      # label std:
      # [0.00402147 0.0027128  0.00366952 0.06134562 0.05807932 0.09321981]


      # input_mean = np.array([ 2.11000012e-03, -1.23517703e-01,  1.69018638e+00, -1.60546026e-03, 1.11078712e-04, -7.09673307e-04, -7.20106148e-02, -2.80358587e+00,-3.59110503e-01])
      # label_mean = np.array([-2.20775283e-06, -2.09489040e-21, -7.94167111e-07, -2.62319987e-06,-1.04405924e-06, -4.57488516e-06])
      # input_std = np.array([0.34448658, 0.13729068, 0.15727606, 2.92362317, 1.96778483, 2.65309085, 4.02805686, 2.17855077, 0.63311685])
      # label_std = np.array([0.00402147, 0.0027128,  0.00366952, 0.06134562, 0.05807932, 0.09321981])


      # input_mean = np.array([ 1.97126336e-03,-1.23687695e-01,1.69440854e+00,-6.45365770e-02,-2.79975955e+00,-3.70322215e-01])
      # label_mean = np.array([-6.15864993e-07,1.12945690e-07,-1.49176520e-07])
      # input_std = np.array([0.3340376,0.13628615,0.15371701,3.87782059,2.14239459,0.61925939])
      # label_std = np.array([0.00388251,0.0026709,0.00356653])

      input_mean = np.array([ 9.52937974e-03,-4.94308597e-01,1.74239498e+00,7.14986760e-04,-4.75616066e-03,3.74759640e-03,1.66596370e-03,-2.49716428e+00,8.13625268e-03])
      label_mean = np.array([ 9.53045912e-03,-4.94314867e-01,1.74239682e+00,7.25437929e-04,-4.75497788e-03,3.74587516e-03])
      input_std = np.array([0.60860031,0.62154495,0.60837825,1.37276672,1.4369087,4.5993707,5.01312888,2.99941889,3.0007814 ])
      label_std = np.array([0.60860055,0.62154245,0.60837909,1.37277664,1.4369088,4.59937079])


      input_mean = np.array([-5.63630997e-02,-5.65476202e-01,1.92856845e+00,3.31668130e-03,-1.92561544e-03,6.24937002e-03,-1.99075413e-02,-2.00690907e+00,3.47929959e-03])
      input_std = np.array([0.56475651,0.50232694,0.54082144,1.69021032,1.30510487,3.9651094,5.00281954,2.99900184,1.25186568])
      label_mean = np.array([ 4.13950331e-06,-2.47471533e-06,7.76663292e-06,1.24652056e-05,-3.07650113e-05,-6.82392144e-06])
      label_std = np.array([0.00211274,0.00163136,0.00495636,0.15486125,0.14707174,0.45192963])


      # self.args.norm = True
      # self.args.norm = False
      # self.just_pos = False
      # # self.input_torque = [75.0, -75.0, 75.0]
      # if self.args.norm:
      #   if self.just_pos:
      #     inputs = (np.array(self.joints + self.input_torque) - input_mean)/input_std
      #   else:
      #     inputs = (np.array(self.joints + self.joint_vel + self.input_torque) - input_mean)/input_std
      # else:
      #   if self.just_pos:
      #     inputs = np.array(self.joints + self.input_torque)
      #   else:
      #     inputs = np.array(self.joints + self.joint_vel + self.input_torque)
      # self.input_torques.append(inputs.reshape(self.act_model.input_size,))
      # act_model_pos = self.act_model.step(np.array(self.input_torques).reshape(-1,self.act_model.rnn_length,self.act_model.input_size))[0][-1] 
      
      # input_data = np.array([self.joints+self.joint_vel+self.input_torque])
      # self.input_torque = [-75,0,0]
      # input_data = np.array([self.prev_state +self.input_torque])
      input_data = np.array([self.joints + self.joint_vel +self.input_torque])
      # input_data =  np.clip((input_data - input_mean)/input_std, -5, 5)
      act_model_pos = self.act_model.predict(input_data)[0]
      # act_model_pos = (act_model_pos * label_std) + label_mean
      # act_model_pos += np.array(self.prev_state)
      # act_model_pos = [-2,2,2]
      print("tor:", self.input_torque)
      print("act:", act_model_pos[:3])
      print("obs:", self.joints)
      print("exp:", self.exp_joints)
      
      # print(act_model_pos)
      # [-0.07622801 -2.80571822 -0.3558065 ] [4.06731155 2.19501669 0.63668901]
      # self.all_torques['input'].append(self.input_torque)
      # self.all_torques['output'].append(self.label_torque)
      # self.all_torques['pd'].append(self.pd_torque)
      # self.all_torques['rnn'].append(self.new_pd_torque)
      # if self.steps % 100 == 0:
      #   x1,x2,x3 = np.array(self.all_torques['input'])[:,0],np.array(self.all_torques['input'])[:,1],np.array(self.all_torques['input'])[:,2]
      #   y1,y2,y3 = np.array(self.all_torques['output'])[:,0],np.array(self.all_torques['output'])[:,1],np.array(self.all_torques['output'])[:,2]
      #   z1,z2,z3 = np.array(self.all_torques['pd'])[:,0],np.array(self.all_torques['pd'])[:,1],np.array(self.all_torques['pd'])[:,2]
      #   h1,h2,h3 = np.array(self.all_torques['rnn'])[:,0],np.array(self.all_torques['rnn'])[:,1],np.array(self.all_torques['rnn'])[:,2]
      #   # subplot([[x1,y1,z1,h1],[x2,y2,z2,h2],[x3,y3,z3,h3]], PATH=self.PATH, legend=[['input','output','pd','rnn']]*3,title=['coxa','femur','tibia'], x_initial=0)
      #   subplot([[x1,y1],[x2,y2],[x3,y3]], PATH=self.PATH, legend=[['input','output']]*3,title=['coxa','femur','tibia'], x_initial=0)

      # self.pd_torque = self.pd_torque*rnn_std + rnn_mean
      # print("real", self.input_torque)
      # print("rnn output", self.new_pd_torque)
      # print("label", self.pd_torque)
    # print(self.pd_torque)
    if self.sim_model is not None:
      # print(np.array(self.sim_model_input).shape)
      if self.act_model is not None and not self.model_breakdown:
        # print(act_model_pos)
        self.sim_model_input.append(self.prev_state + list(act_model_pos))
        # self.sim_model_input.append(self.prev_state + [1,-2,-1,0,0,0])
        # print(self.sim_model_input)
      else:
        print(self.steps)

        # if self.steps > 2000 and self.steps < 5000:
        #   self.sim_model_input.append(self.prev_state + [0,0,0,0,0,0])
        # else:
        # self.sim_model_input.append(self.prev_state + self.exp_joints + self.exp_joint_vel)
        # self.sim_model_input.append(self.prev_state + [0,0,0,0,0,0])
        self.sim_model_input.append(self.prev_state + list(act_model_pos))

      self.new_pd_torque = self.sim_model.step(np.array(self.sim_model_input).reshape(-1,self.sim_model.rnn_length,self.sim_model.input_size))[0][-1]

      # print(self.new_pd_torque)
    for _ in range(self.actionRepeat):
      
      if self.sim_model is not None:
        p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.new_pd_torque) 
      else:
        if self.get_data:
          p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.pd_torque) 
        else:
          p.setJointMotorControlArray(self.exp2_Id, self.exp2_motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque) 

        # p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.pd_torque+20*np.random.normal(0,1)) 
      if self.model_breakdown:
        if self.get_data:
          p.setJointMotorControlArray(self.exp2_Id, self.exp2_motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque) 
        else:
          # p.setJointMotorControlArray(self.exp2_Id, self.exp2_motors,controlMode=p.POSITION_CONTROL, targetPositions=act_model_pos[:3])  
          # self.set_position([0,1,1.5], [0,0,0,1], joints=act_model_pos[:3], robot_id=self.Id)
          p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=act_model_pos[:3])  
      if self.all_legs: 
        # p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=self.pd_torque) 
        p.setJointMotorControlArray(self.exp2_Id, self.exp2_motors,controlMode=p.TORQUE_CONTROL, forces=self.input_torque)  
        p.setJointMotorControlArray(self.exp3_Id, self.exp3_motors,controlMode=p.TORQUE_CONTROL, forces=self.label_torque) 
        p.setJointMotorControlArray(self.exp4_Id, self.exp4_motors,controlMode=p.TORQUE_CONTROL, forces=self.new_pd_torque)  
      p.stepSimulation()
    if self.render:
      time.sleep(0.002)
    self.get_observation()

    reward, done = self.get_reward()
    self.total_reward += reward
    if self.record_step: 
      self.record_sim_data()
    self.steps += 1
    if (self.sample_pointer-self.initial_pointer) % self.sample_delay == 0:
      self.reset_to_sample(leg_num=1)
      # pass
    self.reset_to_sample(leg_num=0)
    # self.reset_to_sample(leg_num=2)
    # self.reset_to_sample(leg_num=3)
    # return np.array(self.desired_torque + self.joints + self.joint_vel), reward, done, None
    return np.array(self.desired_torque), reward, done, None

  def get_reward(self):
    reward = 0
    reward -= 20*np.sum(np.array(self.exp_joints) - np.array(self.joints))**2
    reward -= np.clip(0.001*np.sum(np.array(self.exp_joint_vel) - np.array(self.joint_vel))**2, 0, 0.25)
    self.reward_breakdown['pos'].append(20*np.sum(np.array(self.exp_joints) - np.array(self.joints))**2)
    self.reward_breakdown['vel'].append(np.clip(0.001*np.sum(np.array(self.exp_joint_vel) - np.array(self.joint_vel))**2, 0, 0.25))
    # print(np.mean(self.reward_breakdown['pos']))
    done = False
    # if self.steps > self.horizon:
    # if self.steps > 200:
    # if self.sample_pointer == 500000:
    # if self.sample_pointer == 100000:
    if self.sample_pointer == 30000:
    # if self.sample_pointer == 90000:
    # if self.sample_pointer == 25000:
      done = True
    return reward, done
 
  def get_observation(self):
    self.ob_dict = {}
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    # self.ob_dict = {self.state[i]:jointStates[j[0]][0] for i,j in enumerate(self.ordered_joints[:int(18)])}
    # self.ob_dict.update({self.state[i+18]:jointStates[j[0]][1] for i,j in enumerate(self.ordered_joints[:int(18)])})
    self.joints = [jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]
    self.joint_vel = [jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]
    for n in self.feet_dict:
      self.ob_dict[n + '_left_ground']= not len(p.getContactPoints(self.Id, -1, self.feet_dict[n], -1))>0
    self.contacts = [self.ob_dict[m + '_left_ground'] for m in self.feet_dict]

  def get_sample(self, sample):
    gz_motor_names = ['BR_coxa_joint','BR_femur_joint','BR_tibia_joint']
    state = [m + '_pos' for m in gz_motor_names]
    state += [m + '_vel' for m in gz_motor_names]
    state += [m + '_targ' for m in gz_motor_names]
    state += [m + '_torq' for m in gz_motor_names]
    self.sample_dict = {name: val for name, val in zip(state, sample)}
    # Add noise to position and vel to fight distributional shift..
    # self.exp_joints =  [self.sample_dict[j + '_pos']+np.random.normal(0.0,0.01) for j in self.motor_names]
    # self.exp_joint_vel = [self.sample_dict[j + '_vel']+np.random.normal(0.0,0.05) for j in self.motor_names]
    
    self.exp_joints =  [self.sample_dict[j + '_pos'] for j in self.motor_names]
    self.exp_joint_vel = [self.sample_dict[j + '_vel'] for j in self.motor_names]
    self.input_torque = [self.sample_dict[j + '_targ'] for j in self.motor_names]
    self.label_torque = [self.sample_dict[j + '_torq'] for j in self.motor_names]

  def reset_to_sample(self,leg_num=0):
    if leg_num == 4:
      y_offset = 4
    elif leg_num == 3:
      y_offset = 3
    elif leg_num == 2:
      y_offset = 2
    elif leg_num == 1:
      y_offset = 1
    else:
      y_offset = 0
    # joints = [self.sample_dict[j + '_pos'] for j in self.motor_names]
    # joint_vel = [self.sample_dict[j + '_vel'] for j in self.motor_names]
    pos = [0, y_offset, 1.5]
    orn = [0,0,0,1]
    # if expert:
    if leg_num == 4:
      self.set_position(pos, orn, joints=self.exp_joints, joint_vel=self.exp_joint_vel , robot_id=self.exp4_Id)  
    elif leg_num == 3:
      self.set_position(pos, orn, joints=self.exp_joints, joint_vel=self.exp_joint_vel , robot_id=self.exp3_Id)  
    elif leg_num == 2:
      self.set_position(pos, orn, joints=self.exp_joints, joint_vel=self.exp_joint_vel , robot_id=self.exp2_Id)   
    elif leg_num == 1:
      self.set_position(pos, orn, joints=self.exp_joints, joint_vel=self.exp_joint_vel , robot_id=self.Id)   
    else:
      self.set_position(pos, orn, joints=self.exp_joints, joint_vel=self.exp_joint_vel , robot_id=self.exp1_Id)   

  def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))
        np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))

        df = pd.DataFrame (np.array(self.inputs))  
        df.to_excel(path + '/inputs.xlsx', index=False)
        
        df = pd.DataFrame (np.array(self.labels))  
        df.to_excel(path + '/labels.xlsx', index=False)
        
        df = pd.DataFrame (np.array(self.pb_inputs))  
        df.to_excel(path + '/pb_inputs.xlsx', index=False)
        
        df = pd.DataFrame (np.array(self.pb_labels))  
        df.to_excel(path + '/pb_labels.xlsx', index=False)
        
        df = pd.DataFrame (np.array(self.delayed))  
        df.to_excel(path + '/delayed.xlsx', index=False)
        
        df = pd.DataFrame (np.array(self.pos_and_vel))  
        df.to_excel(path + '/pos_and_vel.xlsx', index=False)
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    # print(type(self.input_torque), type(self.contacts), type(list(self.label_torque)))
    # self.reza_data.append(self.input_torque + self.contacts + list(self.label_torque))
    self.inputs.append(self.prev_state + self.pd_torque)
    self.labels.append(self.joints + self.joint_vel)
    # self.inputs.append(self.input_torque + self.prev_exp_state)
    # self.labels.append(self.exp_joints+self.exp_joint_vel)
    self.delayed.append(self.label_torque)
    self.pos_and_vel.append(self.exp_joints + self.exp_joint_vel + self.joints + self.joint_vel)    
    
    self.pb_inputs.append(self.prev_state + self.joints + self.joint_vel)
    self.pb_labels.append(self.pd_torque)

    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)

    # if len(self.exp_sim_data) > 100000: return
    # pos, orn = p.getBasePositionAndOrientation(self.exp_Id)
    # exp_data = [pos, orn]
    # joints = p.getJointStates(self.exp_Id, self.motors)
    # exp_data.append([i[0] for i in joints])
    # self.exp_sim_data.append(exp_data)
