#ifndef COLLECT_DATA_NODE_H_
#define COLLECT_DATA_NODE_H_

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <std_srvs/SetBool.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/JointState.h>
#include <gazebo_msgs/ModelStates.h>
#include <leg_control/joint_state.h>
#include <fstream>

class CollectDataNode
{
public:
  CollectDataNode();
  void run(void);

  std::map<std::string, ros::Subscriber> gazebo_joint_subscribers_;
  std::map<std::string, std_msgs::Float64> current_gazebo_joint_desired_;
  std::map<std::string, std_msgs::Float64> gazebo_joint_desired_;
  std::vector<std::string> joint_ids = {"AL_coxa_joint","AL_femur_joint","AL_tibia_joint",
                                        "AR_coxa_joint","AR_femur_joint","AR_tibia_joint",
                                        "BL_coxa_joint","BL_femur_joint","BL_tibia_joint",
                                        "BR_coxa_joint","BR_femur_joint","BR_tibia_joint",
                                        "CL_coxa_joint","CL_femur_joint","CL_tibia_joint",
                                        "CR_coxa_joint","CR_femur_joint","CR_tibia_joint"};

private:
  void initialiseStuff(void);
  void saveSamples(void);
  void publishActions(void);
  void getObservation(void);
  void jointStateCallback(const sensor_msgs::JointState::ConstPtr &msg);
  void modelStateCallback(const gazebo_msgs::ModelStates::ConstPtr &msg);
  sensor_msgs::JointState input_joint_states_;
  geometry_msgs::Pose model_pose_;
  geometry_msgs::Twist model_twist_;
  ros::ServiceClient manual_override_service_client_; 
  ros::Publisher manual_velocity_publisher_;           ///< The ROS publisher for sending manual velocity command data.
  std::map<std::string, ros::Publisher> gazebo_joint_publishers_;
  ros::Subscriber joint_state_subscriber_;
  ros::Subscriber model_state_subscriber_;

  ros::Subscriber AR_coxa_subscriber_;
  ros::Subscriber AR_femur_subscriber_;
  ros::Subscriber AR_tibia_subscriber_;
  ros::Subscriber AL_coxa_subscriber_;
  ros::Subscriber AL_femur_subscriber_;
  ros::Subscriber AL_tibia_subscriber_;
  ros::Subscriber BR_coxa_subscriber_;
  ros::Subscriber BR_femur_subscriber_;
  ros::Subscriber BR_tibia_subscriber_;
  ros::Subscriber BL_coxa_subscriber_;
  ros::Subscriber BL_femur_subscriber_;
  ros::Subscriber BL_tibia_subscriber_;
  ros::Subscriber CR_coxa_subscriber_;
  ros::Subscriber CR_femur_subscriber_;
  ros::Subscriber CR_tibia_subscriber_;
  ros::Subscriber CL_coxa_subscriber_;
  ros::Subscriber CL_femur_subscriber_;
  ros::Subscriber CL_tibia_subscriber_;
  inline void AR_coxa_Callback(std_msgs::Float64 msg){current_gazebo_joint_desired_["AR_coxa_joint"]  = msg;}
  inline void AR_femur_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["AR_femur_joint"] = msg;}
  inline void AR_tibia_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["AR_tibia_joint"] = msg;}
  inline void AL_coxa_Callback(std_msgs::Float64 msg){  current_gazebo_joint_desired_["AL_coxa_joint"]  = msg;}
  inline void AL_femur_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["AL_femur_joint"] = msg;}
  inline void AL_tibia_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["AL_tibia_joint"] = msg;}
  inline void BR_coxa_Callback(std_msgs::Float64 msg){  current_gazebo_joint_desired_["BR_coxa_joint"]  = msg;}
  inline void BR_femur_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["BR_femur_joint"] = msg;}
  inline void BR_tibia_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["BR_tibia_joint"] = msg;}
  inline void BL_coxa_Callback(std_msgs::Float64 msg){  current_gazebo_joint_desired_["BL_coxa_joint"]  = msg;}
  inline void BL_femur_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["BL_femur_joint"] = msg;}
  inline void BL_tibia_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["BL_tibia_joint"] = msg;}
  inline void CR_coxa_Callback(std_msgs::Float64 msg){  current_gazebo_joint_desired_["CR_coxa_joint"]  = msg;}
  inline void CR_femur_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["CR_femur_joint"] = msg;}
  inline void CR_tibia_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["CR_tibia_joint"] = msg;}
  inline void CL_coxa_Callback(std_msgs::Float64 msg){  current_gazebo_joint_desired_["CL_coxa_joint"]  = msg;}
  inline void CL_femur_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["CL_femur_joint"] = msg;}
  inline void CL_tibia_Callback(std_msgs::Float64 msg){ current_gazebo_joint_desired_["CL_tibia_joint"] = msg;}

  std::vector<std::vector<double>> samples;
  int steps = 0;
};

#endif /* COLLECT_DATA_NODE */
