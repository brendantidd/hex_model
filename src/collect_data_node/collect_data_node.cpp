/*
Subscribe to joint commands
Publish at fixed interval
Record desired torque, and resulting joint state + body position
*/

#include "collect_data_node.h"
#include <time.h>

CollectDataNode::CollectDataNode(void)
{
  // Initialise subscribers
  // Initialise publishers
  initialiseStuff();
}

void CollectDataNode::initialiseStuff(void)
{   
  ros::NodeHandle nh;
  ROS_INFO("Initialising stuff!!");
 
  joint_state_subscriber_ = nh.subscribe<sensor_msgs::JointState>("/r1/joint_states", 1,
    &CollectDataNode::jointStateCallback, this, ros::TransportHints().tcpNoDelay());
  model_state_subscriber_ = nh.subscribe<gazebo_msgs::ModelStates>("/gazebo/model_states", 1,
    &CollectDataNode::modelStateCallback, this, ros::TransportHints().tcpNoDelay());

  // ROS_INFO("joint_ids size %d", joint_ids.size());

  for(int i = 0; i < joint_ids.size(); i++)         
  {
    // ROS_INFO("Joint: %s",joint_ids[i]);
    std_msgs::Float64 data;
    data.data = 0.0;
    gazebo_joint_desired_[joint_ids[i]] = data;
    ros::Publisher gazebo_joint_publisher = nh.advertise<std_msgs::Float64>("/r1/" + joint_ids[i] + "/command", 1);
    gazebo_joint_publishers_[joint_ids[i]] = gazebo_joint_publisher;   
  }
  
  AR_coxa_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/AR_coxa_joint/command", 1,
  &CollectDataNode::AR_coxa_Callback, this, ros::TransportHints().tcpNoDelay());
  AR_femur_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/AR_femur_joint/command", 1,
  &CollectDataNode::AR_femur_Callback, this, ros::TransportHints().tcpNoDelay());
  AR_tibia_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/AR_tibia_joint/command", 1,
  &CollectDataNode::AR_tibia_Callback, this, ros::TransportHints().tcpNoDelay());
  AL_coxa_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/AL_coxa_joint/command", 1,
  &CollectDataNode::AL_coxa_Callback, this, ros::TransportHints().tcpNoDelay());
  AL_femur_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/AL_femur_joint/command", 1,
  &CollectDataNode::AL_femur_Callback, this, ros::TransportHints().tcpNoDelay());
  AL_tibia_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/AL_tibia_joint/command", 1,
  &CollectDataNode::AL_tibia_Callback, this, ros::TransportHints().tcpNoDelay());
  BR_coxa_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/BR_coxa_joint/command", 1,
  &CollectDataNode::BR_coxa_Callback, this, ros::TransportHints().tcpNoDelay());
  BR_femur_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/BR_femur_joint/command", 1,
  &CollectDataNode::BR_femur_Callback, this, ros::TransportHints().tcpNoDelay());
  BR_tibia_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/BR_tibia_joint/command", 1,
  &CollectDataNode::BR_tibia_Callback, this, ros::TransportHints().tcpNoDelay());
  BL_coxa_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/BL_coxa_joint/command", 1,
  &CollectDataNode::BL_coxa_Callback, this, ros::TransportHints().tcpNoDelay());
  BL_femur_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/BL_femur_joint/command", 1,
  &CollectDataNode::BL_femur_Callback, this, ros::TransportHints().tcpNoDelay());
  BL_tibia_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/BL_tibia_joint/command", 1,
  &CollectDataNode::BL_tibia_Callback, this, ros::TransportHints().tcpNoDelay());
  CR_coxa_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/CR_coxa_joint/command", 1,
  &CollectDataNode::CR_coxa_Callback, this, ros::TransportHints().tcpNoDelay());
  CR_femur_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/CR_femur_joint/command", 1,
  &CollectDataNode::CR_femur_Callback, this, ros::TransportHints().tcpNoDelay());
  CR_tibia_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/CR_tibia_joint/command", 1,
  &CollectDataNode::CR_tibia_Callback, this, ros::TransportHints().tcpNoDelay());
  CL_coxa_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/CL_coxa_joint/command", 1,
  &CollectDataNode::CL_coxa_Callback, this, ros::TransportHints().tcpNoDelay());
  CL_femur_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/CL_femur_joint/command", 1,
  &CollectDataNode::CL_femur_Callback, this, ros::TransportHints().tcpNoDelay());
  CL_tibia_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1_exp/CL_tibia_joint/command", 1,
  &CollectDataNode::CL_tibia_Callback, this, ros::TransportHints().tcpNoDelay());

  manual_override_service_client_ = nh.serviceClient<std_srvs::SetBool>("/r1/locomotion_engine/command/manual_velocity_override");
  manual_velocity_publisher_ = nh.advertise<geometry_msgs::TwistStamped>("/r1/locomotion_engine/manual_velocity_command", 1);

  sleep(3);

  // Set to manual override
  std_srvs::SetBool service;
  service.request.data = true;
  manual_override_service_client_.call(service);

  for (auto& joint_id : joint_ids)
  {
    input_joint_states_.name.push_back(joint_id);
    input_joint_states_.position.push_back(0);
    input_joint_states_.velocity.push_back(0);
    input_joint_states_.effort.push_back(0);
  }

  sleep(3);

  // self.change_params_service = rospy.ServiceProxy('/' + self.robot_name + '/locomotion_engine_node/set_parameters', Reconfigure())
  // self.params = Config()
  // self.param_list = ['swing_height', 'swing_width', 'swing_depth', 'liftoff_clearance', 'touchdown_clearance']
  // self.maxes = {'swing_height':[0.000, 0.500], 'swing_width':[-.200, 0.200],'swing_depth':[-0.200, 0.200],'liftoff_clearance':[0.000, 0.5000],'touchdown_clearance':[0.000, 0.500]}

  // def publish_actions(self, actions):

  //   self.params.doubles = [DoubleParameter() for _ in range(self.ac_size)]
  //   for i, (key, act) in enumerate(zip(self.param_list, actions)):
  //     self.params.doubles[i].name = key
  //     self.params.doubles[i].value = np.clip(act, self.maxes[key][0], self.maxes[key][1])
  //   self.change_params_service(self.params)
    
}

void CollectDataNode::publishActions(void)
{
  for(int i = 0; i < joint_ids.size(); i++)         
  {
    std_msgs::Float64 data;
    data = gazebo_joint_desired_[joint_ids[i]];  
    gazebo_joint_publishers_[joint_ids[i]].publish(data);
  }
}

void CollectDataNode::getObservation(void)
{

  std::vector<double> sample;
  for(int i = 0; i < joint_ids.size(); i++)
  {
    sample.push_back(input_joint_states_.position[i]);
  }
  for(int i = 0; i < joint_ids.size(); i++)
  {
    sample.push_back(input_joint_states_.velocity[i]);
  }
  for(int i = 0; i < joint_ids.size(); i++)
  {
    sample.push_back(input_joint_states_.effort[i]);
  }

  sample.push_back(model_pose_.position.x);
  sample.push_back(model_pose_.position.y);
  sample.push_back(model_pose_.position.z);
  sample.push_back(model_pose_.orientation.x);
  sample.push_back(model_pose_.orientation.y);
  sample.push_back(model_pose_.orientation.z);
  sample.push_back(model_pose_.orientation.w);
  sample.push_back(model_twist_.linear.x);
  sample.push_back(model_twist_.linear.y);
  sample.push_back(model_twist_.linear.z);
  sample.push_back(model_twist_.angular.x);
  sample.push_back(model_twist_.angular.y);
  sample.push_back(model_twist_.angular.z);

  for(int i = 0; i < joint_ids.size(); i++)
  {
    sample.push_back((double) gazebo_joint_desired_[joint_ids[i]].data);
    // ROS_INFO("joint_desired %f",(double) gazebo_joint_desired_[joint_ids[i]].data );
  }
  samples.push_back(sample); 
  
}

void CollectDataNode::modelStateCallback(const gazebo_msgs::ModelStates::ConstPtr &msg)
{  //convert joint state information to leg_control format and input to controller
  // if msg->name == 'r1':
  int idx = 0;
  int count = 0;
  const char * robot_name = "_r1";
  for (auto const &v : msg->name)
    { 
      // ROS_INFO("robot name: %s", v.c_str());
      if (strcmp(v.c_str(),robot_name))
      {
        // ROS_INFO("robot name : %s", v.c_str());
        idx = count;
      }
      count++;
    }
  // ROS_INFO("index of _r1 %d", idx);
  model_pose_ = msg->pose[1];
  model_twist_ = msg->twist[1];
}

void CollectDataNode::jointStateCallback(const sensor_msgs::JointState::ConstPtr &msg)
{ 
  input_joint_states_.name = msg->name;
  input_joint_states_.position = msg->position;
  input_joint_states_.velocity = msg->velocity;
  input_joint_states_.effort = msg->effort;
}

void CollectDataNode::saveSamples(void)
{

  // printf("%d \n", samples.size());
  std::ofstream f("/home/brendan/data/full_hex_samples.csv");
  for (auto const &sample : samples)
  {
    // printf("%d \n", sample.size());
    for(auto const& v : sample)
    {
      f << v << ',';
    }
    f << '\n';
  }
}

void CollectDataNode::run(void)
{
  double timestep = 0.00125;
  while (ros::ok())
  { 
    // if (steps > 400000)
    if (steps > 40000)
    {
      saveSamples();
      exit(EXIT_SUCCESS);
    }
    geometry_msgs::Twist msg;
    msg.linear.x = 0.5;
    msg.linear.y = 0.0;
    msg.angular.z = 0.0;
    geometry_msgs::TwistStamped stamped_msg;
    stamped_msg.header.stamp = ros::Time::now();
    stamped_msg.twist = msg;
    manual_velocity_publisher_.publish(stamped_msg);
    // Retrieve current desired joints. We do this so that getObservation has the actioned torques
    gazebo_joint_desired_ = current_gazebo_joint_desired_;
    publishActions();
    clock_t start = clock();
    while((( (double)clock() - start ) / CLOCKS_PER_SEC ) < timestep)
    {
      ros::spinOnce();
    }
    // ROS_INFO ( "Time: %f, desired: %f", ( (double)clock() - start ) / CLOCKS_PER_SEC, timestep );
    getObservation();
    steps++;
  }
}

int main (int argc, char** argv)
{
  ros::init(argc, argv, "collect_data_node");
  ros::NodeHandle nh;
  CollectDataNode collect_data_node;
  collect_data_node.run();
  return 0;

}