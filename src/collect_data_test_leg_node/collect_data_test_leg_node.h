#ifndef COLLECT_DATA_NODE_H_
#define COLLECT_DATA_NODE_H_

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <std_srvs/SetBool.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/JointState.h>
#include <gazebo_msgs/ModelStates.h>
#include <leg_control/joint_state.h>
#include <fstream>
#include <chrono>
#include <random>
#include <math.h>

class CollectDataNode
{
public:
  CollectDataNode();
  void run(void);
  std::map<std::string, std_msgs::Float64> current_gazebo_joint_desired_;
  std::map<std::string, std_msgs::Float64> gazebo_joint_desired_;
  std::map<std::string, std_msgs::Float64> gazebo_joint_desired_pos_;
  std::map<std::string, std_msgs::Float64> prev_gazebo_joint_desired_;
  std::vector<std::string> joint_ids = {"BR_coxa_joint","BR_femur_joint","BR_tibia_joint"};

private:
  void initialiseStuff(void);
  void saveSamples(void);
  void publishActions(void);
  void getObservation(void);
  void getDesJointPos(void);
  void jointStateCallback(const sensor_msgs::JointState::ConstPtr &msg);
  sensor_msgs::JointState input_joint_states_;
  std::map<std::string, ros::Publisher> gazebo_joint_publishers_;
  ros::Subscriber joint_state_subscriber_;
  ros::Subscriber model_state_subscriber_;
  ros::ServiceClient pauseGazebo;
  ros::ServiceClient unpauseGazebo;


  std::vector<double> pos_and_vel;
  std::vector<std::vector<double>> inputs;
  std::vector<std::vector<double>> labels;
  int steps = 0;
  std_srvs::Empty pauseSrv;
  std_srvs::Empty unpauseSrv;
  double coxa_speed;
  double femur_speed;
  double tibia_speed;
  double coxa_scale_;
  double femur_scale_;
  double tibia_scale_;
  double coxa_x = 0;
  double femur_x = 0;
  double tibia_x = 0;
};

#endif /* COLLECT_DATA_NODE */
