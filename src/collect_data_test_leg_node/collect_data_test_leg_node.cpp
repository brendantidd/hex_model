/*
Subscribe to joint commands
Publish at fixed interval
Record desired torque, and resulting joint state + body position
*/

#include "collect_data_test_leg_node.h"
#include <time.h>

CollectDataNode::CollectDataNode(void)
{
  // Initialise subscribers
  // Initialise publishers
  initialiseStuff();
}

void CollectDataNode::initialiseStuff(void)
{   
  ros::NodeHandle nh;
  ROS_INFO("Initialising stuff!!");
 
  ros::ServiceClient pauseGazebo = nh.serviceClient<std_srvs::Empty>("/gazebo/pause_physics");
  ros::ServiceClient unpauseGazebo = nh.serviceClient<std_srvs::Empty>("/gazebo/unpause_physics");

  joint_state_subscriber_ = nh.subscribe<sensor_msgs::JointState>("/r1/joint_states", 1,
    &CollectDataNode::jointStateCallback, this, ros::TransportHints().tcpNoDelay());

  for(int i = 0; i < joint_ids.size(); i++)         
  {
    // ROS_INFO("Joint: %s",joint_ids[i]);
    std_msgs::Float64 data;
    data.data = 0.0;
    gazebo_joint_desired_[joint_ids[i]] = data;
    ros::Publisher gazebo_joint_publisher = nh.advertise<std_msgs::Float64>("/r1/" + joint_ids[i] + "/command", 1);
    gazebo_joint_publishers_[joint_ids[i]] = gazebo_joint_publisher;   
  }
  
  for (auto& joint_id : joint_ids)
  {
    input_joint_states_.name.push_back(joint_id);
    input_joint_states_.position.push_back(0);
    input_joint_states_.velocity.push_back(0);
    input_joint_states_.effort.push_back(0);
  }

  for(int i = 0; i< joint_ids.size()*2; i++)
  {    
    pos_and_vel.push_back(0.0);
  }
  
  sleep(10);
    
}

void CollectDataNode::publishActions(void)
{
  for(int i = 0; i < joint_ids.size(); i++)         
  {
    std_msgs::Float64 data;
    data = gazebo_joint_desired_[joint_ids[i]];  
    // ROS_INFO("%f",data.data);
    gazebo_joint_publishers_[joint_ids[i]].publish(data);
  }
}

void CollectDataNode::jointStateCallback(const sensor_msgs::JointState::ConstPtr &msg)
{ 
  input_joint_states_.name = msg->name;
  input_joint_states_.position = msg->position;
  input_joint_states_.velocity = msg->velocity;
  input_joint_states_.effort = msg->effort;
}

void CollectDataNode::getDesJointPos(void)
{
  std_msgs::Float64 coxa_data, femur_data, tibia_data;
  double pi = 3.14159;
  double coxa_max = 0.5, coxa_min = -0.5;
  double femur_max = 0.1, femur_min = -0.9;
  double tibia_max = 2.3, tibia_min = 1.0;
  double coxa_scale = coxa_scale_*(coxa_max - coxa_min)/2;
  double coxa_middle = (coxa_max + coxa_min)/2;
  double P_coxa = 50, P_femur = 80, P_tibia = 10;
  coxa_x += coxa_speed*2*pi/(800);
  coxa_data.data = P_coxa*((coxa_scale*sin(coxa_x) + coxa_middle)  -  input_joint_states_.position[0]);
  double femur_scale = femur_scale_*(femur_max - femur_min)/2;
  double femur_middle = (femur_max + femur_min)/2;
  femur_x += femur_speed*2*pi/(800);
  femur_data.data = P_femur*((femur_scale*sin(femur_x) + femur_middle) -  input_joint_states_.position[1]);
  double tibia_scale = tibia_scale_*(tibia_max - tibia_min)/2;
  double tibia_middle = (tibia_max + tibia_min)/2;
  tibia_x += tibia_speed*2*pi/(800);
  tibia_data.data = P_tibia*((tibia_scale*sin(tibia_x) + tibia_middle) -  input_joint_states_.position[2]);
  gazebo_joint_desired_["BR_coxa_joint"] = coxa_data;
  gazebo_joint_desired_["BR_femur_joint"] = femur_data;
  gazebo_joint_desired_["BR_tibia_joint"] = tibia_data;

}

// void CollectDataNode::loadSamples(void) 
// { 
  
//   // File pointer 
//   fstream fin; 

//   // Open an existing file 
//   fin.open("/home/brendan/data/test_leg_15_11/6310b0ec-2759-4ae5-b5af-6db50b34eed7.csv", ios::in); 

//   // Read the Data from the file 
//   // as String Vector 
//   vector<string> row; 
//   string line, word, temp; 

//   while (fin >> temp) 
//   { 
//     row.clear(); 

//     // read an entire row and 
//     // store it in a string variable 'line' 
//     getline(fin, line); 

//     // used for breaking words 
//     stringstream s(line); 

//     // read every column data of a row and 
//     // store it in a string variable, 'word' 
//     while (getline(s, word, ', ')) 
//     { 

//         // add all the column data 
//         // of a row to a vector 
//         row.push_back(word); 
//     } 

//   } 
//   if (count == 0) 
//       cout << "Record not found\n"; 
// } 

void CollectDataNode::saveSamples(void)
{

  // printf("%d \n", samples.size());
  std::ofstream f("/home/brendan/data/iros_data/gz_fm/inputs.csv");
  for (auto const &sample : inputs)
  {
    // printf("%d \n", sample.size());
    for(auto const& v : sample)
    {
      f << v << ',';
    }
    f << '\n';
  }
  // printf("%d \n", samples.size());
  std::ofstream p("/home/brendan/data/iros_data/gz_fm/labels.csv");
  for (auto const &sample : labels)
  {
    // printf("%d \n", sample.size());
    for(auto const& v : sample)
    {
      p << v << ',';
    }
    p << '\n';
  }
  ROS_INFO ( "*************** Data has been saved *****************");

}

void CollectDataNode::getObservation(void)
{
  // labels.push_back(pos_and_vel); 
  std::vector<double> sample;
  // Start with previous pos and vel and add torque
  sample = pos_and_vel;
  for(int i = 0; i < joint_ids.size(); i++)
  {
    sample.push_back((double) gazebo_joint_desired_[joint_ids[i]].data);
    // ROS_INFO("joint_desired %f",(double) gazebo_joint_desired_[joint_ids[i]].data );
  }
  inputs.push_back(sample); 
  
  // Save joint pos and vel as label
  std::vector<double> next_sample;
  for(int i = 0; i < joint_ids.size(); i++)
  {
    next_sample.push_back(input_joint_states_.position[i]);
  }
  for(int i = 0; i < joint_ids.size(); i++)
  {
    next_sample.push_back(input_joint_states_.velocity[i]);
  }
  pos_and_vel = next_sample;
  labels.push_back(next_sample); 

  // effort from joint_states seems crazy? constantly 75, -75 or 0. Going to save previous commanded torque instead
  // for(int i = 0; i < joint_ids.size(); i++)
  // {
  //   sample.push_back(input_joint_states_.effort[i]);
  // }
  // for(int i = 0; i < joint_ids.size(); i++)
  // {
  //   sample.push_back((double) prev_gazebo_joint_desired_[joint_ids[i]].data);
  // }
 
  // labels.push_back(prev_pos_vel); 
  
}

void CollectDataNode::run(void)
{
  // unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  unsigned int seed = 42;
  std::default_random_engine generator (seed);
  std::normal_distribution<double> coxa_dist (0.0,5.0);
  std::normal_distribution<double> femur_dist (-2.5,3);
  std::normal_distribution<double> tibia_dist (0.0,3);
  // Working with 800Hz
  double timestep = 0.00125;
  while (ros::ok())
  { 
    if (steps > 100000)
    {
      saveSamples();
      exit(EXIT_SUCCESS);
    }
    if (steps % (800*3) == 0)
    {
      std::uniform_real_distribution<double> distribution(0.2,2.0);
      coxa_speed = distribution(generator);
      femur_speed = distribution(generator);
      tibia_speed = distribution(generator);
      std::uniform_real_distribution<double> scale_distribution(0.2,1.0);
      coxa_scale_ = scale_distribution(generator);
      femur_scale_ = scale_distribution(generator);
      tibia_scale_ = scale_distribution(generator);
    }
    // Retrieve current desired joints. We do this so that getObservation has the actioned torques
    // self.pd_torque = [np.random.normal(0,5),np.random.normal(-1.75,3),np.random.normal(0,1.25)]
    // if (steps % 10 == 0)
    // {
    //   ROS_INFO ( "New action %d", ( steps));
      
    prev_gazebo_joint_desired_ = gazebo_joint_desired_;
    getDesJointPos();
    // std_msgs::Float64 coxa_data;
    // coxa_data.data = coxa_dist(generator);
    // gazebo_joint_desired_["BR_coxa_joint"] = coxa_data;
    // std_msgs::Float64 femur_data;
    // femur_data.data = femur_dist(generator);
    // gazebo_joint_desired_["BR_femur_joint"] = femur_data;
    // std_msgs::Float64 tibia_data;
    // tibia_data.data = tibia_dist(generator);
    // gazebo_joint_desired_["BR_tibia_joint"] = tibia_data;
    // }
    // printf( "Coxa: %f ", gazebo_joint_desired_["BR_coxa_joint"].data);
    // printf( "femur: %f ", gazebo_joint_desired_["BR_femur_joint"].data);
    // printf( "Tibia: %f \n", gazebo_joint_desired_["BR_tibia_joint"].data);
    publishActions();
    unpauseGazebo.call(unpauseSrv);
    clock_t start = clock();
    while((( (double)clock() - start ) / CLOCKS_PER_SEC ) < timestep)
    {
      ros::spinOnce();
    }
    pauseGazebo.call(pauseSrv);
    // printf( "Coxa: %f ", gazebo_joint_desired_["BR_coxa_joint"].data);
    // printf( "femur: %f ", gazebo_joint_desired_["BR_femur_joint"].data);
    // printf( "Tibia: %f ", gazebo_joint_desired_["BR_tibia_joint"].data);
    // printf("Made changes");
    printf("Steps: %d ", steps);
    // printf(" Time: %f ", ( steps, ((double)clock() - start ) / CLOCKS_PER_SEC));
    // printf("    desired: %f \n", timestep);
    printf("%f ", coxa_speed);
    printf("%f ", femur_speed);
    printf("%f ", tibia_speed);
    printf("%f ", coxa_scale_);
    printf("%f ", femur_scale_);
    printf("%f \n", tibia_scale_);
    // ROS_INFO ( "Steps: %d, Time: %f, desired: %f", ( steps, ((double)clock() - start ) / CLOCKS_PER_SEC), timestep );
    getObservation();
    steps++;
  }
}

int main (int argc, char** argv)
{
  ros::init(argc, argv, "collect_data_test_leg_node");
  ros::NodeHandle nh;
  CollectDataNode collect_data_node;
  collect_data_node.run();
  return 0;

}