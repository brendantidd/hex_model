/*
Subscribe to joint commands
Publish at fixed interval
Record desired torque, and resulting joint state + body position
*/

#include "real_interface.h"
#include <time.h>

RealInterface::RealInterface(void)
{   
  ros::NodeHandle nh;
  ROS_INFO("Initialising stuff!!");

  coxa_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1/TEST_coxa_joint/command", 1,
    &RealInterface::coxaStateCallback, this, ros::TransportHints().tcpNoDelay());
  femur_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1/TEST_femur_joint/command", 1,
    &RealInterface::femurStateCallback, this, ros::TransportHints().tcpNoDelay());
  tibia_subscriber_ = nh.subscribe<std_msgs::Float64>("/r1/TEST_tibia_joint/command", 1,
    &RealInterface::tibiaStateCallback, this, ros::TransportHints().tcpNoDelay());    
}

// void RealInterface::publishActions(void)
// {
//   for(int i = 0; i < joint_ids.size(); i++)         
//   {
//     std_msgs::Float64 data;
//     data = gazebo_joint_desired_[joint_ids[i]];  
//     // ROS_INFO("%f",data.data);
//     gazebo_joint_publishers_[joint_ids[i]].publish(data);
//   }
// }

void RealInterface::coxaStateCallback(const std_msgs::Float64 msg)
{ 
  coxa = msg.data;
}

void RealInterface::femurStateCallback(const std_msgs::Float64 msg)
{ 
  femur = msg.data;
}
void RealInterface::tibiaStateCallback(const std_msgs::Float64 msg)
{ 
  tibia = msg.data;
}


int main (int argc, char** argv)
{
  ros::init(argc, argv, "real_interface");
  ros::NodeHandle nh;
  RealInterface real_interface;
  return 0;

}