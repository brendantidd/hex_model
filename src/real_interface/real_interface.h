#ifndef REAL_INTERFACE_H_
#define REAL_INTERFACE_H_

#include <ros/ros.h>
#include <std_msgs/Float64.h>
class RealInterface
{
public:
  RealInterface();

private:
  ros::Subscriber coxa_subscriber_;
  ros::Subscriber femur_subscriber_;
  ros::Subscriber tibia_subscriber_;
  void coxaStateCallback(const std_msgs::Float64 msg);
  void femurStateCallback(const std_msgs::Float64 msg);
  void tibiaStateCallback(const std_msgs::Float64 msg);
  
  double coxa;
  double femur;
  double tibia;
  
};

#endif /* REAL_INTERFACE */
