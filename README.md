# README #

This repo is for using supervised learning for parts of model identification for DSH. 

### What is this repository for? ###

* Collecting joint data, at this stage from gazebo and saving to a data folder in your home directory
* Training an actuator model by loading in this data

### How do I get set up? ###

* Build subt_gazebo, subt_msgs, and subt_sensors packages in subt
* Follow installation instructions for dsh_sim (AS DSH)
