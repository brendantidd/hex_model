import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
import numpy as np
import pybullet as p
import time

def write_new_dynamics(i):
  path = "temp.urdf"
  tree = ET.parse(path)
  root = tree.getroot()

  for link in root.findall('link'):
    # print(name)
    name = link.get('name')
    if 'body' not in name and ('coxa' in name or 'femur' in name or 'tibia' in name):
      name = name.split("_")[1]
      if name == 'coxa':
        n = 0
      elif name == 'femur':
        n = 1
      elif name == 'tibia':
        n = 2
      inertial = link.find('inertial')
      mass_ = inertial.find('mass')
      mass = float(mass_.get('value'))
      # print("old_mass", mass)
      new_mass = str(np.random.uniform(mass*(1-0.25),mass*(1+0.25)))

      print("new_mass", new_mass)
      mass_.set('value', new_mass)

  for joint in root.findall('joint'):
    name = joint.get('name')
    if 'body' not in name and ('coxa' in name or 'femur' in name or 'tibia' in name):
      name = joint.get('name').split("_")[1]
      if name == 'coxa':
        n = 0
      elif name == 'femur':
        n = 1
      elif name == 'tibia':
        n = 2
      joint_origin = joint.find('origin')
      x = ''
      for j in [float(s) for s in (joint_origin.get('xyz')).split()]:
        x += str(j + np.random.uniform(-0.005,0.005)) + ' '
      joint_origin.set('xyz', x)

      dynamics = joint.find('dynamics')
      # dynamics.set('friction', str(float(dynamics.get('friction')) + np.random.uniform(0,1.0)))
      # dynamics.set('damping',  str(float(dynamics.get('damping')) + np.random.uniform(0,1.0)))
      dynamics.set('friction', str(10))
      dynamics.set('damping',  str(10))
      # if self.debug:
      # print(name, "joint xyz, friction, damping", joint_origin.get('xyz'), dynamics.get('friction'), dynamics.get('damping'))
      # print(name, "joint xyz", joint_origin.get('xyz'), mass_.get('value'))
  tree.write("temp" + str(i) + ".urdf")

if __name__=="__main__":
  import os
  # physicsClientId = p.connect(p.GUI)
  physicsClientId = p.connect(p.DIRECT)
  # p.resetSimulation()
  torques = np.load("../samples/torque_samples.npy")
  print(torques.shape)


  i = 1
  while True:
    p.resetSimulation()

    # numJoints = p.getNumJoints(self.Id)
    # self.jdict = {}
    # self.ordered_joints = []
    # self.ordered_joint_indices = []
    # self.joint_links = []
    # for j in range( p.getNumJoints(self.Id) ):
    #   info = p.getJointInfo(self.Id, j)
    #   exp_info = p.getJointInfo(self.exp_Id, j)
    #   link_name = info[12].decode("ascii")
    #   exp_link_name = info[12].decode("ascii")
    #   if link_name == 'BR_foot_link': self.foot_link = j
    #   self.ordered_joint_indices.append(j)
    #   if info[2] != p.JOINT_REVOLUTE: continue
    #   self.joint_links.append(j)
    #   jname = info[1].decode("ascii")
    #   lower, upper = (info[8], info[9])
    #   self.ordered_joints.append( (j, lower, upper) )
    #   self.jdict[jname] = j
    # # physicsClientId = p.disconnect(p.DIRECT)
    # physicsClientId = p.connect(p.DIRECT)
    
    write_new_dynamics(i)
    # print("loading new robot")
    print(i)
    Id = p.loadURDF("temp" + str(i) + ".urdf", basePosition=[0,0,0])
    os.remove("temp" + str(i) + ".urdf")
    # body_xyz, _ = p.getBasePositionAndOrientation(Id)
    # print(body_xyz)
    # print(Id)

    p.stepSimulation()
    for j in range(3,6):
      print(Id,"temp" + str(i) + ".urdf", p.getDynamicsInfo(Id, j)[0])
    time.sleep(5)
    i += 1
    # if i == 3:
    #   i = 1
    #   break