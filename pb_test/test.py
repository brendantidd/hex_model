import pybullet as p
import numpy as np
import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--set_joint', default=False, action='store_true')  
parser.add_argument('--render', default=False, action='store_true')  
args = parser.parse_args()


ac_size = 3

if args.render:
  physicsClientId = p.connect(p.GUI)
else:
  physicsClientId = p.connect(p.DIRECT)

p.setTimeStep(1/200)
p.setGravity(0,0,-9.8)

p.resetSimulation()

p.loadMJCF("./ground.xml")
Id = p.loadURDF("./test_leg.urdf")
p.resetBasePositionAndOrientation(Id, [0,0,2],[0,0,0,1])

numJoints = p.getNumJoints(Id)
jdict = {}
ordered_joints = []
ordered_joint_indices = []
joint_links = []
for j in range( p.getNumJoints(Id) ):
  info = p.getJointInfo(Id, j)
  link_name = info[12].decode("ascii")
  ordered_joint_indices.append(j)
  if info[2] != p.JOINT_REVOLUTE: continue
  joint_links.append(j)
  jname = info[1].decode("ascii")
  lower, upper = (info[8], info[9])
  ordered_joints.append( (j, lower, upper) )
  jdict[jname] = j

motor_names = ["BR_coxa_joint","BR_femur_joint","BR_tibia_joint"]
motors = [jdict[n] for n in motor_names]

p.setJointMotorControlArray(Id, motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(motor_names))

sample_torques = np.load("/home/aslab/data/06_02_2020/baseline/observed_torques.npy")

sample_pointer = 0
while True:
  actions = sample_torques[sample_pointer,:]
  
  p.setJointMotorControlArray(Id, motors,controlMode=p.TORQUE_CONTROL, forces=actions)
  
  
  p.stepSimulation()

  jointStates = p.getJointStates(Id,ordered_joint_indices)
  joints = list(np.array([jointStates[j[0]][0] for j in ordered_joints[:int(ac_size)]]))
  joint_vel = list(np.array([jointStates[j[0]][1] for j in ordered_joints[:int(ac_size)]]))

  # joint_before = joints

  if args.set_joint:
    for j, jv, m in zip(joints, joint_vel, motors):
      p.resetJointState(Id, m, targetValue=j, targetVelocity=jv)
      # p.resetJointState(Id, m, targetValue=j)

  jointStates = p.getJointStates(Id,ordered_joint_indices)
  joints = list(np.array([jointStates[j[0]][0] for j in ordered_joints[:int(ac_size)]]))
  joint_vel = list(np.array([jointStates[j[0]][1] for j in ordered_joints[:int(ac_size)]]))

  
  sample_pointer += 1
  if sample_pointer > sample_torques.shape[0] - 1:
    # sample_pointer = 0
    print(joints)
    break


  time.sleep(0.005)

