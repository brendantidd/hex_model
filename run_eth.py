import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger


def run(args):

  PATH = home + '/results/hex_model/latest/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  if args.train_pol or (args.test_pol and not args.gz) and not args.rl_model:
    if args.eth_rnn:
      from models import eth_rnn
      lstm_size = 128
      eth_model = eth_rnn.RNN(name='actuator_model', input_size=3, output_size=1, sess=sess)
    else:  
      from models import eth_model
      eth_model = eth_model.NN(name='eth_model', input_size=15, output_size=1, ac_size=3, sess=sess)

  if args.train_pol or (args.test_pol and not args.gz):
    from assets.just_test_leg_fm_rl import Env
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, rand_dynamics=args.rand_dynamics)
  elif args.gz:
    from assets.test_leg_gz import Env
    env = Env(render=args.render, PATH=PATH, args=args, delay=args.gz_delay)
    time.sleep(10)
  else:
    from assets.just_test_leg import Env
    env = Env(render=args.render, PATH=PATH, args=args, horizon=horizon, test=args.test, cur=args.cur, use_expert=args.use_expert)
  
  if args.train_pol or args.test_pol:
    from models.ppo import Model
    pol = Model("pi", env, ob_size=env.ob_size, ac_size=env.ac_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
    
    initialize()
    sync_from_root(sess, pol.vars, comm=comm) #pylint: disable=E1101
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if (args.train_pol or (args.test_pol and not args.gz)):
    # eth_model.load(home + '/data/iros_data/real/300/weights_eth_/best/')
    # eth_model.load(home + '/data/iros_data/real_29_1/weights_eth_/best/')
    if args.eth_rnn:
      eth_model.load(home + '/data/iros_data/real/8_2/weights_eth_rnn_/best/')
    else:
      eth_model.load(home + '/data/iros_data/real/8_2/weights_eth_/best/')

  if args.test_pol:
    if args.hpc:
      pol.load(home + '/hpc-home/results/hex_model/latest/' + args.exp + '/')      
    else:
      pol.load(PATH)

  prev_done = True
  ob = env.reset()
  im = np.zeros(env.im_size)
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  input_buf = {i:deque([0.0]*15,maxlen=15) for i in env.motor_names}
  
  env.reward_breakdown = {'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100)}
  plot_len = 200
  y1 = [[],[],[],[],[],[]]
  y2 = [[],[],[],[],[],[]]
  x1 = [[],[],[]]
  x2 = [[],[],[]]
  x3 = [[],[],[]]
  id_x = []
  id_y = []
  id_y_pos = []
  plot_length = 256
  eth_pred =  deque(maxlen=plot_length)
  eth_label = deque(maxlen=plot_length)
  eth_target = deque(maxlen=plot_length)
  fm_label = deque(maxlen=plot_length)
  fm_target = deque(maxlen=plot_length)
  pol_pred = deque(maxlen=plot_length)
  id_pointer = 0
  ep_steps = 0
  full = False
  if args.eth_rnn:
    eth_model_state = [np.zeros([3,lstm_size]), np.zeros([3,lstm_size])] 
  # id_xs = deque(maxlen=256)
  # id_ys = deque(maxlen=256)
  # desired_next_state = deque(maxlen=seq_length)
  # actual_next_state = deque(maxlen=seq_length)
  id_loss = deque(maxlen=100)
  training_steps = 0
  while True:
    # act = np.zeros(env.ac_size)
    if args.train_pol or (args.test_pol and not args.gz):
      if pol.timesteps_so_far > pol.max_timesteps:
        break 
      if args.test_pol:
        act, vpred, _, nlogp = pol.step(ob, im, stochastic=False)
      else:
        act, vpred, _, nlogp = pol.step(ob, im, stochastic=True)
     
      if args.eth_rnn:
        torques, eth_model_state = eth_model.step(env.joints + env.joint_vel + list(act), eth_model_state)  
      else:
        batch_input = [] 
        for m,j,v,dt in zip(env.motor_names, env.joints, env.joint_vel, act):
          input_buf[m].extend([j,v,dt])
          batch_input.append(np.array(input_buf[m]))
        torques = eth_model.step(batch_input)   
    elif args.test_pol:
      torques, _, _, _ = pol.step(ob, im, stochastic=False)
    else:
      torques = None
    next_ob, rew, done, _ = env.step(torques)
    next_im = np.zeros(env.im_size)
    
    if args.train_pol:
      fm_label.append(env.joints+env.joint_vel)
      fm_target.append(list(env.exp_joints)+list(env.exp_joint_vel))
      eth_pred.append(act)
      eth_label.append(torques)
      eth_target.append(list(env.exp_torques))
        
    if args.train_pol:
      pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
    prev_done = done
    ob = next_ob
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
    
    if args.train_pol and ep_steps % horizon == 0: 

      _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
      pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
      pol.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
      training_steps += 1

    if done:    
      if rank == 0  and training_steps % 10 == 0:
        subplot([[np.array(fm_label)[:,0], np.array(fm_target)[:,0]],[np.array(fm_label)[:,1], np.array(fm_target)[:,1]],[np.array(fm_label)[:,2], np.array(fm_target)[:,2]],[np.array(fm_label)[:,3], np.array(fm_target)[:,3]],[np.array(fm_label)[:,4], np.array(fm_target)[:,4]],[np.array(fm_label)[:,5], np.array(fm_target)[:,5]]], legend=[["actual", "target"]]*6, PATH=PATH + 'fm_')
        subplot([[np.array(eth_pred)[:,0], np.array(eth_label)[:,0], np.array(eth_target)[:,0]],[np.array(eth_pred)[:,1], np.array(eth_label)[:,1], np.array(eth_target)[:,1]],[np.array(eth_pred)[:,2], np.array(eth_label)[:,2], np.array(eth_target)[:,2]]], legend=[["torque from policy", "torque from actuator model","target torque",]]*3, PATH=PATH)
      fm_label = deque(maxlen=plot_length)
      fm_target = deque(maxlen=plot_length)
      ob = env.reset()
      im = np.zeros(env.im_size)
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
      input_buf = {i:deque([0.0]*15,maxlen=15) for i in env.motor_names}
      if args.eth_rnn:
        eth_model_state = [np.zeros([3,lstm_size]), np.zeros([3,lstm_size])] 
        

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--ae', default=False, action='store_true')
  parser.add_argument('--error', default=False, action='store_true')
  parser.add_argument('--norm', default=False, action='store_true')
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--use_expert', default=False, action='store_true')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--rnn', default=False, action='store_true')
  parser.add_argument('--stand', default=False, action='store_true')
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--test_with_gz', default=False, action='store_true')  
  parser.add_argument('--test_leg', default=False, action='store_true')  
  parser.add_argument('--use_act_model', default=False, action='store_true')  
  parser.add_argument('--use_sim_model', default=False, action='store_true')  
  parser.add_argument('--manual', default=False, action='store_true')  
  parser.add_argument('--train_pol', default=False, action='store_true')  
  parser.add_argument('--test_pol', default=False, action='store_true') 
  parser.add_argument('--pd', default=False, action='store_true')  
  parser.add_argument('--vis', default=False, action='store_true')  
  parser.add_argument('--train_id', default=False, action='store_true')  
  parser.add_argument('--gz', default=False, action='store_true')  
  parser.add_argument('--eth', default=False, action='store_true')  
  parser.add_argument('--rl_model', default=False, action='store_true')  
  parser.add_argument('--eth_rnn', default=False, action='store_true')  
  parser.add_argument('--set_pos', default=False, action='store_true')  
  parser.add_argument('--const_std', default=True, action='store_false')  
  parser.add_argument('--new_urdf', default=True, action='store_false')  
  parser.add_argument('--control', default="torque")  
  parser.add_argument('--adam', default=False, action='store_true')  
  parser.add_argument('--rand_dynamics', default=False, action='store_true')  
  parser.add_argument('--delay', default=False, action='store_true')  
  parser.add_argument('--just_pos', default=False, action='store_true')  
  parser.add_argument('--obs_torque', default=True, action='store_false')  
  parser.add_argument('--gz_delay', default=True, action='store_false')  
  parser.add_argument('--baseline', default=True, action='store_false')  
  parser.add_argument('--blank_id', default=False, action='store_true')  
  parser.add_argument('--max_ts', default=50e6, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)