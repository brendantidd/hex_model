# !/usr/bin/env python

'''
Changes URDF. 
joint position, 
link mass, 
link mass position
'''
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
import numpy as np
import pybullet as p
# path = '/home/brendan/Dropbox/robot/mybot_ws/src/hex_model/assets/test_leg.urdf'

class ChangeURDF:
  def __init__(self, path='assets/test_leg.urdf', ac_size=3, debug=False, normal=True):
    '''
    Set normal = True for training parameters, set normal = False to sample uniformly from parameters during policy training
    '''
    self.path = path
    self.debug = debug
    self.normal = normal
    self.ac_size = ac_size
    self.stds = 0.0
    # self.params = ['damping', 'friction', 'joint_pos', 'inertia', 'mass_pos', 'mass']
    self.params = ['damping', 'friction', 'inertia', 'mass_pos', 'mass']
    # self.params = ['damping', 'friction', 'joint_pos', 'mass_pos', 'mass']
    # self.params = ['damping', 'friction', 'mass_pos', 'mass']
    self.joint_names = ['coxa', 'femur', 'tibia']
    self.xyz = ['x','y','z']
    self.uniform_params = {
      'friction':{'coxa':0.05,'femur':0.05,'tibia':0.05},
      'damping':{'coxa':0.05,'femur':0.05,'tibia':0.05},
      'joint_pos':{'coxa':0.02,'femur':0.02,'tibia':0.02},
      'mass_pos':{'coxa':0.02,'femur':0.02,'tibia':0.02},
      'mass':{'coxa':0.15,'femur':0.15,'tibia':0.15},
      'inertia':{'coxa':0.0001,'femur':0.0001,'tibia':0.0001}
      }
      
    self.normal_params = {}
    for param in self.params:
      self.normal_params[param] = {}
      for joint in self.joint_names:
        self.normal_params[param][joint] = {}
        if param in ['joint_pos', 'inertia', 'mass_pos']:
          for pos in self.xyz:
            self.normal_params[param][joint][pos] = {}
            self.normal_params[param][joint][pos]['mu'] = 0
            if param == 'inertia':
              self.normal_params[param][joint][pos]['std'] = 0.001
            else:
              self.normal_params[param][joint][pos]['std'] = 0.01
        else:
          self.normal_params[param][joint]['mu'] = 0
          self.normal_params[param][joint]['std'] = 0.1 
  
  def update_and_sample_params(self, params=None):
    if params is not None:
      self.set_values(params)  

    self.values = {
      'damping': [np.clip(np.random.normal(self.pms['damping'][name]['mu'],self.pms['damping'][name]['std']),0,1) for name in self.joint_names],
      'inertia': [[np.clip(np.random.normal(self.pms['inertia'][name][pos]['mu'],self.pms['inertia'][name][pos]['std']),0,1) for pos in self.xyz] for name in self.joint_names],
      'mass': [np.clip(np.random.normal(self.pms['mass'][name]['mu'],self.pms['mass'][name]['std']),0,5) for name in self.joint_names],
      # 'delay': int(np.clip(np.random.normal(self.pms['delay']['mu'],self.pms['delay']['std']),1,4))
      'delay': 1 
      }

    self.write_new_dynamics()
    self.get_values()

    return value

  def set_params(self, params, std_limit=False):
    means, stds = params
    count = 0
    self.normal_params = {}
    for param in self.params:
      self.normal_params[param] = {}
      for joint in self.joint_names:
        self.normal_params[param][joint] = {}
        if param in ['joint_pos', 'inertia', 'mass_pos']:
          for pos in self.xyz:
            self.normal_params[param][joint][pos] = {}
            self.normal_params[param][joint][pos]['mu'] = means[count]
            if param == 'inertia':
              if std_limit:
                self.normal_params[param][joint][pos]['std'] = max(stds[count], 0.00075)
              else:
                self.normal_params[param][joint][pos]['std'] = stds[count]
            else:
              if std_limit:
                self.normal_params[param][joint][pos]['std'] = max(stds[count], 0.005)
              else:
                self.normal_params[param][joint][pos]['std'] = stds[count]
            count += 1
        else:
          self.normal_params[param][joint]['mu'] = means[count]
          if param == 'damping' or param == 'friction':
            if std_limit:
              self.normal_params[param][joint]['std'] = max(stds[count], 0.05)
            else:
              self.normal_params[param][joint]['std'] = stds[count]
          else:
            if std_limit:
              self.normal_params[param][joint]['std'] = max(stds[count], 0.075)
            else:
              self.normal_params[param][joint]['std'] = stds[count]
          count += 1
    # print(self.normal_parasscms)

  def sample_uniform(self):
    print("todo, sample uniform")
    exit()
    self.values = {}
    for param in self.params:
      self.values[param] = {}
      for joint in self.joint_names:
        self.values[param][joint] = {}
        if param in ['joint_pos', 'inertia', 'mass_pos']:
          self.values[param][joint][pos] = {}
          for pos in self.xyz:
            self.values[param][joint][pos] = np.random.normal(self.normal_params[joint][pos]['mu'],self.normal_params[joint][pos]['std'])
        else:
          self.values[param][joint] = np.random.normal(self.normal_params[joint][pos]['mu'],self.normal_params[joint][pos]['std'])

  def sample_normal(self):
      self.values = {}
      for param in self.params:
        self.values[param] = {}
        for joint in self.joint_names:
          self.values[param][joint] = {}
          if param in ['joint_pos', 'inertia', 'mass_pos']:
            for pos in self.xyz:
              self.values[param][joint][pos] = np.random.normal(self.normal_params[param][joint][pos]['mu'],self.normal_params[param][joint][pos]['std'])
          else:
            if param in ['friction', 'damping']:
              self.values[param][joint] = np.clip(np.random.normal(self.normal_params[param][joint]['mu'],self.normal_params[param][joint]['std']), 0.0,0.2)
            else:
              self.values[param][joint] = np.clip(np.random.normal(self.normal_params[param][joint]['mu'],self.normal_params[param][joint]['std']),0.0,0.2)
        
  def get_means(self):
      self.values = {}
      for param in self.params:
        self.values[param] = {}
        for joint in self.joint_names:
          self.values[param][joint] = {}
          if param in ['joint_pos', 'inertia', 'mass_pos']:
            for pos in self.xyz:
              self.values[param][joint][pos] = self.normal_params[param][joint][pos]['mu']
          else:
            if param in ['friction', 'damping']:
              self.values[param][joint] = self.normal_params[param][joint]['mu']
            else:
              self.values[param][joint] = self.normal_params[param][joint]['mu']
      # print(self.values)
      print(self.normal_params)

  def get_values(self):
    # self.sample_normal()
    values = []
    for param in self.params:
      for joint in self.joint_names:
        if param in ['joint_pos', 'inertia', 'mass_pos']:
          for pos in self.xyz:
            values.append(self.values[param][joint][pos])
        else:
          values.append(self.values[param][joint])
    return np.array(values)
      
  def write_new_dynamics(self, save_path, load_from_origin=True):
    if load_from_origin:
      tree = ET.parse(self.path)
    else:
      tree = ET.parse(save_path)
    root = tree.getroot()

    for link in root.findall('link'):
      # print(name)
      name = link.get('name')
      if 'body' not in name and ('coxa' in name or 'femur' in name or 'tibia' in name):
        name = name.split("_")[1]
        inertial = link.find('inertial')
        mass_ = inertial.find('mass')
        mass = float(mass_.get('value'))
        if self.normal:
          mass_.set('value', str(mass + self.values['mass'][name]))
        else:
          mass_.set('value', str(np.random.uniform(mass*(1-self.uniform_params['mass'][name]),mass*(1+self.uniform_params['mass'][name]))))
        mass_origin = inertial.find('origin')
        x = ''
        for pos, m in zip(self.xyz, [float(s) for s in (mass_origin.get('xyz')).split()]):
          if self.normal:
            x += str(m + self.values['mass_pos'][name][pos]) + ' '
          else:
            x += str(m + np.random.uniform(-self.uniform_params['mass_pos'][name],self.uniform_params['mass_pos'][name])) + ' '
        mass_origin.set('xyz', x)
        if 'inertia' in self.params:
          inertia = inertial.find('inertia')
          # x = ''
          for pos, xyz, inert in zip(['ixx','iyy','izz'],  self.xyz, [float(inertia.get(i)) for i in ['ixx','iyy','izz']]):
            # print("prev inertia ", pos, inert)
            if self.normal:
              x = str(max(inert + self.values['inertia'][name][xyz], 0.00001))
            else:
              x = str(max(inert + np.random.uniform(-self.uniform_params['inertia'][name],self.uniform_params['inertia'][name]),0.00001)) 
            inertia.set(pos, x)
            # print("post inertia ", pos, inertia.get(pos))
        if self.debug:      
          print(name , "link mass, mass xyz", mass_.get('value'), mass_origin.get('xyz'))

    for joint in root.findall('joint'):
      name = joint.get('name')
      if 'body' not in name and ('coxa' in name or 'femur' in name or 'tibia' in name):
        name = joint.get('name').split("_")[1]
        # if 'joint_pos' in self.values:
        #   joint_origin = joint.find('origin')
        #   x = ''
        #   for pos, j in zip(self.xyz, [float(s) for s in (joint_origin.get('xyz')).split()]):
        #     if self.normal:
        #       x += str(j + self.values['joint_pos'][name][pos]) + ' '
        #     else:
        #       x += str(j + np.random.uniform(-self.uniform_params['joint_pos'][name],self.uniform_params['joint_pos'][name])) + ' '
        #   joint_origin.set('xyz', x)

        dynamics = joint.find('dynamics')
        if self.normal:
          dynamics.set('friction', str(float(dynamics.get('friction')) + self.values['friction'][name]))
          dynamics.set('damping',  str(float(dynamics.get('damping')) + self.values['damping'][name]))
        else:
          dynamics.set('friction', str(float(dynamics.get('friction')) + np.random.uniform(0,self.uniform_params['friction'][name])))
          dynamics.set('damping',  str(float(dynamics.get('damping')) + np.random.uniform(0,self.uniform_params['damping'][name])))
        if self.debug:
          print(name, "joint xyz, friction, damping", joint_origin.get('xyz'), dynamics.get('friction'), dynamics.get('damping'))
    tree.write(save_path)


  '''
    mass  double
    lateralFriction double  lateral (linear) contact friction
    spinningFriction double torsional friction around the contact normal
    rollingFriction double torsional friction orthogonal to contact normal
    restitution double bouncyness of contact. Keep it a bit less than 1.
    linearDamping double linear damping of the link (0.04 by default)
    angularDamping double  angular damping of the link (0.04 by default)
    contactStiffness  double  stiffness of the contact constraints, used together with contactDamping.
    contactDamping double damping of the contact constraints for this body/link. Used together with contactStiffness. This overrides the value if it was specified in the URDF file in the contact section.
    frictionAnchor int enable or disable a friction anchor: positional friction correction (disabled by default, unless set in the URDF contact section)
    localInertiaDiagnoal  vec3  diagonal elements of the inertia tensor. Note that the base and links are centered around the center of mass and aligned with the principal axes of inertia so there are no off-diagonal elements in the inertia tensor.
    ccdSweptSphereRadiu   float  radius of the sphere to perform continuous collision detection. See Bullet/examples/pybullet/examples/experimentalCcdSphereRadius.py for an example.
    contactProcessingThreshold  float  contacts with a distance below this threshold will be processed by the constraint solver. For example, if contactProcessingThreshold = 0, then contacts with distance 0.01 will not be processed as a constraint.
    
    jointDamping double Joint damping coefficient applied at each joint. This coefficient is read from URDF joint damping field. Keep the value close to 0.
    Joint damping force = -damping_coefficient * joint_velocity.
    
    anisotropicFriction   float  anisotropicFriction coefficient to allow scaling of friction in different directions.
    
    maxJointVelocity  float  maximum joint velocity for a given joint, if it is exceeded during constraint solving, it is clamped. Default maximum joint velocity is 100 units.
  '''

if __name__=="__main__":
  cu = ChangeURDF()
  cu.new_dynamics()