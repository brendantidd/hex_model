import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import numpy as np
from scripts.utils import *

data = np.load('samples/new_samples.npy')
print(data.shape)
# data[:,5] = -data[:,4]
# data[18,3] = 4.3
# data[44,3] = -2.9
# data[45,3] = -2.9
# data[156,3] = 5.7
# data[157,3] = 5.7
# data[158,3] = 5.7

# np.save('samples/new_samples.npy', data)

# data[1:,3:6] = (data[1:,:3] - data[:-1,:3])
# i,j = 150,170
# print(data[i:j,3])
# subplot([[data[i:j,3]],[data[i:j,3]]])

# print(data.shape)
# print(data)
subplot([[data[:,0]],[data[:,1]],[data[:,2]],[data[:,3]],[data[:,4]],[data[:,5]]])