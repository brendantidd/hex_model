import numpy as np
import os   
from glob import glob   
import re 
from pathlib import Path
home = str(Path.home())
from utils import *
import shutil

def plot2():
  path = home + '/data/iros_data/real_29_1/'
  new_path = home + '/data/iros_data/real_29_1_new/'


  # coxa, femur, tibia, coxa, femur, tibia, coxa, femur, tibia
  inputs_mask = np.array([-1,1,1,-1,1,1,-1,1,1])
  inputs = np.load(path + 'inputs.npy')
  # print(inp.shape)
  # subplot([[inp[:,0]],[inp[:,1]],[inp[:,2]],[inp[:,3]],[inp[:,4]],[inp[:,5]],[inp[:,6]],[inp[:,7]],[inp[:,8]]])
  
  labels_mask = np.array([-1,1,1,-1,1,1])
  labels = np.load(path + 'labels.npy')[1:,:]
  # subplot([[inp[:,0]],[inp[:,1]],[inp[:,2]],[inp[:,3]],[inp[:,4]],[inp[:,5]]])

  # inp = np.load(path + 'eth_inputs.npy')[1:,:]
  # subplot([[inp[:,0]],[inp[:,1]],[inp[:,2]],[inp[:,3]],[inp[:,4]],[inp[:,5]],[inp[:,6]],[inp[:,7]],[inp[:,8]]])
  

  # inp = np.load(path + 'eth_labels.npy')[1:]
  # subplot([[inp[:]],[inp[:]]])

  new_inputs = np.concatenate([inputs, inputs*inputs_mask], axis=0)
  new_labels = np.concatenate([labels, labels*labels_mask], axis=0)

  np.save(new_path + 'inputs.npy', new_inputs)
  np.save(new_path + 'labels.npy', new_labels)
  # print(new_inputs.shape, new_labels.shape)

def clean(args):
  path = home + '/data/03_02_2020_clean/'
  folders = sorted(glob(path + "*/"))
  i = 0  
  for f in folders:
    # if i == 11:
    # print(f,i)
    if i == args.i:
      print(f,i)
      if args.delete:
        print("deleting ")
        shutil.rmtree(f)
        exit()
      # print(data.shape)
      start_idx, end_idx = args.start_idx, args.end_idx
      input_data = np.load(f + 'inputs.npy')[start_idx:end_idx, :]
      # data_buf = np.empty((1,9))

      if args.save:
        label_data = np.load(f + 'labels.npy')[start_idx:end_idx, :]
        # data_buf = np.empty((1,6))
        input_eth_data = np.load(f + 'eth_inputs.npy')[start_idx:end_idx, :]
        label_eth_data = np.load(f + 'eth_labels.npy')[start_idx:end_idx]

        np.save(f + 'inputs.npy', input_data)
        np.save(f + 'labels.npy', label_data)
        np.save(f + 'eth_inputs.npy', input_eth_data)
        np.save(f + 'eth_labels.npy', label_eth_data)

      else:
        inp = input_data
        print(inp.shape)
        subplot([[inp[:,0]],[inp[:,1]],[inp[:,2]],[inp[:,3]],[inp[:,4]],[inp[:,5]],[inp[:,6]],[inp[:,7]],[inp[:,8]]])
      # np.save(f + 'eth_inputs.npy', inp)
      # inp = np.load(f + 'eth_labels.npy')[1:]
      # np.save(f + 'eth_labels.npy', inp)
      # data_buf = np.empty((1,9))
      # data = np.load(f + 'eth_labels.npy')
      # data_buf = np.empty((1,))
      
      # for d in data:
      #   data_buf = np.concatenate((data_buf, d), axis=0)
      # print("looking at ", f)
      # inp = data_buf[1:,:]
      # np.save(f + 'inputs.npy', inp)
      # np.save(f + 'labels.npy', inp)
      # exit()
    # else:
    #   print("file not found")
    i += 1


def combine():
  just_mine = True
  # just_mine = False
  # seq_length = 512
  path = home + '/data/10_02_2020/'
  folders = sorted(glob(path + "*/"))
  
  save_path = home + '/data/iros_data/real/13_2/'
  # folders += [home + '/data/iros_data/real/toms/300/']
  # folders = folders[2:10] + [home + '/data/iros_data/real/toms/300/']
  # folders = folders[2:10] 
    # folders += [home + '/data/29_01_2020_clean/']
  inputs = []
  labels = []
  observed_torques = []
  dones = []
  eth_inputs = []
  eth_labels = []
  for f in folders:
    if 'toms' in f:
      inp = np.load(f + 'inputs.npy').reshape(-1,9)
      inputs.extend(inp)
      labels.extend(np.load(f + 'labels.npy').reshape(-1,6))
      dones.extend([1] + [0 for _ in range(inp.shape[0] - 1)])
    else:
      # print(f)
      inp = np.load(f + 'inputs.npy')[400:,:]
      inputs.extend(inp)
      observed_torques.extend(np.load(f + 'observed_torques.npy')[400:,:])
      labels.extend(np.load(f + 'labels.npy')[400:,:])
      dones.extend([1] + [0 for _ in range(inp.shape[0] - 1)])
    print(f)
    eth_inputs.extend(np.load(f + 'eth_inputs.npy')[400:,:])
    eth_labels.extend(np.load(f + 'eth_labels.npy')[400:])
    

  inputs = np.array(inputs)
  observed_torques = np.array(observed_torques)
  labels = np.array(labels)
  dones = np.array(dones)
  eth_inputs = np.array(eth_inputs)
  eth_labels = np.array(eth_labels)


  # old_path = home + '/data/iros_data/real/10_2/'

  # old_inputs = np.load(old_path + 'inputs.npy')[:inputs.shape[0],:]
  # old_observed_torques = np.load(old_path + 'observed_torques.npy')[:inputs.shape[0],:]
  # old_labels = np.load(old_path + 'labels.npy')[:inputs.shape[0],:]
  # old_dones = np.load(old_path + 'dones.npy')[:inputs.shape[0]]
  # old_eth_inputs = np.load(old_path + 'eth_inputs.npy')[:inputs.shape[0],:]
  # old_eth_labels = np.load(old_path + 'eth_labels.npy')[:inputs.shape[0]]

  # inputs = np.concatenate([inputs, old_inputs], axis=0)
  # labels = np.concatenate([labels, old_labels], axis=0)
  # eth_inputs = np.concatenate([eth_inputs, old_eth_inputs], axis=0)
  # eth_labels = np.concatenate([eth_labels, old_eth_labels], axis=0)
  # observed_torques = np.concatenate([observed_torques, old_observed_torques], axis=0)
  # dones = np.concatenate([dones, old_dones], axis=0)
  
  # inputs_mask = np.array([-1,1,1,-1,1,1,-1,1,1])
  # labels_mask = np.array([-1,1,1,-1,1,1])
  # observed_torque_mask = np.array([-1,1,1])
  
  # inputs = np.concatenate([inputs, inputs*inputs_mask], axis=0)
  # labels = np.concatenate([labels, labels*labels_mask], axis=0)
  # observed_torques = np.concatenate([observed_torques, observed_torques*observed_torque_mask], axis=0)
  # dones = np.concatenate([dones, dones], axis=0)

  print(inputs.shape, observed_torques.shape, labels.shape, dones.shape, eth_inputs.shape, eth_labels.shape)

  if args.save:
    np.save(save_path + 'inputs.npy', inputs)
    np.save(save_path + 'labels.npy', labels)
    np.save(save_path + 'observed_torques.npy', observed_torques)
    np.save(save_path + 'dones.npy', dones)
    np.save(save_path + 'eth_inputs.npy', eth_inputs)
    np.save(save_path + 'eth_labels.npy', eth_labels)
  else:
    start, end = 30000, 32000
    inp = inputs
    subplot([[inp[start:end,0]],[inp[start:end,1]],[inp[start:end,2]],[inp[start:end,3]],[inp[start:end,4]],[inp[start:end,5]],[inp[start:end,6]],[inp[start:end,7]],[inp[start:end,8]]], PATH=save_path)
    # subplot([[eth_labels[500:1000]],[eth_labels[500:1000]]], PATH=save_path + 'eth_')
  

  # print(np.array(inputs).shape, np.array(labels).shape, np.array(dones).shape)
  # print(dones)
  # for i in ['inputs', 'labels']:
  #   if 'inputs' == i:
  #   elif 'labels' == i:
  #     data_buf = []
  #   for f in folders:
  #     data = np.load(f + i + '.npy')
  #     data_buf = np.concatenate((data_buf, data), axis=0)

      # print(data.shape, data_buf.shape)
      # for d in data:
      #   data_buf = np.concatenate((data_buf, d), axis=0)

    # print(data_buf.shape)
    # np.save(path + i + '.npy', data_buf[1:,:])
    # exit()

  # data_buf = np.empty((1,15))
  # for f in folders:
  #   data = np.load(f + 'eth_inputs.npy')
  #   # print(data.shape)
  #   data_buf = np.concatenate((data_buf, data), axis=0)
  # print(data_buf.shape)
  # np.save(path + 'eth_inputs.npy', data_buf[1:,:])

  # data_buf = np.empty((1,))
  # for f in folders:
  #   data = np.load(f + 'eth_labels.npy')
  #   # print(data.shape)
  #   data_buf = np.concatenate((data_buf, data), axis=0)
  # print(data_buf.shape)
  # np.save(path + 'eth_labels.npy', data_buf[1:])

if __name__=="__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--type', default="clean")
  parser.add_argument('--save', default=False, action='store_true')
  parser.add_argument('--delete', default=False, action='store_true')
  parser.add_argument('--i', default=0, type=int)
  parser.add_argument('--start_idx', default=0, type=int)
  parser.add_argument('--end_idx', default=1000000, type=int)
  args = parser.parse_args()
  if args.type == "clean":
    clean(args)
  elif args.type == "combine":
    combine()

  # plot2(args)