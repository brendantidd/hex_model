import pandas as pd 
import os
from glob import glob   
import re
from pathlib import Path
home = str(Path.home())
import numpy as np
from utils import *
# '''
# Load all the data that is labelled data+num under ~/data/
# '''
# DATA_PATH = '/home/tid010/data/'
# folders = glob(DATA_PATH + "*/")
# latest = [f.split('/') for f in folders]
# latest = [q for l in latest for q in l if q not in home]
# l = ''
# for item in latest:
#   l += item
# nums = re.findall('(\d+)',l)
# nums = np.sort([int(n) for n in nums])
# print("loading data from folder: ", nums)
# data = []
# for num in nums:
#   # temp_data = pd.ExcelFile(DATA_PATH + 'data' + str(num) + '/samples.xlsx')
#   temp_data = pd.read_excel(DATA_PATH + 'data' + str(num) + '/samples.xlsx', header=None)
#   print(temp_data.shape)
#   data.extend(temp_data.as_matrix())
#   # data.extend(np.load(DATA_PATH + 'data' + str(num) + '/samples.npy', allow_pickle=True))
# print(np.array(data).shape)
# df = pd.DataFrame (np.array(data))
# print(df.shape)
# df.to_excel( '/home/tid010/data/eth.xlsx', index=False)

data = load_data(DATA_PATH = home + '/data/test/')
print(data.shape)
x1,x2,x3,x4 = data[1000:1500,1-data.shape[1]],data[1000:1500,2-data.shape[1]],data[1000:1500,3-data.shape[1]],data[1000:1500,4-data.shape[1]]
y1,y2,y3,y4 = data[1000:1500,1+18-data.shape[1]],data[1000:1500,2+18-data.shape[1]],data[1000:1500,3+18-data.shape[1]],data[1000:1500,3+18-data.shape[1]]
# plot([x1,x2,x3,x4])
print(x1)
print(y1)
subplot([[x1,y1],[x2,y2],[x3,y3],[x4,y4]],legend=[['input','label'],['input','label'],['input','label'],['input','label']])
