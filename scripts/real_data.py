import pandas as pd
import os
from glob import glob   
import re 
from pathlib import Path
from utils import *
home = str(Path.home())

# DATA_PATH = home + '/data/Test Leg/2019-11-12_10-39-14_test_leg_accelerations/6a6d7faa-9985-4c0f-94c9-b0bb26c20aac.csv'
# DATA_PATH = home + '/data/2019-11-13_10-22-13_test_leg_traj_comp_task_impedance_new/159dd830-bd87-485a-a0e8-0f9cfd9eeb93.csv'
DATA_PATH = home + '/data/test_leg_15_11/6310b0ec-2759-4ae5-b5af-6db50b34eed7.csv'
# DATA_PATH = home + '/data/Test Leg/2019-11-11_11-15-44_test_leg_no_ID/8f15fe4b-353d-44fc-9cb0-0b3f6063c179.csv'
# DATA_PATH = home + '/data/Test Leg/2019-11-11_10-42-58_test_leg_new_gains/5acc5475-3647-478f-a409-df5218d3695e.csv'
all_data = pd.read_csv(DATA_PATH)
print(all_data.shape)
# print(all_data.head(0))
for d in all_data:
  # print(all_data[d][300000:300005])
  print(d)
# options = [
# '/TEST_coxa/miso/js__joint__position__rad',
# '/TEST_coxa/miso/js__joint__velocity__radps',
# '/TEST_coxa/miso/js__joint__effort__Nm',
# '/TEST_coxa/mosi/cmd__joint__position__rad',
# '/TEST_coxa/mosi/cmd__joint__velocity__radps',
# '/TEST_coxa/mosi/cmd__joint__effort__nm',
# '/TEST_coxa/miso/actuator__force__N',
# '/TEST_coxa/miso/actuator__position__m',
# '/TEST_coxa/miso/actuator__velocity__mps',
# '/TEST_coxa/miso/motor__position__Rad',
# '/TEST_coxa/miso/motor__velocity__Radps']
options = [
'/TEST_coxa/miso/js__joint__position__rad',
'/TEST_coxa/miso/js__joint__velocity__radps',
'/TEST_coxa/miso/js__joint__effort__Nm',
'/TEST_coxa/mosi/cmd__joint__position__rad',
'/TEST_coxa/mosi/cmd__joint__velocity__radps',
'/TEST_coxa/mosi/cmd__joint__effort__nm',
'/TEST_coxa/miso/actuator__force__N',
'/TEST_coxa/miso/actuator__position__m',
'/TEST_coxa/miso/actuator__velocity__mps',
'/TEST_coxa/miso/motor__position__Rad',
'/TEST_coxa/miso/motor__velocity__Radps']

# Torques
# /TEST_tibia/mosi/cmd__joint__effort__nm
input_post = '/mosi/cmd__joint__effort__nm'
output_post = '/miso/js__joint__effort__Nm'
# Positions
# input_post = '/mosi/cmd__joint__effort__nm'
# output_post = '/miso/js__joint__effort__Nm'
# inputs = np.array([all_data['/TEST_coxa/mosi/cmd__joint__effort__nm'],all_data['/TEST_femur/mosi/cmd__joint__effort__nm'],all_data['/TEST_tibia/mosi/cmd__joint__effort__nm']]).T
# labels = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'],all_data['/TEST_femur/miso/js__joint__effort__Nm'],all_data['/TEST_tibia/miso/js__joint__effort__Nm']]).T
inputs = np.array([all_data['/TEST_coxa' + input_post],all_data['/TEST_femur' + input_post],all_data['/TEST_tibia' + input_post]]).T
labels = np.array([all_data['/TEST_coxa' + output_post],all_data['/TEST_femur' + output_post],all_data['/TEST_tibia' + output_post]]).T

# labels = np.array([all_data['/TEST_coxa/miso/actuator__force__N'],all_data['/TEST_femur/miso/actuator__force__N'],all_data['/TEST_tibia/miso/actuator__force__N']]).T
# save_data = False
save_data = True
if save_data:
#   /TEST_femur/miso/js__joint__position__rad
# /TEST_femur/miso/js__joint__velocity__radps
# /TEST_femur/miso/js__joint__effort__Nm

  pos = np.array([all_data['/TEST_coxa/miso/js__joint__position__rad'],all_data['/TEST_femur/miso/js__joint__position__rad'],all_data['/TEST_tibia/miso/js__joint__position__rad']]).T
  vel = np.array([all_data['/TEST_coxa/miso/js__joint__velocity__radps'],all_data['/TEST_femur/miso/js__joint__velocity__radps'],all_data['/TEST_tibia/miso/js__joint__velocity__radps']]).T
  des_torq = np.array([all_data['/TEST_coxa/mosi/cmd__joint__effort__nm'],all_data['/TEST_femur/mosi/cmd__joint__effort__nm'],all_data['/TEST_tibia/mosi/cmd__joint__effort__nm']]).T
  torq = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'],all_data['/TEST_femur/miso/js__joint__effort__Nm'],all_data['/TEST_tibia/miso/js__joint__effort__Nm']]).T
  # exp_torq = np.array([all_data['/TEST_coxa/miso/js__joint__velocity__radps'],all_data['/TEST_femur/miso/js__joint__velocity__radps'],all_data['/TEST_tibia/miso/js__joint__velocity__radps']]).T
  print(pos.shape, vel.shape, des_torq.shape, torq.shape)
  all_pos_vel = np.concatenate([pos,vel,des_torq, torq],axis=1)
  print(all_pos_vel.shape)
  np.save(home + '/data/test_leg_15_11/sample.npy', all_pos_vel)

  # path = home + '/data/eth_test_leg'
  # df = pd.DataFrame(labels)
  # df.to_excel(path + '/labels.xlsx', index=False)
  # df = pd.DataFrame(inputs)
  # df.to_excel(path + '/inputs.xlsx', index=False)

print(inputs.shape)
# pos1, pos2 = 340000, 342000
# pos1, pos2 = 293700, 295000
pos1, pos2 = 0, 400000
# pos1, pos2 = 290000,300000
x1,x2,x3 = inputs[pos1:pos2,0],inputs[pos1:pos2,1],inputs[pos1:pos2,2]
y1,y2,y3 = labels[pos1:pos2,0],labels[pos1:pos2,1],labels[pos1:pos2,2]
subplot([[x1,y1],[x2,y2],[x3,y3]], legend=[['input','label'],['input','label'],['input','label'],['input','label']], title=['coxa','femur','tibia'], x_initial=pos1)

x1,x2,x3,x4 = inputs[1000:1500,1],inputs[1000:1500,2],inputs[1000:1500,3],inputs[1000:1500,4]
y1,y2,y3,y4 = labels[1000:1500,1],labels[1000:1500,2],labels[1000:1500,3],labels[1000:1500,4]
# plot([x1,x2,x3,x4])
# print(x1)
# print(y1)
subplot([[x1,y1],[x2,y2],[x3,y3],[x4,y4]])

# for option in options:
  # inputs
