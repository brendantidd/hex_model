import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import pybullet as p


def run(args):

  PATH = home + '/results/hex_model/latest/' + args.exp + '_eval/'
  
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 
  
  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  lstm_size = 128

  if not args.baseline:
    if args.eth_rnn:
      from models import eth_rnn
      eth_model = eth_rnn.RNN(name='actuator_model', input_size=3, output_size=1, sess=sess)
    else:
      from models import eth_model
      eth_model = eth_model.NN(name='eth_model', input_size=15, output_size=1, ac_size=3, sess=sess)
    if args.fm:
      from models import simple_rnn
      forward_model = simple_rnn.RNN(name='forward_model', input_size=9, output_size=6, lstm_size=128, dense_size=128, sess=sess, norm=True, delta=False)
    elif args.residual:
      from models import residual
      rrnn = residual.ResidualNet(name='residual', input_size=9, output_size=6, lstm_size=128, sess=sess)  
    elif args.forward_residual:
      from models import forward_residual
      fr_rnn = forward_residual.FRNet(name='fr', input_size=15, output_size=6, lstm_size=128, sess=sess)  

  # from assets.just_test_leg_fm_rl import Env
  from assets.playback_env import PlayBackEnv

  env = PlayBackEnv(args=args) 
  
  if args.lstm_pol:
    from models.ppo_lstm import Model
  else:
    from models.ppo import Model
    
  initialize()
  
  if not args.baseline:
    if args.eth_rnn:
      eth_model.load(home + '/data/iros_data/real/13_2/weights_eth_rnn_/best/')
    else:
      eth_model.load(home + '/data/iros_data/real/13_2/weights_eth_/best/')
    if args.fm:
      forward_model.load(home + '/data/iros_data/real/13_2/weights_/best/')
    elif args.residual:      
      if args.rand_residual:
        # rrnn.load(home + '/data/iros_data/real/13_2/residual/rand/weights_residual_/best/')
        rrnn.load(home + '/data/iros_data/real/13_2/residual/rand/weights_residual_/best/')
      else:
        rrnn.load(home + '/data/iros_data/real/13_2/residual/weights_residual_/best/')
    elif args.forward_residual:      
      if args.rand_residual:
        fr_rnn.load(home + '/data/iros_data/real/13_2/fr/rand/weights_/best/')
      else:
        # fr_rnn.load(home + '/data/iros_data/real/13_2/fr/weights_/best/')
        fr_rnn.load(home + '/hpc-home/data/iros_data/real/13_2/fr_eth/weights_/best/')

  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  tf.get_default_graph().finalize()

  # Load the data

  # samples = np.load(home + '/data/12_02_2020/leg_eth_rnn_fr_2020_02_12_08_26_49/inputs.npy')[400:,:]
  # labels = np.load(home + '/data/12_02_2020/leg_eth_rnn_fr_2020_02_12_08_26_49/labels.npy')[400:,:]
  # obs_torques = np.load(home + '/data/12_02_2020/leg_eth_rnn_fr_2020_02_12_08_26_49/observed_torques.npy')[400:,:]
  path = home + '/data/iros_data/real/13_2/'
  samples = np.load(path + 'inputs.npy')[400:,:]
  labels = np.load(path + 'labels.npy')[400:,:]
  obs_torques = np.load(path + 'observed_torques.npy')[400:,:]

  cmd_torques = samples[:,6:]
  sample_size = samples.shape[0]
  sample_pointer = 0

  prev_done = True

  ob = env.reset(samples[sample_pointer,:3],samples[sample_pointer,3:6])
  prev_ob = ob
  
  if not args.baseline: 
    if args.eth_rnn:
      eth_model_state = [np.zeros([3,lstm_size]), np.zeros([3,lstm_size])] 
    else:
      eth_input_buf = {i:deque([0.0]*15,maxlen=15) for i in env.motor_names}
    if args.fm:
      fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
    elif args.residual:
      rrnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
    elif args.forward_residual:
      fr_rnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]

  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  
  env.reward_breakdown = {'pos':deque(maxlen=100), 'tor':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100)}
  
  data_length = 128
  plot_length = 256
  fm_pred = deque(maxlen=plot_length)
  fm_label = deque(maxlen=plot_length)
  fm_target = deque(maxlen=plot_length)
  pol_pred = deque(maxlen=plot_length)
  pol_label = deque(maxlen=plot_length)
  pol_target = deque(maxlen=plot_length)
  if args.lstm_pol:
    pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  
  id_pointer = 0
  ep_steps = 0
  stochastic = False
  metric = deque(maxlen=sample_size)
  metrics = []
  torque_metric = deque(maxlen=sample_size)
  torque_metrics = []
  
  training_steps = 0
  action_buffer = deque(maxlen=3)
  prev_cmd_torque = [0,0,0]
  while True:

    cmd_torque = cmd_torques[sample_pointer,:]
    
    if not args.baseline:
      # Through actuator model
      if args.eth_rnn:
        torque, eth_model_state = eth_model.step(env.joints + env.joint_vel + list(cmd_torque), eth_model_state)  
      else:
        batch_input = [] 
        for m,j,v,dt in zip(env.motor_names, env.joints, env.joint_vel, cmd_torque):
          eth_input_buf[m].extend([j,v,dt])
          batch_input.append(np.array(eth_input_buf[m]))
        torque = eth_model.step(batch_input)  
        # print(torque)
      # Through dynamics model
      if args.fm:
        fm_output, fm_state = forward_model.step(np.array(env.joints + env.joint_vel + list(torque)), fm_state)       
        next_ob, rew, done, _ = env.step(torque, fm_output, samples[sample_pointer,:3])    
        fm_pred.append(fm_output)  
      elif args.residual:        
        next_ob, rew, done, _ = env.step(torque, samples[sample_pointer,:3])    
        rrnn_output, rrnn_state = rrnn.step(np.array(env.prev_joints + env.prev_joint_vel + list(torque)), rrnn_state)       
        next_ob[:6] = np.array(env.joints + env.joint_vel) + rrnn_output
        # Set position is a bad idea!! Hopefully ok for collecting residual data.
        # env.set_position([0,0,2],[0,0,0,1],joints=next_ob[:3], joint_vel=next_ob[3:6])
        for j, jv, m in zip(next_ob[:3], next_ob[3:6], env.motors):
          # p.setJointMotorControl2(env.Id, m,controlMode=p.POSITION_CONTROL, targetPosition=j, targetVelocity=jv, maxVelocity=8)
          p.setJointMotorControl2(env.Id, m,controlMode=p.POSITION_CONTROL, targetPosition=j, targetVelocity=jv)
        # Need to reset the joint to use torque control at the next timestep
        p.setJointMotorControlArray(env.Id, env.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(env.motor_names))
        fm_pred.append(next_ob[:6])        
      elif args.forward_residual:        
        _, _, rew, done = env.step(torque, samples[sample_pointer,:3]) 
        
        fr_rnn_output, fr_rnn_state = fr_rnn.step(np.array(env.prev_joints + env.prev_joint_vel + list(torque) + env.joints + env.joint_vel), fr_rnn_state)
        next_ob = list(fr_rnn_output) + list(samples[sample_pointer,:3])
        # print(next_ob)
        fm_pred.append(next_ob[:6])  
      else:
        next_ob, rew, done, _ = env.step(torque, samples[sample_pointer,:3])
        fm_pred.append(next_ob[:6])  
     

    else:
      torque = cmd_torque
      next_ob, rew, done, _ = env.step(torque)
      fm_pred.append(next_ob[:6])  

  
    
    fm_label.append(labels[sample_pointer,:])
    fm_target.append(list(env.joints)+list(env.joint_vel))

    pol_label.append(obs_torques[sample_pointer,:])
    pol_pred.append(torque)
    pol_target.append(cmd_torque)

    prev_done = done
    ob = next_ob
    ep_ret += rew
    ep_len += 1
    ep_steps += 1

  
    metric.append(np.mean((np.array(env.joints) - np.array(samples[sample_pointer,:3]))**2))
    torque_metric.append(np.mean((np.array(cmd_torque) - np.array(cmd_torques[sample_pointer,:3]))**2))
    if len(metric) == sample_size:
      metrics.append(np.mean(metric))
      metric = deque(maxlen=env.sample_size)
      torque_metrics.append(np.mean(torque_metric))
      torque_metric = deque(maxlen=env.sample_size)

    sample_pointer += 1
    if sample_pointer > samples.shape[0] - 1:
      sample_pointer = 0

    if ep_steps > plot_length:    
      tag = 'eval_'
      print("closeness to expert: ", args.exp)
      print("pos", np.mean(metric))
      print("torque", np.mean(torque_metric))
      
      # subplot([[np.array(fm_label)[:,0], np.array(fm_pred)[:,0]],[np.array(fm_label)[:,1], np.array(fm_pred)[:,1]],[np.array(fm_label)[:,2], np.array(fm_pred)[:,2]],[np.array(fm_label)[:,3], np.array(fm_pred)[:,3]],[np.array(fm_label)[:,4], np.array(fm_pred)[:,4]],[np.array(fm_label)[:,5], np.array(fm_pred)[:,5]]], legend=[["label","pred"]]*6, PATH=PATH + 'fm_' + tag)
      # subplot([[np.array(pol_label)[:,0], np.array(pol_pred)[:,0]],[np.array(pol_label)[:,1], np.array(pol_pred)[:,1]],[np.array(pol_label)[:,2], np.array(pol_pred)[:,2]]], legend=[["label","pred"]]*6, PATH=PATH + 'torques_' + tag)

      subplot([[np.array(fm_label)[:,0], np.array(fm_pred)[:,0], np.array(fm_target)[:,0]],[np.array(fm_label)[:,1], np.array(fm_pred)[:,1], np.array(fm_target)[:,1]],[np.array(fm_label)[:,2], np.array(fm_pred)[:,2], np.array(fm_target)[:,2]],[np.array(fm_label)[:,3], np.array(fm_pred)[:,3], np.array(fm_target)[:,3]],[np.array(fm_label)[:,4], np.array(fm_pred)[:,4], np.array(fm_target)[:,4]],[np.array(fm_label)[:,5], np.array(fm_pred)[:,5], np.array(fm_target)[:,5]]], legend=[["label","pred", "input"]]*6, PATH=PATH + 'fm_' + tag)
      subplot([[np.array(pol_label)[:,0], np.array(pol_pred)[:,0], np.array(pol_target)[:,0]],[np.array(pol_label)[:,1], np.array(pol_pred)[:,1], np.array(pol_target)[:,1]],[np.array(pol_label)[:,2], np.array(pol_pred)[:,2], np.array(pol_target)[:,2]]], legend=[["label","pred","input"]]*6, PATH=PATH + 'torques_' + tag)
      exit()
        
      fm_pred = deque(maxlen=plot_length)
      fm_label = deque(maxlen=plot_length)
      fm_target = deque(maxlen=plot_length)
      pol_pred = deque(maxlen=plot_length)
      pol_label = deque(maxlen=plot_length)
      pol_target = deque(maxlen=plot_length)
      action_buffer = deque(maxlen=3)
      ob = env.reset()

      prev_ob = ob
      
      if not args.baseline: 
        if args.eth_rnn:
          eth_model_state = [np.zeros([3,lstm_size]), np.zeros([3,lstm_size])] 
        else:
          eth_input_buf = {i:deque([0.0]*15,maxlen=15) for i in env.motor_names}
        if args.fm:
          fm_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
        elif args.residual:
          rrnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]
        elif args.forward_residual:
          fr_rnn_state = [np.zeros([1,lstm_size]), np.zeros([1,lstm_size])]

      
      ep_rets.append(ep_ret)     
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0        

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--ae', default=False, action='store_true')
  parser.add_argument('--baseline', default=False, action='store_true')
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--test_pol', default=False, action='store_true') 
  parser.add_argument('--vis', default=False, action='store_true')  
  parser.add_argument('--const_std', default=False, action='store_true')  
  parser.add_argument('--lstm_pol', default=False, action='store_true')  
  parser.add_argument('--set_pos_test', default=False, action='store_true')  
  parser.add_argument('--obs_torque', default=False, action='store_true')  
  parser.add_argument('--stand', default=False, action='store_true')  
  parser.add_argument('--residual', default=False, action='store_true')  
  parser.add_argument('--forward_residual', default=False, action='store_true')  
  parser.add_argument('--fm', default=False, action='store_true')  
  parser.add_argument('--eth_rnn', default=False, action='store_true')  
  parser.add_argument('--history', default=True, action='store_false')  
  parser.add_argument('--new_urdf', default=False, action='store_true')  
  parser.add_argument('--mpi_avail', default=True, action='store_false')  
  parser.add_argument('--add_dist', default=True, action='store_false')  
  parser.add_argument('--neg_rew', default=True, action='store_false')  
  parser.add_argument('--rand_dynamics', default=True, action='store_false')  
  parser.add_argument('--rand_residual', default=False, action='store_true')  
  parser.add_argument('--max_ts', default=50e6, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)