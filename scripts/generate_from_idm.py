# Sometimes parent folder is not in path?
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from models.rnn import RNN
from utils import *
import numpy as np
import pandas as pd
from pathlib import Path
home = str(Path.home())

# path = home + '/data/my_gz'
# path = home + '/results/hex_model/latest/gz'
# save_path = home + '/data/my_gz'
rnn = True
if rnn:
  path = home + '/data/my_rnn_real'
  save_path = home + '/data/my_rnn_real'
else:
  path = home + '/data/my_real'
  save_path = home + '/data/my_real'

labels = load_data_xlxs(DATA_PATH=path, sample_name='/my_labels')
inputs = load_data_xlxs(DATA_PATH=path, sample_name='/my_inputs')

print("first column")
print(inputs[0,:], labels[0,:])

input_size = 12
output_size = 3
rnn_length = 128
data = tf.placeholder(tf.float32, [None, rnn_length, input_size])
target = tf.placeholder(tf.float32, [None, rnn_length, output_size])

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                        intra_op_parallelism_threads=1,             
                                        gpu_options=gpu_options), graph=None)
sim_model = RNN('model', sess, data, target)

WEIGHTS_PATH = home + '/data/pb_id/weights_/'
sim_model.load(WEIGHTS_PATH)

print(inputs.shape, labels.shape)
if rnn:
  rnn_input = inputs
else:
  # History is 5 things, state length is 6, want the 5th entry, so 4*6:6+4*6
  rnn_input = np.concatenate((inputs[:-1,4*6:6+4*6],labels), axis=1)

dim = rnn_length*(((rnn_input.shape[0]-1)*input_size)//int(rnn_length*input_size))
print(rnn_input.shape)
print(rnn_input[:10, :])
rnn_input = rnn_input[:dim,:].reshape(-1,rnn_length,input_size)
print(rnn_input.shape)
print(rnn_input[0,:10,:])
output = sim_model.step(rnn_input).reshape(-1,output_size)
print(output.shape)

if rnn:
  inputs = np.concatenate((inputs[:,:6],labels), axis=1)
  print("new inputs shape", inputs.shape)

inputs = inputs[:dim,:]
train_labels, test_labels = output[:int(output.shape[0]*0.95),:],output[int(output.shape[0]*0.95):,:] 
train_inputs, test_inputs = inputs[:int(output.shape[0]*0.95),:],inputs[int(output.shape[0]*0.95):,:] 
print(train_labels[0,:], train_inputs[0,:])
print(train_inputs.shape, train_labels.shape, test_inputs.shape, test_labels.shape)
df = pd.DataFrame (np.array(train_inputs))  
df.to_excel(save_path + '/train_inputs.xlsx', index=False)
df = pd.DataFrame (np.array(test_inputs))  
df.to_excel(save_path + '/test_inputs.xlsx', index=False)
df = pd.DataFrame (np.array(train_labels))  
df.to_excel(save_path + '/train_labels.xlsx', index=False)
df = pd.DataFrame (np.array(test_labels))  
df.to_excel(save_path + '/test_labels.xlsx', index=False)
df = pd.DataFrame (np.array(train_labels))  


