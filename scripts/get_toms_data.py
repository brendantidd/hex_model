import pandas as pd
import os
from glob import glob   
import re 
from pathlib import Path
from utils import *
home = str(Path.home())
from collections import deque

DATA_PATH = home + '/data/iros_data/25f1569c-e982-4486-bbca-5fa58bccd5f1.csv'
all_data = pd.read_csv(DATA_PATH)

# for d in all_data:
#   print(d)

start_idx = 200000
stop_idx = 500000
tip_input = np.array(all_data['TEST/input/position/x'])[start_idx:stop_idx]

prev_t = tip_input[0]
first_tip_change = 0
for i,t in enumerate(tip_input):
  if t != prev_t:
    first_tip_change = i + start_idx
    break
  prev_t = t

idx = [i for i in range(first_tip_change, stop_idx, 4)]
print(idx[:20])

# eth = False
eth = True
if eth:
  # coxa_input_torques =   np.array([all_data['/TEST_coxa/mosi/cmd__joint__effort__nm'])[idx]
  # femur_input_torques = np.array([all_data['/TEST_femur/mosi/cmd__joint__effort__nm'])[idx]
  # tibia_input_torques = np.array([all_data['/TEST_tibia/mosi/cmd__joint__effort__nm'])[idx]

  # coxa_output_torques =   np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'])[idx]
  # femur_output_torques = np.array([all_data['/TEST_femur/miso/js__joint__effort__Nm'])[idx]
  # tibia_output_torques = np.array([all_data['/TEST_tibia/miso/js__joint__effort__Nm'])[idx]

  # coxa_input_pos = np.array([all_data['/TEST_coxa/miso/js__joint__position__rad'])[idx]
  # femur_input_pos = np.array([all_data['/TEST_femur/miso/js__joint__position__rad'])[idx]
  # tibia_input_pos = np.array([all_data['/TEST_tibia/miso/js__joint__position__rad'])[idx]
                        
  # coxa_input_vel = np.array([all_data['/TEST_coxa/miso/js__joint__velocity__radps'])[idx]
  # femur_input_vel = np.array([all_data['/TEST_femur/miso/js__joint__velocity__radps'])[idx]
  # tibia_input_vel = np.array([all_data['/TEST_tibia/miso/js__joint__velocity__radps'])[idx]

  input_torques = np.array([all_data['/TEST_coxa/mosi/cmd__joint__effort__nm'][idx],
                            all_data['/TEST_femur/mosi/cmd__joint__effort__nm'][idx],
                            all_data['/TEST_tibia/mosi/cmd__joint__effort__nm'][idx]]).T

  output_torques = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'][idx],
                            all_data['/TEST_femur/miso/js__joint__effort__Nm'][idx],
                            all_data['/TEST_tibia/miso/js__joint__effort__Nm'][idx]]).T
                            
  input_pos = np.array([all_data['/TEST_coxa/miso/js__joint__position__rad'][idx],
                        all_data['/TEST_femur/miso/js__joint__position__rad'][idx],
                        all_data['/TEST_tibia/miso/js__joint__position__rad'][idx]]).T

  input_vel = np.array([ all_data['/TEST_coxa/miso/js__joint__velocity__radps'][idx],
                        all_data['/TEST_femur/miso/js__joint__velocity__radps'][idx],
                        all_data['/TEST_tibia/miso/js__joint__velocity__radps'][idx]]).T

  print(input_torques.shape)
  # history of 5? 
  motor_names = ['coxa','femur','tibia']
  eth_input_buf = {i:deque([0.0]*15,maxlen=15) for i in motor_names}
  eth_inputs = []
  eth_labels = []
  for i,m in enumerate(motor_names):
    for j,v,it,dt in zip(input_pos[:,i], input_vel[:,i], input_torques[:,i], output_torques[:,i]):
      eth_input_buf[m].extend([j,v,it])
      eth_inputs.append(np.array(eth_input_buf[m]))
      eth_labels.append(dt)
  print(np.array(eth_inputs).shape, np.array(eth_labels).shape)
  save_path = home + '/data/iros_data/real/300/'
  np.save(save_path + 'eth_inputs.npy', np.array(eth_inputs))
  np.save(save_path + 'eth_labels.npy', np.array(eth_labels))
else:
  input_torques = np.array([all_data['/TEST_coxa/mosi/cmd__joint__effort__nm'][idx],
                            all_data['/TEST_femur/mosi/cmd__joint__effort__nm'][idx],
                            all_data['/TEST_tibia/mosi/cmd__joint__effort__nm'][idx]]).T

  output_torques = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'][idx],
                            all_data['/TEST_femur/miso/js__joint__effort__Nm'][idx],
                            all_data['/TEST_tibia/miso/js__joint__effort__Nm'][idx]]).T
                            
  input_pos = np.array([all_data['/TEST_coxa/miso/js__joint__position__rad'][idx],
                        all_data['/TEST_femur/miso/js__joint__position__rad'][idx],
                        all_data['/TEST_tibia/miso/js__joint__position__rad'][idx]]).T

  input_vel = np.array([ all_data['/TEST_coxa/miso/js__joint__velocity__radps'][idx],
                        all_data['/TEST_femur/miso/js__joint__velocity__radps'][idx],
                        all_data['/TEST_tibia/miso/js__joint__velocity__radps'][idx]]).T

  print(input_torques.shape, input_pos.shape, output_torques.shape)


  inputs = np.concatenate([input_pos[:-1,:], input_vel[:-1,:], input_torques[:-1,:]], axis=1)
  labels = np.concatenate([input_pos[1:,:], input_vel[1:,:]], axis=1)

  save_path = home + '/data/iros_data/real/300/'
  np.save(save_path + 'inputs_orig.npy', inputs)
  np.save(save_path + 'labels_orig.npy', labels)

  print(inputs.shape, labels.shape)
  dim = (inputs.shape[0]//513)*513
  print(inputs.shape[0], dim)
  inputs = inputs[:dim,:]
  labels = labels[:dim,:]
  inputs = inputs.reshape(-1,513,9)
  labels = labels.reshape(-1,513,6)

  np.save(save_path + 'inputs.npy', inputs)
  np.save(save_path + 'labels.npy', labels)


  print(inputs.shape, labels.shape)


  subplot([[inputs[0,:,0]],[inputs[0,:,1]],[inputs[0,:,2]],[inputs[0,:,3]],[inputs[0,:,4]],[inputs[0,:,5]],[inputs[0,:,6]],[inputs[0,:,7]],[inputs[0,:,8]]], x_initial=start_idx)

