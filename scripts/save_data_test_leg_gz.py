import pandas as pd 
import os
from glob import glob   
import re
from pathlib import Path
home = str(Path.home())
import numpy as np
from utils import *

# data_type = 'pb_'
data_type = ''

path = home + '/data/gz_data/'
save_path = home + '/data/gz_test_leg_pos'

samples = pd.read_csv(path + 'test_leg_samples.csv', header=None).as_matrix()
inputs = np.concatenate([samples[:-1,:3],samples[1:,9:12]], axis=1)
# labels = np.array(samples[1:,:6]) - np.array(samples[:-1,:6])
labels = np.array(samples[1:,:3])
print(samples[:5,:12])

train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:],labels[int(labels.shape[0]*0.9):,:] 
train_inputs, test_inputs = inputs[:int(labels.shape[0]*0.9),:],inputs[int(labels.shape[0]*0.9):,:] 

df = pd.DataFrame (np.array(train_inputs))  
df.to_excel(save_path + '/train_'+ data_type + 'inputs.xlsx', index=False)
df = pd.DataFrame (np.array(test_inputs))  
df.to_excel(save_path + '/test_'+ data_type + 'inputs.xlsx', index=False)
df = pd.DataFrame (np.array(train_labels))  
df.to_excel(save_path + '/train_'+ data_type + 'labels.xlsx', index=False)
df = pd.DataFrame (np.array(test_labels))  
df.to_excel(save_path + '/test_'+ data_type + 'labels.xlsx', index=False)
df = pd.DataFrame (np.array(train_labels))  

# inputs = np.concatenate([samples[:-1,:6],samples[1:,:6]], axis=1)
# labels = samples[:-1,9:12]

# train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:],labels[int(labels.shape[0]*0.9):,:] 
# train_inputs, test_inputs = inputs[:int(labels.shape[0]*0.9),:],inputs[int(labels.shape[0]*0.9):,:] 

# print(train_inputs.shape, train_labels.shape, test_inputs.shape, test_labels.shape)
# df = pd.DataFrame (np.array(train_inputs))  
# df.to_excel(save_path + '/train_'+ data_type + 'inputs_id.xlsx', index=False)
# df = pd.DataFrame (np.array(test_inputs))  
# df.to_excel(save_path + '/test_'+ data_type + 'inputs_id.xlsx', index=False)
# df = pd.DataFrame (np.array(train_labels))  
# df.to_excel(save_path + '/train_'+ data_type + 'labels_id.xlsx', index=False)
# df = pd.DataFrame (np.array(test_labels))  
# df.to_excel(save_path + '/test_'+ data_type + 'labels_id.xlsx', index=False)
# df = pd.DataFrame (np.array(train_labels))  


