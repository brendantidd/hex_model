import pandas as pd
import os
from glob import glob   
import re 
from collections import deque
from pathlib import Path
from utils import *
home = str(Path.home())
# path = home + '/data/test_leg_15_11_all/'
# path = home + '/data/test_leg_small'
# path = home + '/data/my_rnn_real'
path = home + '/data/my_fm'
# DATA_PATH = home + '/data/Test Leg/2019-11-12_10-39-14_test_leg_accelerations/6a6d7faa-9985-4c0f-94c9-b0bb26c20aac.csv'
# DATA_PATH = home + '/data/2019-11-13_10-22-13_test_leg_traj_comp_task_impedance_new/159dd830-bd87-485a-a0e8-0f9cfd9eeb93.csv'
DATA_PATH = home + '/data/6310b0ec-2759-4ae5-b5af-6db50b34eed7.csv'
# DATA_PATH = home + '/data/rt_control.csv-x04.csv'
# DATA_PATH = home + '/data/Test Leg/2019-11-11_11-15-44_test_leg_no_ID/8f15fe4b-353d-44fc-9cb0-0b3f6063c179.csv'
# DATA_PATH = home + '/data/Test Leg/2019-11-11_10-42-58_test_leg_new_gains/5acc5475-3647-478f-a409-df5218d3695e.csv'
all_data = pd.read_csv(DATA_PATH)
print(all_data.shape)
# print(all_data.head(0))
# for d in all_data:
  # print(all_data[d][300000:300005])
  # print(d)
# options = [
# '/TEST_coxa/miso/js__joint__position__rad',
# '/TEST_coxa/miso/js__joint__velocity__radps',
# '/TEST_coxa/miso/js__joint__effort__Nm',
# '/TEST_coxa/mosi/cmd__joint__position__rad',
# '/TEST_coxa/mosi/cmd__joint__velocity__radps',
# '/TEST_coxa/mosi/cmd__joint__effort__nm',
# '/TEST_coxa/miso/actuator__force__N',
# '/TEST_coxa/miso/actuator__position__m',
# '/TEST_coxa/miso/actuator__velocity__mps',
# '/TEST_coxa/miso/motor__position__Rad',
# '/TEST_coxa/miso/motor__velocity__Radps']
# options = [
# '/TEST_coxa/miso/js__joint__position__rad',
# '/TEST_coxa/miso/js__joint__velocity__radps',
# '/TEST_coxa/miso/js__joint__effort__Nm',
# '/TEST_coxa/mosi/cmd__joint__position__rad',
# '/TEST_coxa/mosi/cmd__joint__velocity__radps',
# '/TEST_coxa/mosi/cmd__joint__effort__nm',
# '/TEST_coxa/miso/actuator__force__N',
# '/TEST_coxa/miso/actuator__position__m',
# '/TEST_coxa/miso/actuator__velocity__mps',
# '/TEST_coxa/miso/motor__position__Rad',
# '/TEST_coxa/miso/motor__velocity__Radps']

# Torques
# /TEST_tibia/mosi/cmd__joint__effort__nm
input_post = '/mosi/cmd__joint__effort__nm'
output_post = '/miso/js__joint__effort__Nm'
# Positions
# input_post = '/mosi/cmd__joint__effort__nm'
# output_post = '/miso/js__joint__effort__Nm'
# inputs = np.array([all_data['/TEST_coxa/mosi/cmd__joint__effort__nm'],all_data['/TEST_femur/mosi/cmd__joint__effort__nm'],all_data['/TEST_tibia/mosi/cmd__joint__effort__nm']]).T
# labels = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'],all_data['/TEST_femur/miso/js__joint__effort__Nm'],all_data['/TEST_tibia/miso/js__joint__effort__Nm']]).T
# inputs = np.array([all_data['/TEST_coxa' + input_post],all_data['/TEST_femur' + input_post],all_data['/TEST_tibia' + input_post]]).T
# labels = np.array([all_data['/TEST_coxa' + output_post],all_data['/TEST_femur' + output_post],all_data['/TEST_tibia' + output_post]]).T

# labels = np.array([all_data['/TEST_coxa/miso/actuator__force__N'],all_data['/TEST_femur/miso/actuator__force__N'],all_data['/TEST_tibia/miso/actuator__force__N']]).T
# save_data = False
if False:
  # Temp stuff -------------\----------------------------------------------------------------------------------------------------------
  # legs = ['AR_coxa','AL_coxa','BL_coxa','CR_coxa','CL_coxa','AR_femur','AL_femur','BL_femur','CR_femur','CL_femur','AR_tibia','AL_tibia','BL_tibia','CR_tibia','CL_tibia']
  # legs = ['AR_coxa','AL_coxa','BL_coxa','CR_coxa','AR_femur','AL_femur','BL_femur','CR_femur','AR_tibia','AL_tibia','BL_tibia','CR_tibia','CL_tibia','CL_femur']
  legs = ['AR_femur']
  start_idx, stop_idx = 12000, 50000 
  temps = []
  currents = []
  efforts = []
  for leg in legs:
    x1 = np.array(all_data['/'+ leg + '/miso/motor__core_temp_est__C'][start_idx:stop_idx,], dtype=np.float32).reshape(-1,)
    x2 = np.array(all_data['/'+ leg + '/miso/motor__current__A'][start_idx:stop_idx,], dtype=np.float32).reshape(-1,)
    x3 = np.array(all_data['/'+ leg + '/mosi/cmd__joint__effort__nm'][start_idx:stop_idx,], dtype=np.float32).reshape(-1,)
    # subplot([[x1],[x2],[x3]], legend=[[leg + ' core temp'],['current'],['cmd torque']], PATH=path + '/'+leg + '_')  
    subplot([[x1],[x2],[x3]], legend=[[leg + ' core temp'],['current'],['cmd torque']])  
    temps.extend(x1)
    currents.extend(x2)
    efforts.extend(x3)
  # exit()
  inputs = np.array(efforts).reshape(-1,1)
  labels = np.array(temps).reshape(-1,1)
  print(inputs.shape, labels.shape)
  train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:],labels[int(labels.shape[0]*0.9):,:] 
  train_inputs, test_inputs = inputs[:int(labels.shape[0]*0.9),:],inputs[int(labels.shape[0]*0.9):,:] 
  data_type = ''
  df = pd.DataFrame (np.array(train_inputs))  
  df.to_excel(path + '/train_'+ data_type + 'inputs.xlsx', index=False)
  df = pd.DataFrame (np.array(test_inputs))  
  df.to_excel(path + '/test_'+ data_type + 'inputs.xlsx', index=False)
  df = pd.DataFrame (np.array(train_labels))  
  df.to_excel(path + '/train_'+ data_type + 'labels.xlsx', index=False)
  df = pd.DataFrame (np.array(test_labels))  
  df.to_excel(path + '/test_'+ data_type + 'labels.xlsx', index=False)
  print(np.array(temps).shape, np.array(currents).shape, np.array(efforts).shape)
  exit()
# ------------------------------------------------------------------------------------------------------------------------------------- 



# /CL_femur/miso/motor__core_temp_est__C
# temp = np.array(all_data['/TEST_coxa/miso/js__joint__position__rad']).reshape(-1,1)
# np.array((all_data['/CR_coxa/miso/js__joint__effort__Nm']).reshape(-1,1)
save_data = True
if save_data:
#   /TEST_femur/miso/js__joint__position__rad
# /TEST_femur/miso/js__joint__velocity__radps
# /TEST_femur/miso/js__joint__effort__Nm
  # coxa = np.array(all_data['/TEST_coxa/miso/js__joint__position__rad']).reshape(-1,1)
  # femur = np.array(all_data['/TEST_femur/miso/js__joint__position__rad']).reshape(-1,1)
  # tibia = np.array(all_data['/TEST_tibia/miso/js__joint__position__rad']).reshape(-1,1)
  # coxa_vel = np.array(all_data['/TEST_coxa/miso/js__joint__velocity__radps']).reshape(-1,1)
  # femur_vel = np.array(all_data['/TEST_femur/miso/js__joint__velocity__radps']).reshape(-1,1)
  # tibia_vel = np.array(all_data['/TEST_tibia/miso/js__joint__velocity__radps']).reshape(-1,1)
  # des_torqs = np.array([all_data['/TEST_coxa/mosi/cmd__joint__effort__nm'],all_data['/TEST_femur/mosi/cmd__joint__effort__nm'],all_data['/TEST_tibia/mosi/cmd__joint__effort__nm']]).T
  # act_torqs = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'],all_data['/TEST_femur/miso/js__joint__effort__Nm'],all_data['/TEST_tibia/miso/js__joint__effort__Nm']]).T
  # vels = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'],all_data['/TEST_femur/miso/js__joint__effort__Nm'],all_data['/TEST_tibia/miso/js__joint__effort__Nm']]).T
  my = True
  rnn = True
  if my:
    poss = np.array([all_data['/TEST_coxa/miso/js__joint__position__rad'],all_data['/TEST_femur/miso/js__joint__position__rad'],all_data['/TEST_tibia/miso/js__joint__position__rad']]).T
    vels = np.array([all_data['/TEST_coxa/miso/js__joint__velocity__radps'],all_data['/TEST_femur/miso/js__joint__velocity__radps'],all_data['/TEST_tibia/miso/js__joint__velocity__radps']]).T
    des_torqs = np.array([all_data['/TEST_coxa/mosi/cmd__joint__effort__nm'],all_data['/TEST_femur/mosi/cmd__joint__effort__nm'],all_data['/TEST_tibia/mosi/cmd__joint__effort__nm']]).T
    ob_torqs = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'],all_data['/TEST_femur/miso/js__joint__effort__Nm'],all_data['/TEST_tibia/miso/js__joint__effort__Nm']]).T
    print(poss.shape, vels.shape, des_torqs.shape)
    start_idx, stop_idx = 85000, 385000
    if not rnn:
      desired_torque_buf = deque([0.0]*3*5,maxlen=5*3)
      prev_state_buf = deque([0.0]*6*5,maxlen=5*6)
      labels = []
      inputs = []
      for n, (pos, vel, desired_torq) in enumerate(zip(poss[start_idx:stop_idx,:], vels[start_idx:stop_idx,:], des_torqs[start_idx:stop_idx,:])):
        # print(pos, vel, desired_torq)
        # print(list(pos) + list(vel))
        # labels buffer is the next state, start recording labels at the next step
        if n < (stop_idx- 1 - start_idx):
          prev_state_buf.extend(list(pos) + list(vel))
          desired_torque_buf.extend(desired_torq)
          inputs.append(list(prev_state_buf) + list(desired_torque_buf))
        if n > 0:
          labels.append(list(pos) + list(vel))
    else:
      pos_and_vels = np.concatenate((poss[start_idx:stop_idx, :], vels[start_idx:stop_idx, :]), axis=1)
      print("pos and vels", pos_and_vels.shape)
      # Torque model
      # inputs = np.concatenate((pos_and_vels[:-1,:], pos_and_vels[1:,:]),axis=1)
      # labels = des_torqs[start_idx:stop_idx-1, :]
      # Forward model
      inputs = np.concatenate((pos_and_vels[:-1,:], des_torqs[start_idx:stop_idx-1, :]),axis=1)
      labels = pos_and_vels[1:,:]
      observed = ob_torqs[1+start_idx:stop_idx, :]
  print(np.array(inputs).shape, np.array(labels).shape)
  df = pd.DataFrame (np.array(inputs))  
  df.to_excel(path + '/inputs.xlsx', index=False)
  df = pd.DataFrame (np.array(labels))  
  df.to_excel(path + '/labels.xlsx', index=False)
  df = pd.DataFrame (np.array(observed))  
  df.to_excel(path + '/observed.xlsx', index=False)
  exit()
  # ===============================================================================================================================
  eth = False
  if eth:
    length = 5
    labels = []
    inputs = []
    start_idx, stop_idx = 80000, 400000
    for name in ['coxa', 'femur', 'tibia']:
      temp_torq_buf = deque([0.0 for _ in range(length)],maxlen=length)
      temp_vel_buf = deque([0.0 for _ in range(length)],maxlen=length)
      desired_torqs = np.array(all_data['/TEST_' + name + '/mosi/cmd__joint__effort__nm'])
      observed_torqs = np.array(all_data['/TEST_' + name + '/miso/js__joint__effort__Nm'])
      vels = np.array(all_data['/TEST_' + name + '/miso/js__joint__velocity__radps'])
      for n,(des_torq, observed_torq, vel) in enumerate(zip(desired_torqs[start_idx:stop_idx], observed_torqs[start_idx:stop_idx], vels[save_data:stop_idx])):
        temp_torq_buf.append(des_torq)
        temp_vel_buf.append(vel)
        inputs.append(list(temp_torq_buf) + list(temp_vel_buf))
        labels.append(observed_torq)
      print(name)
      print(np.array(inputs).shape)
      print(np.array(labels).shape)
    
    df = pd.DataFrame (np.array(inputs))  
    df.to_excel(path + '/train_inputs.xlsx', index=False)
    df = pd.DataFrame (np.array(labels))  
    df.to_excel(path + '/train_labels.xlsx', index=False)
    
    labels = []
    inputs = []
    start_idx, stop_idx = 400000, 410000
    for name in ['coxa', 'femur', 'tibia']:
      temp_torq_buf = deque([0.0 for _ in range(length)],maxlen=length)
      temp_vel_buf = deque([0.0 for _ in range(length)],maxlen=length)
      desired_torqs = np.array(all_data['/TEST_' + name + '/mosi/cmd__joint__effort__nm'])
      observed_torqs = np.array(all_data['/TEST_' + name + '/miso/js__joint__effort__Nm'])
      vels = np.array(all_data['/TEST_' + name + '/miso/js__joint__velocity__radps'])
      for n,(des_torq, observed_torq, vel) in enumerate(zip(desired_torqs[start_idx:stop_idx], observed_torqs[start_idx:stop_idx], vels[save_data:stop_idx])):
        temp_torq_buf.append(des_torq)
        temp_vel_buf.append(vel)
        inputs.append(list(temp_torq_buf) + list(temp_vel_buf))
        labels.append(observed_torq)
      print(name)
      print(np.array(inputs).shape)
      print(np.array(labels).shape)
    
    df = pd.DataFrame (np.array(inputs))  
    df.to_excel(path + '/my_inputs.xlsx', index=False)
    df = pd.DataFrame (np.array(labels))  
    df.to_excel(path + '/my_labels.xlsx', index=False)
    exit()
  # ==================================================================================================================================
  # tor_error_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
  # vel_buf = {n:deque([0 for _ in range(5)],maxlen=5) for n in ['coxa','femur','tibia']}
  # # label_torques = {n:0.0 for n in ['coxa','femur','tibia']}
  # vels = 
  # for des_tor, act_torq, vel in range(des_torqs, act_torqs, vels):
  #   for n,name in enumerate(['coxa', 'femur','tibia']):
  #     pos_error_buf[name].append(des_torq[n])
  #     vel_buf[name].append(joint_vel[n])
  #     inputs = np.array(list(pos_error_buf[name]) + list(vel_buf[name]))
  #     labels = torq[n]
  # print(np.array(inputs).shape, np.array(labels).shape)
    # for j in range(i):
    #   pos_temp_deque.append(coxa[])
    #   velpos_temp_deque.append(coxa[])
    #   inputs.append(list(pos_temp_deque) + list(vel_temp_deque))
    #   outputs.append(act_torq)

  pos = np.concatenate([coxa,femur,tibia], axis=1)
  pos_and_vel = np.concatenate([coxa,femur,tibia,coxa_vel,femur_vel,tibia_vel], axis=1)
  print(coxa.shape, femur.shape, tibia.shape, coxa_vel.shape, femur_vel.shape, tibia_vel.shape, des_torq.shape, pos_and_vel.shape)
  # print(des_torq[80001:,:].shape, pos_and_vel[80000:-1,:].shape)
  # inputs = np.concatenate([pos_and_vel[80000:379999,:],des_torq[80001:380000,:]], axis=1)
  # labels = np.array(pos_and_vel[80001:380000,:]) - np.array(pos_and_vel[80000:379999,:]) 
  start_idx, stop_idx = 80000, 480000
  inputs = np.concatenate([pos[start_idx:stop_idx-1,:],des_torq[start_idx+1:stop_idx,:]], axis=1)
  labels = np.array(pos[start_idx+1:stop_idx,:]) - np.array(pos[start_idx:stop_idx-1,:]) 
  print(inputs.shape, labels.shape)

  train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:],labels[int(labels.shape[0]*0.9):,:] 
  train_inputs, test_inputs = inputs[:int(labels.shape[0]*0.9),:],inputs[int(labels.shape[0]*0.9):,:] 
  data_type = ''
  print(train_inputs.shape, test_inputs.shape)
  df = pd.DataFrame (np.array(train_inputs))  
  df.to_excel(path + '/train_'+ data_type + 'inputs.xlsx', index=False)
  df = pd.DataFrame (np.array(test_inputs))  
  df.to_excel(path + '/test_'+ data_type + 'inputs.xlsx', index=False)
  df = pd.DataFrame (np.array(train_labels))  
  df.to_excel(path + '/train_'+ data_type + 'labels.xlsx', index=False)
  df = pd.DataFrame (np.array(test_labels))  
  df.to_excel(path + '/test_'+ data_type + 'labels.xlsx', index=False)

  torq = np.array([all_data['/TEST_coxa/miso/js__joint__effort__Nm'],all_data['/TEST_femur/miso/js__joint__effort__Nm'],all_data['/TEST_tibia/miso/js__joint__effort__Nm']]).T
  # exp_torq = np.array([all_data['/TEST_coxa/miso/js__joint__velocity__radps'],all_data['/TEST_femur/miso/js__joint__velocity__radps'],all_data['/TEST_tibia/miso/js__joint__velocity__radps']]).T
  # print(pos.shape, vel.shape, des_torq.shape, torq.shape)
  # all_pos_vel = np.concatenate([pos,vel,des_torq, torq],axis=1)
  # print(all_pos_vel.shape)
  # np.save(home + '/data/test_leg_15_11/sample.npy', all_pos_vel)

  # path = home + '/data/eth_test_leg'
  # df = pd.DataFrame(labels)
  # df.to_excel(path + '/labels.xlsx', index=False)
  # df = pd.DataFrame(inputs)
  # df.to_excel(path + '/inputs.xlsx', index=False)

print(inputs.shape)
# pos1, pos2 = 340000, 342000
# pos1, pos2 = 293700, 295000
pos1, pos2 = 0, 400000
# pos1, pos2 = 290000,300000
x1,x2,x3 = inputs[pos1:pos2,0],inputs[pos1:pos2,1],inputs[pos1:pos2,2]
y1,y2,y3 = labels[pos1:pos2,0],labels[pos1:pos2,1],labels[pos1:pos2,2]
subplot([[x1,y1],[x2,y2],[x3,y3]], legend=[['input','label'],['input','label'],['input','label'],['input','label']], title=['coxa','femur','tibia'], x_initial=pos1)

x1,x2,x3,x4 = inputs[1000:1500,1],inputs[1000:1500,2],inputs[1000:1500,3],inputs[1000:1500,4]
y1,y2,y3,y4 = labels[1000:1500,1],labels[1000:1500,2],labels[1000:1500,3],labels[1000:1500,4]
# plot([x1,x2,x3,x4])
# print(x1)
# print(y1)
subplot([[x1,y1],[x2,y2],[x3,y3],[x4,y4]])

# for option in options:
  # inputs
