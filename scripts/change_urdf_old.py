# !/usr/bin/env python

'''
Changes URDF. 
joint position, 
link mass, 
link mass position
'''
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
import numpy as np
import pybullet as p
# path = '/home/brendan/Dropbox/robot/mybot_ws/src/hex_model/assets/test_leg.urdf'

class ChangeURDF:
  def __init__(self, path='assets/test_leg.urdf', ac_size=3, debug=False, normal=True, rank=1, env=None):
    self.path = path
    self.debug = debug
    self.normal = normal
    self.rank = rank
    self.env = env
    self.ac_size = ac_size
    self.stds = 0.0
    self.joint_names = ['coxa', 'femur', 'tibia']
    self.xyz = ['x','y','z']
    if not self.normal:
      self.pms = {
        'friction':{'coxa':0.2,'femur':0.2,'tibia':0.2},
        'damping':{'coxa':0.2,'femur':0.2,'tibia':0.2},
        'joint_pos':{'coxa':0.02,'femur':0.02,'tibia':0.02},
        'mass_pos':{'coxa':0.02,'femur':0.02,'tibia':0.02},
        'mass':{'coxa':0.15,'femur':0.15,'tibia':0.15}
        }
    # else:
      # self.pms = {
      #   'friction':{'coxa':{'mu': 0.0, 'std': 0.5},'femur':{'mu': 0.0, 'std': 0.5},'tibia':{'mu': 0.0, 'std': 0.5}},
      #   'damping':{'coxa':{'mu': 0.0, 'std': 0.5},'femur':{'mu': 0.0, 'std': 0.5},'tibia':{'mu': 0.0, 'std': 0.5}},
      #   'joint_pos':{'coxa':{'mu': 0.0, 'std': 0.005},'femur':{'mu': 0.0, 'std': 0.005},'tibia':{'mu': 0.0, 'std': 0.005}},
      #   'mass_pos':{'coxa':{'mu': 0.0, 'std': 0.005},'femur':{'mu': 0.0, 'std': 0.005},'tibia':{'mu': 0.0, 'std': 0.005}},
      #   'mass':{'coxa':{'mu': 0.0, 'std': 0.2},'femur':{'mu': 0.0, 'std': 0.2},'tibia':{'mu': 0.0, 'std': 0.1}}
      #   }
      # self.pms = {
      #   'damping':{'coxa':{'mu': 0.0, 'std': 0.5},'femur':{'mu': 0.0, 'std': 0.5},'tibia':{'mu': 0.0, 'std': 0.5}},
      #   'inertia':{'coxa':{'x':{'mu': 0.0, 'std': 0.005},'y':{'mu': 0.0, 'std': 0.005},'z':{'mu': 0.0, 'std': 0.005}},
      #             'femur':{'x':{'mu': 0.0, 'std': 0.005},'y':{'mu': 0.0, 'std': 0.005},'z':{'mu': 0.0, 'std': 0.005}}, 
      #             'tibia':{'x':{'mu': 0.0, 'std': 0.005},'y':{'mu': 0.0, 'std': 0.005},'z':{'mu': 0.0, 'std': 0.005}}},
      #   'mass':{'coxa':{'mu': 1.2, 'std': 0.2},'femur':{'mu': 1.2, 'std': 0.2},'tibia':{'mu': 0.8, 'std': 0.1}}
      #   }

      # {'damping': {'coxa': {'mu': 0.3176794571690582, 'std': 0.1}, 'femur': {'mu': 0.09986817981836686, 'std': 0.1}, 'tibia': {'mu': 0.3689563996456108, 'std': 0.1}}, 'inertia': {'coxa': {'x': {'mu': 0.04403776645786138, 'std': 0.01}, 'y': {'mu': 0.5667133817003311, 'std': 0.01}, 'z': {'mu': 0.004506825930682037, 'std': 0.01}}, 'femur': {'x': {'mu': 0.009155808001085822, 'std': 0.01}, 'y': {'mu': 0.030054355912201836, 'std': 0.01}, 'z': {'mu': 0.038802173957765654, 'std': 0.01}}, 'tibia': {'x': {'mu': 0.00999874900145778, 'std': 0.01}, 'y': {'mu': 0.04563659178942328, 'std': 0.01}, 'z': {'mu': 0.7243076936207291, 'std': 0.01}}}, 'mass': {'coxa': {'mu': 2.608330188151605, 'std': 0.1}, 'femur': {'mu': 1.2211437875300508, 'std': 0.1}, 'tibia': {'mu': 0.714188505260358, 'std': 0.1}}}


      # 1020.5276731985452
      # {'damping': {'coxa': {'mu': 0.14750835443324656, 'std': 0.01}, 'femur': {'mu': 0.011815727325006506, 'std': 0.01}, 'tibia': {'mu': 0.1662451609576731, 'std': 0.01}}, 'inertia': {'coxa': {'x': {'mu': 0.007267512075428625, 'std': 0.001}, 'y': {'mu': 0.011851693856231334, 'std': 0.001}, 'z': {'mu': 0.009190862267439754, 'std': 0.001}}, 'femur': {'x': {'mu': 0.00368250745898869, 'std': 0.001}, 'y': {'mu': 0.009095532812301303, 'std': 0.001}, 'z': {'mu': 0.02895698072680119, 'std': 0.001}}, 'tibia': {'x': {'mu': 0.010234535560590818, 'std': 0.001}, 'y': {'mu': 0.000543225640379844, 'std': 0.001}, 'z': {'mu': 0.07093126339849662, 'std': 0.001}}}, 'mass': {'coxa': {'mu': 1.1944665687922682, 'std': 0.01}, 'femur': {'mu': 1.2063750491567915, 'std': 0.01}, 'tibia': {'mu': 0.7329006953366785, 'std': 0.01}}}

      # 1289.1339786928718 (40,) 
      # self.pms = {'damping': {'coxa': {'mu': 0.062452492133458146, 'std': 0.01}, 'femur': {'mu': 0.009044116480109027, 'std': 0.01}, 'tibia': {'mu': 0.16752150787365022, 'std': 0.01}}, 'inertia': {'coxa': {'x': {'mu': 0.01383905078487518, 'std': 0.001}, 'y': {'mu': 0.0012809267120218895, 'std': 0.001}, 'z': {'mu': 0.007713626062014764, 'std': 0.001}}, 'femur': {'x': {'mu': 0.007923771051626824, 'std': 0.001}, 'y': {'mu': 0.011087935127227312, 'std': 0.001}, 'z': {'mu': 0.024017868961187863, 'std': 0.001}}, 'tibia': {'x': {'mu': 0.01255723739806823, 'std': 0.001}, 'y': {'mu': 0.008102492367929325, 'std': 0.001}, 'z': {'mu': 0.08910304486233525, 'std': 0.001}}}, 'mass': {'coxa': {'mu': 1.126083451761888, 'std': 0.01}, 'femur': {'mu': 1.0371200553651279, 'std': 0.01}, 'tibia': {'mu': 0.706005215209589, 'std': 0.01}}, 'delay':{'mu':2,'std':1}}

      # self.pms = {'damping': {'coxa': {'mu': 0.062452492133458146, 'std': 0.1}, 'femur': {'mu': 0.009044116480109027, 'std': 0.1}, 'tibia': {'mu': 0.16752150787365022, 'std': 0.01}}, 'inertia': {'coxa': {'x': {'mu': 0.01383905078487518, 'std': 0.001}, 'y': {'mu': 0.0012809267120218895, 'std': 0.001}, 'z': {'mu': 0.007713626062014764, 'std': 0.001}}, 'femur': {'x': {'mu': 0.007923771051626824, 'std': 0.001}, 'y': {'mu': 0.011087935127227312, 'std': 0.001}, 'z': {'mu': 0.024017868961187863, 'std': 0.001}}, 'tibia': {'x': {'mu': 0.01255723739806823, 'std': 0.001}, 'y': {'mu': 0.008102492367929325, 'std': 0.001}, 'z': {'mu': 0.08910304486233525, 'std': 0.001}}}, 'mass': {'coxa': {'mu': 1.126083451761888, 'std': 0.1}, 'femur': {'mu': 1.0371200553651279, 'std': 0.1}, 'tibia': {'mu': 0.706005215209589, 'std': 0.1}}, 'delay':{'mu':2,'std':1}}

    # self.friction = {'coxa':4.0,'femur':4.0,'tibia':4.0}
    # self.damping = {'coxa':4.0,'femur':4.0,'tibia':4.0}
    # self.joint_pos = {'coxa':0.02,'femur':0.02,'tibia':0.02}
    # self.mass_pos = {'coxa':0.02,'femur':0.02,'tibia':0.02}
    # self.mass = {'coxa':0.15,'femur':0.15,'tibia':0.15}

  def update_and_sample_params(self, params=None):
    if params is not None:
      self.set_values(params)
      
      # print(self.stds)
      if not self.normal:
        self.values = {
          'damping': [np.random.uniform(self.pms['damping'][name]['mu'] - self.pms['damping'][name]['std'], self.pms['damping'][name]['mu'] + self.pms['damping'][name]['std']) for name in self.joint_names],
          'joint_pos': [[np.random.uniform(self.pms['joint_pos'][name][pos]['mu'] - self.pms['joint_pos'][name][pos]['std'], self.pms['joint_pos'][name][pos]['mu'] + self.pms['joint_pos'][name][pos]['std']) for pos in self.xyz] for name in self.joint_names],
          'mass': [np.random.uniform(self.pms['mass'][name]['mu'] - self.pms['mass'][name]['std'], self.pms['mass'][name]['mu'] + self.pms['mass'][name]['std']) for name in self.joint_names],
          'joint_pos': [np.random.normal(self.pms['joint_pos'][name]['mu'],self.pms['joint_pos'][name]['std']) for name in self.joint_names],
          }
      else:
        self.values = {
          'damping': [np.clip(np.random.normal(self.pms['damping'][name]['mu'],self.pms['damping'][name]['std']),0,1) for name in self.joint_names],
          'inertia': [[np.clip(np.random.normal(self.pms['inertia'][name][pos]['mu'],self.pms['inertia'][name][pos]['std']),0,1) for pos in self.xyz] for name in self.joint_names],
          'mass': [np.clip(np.random.normal(self.pms['mass'][name]['mu'],self.pms['mass'][name]['std']),0,5) for name in self.joint_names],
          # 'delay': int(np.clip(np.random.normal(self.pms['delay']['mu'],self.pms['delay']['std']),1,4))
          'delay': 1 
          }

    # self.values = {
    #   'friction': [np.clip(np.random.normal(self.pms['friction'][name]['mu'],self.pms['friction'][name]['std']),0,1) for name in self.joint_names],
    #   'damping': [np.clip(np.random.normal(self.pms['damping'][name]['mu'],self.pms['damping'][name]['std']),0,1) for name in self.joint_names],
    #   'joint_pos': [np.random.normal(self.pms['joint_pos'][name]['mu'],self.pms['joint_pos'][name]['std']) for name in self.joint_names],
    #   'mass_pos': [np.random.normal(self.pms['mass_pos'][name]['mu'],self.pms['mass_pos'][name]['std']) for name in self.joint_names],
    #   'mass': [np.random.normal(self.pms['mass'][name]['mu'],self.pms['mass'][name]['std']) for name in self.joint_names]
    #   }
    # self.new_dynamics()
    # self.values['delay'] = 1
    # self.env.delay_length = self.values['delay']
    self.set_new_dynamics()
    # self.write_new_dynamics()
    
    # if not self.normal:
    #   # print(self.values['mass'])
    #   value = []
    #   for key in ['damping', 'inertia', 'mass', 'delay']:
    #     if key == 'delay':
    #       # print(self.values[key])
    #       value.append(self.values[key])
    #     else:
    #       for j in range(len(self.joint_names)):
    #         if key == 'inertia':
    #           for i in range(len(self.xyz)):
    #             value.append(self.values[key][j][i])
    #         else:
    #           value.append(self.values[key][j])
    


    return value
    # return [value for key in ['damping','inertia','mass'] for value in self.values[key]]
    # return self.values

  def set_values(self, params):
    means, stds = params
    count = 0
    sum_stds = []
    for key in ['damping', 'inertia', 'mass', 'delay']:
      if key == 'delay':
        self.pms[key]['mu'] = means[count]
        self.pms[key]['std'] = max(stds[count], 0.1)
        sum_stds.append(self.pms[key]['std'])
      else:          
        for j in self.joint_names:
          if key == 'inertia':
            for i in self.xyz:
              self.pms[key][j][i]['mu'] = means[count]
              self.pms[key][j][i]['std'] = max(stds[count], 0.0001)
              sum_stds.append(self.pms[key][j][i]['std'])
              # self.pms[key][j][i]['std'] = 0.0001
              count += 1
          else:
            self.pms[key][j]['mu'] = means[count]
            self.pms[key][j]['std'] = max(stds[count], 0.01)
            # self.pms[key][j]['std'] = 0.01
            sum_stds.append(self.pms[key][j]['std'])
            count += 1
      print(self.pms)
      self.stds = np.sum(sum_stds)
  def get_values(self):
      if not self.normal:
    # print(self.values['mass'])
    value = []
    for key in ['damping', 'inertia', 'mass', 'delay']:
      if key == 'delay':
        # print(self.values[key])
        value.append(self.values[key])
      else:
        for j in range(len(self.joint_names)):
          if key == 'inertia':
            for i in range(len(self.xyz)):
              value.append(self.values[key][j][i])
          else:
            value.append(self.values[key][j])
    return value

  def set_new_dynamics(self, Id=1, links=[1,2,3]):
    # print(self.values['mass'])
    for n,l in enumerate(links):
      # print("setting new parmas", self.values['mass'][n], self.values['damping'][n])
      p.changeDynamics(Id, l, mass=self.values['mass'][n], jointDamping=self.values['damping'][n])
      
      # p.changeDynamics(Id, l, mass=v['mass'][n], localInertiaDiagonal=[v['inertia'][n][pos] for pos in range(len(self.xyz))], jointDamping=v['damping'][n])

      # mass, linear damping, angularDamping
      # rolling, spinning and lateral friction
      # restitution
      # contactStiffness, contactDamping
      # localInertiaDiagnoal 3
      # jointDamping


  def write_new_dynamics(self, save_path):
    tree = ET.parse(self.path)
    root = tree.getroot()

    for link in root.findall('link'):
      # print(name)
      name = link.get('name')
      if 'body' not in name and ('coxa' in name or 'femur' in name or 'tibia' in name):
        name = name.split("_")[1]
        if name == 'coxa':
          n = 0
        elif name == 'femur':
          n = 1
        elif name == 'tibia':
          n = 2
        inertial = link.find('inertial')
        mass_ = inertial.find('mass')
        mass = float(mass_.get('value'))
        if self.normal:
          mass_.set('value', str(mass + self.values['mass'][n]))
          # mass_.set('value', str(np.random.uniform(0.1, 100)))
          # print('updating mass')
        else:
          mass_.set('value', str(np.random.uniform(mass*(1-self.pms['mass'][name]),mass*(1+self.pms['mass'][name]))))
        mass_origin = inertial.find('origin')
        x = ''
        for m in [float(s) for s in (mass_origin.get('xyz')).split()]:
          if self.normal:
            x += str(m + self.values['mass_pos'][n]) + ' '
          else:
            x += str(m + np.random.uniform(-self.pms['mass_pos'][name],self.pms['mass_pos'][name])) + ' '
        mass_origin.set('xyz', x)
        if self.debug:
          print(name , "link mass, mass xyz", mass_.get('value'), mass_origin.get('xyz'))

    for joint in root.findall('joint'):
      name = joint.get('name')
      if 'body' not in name and ('coxa' in name or 'femur' in name or 'tibia' in name):
        name = joint.get('name').split("_")[1]
        if name == 'coxa':
          n = 0
        elif name == 'femur':
          n = 1
        elif name == 'tibia':
          n = 2
        joint_origin = joint.find('origin')
        x = ''
        for j in [float(s) for s in (joint_origin.get('xyz')).split()]:
          if self.normal:
            x += str(j + self.values['joint_pos'][n]) + ' '
          else:
            x += str(j + np.random.uniform(-self.pms['joint_pos'][name],self.pms['joint_pos'][name])) + ' '
        joint_origin.set('xyz', x)

        dynamics = joint.find('dynamics')
        if self.normal:
          dynamics.set('friction', str(float(dynamics.get('friction')) + self.values['friction'][n]))
          dynamics.set('damping',  str(float(dynamics.get('damping')) + self.values['damping'][n]))
        else:
          dynamics.set('friction', str(float(dynamics.get('friction')) + np.random.uniform(0,self.pms['friction'][name])))
          dynamics.set('damping',  str(float(dynamics.get('damping')) + np.random.uniform(0,self.pms['damping'][name])))
        if self.debug:
          print(name, "joint xyz, friction, damping", joint_origin.get('xyz'), dynamics.get('friction'), dynamics.get('damping'))
    tree.write(save_path)


  '''
    mass  double
    lateralFriction double  lateral (linear) contact friction
    spinningFriction double torsional friction around the contact normal
    rollingFriction double torsional friction orthogonal to contact normal
    restitution double bouncyness of contact. Keep it a bit less than 1.
    linearDamping double linear damping of the link (0.04 by default)
    angularDamping double  angular damping of the link (0.04 by default)
    contactStiffness  double  stiffness of the contact constraints, used together with contactDamping.
    contactDamping double damping of the contact constraints for this body/link. Used together with contactStiffness. This overrides the value if it was specified in the URDF file in the contact section.
    frictionAnchor int enable or disable a friction anchor: positional friction correction (disabled by default, unless set in the URDF contact section)
    localInertiaDiagnoal  vec3  diagonal elements of the inertia tensor. Note that the base and links are centered around the center of mass and aligned with the principal axes of inertia so there are no off-diagonal elements in the inertia tensor.
    ccdSweptSphereRadiu   float  radius of the sphere to perform continuous collision detection. See Bullet/examples/pybullet/examples/experimentalCcdSphereRadius.py for an example.
    contactProcessingThreshold  float  contacts with a distance below this threshold will be processed by the constraint solver. For example, if contactProcessingThreshold = 0, then contacts with distance 0.01 will not be processed as a constraint.
    
    jointDamping double Joint damping coefficient applied at each joint. This coefficient is read from URDF joint damping field. Keep the value close to 0.
    Joint damping force = -damping_coefficient * joint_velocity.
    
    anisotropicFriction   float  anisotropicFriction coefficient to allow scaling of friction in different directions.
    
    maxJointVelocity  float  maximum joint velocity for a given joint, if it is exceeded during constraint solving, it is clamped. Default maximum joint velocity is 100 units.
  '''

if __name__=="__main__":
  cu = ChangeURDF()
  cu.new_dynamics()