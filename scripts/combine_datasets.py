import numpy as np
from pathlib import Path
home = str(Path.home())
path1 = home + '/data/iros_data/pb/'
path2 = home + '/data/iros_data/verify/'
save_path = home + '/data/iros_data/pb/ft/'
d1_inputs = np.load(path1 + 'inputs.npy').reshape(-1,12)
d1_labels = np.load(path1 + 'labels.npy').reshape(-1,3)
d2_inputs = np.load(path2 + 'inputs.npy')
d2_labels = np.load(path2 + 'labels.npy')
print(d1_inputs.shape, d2_inputs.shape)
new_inputs = np.concatenate((d1_inputs, d2_inputs), axis=0)
new_labels = np.concatenate((d1_labels, d2_labels), axis=0)
print(new_inputs.shape, new_labels.shape)
np.save(save_path + 'inputs.npy', new_inputs)
np.save(save_path + 'labels.npy', new_labels)