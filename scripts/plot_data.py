import pandas as pd 
import os
from glob import glob   
import re
from pathlib import Path
home = str(Path.home())
import numpy as np
from utils import *
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--pb', default=False, action='store_true')
parser.add_argument('--id', default=False, action='store_true')
parser.add_argument('--pandas', default=False, action='store_true')
args = parser.parse_args()

data_path = home + '/data/iros_data/real/13_2/'
# data_path = home + '/data/iros_data/pb/'
# data_path = home + '/results/hex_model/latest/gz_200/'
# data_path = home + '/results/hex_model/latest/pb_200/'
# data_path = home + '/data/iros_data/rand_net/'
# data_path = home + '/data/iros_data/gz_300/'
# data_path = home + '/data/iros_data/pb_sin_100/'
data_type = ""
if args.pandas:
  # labels = load_data_xlxs(DATA_PATH=data_path, sample_name=data_type + 'labels')
  # inputs = load_data_xlxs(DATA_PATH=data_path, sample_name=data_type + 'inputs')
  labels = load_data_xlxs(DATA_PATH=data_path, sample_name=data_type + 'test_labels')
  inputs = load_data_xlxs(DATA_PATH=data_path, sample_name=data_type + 'test_inputs')
else:
  inputs = np.load(data_path + 'inputs.npy')
  labels = np.load(data_path + 'labels.npy')
  # inputs = inputs.reshape(-1, inputs.shape[-1])
  # labels = labels.reshape(-1, labels.shape[-1])
num_joints = 3

print(labels.shape, inputs.shape)
# first, last = 18000, 20000
# first, last = 100, 400
# first, last = 290000,290500
# first, last = 0,inputs.shape[0]
first, last = 0, 2000
# first, last = 500, 1148
# idx = [i for i in range(first, last) if i % 8 == 0]
# idx = [i for i in range(first, last)]
# sample_torques = labels[idx,:]
# sample = inputs[idx,:6]


# print(len(idx), sample_torques.shape)
# print(np.mean(sample_torques,axis=0))
# print(np.std(sample_torques, axis=0))
# print(np.max(sample_torques, axis=0))
# print(np.min(sample_torques, axis=0))

# np.save(data_path + 'torque_samples.npy',sample_torques)
# np.save(data_path + 'samples.npy',sample)

# s = np.load(data_path + 'samples.npy')
# s_torque = np.load(data_path + 'torque_samples.npy')
# print(s.shape)
# # s = sample_torques
# subplot([[s[:,0]],[s[:,1]],[s[:,2]],[s[:,3]],[s[:,4]],[s[:,5]]], x_initial=first)
# subplot([[s_torque[:,0]],[s_torque[:,1]],[s_torque[:,2]]], x_initial=first)
x1,x2,x3,x4,x5,x6 = inputs[first:last,0],inputs[first:last,1],inputs[first:last,2],inputs[first:last,3],inputs[first:last,4],inputs[first:last,5]
if 'gz' in data_path:
  x7, x8, x9 = inputs[first:last,6],inputs[first:last,7],inputs[first:last,8]
else:
  x7, x8, x9 = inputs[first:last,6],inputs[first:last,7],inputs[first:last,8]
subplot([[x1],[x2],[x3],[x4],[x5],[x6],[x7],[x8],[x9]], x_initial=first, PATH="/home/brendan/Desktop/")


