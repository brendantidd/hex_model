import pandas as pd 
import os
from glob import glob   
import re
from pathlib import Path
home = str(Path.home())
import numpy as np
from utils import *

# data_type = 'pb_'
data_type = ''
# path = home + '/data/pb_id2'
# path = home + '/data/joint_test'
path = home + '/data/iros_data/gz_fm_100_rand/'
# path = home + '/data/eth_gz'
# path = home + '/data/my_gz'

# data_path = home + '/results/hex_model/latest/pb_fm/'
# data_path = home + '/results/hex_model/latest/pb_id2/'
# data_path = home + '/data/joint_test/'
data_path = path
# data_path = home + '/results/hex_model/latest/gz/'

# labels = load_data_xlxs(DATA_PATH=data_path, sample_name=data_type + 'labels')
# inputs = load_data_xlxs(DATA_PATH=data_path, sample_name=data_type + 'inputs')

labels = load_data_xlxs(DATA_PATH=data_path, sample_name=data_type + 'labels')
inputs = load_data_xlxs(DATA_PATH=data_path, sample_name=data_type + 'inputs')

test_leg = True
if test_leg:
  num_joints = 3
else:
  num_joints = 18
print(inputs.shape, labels.shape)
# train_labels, test_labels = labels[:int(labels.shape[0]*0.2),:],labels[int(labels.shape[0]*0.2):,:] 
# train_inputs, test_inputs = inputs[:int(labels.shape[0]*0.2),:],inputs[int(labels.shape[0]*0.2):,:] 
# train_labels, test_labels = labels[:int(labels.shape[0]*0.95),:],labels[int(labels.shape[0]*0.95):,:] 
# train_inputs, test_inputs = inputs[:int(labels.shape[0]*0.95),:],inputs[int(labels.shape[0]*0.95):,:] 
train_labels = labels[:3000,:]
train_inputs = inputs[:3000,:]

df = pd.DataFrame (np.array(train_inputs))  
df.to_excel(path + '/small_'+ data_type + 'inputs.xlsx', index=False)
df = pd.DataFrame (np.array(train_labels))  
df.to_excel(path + '/small_'+ data_type + 'labels.xlsx', index=False)

# print(train_inputs.shape, test_inputs.shape)
# df = pd.DataFrame (np.array(train_inputs))  
# df.to_excel(path + '/train_'+ data_type + 'inputs.xlsx', index=False)
# df = pd.DataFrame (np.array(test_inputs))  
# df.to_excel(path + '/test_'+ data_type + 'inputs.xlsx', index=False)
# df = pd.DataFrame (np.array(train_labels))  
# df.to_excel(path + '/train_'+ data_type + 'labels.xlsx', index=False)
# df = pd.DataFrame (np.array(test_labels))  
# df.to_excel(path + '/test_'+ data_type + 'labels.xlsx', index=False)
# df = pd.DataFrame (np.array(train_labels))  

