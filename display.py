'''
Script used to display robot stuff while training (loads in save robot state)
'''
import pybullet as p
import numpy as np
import argparse
from pathlib import Path
import time
from assets.obstacles import Obstacles

obstacles = Obstacles()

home = str(Path.home())

parser = argparse.ArgumentParser()
parser.add_argument('--hpc', default=False,action='store_true')
parser.add_argument('--data', default=False,action='store_true')
parser.add_argument('--stand', default=False,action='store_true')
parser.add_argument('--baseline', default=False,action='store_true')
parser.add_argument('--expert', default=True,action='store_false')
parser.add_argument('--expert2', default=False,action='store_true')
parser.add_argument('--test_path', default='hex_model/latest')
parser.add_argument('--new_urdf', default=False,action='store_true')
parser.add_argument('--exp', default='test')
parser.add_argument('--test_leg', default=True, action='store_false')
args = parser.parse_args()

if 'stand' in args.exp:
  args.stand = True

if args.hpc:
    hpc = 'hpc-home/'
else:
    hpc = ''
if args.data:
  MODEL_PATH = home + '/data/iros_data/' + args.exp + '/'
else:
  MODEL_PATH = home + '/' + hpc + 'results/' + args.test_path + '/' + args.exp + '/'
  # MODEL_PATH = home + '/hpc-home/results/hex_model/a18/leg_const_std1/'
if args.test_leg:
  if args.stand:
    # from assets.just_test_stand import Env
    from assets.just_test_stand_rl import Env
  else:
    from assets.just_test_leg_rl import Env
  env = Env(render=True, record_step=False, args=args)
else:
  from assets.env import Env
  env = Env(render=True, record_step=False, args=args)

print()
print("loading from ", MODEL_PATH)
print()

def data_gen(path, box_path=None):
  print(path)
  env.reset()
  while True:
    try:
      sim_data = np.load(path, allow_pickle=True)
      try:
        if box_path is not None:
          obstacles.remove_obstacles()
          box_info = np.load(box_path, allow_pickle=True)
          for pos, size, colour in zip(box_info[1], box_info[2], box_info[3]):
            obstacles.add_box(pos=pos[:3], orn=pos[3:], size=size, colour=colour)
      except Exception as e:
        print("can't load boxes", e)
      # print(sim_data)
      if len(sim_data) == 0:
        yield None
      count = 0
      for seg in sim_data:
        yield seg, count
        count += 1
    except Exception as e:
        print("exception")
        print(e)
data = data_gen(path=MODEL_PATH + 'sim_data.npy',box_path=MODEL_PATH + 'box_info.npy')
if args.expert or args.expert2:
  exp_data = data_gen(path = MODEL_PATH + 'exp_sim_data.npy')
# if args.expert2:
#   exp2_data = data_gen(path = MODEL_PATH + 'exp1_sim_data.npy')

while True:
  seg, count = data.__next__()
  if seg is not None:
    if args.stand:
      env.set_position(seg[0], seg[1], seg[2],slide=seg[2][3])
    else:
      env.set_position(seg[0], seg[1], seg[2])
  else:
    env.set_position([0, 0, -0.045], [0,0,0,1], np.zeros(12))
  if args.expert or args.expert2:
    exp_seg, exp_count = exp_data.__next__()
    if exp_seg is not None:
      if args.stand:
        env.set_position(exp_seg[0], exp_seg[1], exp_seg[2], robot_id=env.exp_Id, slide=exp_seg[2][3])
      else:
        env.set_position(exp_seg[0], exp_seg[1], exp_seg[2], robot_id=env.exp_Id)
    else:
      env.set_position([0, 1, -0.045], [0,0,0,1], np.zeros(12), robot_id=env.exp_Id)
  # if args.expert2:
  #   exp2_seg, exp2_count = exp2_data.__next__()
  #   if exp2_seg is not None:
  #     if args.stand:
  #       env.set_position(exp2_seg[0], exp2_seg[1], exp2_seg[2], robot_id=env.exp2_Id, slide=exp2_seg[2][3])
  #     else:
  #       env.set_position(exp2_seg[0], exp2_seg[1], exp2_seg[2], robot_id=env.exp2_Id)
  #   else:
  #     env.set_position([0, 1, -0.045], [0,0,0,1], np.zeros(12), robot_id=env.exp2_Id)
  time.sleep(0.005)